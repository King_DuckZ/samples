#include "main.hpp"
#ifdef __INTEL_COMPILER
#pragma hdrstop
#endif

#define NEEDS_CONTEXT

#include "ObjectLoader.hpp"
#include <cstring>
#include <yaml-cpp/yaml.h>
#include "DuckUtils/Helpers.hpp"
#include "DuckCore/AbstractReader.hpp"
#include "DebugOutput.hpp"
#include "ObjectLoader/ContextFunctions.hpp"
#include "ObjectLoader/ObjectLoaderContext.hpp"
#include "ObjectLoader/ObjectLoaderData.hpp"

namespace duckutil {
	namespace {
		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		const char* YamlNodeTypeToDesc (YAML::NodeType::value parType) {
			switch (parType) {
			case YAML::NodeType::Null:
				return "Null";
			case YAML::NodeType::Scalar:
				return "Scalar";
			case YAML::NodeType::Sequence:
				return "Sequence";
			case YAML::NodeType::Map:
				return "Map";
			default:
				return "Unknown";
			}
		}
	} //unnamed namespace

// template<> Type load<Type>(CustomYamlContext& context) {
// 	return Type(load<std::vector<DataType> >(context), load<Machin>(context));
// }
//
//
// template<> Type load<Type>(CustomYamlContext& context) {
// 	return Type(load<std::vector<DataType> >(context.subobject("data")), load<Machin>(context.subobject("machin")));
// }

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	ObjectLoader::ObjectLoader (const ReaderFactoryType& parReaderMaker) :
		m_readerMaker(parReaderMaker),
		m_data(new ObjectLoaderData)
	{
		Assert(NULL != m_data);
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	ObjectLoader::~ObjectLoader() {
		delete m_data;
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	bool ObjectLoader::StoreTypeLoader (const obload::ParameterInfo& parInfo) {
		AssertRelease(NULL != parInfo.loader);

		std::string name;
		name.reserve(parInfo.nametype.size() + 1);
		name = "!";
		name += parInfo.nametype;
		Assert(name.capacity() == parInfo.nametype.size() + 1);

		return m_data->StoreParamInfo(name, parInfo);
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	bool ObjectLoader::Load (const std::string& parPath) {
		std::string allText;
		{
			duckcore::AbstractReaderSmartPtr newReader(m_readerMaker(parPath));
			Assert(NULL != newReader);
			Assert(newReader->IsOpen());

			const size_t allTextSize = duckutil::ReadAllToString(*newReader, allText);
			if (allTextSize == 0)
				return false;
		}

		if (not m_data->Finalize())
			return false;

		std::istringstream yamlStream(allText);
		YAML::Parser yamlParser(yamlStream);
		YAML::Node doc;
		yamlParser.GetNextDocument(doc);
		ObjectLoaderContext context(doc, *m_data, parPath, NULL);

		if (doc.Type() != YAML::NodeType::Map) {
			std::ostringstream oss;
			oss << "Wrong format for yaml file: current node is of type " << YamlNodeTypeToDesc(doc.Type()) << " but type Map was expected.";
			obload::PrintErrorFromContext(oss.str().c_str(), context);
			return 0;
		}

		m_data->MakeNewStorages(context.stores);
		std::string currNodeName;
		for (YAML::Iterator itYaml = doc.begin(); itYaml != doc.end(); ++itYaml) {
			const YAML::Node& currNode = itYaml.first();
			currNode >> currNodeName;

			const u32 retVal = obload::RecursiveLoad(currNodeName, context);
			if (0 == retVal)
				break;
		}
		AssertNotImplemented();
		m_data->DeleteStorages(context.stores);
		return true;
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	bool ObjectLoader::StoreStorageMaker (NewStorageFunc parFunc, obload::TypeIDType parType) {
		return m_data->StoreStorageMaker(parFunc, parType);
	}
} //namespace duckutil

#undef NEEDS_CONTEXT
