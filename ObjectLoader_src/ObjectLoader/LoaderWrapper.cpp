#include "../main.hpp"
#ifdef __INTEL_COMPILER
#pragma hdrstop
#endif

#include "LoaderWrapper.hpp"
#include <sstream>
#include "ContextFunctions.hpp"
#include "DebugOutput.hpp"

namespace duckutil {
	namespace obload {
		namespace {
// 			typedef std::map<std::string, RetrievableParameter> LoaderDependencyMapType;
//
// 			///-----------------------------------------------------------------
// 			///Use this function instead of searching directly. The reason is
// 			///I don't want to keep the map as the container, but I don't want
// 			///to change everything when I swap containers.
// 			///-----------------------------------------------------------------
// 			LoaderDependencyMapType::iterator FindDependency (LoaderDependencyMapType& parMap, const std::string& parDependency) {
// 				return parMap.find(parDependency);
// 			}
		} //unnamed namespace

		template NativeTypeConverter<u8>::NativeTypeConverter(const ParamCoordinate& parCoord, ObjectLoaderContext& parContext);
		template NativeTypeConverter<u16>::NativeTypeConverter(const ParamCoordinate& parCoord, ObjectLoaderContext& parContext);
		template NativeTypeConverter<u32>::NativeTypeConverter(const ParamCoordinate& parCoord, ObjectLoaderContext& parContext);
		template NativeTypeConverter<u64>::NativeTypeConverter(const ParamCoordinate& parCoord, ObjectLoaderContext& parContext);
		template NativeTypeConverter<i8>::NativeTypeConverter(const ParamCoordinate& parCoord, ObjectLoaderContext& parContext);
		template NativeTypeConverter<i16>::NativeTypeConverter(const ParamCoordinate& parCoord, ObjectLoaderContext& parContext);
		template NativeTypeConverter<i32>::NativeTypeConverter(const ParamCoordinate& parCoord, ObjectLoaderContext& parContext);
		template NativeTypeConverter<i64>::NativeTypeConverter(const ParamCoordinate& parCoord, ObjectLoaderContext& parContext);
		template NativeTypeConverter<float>::NativeTypeConverter(const ParamCoordinate& parCoord, ObjectLoaderContext& parContext);
		template NativeTypeConverter<double>::NativeTypeConverter(const ParamCoordinate& parCoord, ObjectLoaderContext& parContext);

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <typename T>
		NativeTypeConverter<T>::NativeTypeConverter (const ParamCoordinate& parCoord, ObjectLoaderContext& parContext) {
			std::string value;
			Assert(parCoord.name);
			const bool valueOk = GetValueFromContext(value, *parCoord.name, parContext);
			if (valueOk) {
				std::istringstream iss;
				iss.str(value);
				iss >> m_value;
			}
			else {
				DOUT << "Can't read scalar \"" << GetNameFromContext(parContext) << "\" from current context\n";
				m_value = T(0);
				AssertNotReached();
			}
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		NativeTypeConverter<std::string>::NativeTypeConverter (const ParamCoordinate& parCoord, ObjectLoaderContext& parContext) {
			Assert(parCoord.name);
			const bool valueOk = GetValueFromContext(m_value, *parCoord.name, parContext);
			if (not valueOk) {
				DOUT << "Can't read scalar \"" << GetNameFromContext(parContext) << "\" from current context\n";
				AssertNotReached();
			}
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		AbstractLoadWrapper::AbstractLoadWrapper (TypeIDType parID) :
			m_typeid(parID)
		{
			Assert(0 != m_typeid);
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		AbstractLoadWrapper::~AbstractLoadWrapper() {
		}
	} //namespace obload
} //namespace duckutil
