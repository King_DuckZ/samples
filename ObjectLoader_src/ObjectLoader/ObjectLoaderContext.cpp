#include "../main.hpp"
#ifdef __INTEL_COMPILER
#pragma hdrstop
#endif

#define NEEDS_CONTEXT

#include <yaml-cpp/yaml.h>
#include "ObjectLoaderContext.hpp"
#include "ObjectLoaderData.hpp"

namespace duckutil {
	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	ObjectLoaderContext::ObjectLoaderContext (const YAML::Node& parNode, const ObjectLoaderData& parObjLoaderData, const std::string& parYamlName, const std::string* parName) :
		stores(),
		node(parNode),
		objloaderdata(parObjLoaderData),
		yamlname(parYamlName),
		name(parName)
	{
		Assert(NULL != &parObjLoaderData);
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	ObjectLoaderContext::ObjectLoaderContext (const YAML::Node& parNode, const std::string* parName, const ObjectLoaderContext& parContextModel) :
		stores(parContextModel.stores),
		node(parNode),
		objloaderdata(parContextModel.objloaderdata),
		yamlname(parContextModel.yamlname),
		name(parName)
	{
		Assert(&node != &parContextModel.node);
	}
} //namespace duckutil

#undef NEEDS_CONTEXT
