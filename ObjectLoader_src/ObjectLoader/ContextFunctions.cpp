#include "../main.hpp"
#ifdef __INTEL_COMPILER
#pragma hdrstop
#endif

#define NEEDS_CONTEXT

#include <yaml-cpp/yaml.h>
#include "ObjectLoaderContext.hpp"
#include "ContextFunctions.hpp"
#include "ObjectLoaderData.hpp"
#include "LoaderWrapper.hpp"
#include "DebugOutput.hpp"

namespace duckutil {
	namespace obload {
		template u8  LoadObject<u8 > ( const char* parName, ObjectLoaderContext& parContext );
		template u16 LoadObject<u16> ( const char* parName, ObjectLoaderContext& parContext );
		template u32 LoadObject<u32> ( const char* parName, ObjectLoaderContext& parContext );
		template u64 LoadObject<u64> ( const char* parName, ObjectLoaderContext& parContext );
		template i16 LoadObject<i16> ( const char* parName, ObjectLoaderContext& parContext );
		template i32 LoadObject<i32> ( const char* parName, ObjectLoaderContext& parContext );
		template i64 LoadObject<i64> ( const char* parName, ObjectLoaderContext& parContext );
		template float LoadObject<float> ( const char* parName, ObjectLoaderContext& parContext );
		template double LoadObject<double> ( const char* parName, ObjectLoaderContext& parContext );

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <typename R>
		R LoadObject (const char* parName, ObjectLoaderContext& parContext) {
			AssertRelease(NULL != parName);
			Assert(parContext.node.Type() == YAML::NodeType::Scalar);
			R retVal;
			parContext.node[parName] >> retVal;
			return retVal;
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		bool GetValueFromContext (std::string& parOut, const std::string& parName, ObjectLoaderContext& parContext) {
			Assert(YAML::NodeType::Map == parContext.node.Type());
			const YAML::Node* const retNode = parContext.node.FindValue(parName.c_str());
			if (retNode) {
				Assert(retNode->Type() == YAML::NodeType::Scalar);
				retNode->GetScalar(parOut);
				return true;
			}
			else {
				return false;
			}
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		u32 RecursiveLoad (const std::string& parName, ObjectLoaderContext& parContext) {
			Assert(YAML::NodeType::Map == parContext.node.Type());
			const YAML::Node* const rootNode = parContext.node.FindValue(parName.c_str());
			u32 retVal;

			if (rootNode) {
				ObjectLoaderContext localContext(*rootNode, &parName, parContext);
				Assert(localContext.name);
				const std::string& nodeType = rootNode->Tag();
				Assert(nodeType != "?");

				AbstractLoadWrapper* const loader = localContext.objloaderdata.GetLoaderForType(nodeType);
				if (NULL == loader) {
					std::ostringstream oss;
					oss << "Missing loader for type \"" << nodeType << " while loading " << *localContext.name << ".";
					PrintErrorFromContext(oss.str().c_str(), localContext);
					retVal = 0;
				}
				else {
					DOUT << "Node: " << *localContext.name << " of type " << nodeType << "\n";
					retVal = loader->Call(localContext);
				}
			}
			else {
				retVal = 0;
			}
			return retVal;
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		void PrintErrorFromContext (const char* parMessage, ObjectLoaderContext& parContext) {
			std::cerr << "Error in ObjectLoader while reading " << parContext.yamlname << ":\n";
			std::cerr << parMessage << "\n";
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		const ParameterInfo* GetParameterInfoFromContext (TypeIDType parType, ObjectLoaderContext& parContext) {
			return parContext.objloaderdata.GetParamInfoForType(parType);
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		RegisteredType* GetStorageFromContext (TypeIDType parType, ObjectLoaderContext& parContext) {
			RegisteredType* retVal = NULL;
			foreach (itStore, parContext.stores) {
				if ((*itStore)->GetClassID() == parType) {
					retVal = *itStore;
					Assert(NULL != retVal);
					break;
				}
			}
			return retVal;
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		const std::string& GetNameFromContext (ObjectLoaderContext& parContext) {
			Assert(parContext.name);
			return *parContext.name;
		}
	} //namespace obload
} //namespace duckutil

#undef NEEDS_CONTEXT
