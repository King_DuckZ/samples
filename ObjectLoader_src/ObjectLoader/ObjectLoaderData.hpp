#ifndef id51C80E3911534EBEAF7FC5D201D54765
#define id51C80E3911534EBEAF7FC5D201D54765

#include "ParameterInfo.hpp"

namespace duckutil {
	namespace obload {
		class AbstractLoadWrapper;
		class RegisteredType;
	} //namespace obload

	class ObjectLoaderData {
		typedef obload::RegisteredType*(*NewStorageFunc)(void);

		struct StorageMaker {
			NewStorageFunc makeNew;
			obload::TypeIDType type;

			bool operator== ( const StorageMaker& parOther ) const { return type == parOther.type; }
			bool operator== ( obload::TypeIDType parID ) const { return type == parID; }
			bool operator< ( const StorageMaker& parOther ) const { return type < parOther.type; }
		};

	public:
		ObjectLoaderData ( void );
		~ObjectLoaderData ( void );

		bool StoreStorageMaker ( NewStorageFunc parFunc, obload::TypeIDType parType );
		bool StoreParamInfo ( const std::string& parName, const obload::ParameterInfo& parParamInfo );
		bool Finalize ( void );
		void MakeNewStorages ( std::vector<obload::RegisteredType*>& parDest ) const;
		void DeleteStorages ( std::vector<obload::RegisteredType*>& parDele ) const;

		obload::AbstractLoadWrapper* GetLoaderForType ( const std::string& parType ) const;
		const obload::ParameterInfo* GetParamInfoForType ( obload::TypeIDType parType ) const;

	private:
		std::map<std::string, obload::ParameterInfo> m_paramInfos;
		std::vector<StorageMaker> m_stores;
	};
} //namespace duckutil

#endif
