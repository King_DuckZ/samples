#ifndef id7C610922E5A144D2A10FD4A54F85D0A8
#define id7C610922E5A144D2A10FD4A54F85D0A8

#include "RegisteredType.hpp"
#include "CallableWithNamedParams.hpp"
#include "ContextFunctions.hpp"

#define LOADOBJECT_SPECIALIZATION(t) \
	template <> \
	inline \
	t* LoadObject<t> ( const char* parName, t* parObject, ObjectLoaderContext& parContext ) { \
		AssertRelease(NULL != parObject); \
		*parObject = obload::LoadObject<t>(parName, parContext); \
		return parObject; \
	}

namespace duckutil {
	struct ObjectLoaderContext;
	class ObjectLoader;

	namespace obload {
		class RegisteredType;

		class AbstractLoadWrapper : public Loki::SmallObject<> {
		public:
			explicit AbstractLoadWrapper ( obload::TypeIDType parID );
			virtual ~AbstractLoadWrapper ( void );
			virtual u32 Call ( ObjectLoaderContext& parContext ) = 0;

		protected:
			typedef std::pair<const std::string*, AbstractLoadWrapper*> DependencyItem;

			bool GetCallableResult ( const std::string& parName, ObjectLoaderContext& parContext );
			obload::TypeIDType GetTypeID ( void ) const { return m_typeid; }

		private:
			const obload::TypeIDType m_typeid;
		};

		template <typename T, typename C, int S>
		class LoaderWrapper : public AbstractLoadWrapper {
		public:
			typedef T ObjectType;
			typedef C CallableType;
			enum {
				ParametersCount = S
			};
			typedef CallableWithNamedParams<CallableType, ParametersCount, ObjectType> CallableWrapperType;
			typedef RegisteredTypeStorage<ObjectType> ObjectStorageType;

			LoaderWrapper ( typename Loki::TypeTraits<CallableType>::ParameterType parCallee, TypeIDType parID );
			virtual ~LoaderWrapper ( void );
			virtual u32 Call ( ObjectLoaderContext& parContext );

		private:
			CallableWrapperType* const m_callableWrapper;
		};

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <typename T, typename C, int S>
		LoaderWrapper<T, C, S>::LoaderWrapper (typename Loki::TypeTraits<CallableType>::ParameterType parCallee, TypeIDType parID) :
			AbstractLoadWrapper(parID),
			m_callableWrapper(new CallableWrapperType(parCallee))
		{
			Assert(NULL != m_callableWrapper);
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <typename T, typename C, int S>
		LoaderWrapper<T, C, S>::~LoaderWrapper() {
			Assert(NULL != m_callableWrapper);
			delete m_callableWrapper;
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <typename T, typename C, int S>
		u32 LoaderWrapper<T, C, S>::Call (ObjectLoaderContext& parContext) {
			const ParameterInfo* const paramInfo = GetParameterInfoFromContext(RegisteredTypeStorage<ObjectType>::StaticGetClassID(), parContext);
			Assert(NULL != paramInfo);
			Assert(ParametersCount == paramInfo->dependencies.size());

			const int ParamsCountMin1 = (ParametersCount < 1 ? 1 : ParametersCount);
			duckmem::FixedSizeArray<ParamCoordinate, ParamsCountMin1> dependenciesObjects;

			for (int z = 0; z < ParametersCount; ++z) {
				if (NULL != paramInfo->dependencies[z].info) {
					Assert(NULL != paramInfo->dependencies[z].info->loader);
					dependenciesObjects[z].id = RecursiveLoad(paramInfo->dependencies[z].name, parContext);
					if (0 == dependenciesObjects[z].id) {
#if defined(DUCK_INTERNAL)
						std::cerr << "Error loading node " << paramInfo->dependencies[z].name << "\n";
#endif
						AssertNotReached();
						return 0;
					}
				}
				else {
					dependenciesObjects[z].name = &paramInfo->dependencies[z].name;
				}
				dependenciesObjects[z].type = paramInfo->dependencies[z].idtype;
			}
			duckmem::TypedMemoryView<ParamCoordinate> dependenciesObjectsView(dependenciesObjects.GetPointer(), ParametersCount);
			ObjectType newObject = m_callableWrapper->Call_GetRetVal(dependenciesObjectsView, parContext);
			RegisteredType* const storage = GetStorageFromContext(RegisteredTypeStorage<ObjectType>::StaticGetClassID(), parContext);
			Assert(paramInfo->idtype == RegisteredTypeStorage<ObjectType>::StaticGetClassID());
			ObjectStorageType* const upcastedStorage = duckcore::checked_cast<ObjectStorageType*>(storage);
			const u32 retVal = upcastedStorage->StoreObject(newObject);
			Assert(0 != retVal);
			return retVal;
		}
	} //namespace obload

	template <typename R>
	R* LoadObject ( const char* parName, R* parObject, ObjectLoaderContext& parContext );
	LOADOBJECT_SPECIALIZATION(u8);
	LOADOBJECT_SPECIALIZATION(u16);
	LOADOBJECT_SPECIALIZATION(u32);
	LOADOBJECT_SPECIALIZATION(u64);
	LOADOBJECT_SPECIALIZATION(float);
	LOADOBJECT_SPECIALIZATION(double);
	LOADOBJECT_SPECIALIZATION(i16);
	LOADOBJECT_SPECIALIZATION(i32);
	LOADOBJECT_SPECIALIZATION(i64);
} //namespace duckutil

#undef LOADOBJECT_SPECIALIZATION

#endif
