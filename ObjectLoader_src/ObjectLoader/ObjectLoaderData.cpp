#include "../main.hpp"
#ifdef __INTEL_COMPILER
#pragma hdrstop
#endif

#include "ObjectLoaderData.hpp"
#include "RegisteredType.hpp"
#include "LoaderWrapper.hpp"
#include "DebugOutput.hpp"

namespace duckutil {
	namespace {
		///---------------------------------------------------------------------
		///Fills in the info pointer for every dependency of every parameter.
		///---------------------------------------------------------------------
		void ConnectDependencies (std::map<std::string, obload::ParameterInfo>& parLoaders) {
			foreach (itCurrParam, parLoaders) {
				foreach (itUpdate, parLoaders) {
					if (itUpdate != itCurrParam) {
						foreach (itDependency, itUpdate->second.dependencies) {
							if (itDependency->idtype == itCurrParam->second.idtype) {
								itDependency->info = &itCurrParam->second;
							}
						}
					}
				}
			}
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		bool FindOrphanedDependencies (const std::map<std::string, obload::ParameterInfo>& parLoaders) {
			static const obload::TypeIDType nativeTypes[] = {
				obload::RegisteredTypeStorage<u8>::StaticGetClassID(),
				obload::RegisteredTypeStorage<u16>::StaticGetClassID(),
				obload::RegisteredTypeStorage<u32>::StaticGetClassID(),
				obload::RegisteredTypeStorage<u64>::StaticGetClassID(),
				obload::RegisteredTypeStorage<i8>::StaticGetClassID(),
				obload::RegisteredTypeStorage<i16>::StaticGetClassID(),
				obload::RegisteredTypeStorage<i32>::StaticGetClassID(),
				obload::RegisteredTypeStorage<i64>::StaticGetClassID(),
				obload::RegisteredTypeStorage<float>::StaticGetClassID(),
				obload::RegisteredTypeStorage<double>::StaticGetClassID(),
				obload::RegisteredTypeStorage<std::string>::StaticGetClassID()
			};
			static const u32 nativeTypesCount = lengthof(nativeTypes);

			bool retVal = false;
			foreachconst (itCurrParam, parLoaders) {
				foreachconst (itDependency, itCurrParam->second.dependencies) {
					//If current dependency is not a primitive type
					//(with regard to the loader itself) and there's no loader
					//associated to it
					if ((itDependency->info == NULL or
						itDependency->info->loader == NULL) and
						std::find(nativeTypes, nativeTypes + nativeTypesCount, itDependency->idtype) == nativeTypes + nativeTypesCount
					) {
						std::cerr << "Parameter \"" << itDependency->name << "\" is of unknown type as no loader has been set.\n";
						retVal = true;
					}
				}
			}
			return retVal;
		}
	} //unnamed namespace

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	ObjectLoaderData::ObjectLoaderData() {
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	ObjectLoaderData::~ObjectLoaderData() {
		foreach(itLoaderWrapper, m_paramInfos) {
			Assert(NULL != itLoaderWrapper->second.loader);
			delete itLoaderWrapper->second.loader;
		}
	}

	///-------------------------------------------------------------------------
	///Only stores the new storage maker if no other one is already there for
	///the same type.
	///-------------------------------------------------------------------------
	bool ObjectLoaderData::StoreStorageMaker (NewStorageFunc parFunc, obload::TypeIDType parType) {
		std::vector<StorageMaker>::iterator itMaker = std::find(m_stores.begin(), m_stores.end(), parType);
		if (m_stores.end() == itMaker) {
			StorageMaker newStorageMaker;
			newStorageMaker.makeNew = parFunc;
			newStorageMaker.type = parType;
			m_stores.push_back(newStorageMaker);
			return true;
		}
		else {
			return false;
		}
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	bool ObjectLoaderData::StoreParamInfo (const std::string& parName, const obload::ParameterInfo& parParamInfo) {
		if (m_paramInfos.find(parName) == m_paramInfos.end()) {
			m_paramInfos[parName] = parParamInfo;
			return true;
		}
		else {
			return false;
		}
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	bool ObjectLoaderData::Finalize() {
		ConnectDependencies(m_paramInfos);
		return not FindOrphanedDependencies(m_paramInfos);
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	obload::AbstractLoadWrapper* ObjectLoaderData::GetLoaderForType (const std::string& parType) const {
		DOUT << "Requested a loader for type " << parType << "... ";
		obload::AbstractLoadWrapper* retVal;
		std::map<std::string, obload::ParameterInfo>::const_iterator itFound = m_paramInfos.find(parType);
		if (itFound == m_paramInfos.end()) {
			DOUT << "not found!";
			retVal = NULL;
		}
		else {
			DOUT << "found by the name of " << itFound->second.nametype;
			retVal = itFound->second.loader;
		}
		DOUT << "\n";
		return retVal;
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	const obload::ParameterInfo* ObjectLoaderData::GetParamInfoForType (obload::TypeIDType parType) const {
		DOUT << "Requested the ParameterInfo for type " << parType << "... ";
		const obload::ParameterInfo* retVal = NULL;
		foreachconst (itCurr, m_paramInfos) {
			if (itCurr->second.idtype == parType) {
				DOUT << "found by the name of " << itCurr->second.nametype;
				retVal = &itCurr->second;
				break;
			}
		}
		DOUT << "\n";
		return retVal;
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	void ObjectLoaderData::MakeNewStorages (std::vector<obload::RegisteredType*>& parDest) const {
		parDest.reserve(m_stores.size());
		foreachconst (itStorageMaker, m_stores) {
			parDest.push_back(itStorageMaker->makeNew());
		}
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	void ObjectLoaderData::DeleteStorages (std::vector<obload::RegisteredType*>& parDele) const {
		foreach (itStorage, parDele) {
			Assert(NULL != *itStorage);
			delete *itStorage;
		}
	}
} //namespace duckutil
