#ifndef idB7C2E0DC5D284A03BE6783B5F96F0919
#define idB7C2E0DC5D284A03BE6783B5F96F0919

#include "ParameterInfo.hpp"

namespace duckutil {
	namespace obload {
		class RegisteredType : public Loki::SmallObject<> {
		public:
			virtual ~RegisteredType ( void ) { }
			virtual TypeIDType GetClassID ( void ) const = 0;
			virtual void Clear ( void ) = 0;

		protected:
			RegisteredType ( void ) { }
		};

		template <typename T>
		class RegisteredTypeStorage : public RegisteredType {
			static int m_staticForAddress;
		public:
			STATIC_ASSERT(sizeof(&m_staticForAddress) <= 8);
			STATIC_ASSERT(sizeof(&m_staticForAddress) == sizeof(TypeIDType));

			RegisteredTypeStorage ( void );
			virtual ~RegisteredTypeStorage ( void );

			bool GetStoredObject ( u32 parID, T& parOut );
			u32 StoreObject ( typename Loki::TypeTraits<T>::ParameterType parObject );
			virtual TypeIDType GetClassID ( void ) const { return StaticGetClassID(); }
			static TypeIDType StaticGetClassID ( void );
			virtual void Clear ( void );

		private:
			typedef std::vector<T> ContainerType;
			ContainerType m_objects;
		};

		template <typename T> int RegisteredTypeStorage<T>::m_staticForAddress = 0;

		template <typename T>
		RegisteredType* MakeNewRegisteredTypeStorage ( void );

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <typename T>
		RegisteredType* MakeNewRegisteredTypeStorage() {
			return new RegisteredTypeStorage<T>;
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <typename T>
		RegisteredTypeStorage<T>::RegisteredTypeStorage() {
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <typename T>
		RegisteredTypeStorage<T>::~RegisteredTypeStorage() {
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <typename T>
		bool RegisteredTypeStorage<T>::GetStoredObject (u32 parID, T& parOut) {
			Assert(parID > 0);
			const size_t index = static_cast<size_t>(parID - 1);
			if (index < m_objects.size()) {
				parOut = m_objects[index];
				return true;
			}
			else {
				return false;
			}
		}

		///---------------------------------------------------------------------
		///Returns index + 1
		///---------------------------------------------------------------------
		template <typename T>
		u32 RegisteredTypeStorage<T>::StoreObject (typename Loki::TypeTraits<T>::ParameterType parObject) {
			m_objects.push_back(parObject);
			return static_cast<u32>(m_objects.size());
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <typename T>
		TypeIDType RegisteredTypeStorage<T>::StaticGetClassID() {
			const TypeIDType retVal = duckcore::pointer_cast<TypeIDType>(&m_staticForAddress);
			return retVal;
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <typename T>
		void RegisteredTypeStorage<T>::Clear() {
			ContainerType empty;
			m_objects.clear();
			std::swap(m_objects, empty);
			return;
		}
	} //namespace obload
} //namespace duckutil

#endif
