#ifndef idFF867BBF49304E1596C85B344A1DE3A9
#define idFF867BBF49304E1596C85B344A1DE3A9

#include "ParameterInfo.hpp"

namespace duckutil {
	struct ObjectLoaderContext;

	namespace obload {
		class RegisteredType;

		bool GetValueFromContext ( std::string& parOut, const std::string& parName, ObjectLoaderContext& parContext ) pure_function;
		const std::string& GetNameFromContext ( ObjectLoaderContext& parContext ) pure_function;
		void PrintErrorFromContext ( const char* parMessage, ObjectLoaderContext& parContext );
		const ParameterInfo* GetParameterInfoFromContext ( TypeIDType parType, ObjectLoaderContext& parContext ) pure_function;
		RegisteredType* GetStorageFromContext ( TypeIDType parType, ObjectLoaderContext& parContext ) pure_function;

		template <typename R>
		R LoadObject ( const char* parName, ObjectLoaderContext& parContext );

		u32 RecursiveLoad ( const std::string& parName, ObjectLoaderContext& parContext );
	} //namespace obload
} //namespace duckutil

#endif
