#ifndef id29D8C4678A9949228A9D4B91200B5748
#define id29D8C4678A9949228A9D4B91200B5748

#include "RegisteredType.hpp"
#include "DuckMem/StridedTypedMemoryView.hpp"

namespace duckutil {
	namespace obload {
		template <typename T, int S>
		class ParamTypesFromFunctor {
		public:
			static void FillTypes ( duckmem::CStridedTypedMemoryView<TypeIDType>& parListOut );
		};

		template <typename T>
		class ParamTypesFromFunctor<T, 0> {
		public:
			static void FillTypes ( duckmem::CStridedTypedMemoryView<TypeIDType>& parListOut ) {
				return;
			}
		};
		template <typename T>
		class ParamTypesFromFunctor<T, 1> {
		public:
			static void FillTypes ( duckmem::CStridedTypedMemoryView<TypeIDType>& parListOut ) {
				Assert(parListOut.size() >= 1);
				STATIC_ASSERT(Loki::TL::Length<typename T::ParmList>::value == 1);
				parListOut[0] = RegisteredTypeStorage<typename Loki::TL::TypeAt<typename T::ParmList, 0>::Result>::StaticGetClassID();
			}
		};
		template <typename T>
		class ParamTypesFromFunctor<T, 2> {
		public:
			static void FillTypes ( duckmem::CStridedTypedMemoryView<TypeIDType>& parListOut ) {
				Assert(parListOut.size() >= 2);
				STATIC_ASSERT(Loki::TL::Length<typename T::ParmList>::value == 2);
				parListOut[0] = RegisteredTypeStorage<typename Loki::TL::TypeAt<typename T::ParmList, 0>::Result>::StaticGetClassID();
				parListOut[1] = RegisteredTypeStorage<typename Loki::TL::TypeAt<typename T::ParmList, 1>::Result>::StaticGetClassID();
			}
		};
		template <typename T>
		class ParamTypesFromFunctor<T, 3> {
		public:
			static void FillTypes ( duckmem::CStridedTypedMemoryView<TypeIDType>& parListOut ) {
				Assert(parListOut.size() >= 3);
				STATIC_ASSERT(Loki::TL::Length<typename T::ParmList>::value == 3);
				parListOut[0] = RegisteredTypeStorage<typename Loki::TL::TypeAt<typename T::ParmList, 0>::Result>::StaticGetClassID();
				parListOut[1] = RegisteredTypeStorage<typename Loki::TL::TypeAt<typename T::ParmList, 1>::Result>::StaticGetClassID();
				parListOut[2] = RegisteredTypeStorage<typename Loki::TL::TypeAt<typename T::ParmList, 2>::Result>::StaticGetClassID();
			}
		};
		template <typename T>
		class ParamTypesFromFunctor<T, 4> {
		public:
			static void FillTypes ( duckmem::CStridedTypedMemoryView<TypeIDType>& parListOut ) {
				Assert(parListOut.size() >= 4);
				STATIC_ASSERT(Loki::TL::Length<typename T::ParmList>::value == 4);
				parListOut[0] = RegisteredTypeStorage<typename Loki::TL::TypeAt<typename T::ParmList, 0>::Result>::StaticGetClassID();
				parListOut[1] = RegisteredTypeStorage<typename Loki::TL::TypeAt<typename T::ParmList, 1>::Result>::StaticGetClassID();
				parListOut[2] = RegisteredTypeStorage<typename Loki::TL::TypeAt<typename T::ParmList, 2>::Result>::StaticGetClassID();
				parListOut[3] = RegisteredTypeStorage<typename Loki::TL::TypeAt<typename T::ParmList, 3>::Result>::StaticGetClassID();
			}
		};
	} //namespace obload
} //namespace duckutil

#endif
