#ifndef id64217D4AB6B04DE8A7C30D0E88E05668
#define id64217D4AB6B04DE8A7C30D0E88E05668

namespace duckutil {
	namespace obload {
		typedef int_t<sizeof(int*), false>::T TypeIDType;

		class AbstractLoadWrapper;
		struct ParameterInfo;

		struct ParameterDependency {
			std::string name;
			TypeIDType idtype;
			ParameterInfo* info;
		};

		//This is the main structure that keeps the instances alive
		struct ParameterInfo {
			std::vector<ParameterDependency> dependencies;
			std::string nametype;
			TypeIDType idtype;
			AbstractLoadWrapper* loader;
			u32 objectid;
		};
	} //namespace obload
} //namespace duckutil

#endif
