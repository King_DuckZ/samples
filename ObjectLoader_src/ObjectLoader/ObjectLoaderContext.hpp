#ifndef id8591BD71653C47A39D1602F1D6A17B7F
#define id8591BD71653C47A39D1602F1D6A17B7F

#if !defined(NEEDS_CONTEXT)
#error "Please make sure you really can't simply pre-declare, then define NEEDS_CONTEXT at the top of your cpp (it's a cpp, isn't it?)"
#endif

namespace duckutil {
	namespace obload {
		class RegisteredType;
	} //namespace obload

	class ObjectLoaderData;

	struct ObjectLoaderContext {
		ObjectLoaderContext ( const YAML::Node& parNode, const ObjectLoaderData& parObjLoaderData, const std::string& parYamlName, const std::string* parName );
		ObjectLoaderContext ( const YAML::Node& parNode, const std::string* parName, const ObjectLoaderContext& parContextModel );

		std::vector<obload::RegisteredType*> stores;
		const YAML::Node& node;
		const ObjectLoaderData& objloaderdata;
		const std::string& yamlname;
		const std::string* const name;

	private:
		ObjectLoaderContext ( void ); //Forbidden
		ObjectLoaderContext ( const ObjectLoaderContext& ); //Forbidden
	};
} //namespace duckutil

#endif
