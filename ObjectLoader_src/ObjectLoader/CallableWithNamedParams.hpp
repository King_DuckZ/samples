#ifndef id16DE7265DC344DF3A9411DECBE7C4E9F
#define id16DE7265DC344DF3A9411DECBE7C4E9F

#include "DuckCore/Helpers.hpp"
#include "RegisteredType.hpp"
#include "ContextFunctions.hpp"

#define CALLABLE_CLASS(s) \
	template <typename Callable, typename R> \
	class CallableWithNamedParams<Callable, s, R> : public AbstractCallableWithRetVal<R> { \
	public: \
		typedef typename AbstractCallableWithRetVal<R>::ReturnType ReturnType; \
		typedef LoaderWrapper<R, Callable, s> LoaderWrapperType; \
		explicit CallableWithNamedParams ( typename Loki::TypeTraits<Callable>::ParameterType parCallee ) : m_callee(parCallee) { } \
		virtual ~CallableWithNamedParams ( void ) { } \
	protected: \
		virtual void DispatchCall ( const duckmem::TypedMemoryView<ParamCoordinate>& parParams, ReturnType* parStoreRet, ObjectLoaderContext& parContext ); \
	private:\
		Callable m_callee;\
	}

namespace duckutil {
	struct ObjectLoaderContext;

	namespace obload {
		template <typename T, typename C, int S>
		class LoaderWrapper;

		struct ParamCoordinate {
			TypeIDType type;
			union {
				u32 id;
				const std::string* name;
			};
		};

		class AbstractCallableWithNamedParams : public Loki::SmallObject<> {
		public:
			AbstractCallableWithNamedParams ( void ) { }
			virtual ~AbstractCallableWithNamedParams ( void ) { }
			virtual bool Call ( const duckmem::TypedMemoryView<ParamCoordinate>& parParams, ObjectLoaderContext& parContext ) = 0;
		};

		template <typename R>
		class AbstractCallableWithRetVal : public AbstractCallableWithNamedParams {
		public:
			typedef R ReturnType;
			typedef RegisteredTypeStorage<R> ObjectStorageType;
			AbstractCallableWithRetVal ( void ) { }
			virtual ~AbstractCallableWithRetVal ( void ) { }
			virtual bool Call ( const duckmem::TypedMemoryView<ParamCoordinate>& parParams, ObjectLoaderContext& parContext );
			ReturnType Call_GetRetVal ( const duckmem::TypedMemoryView<ParamCoordinate>& parParams, ObjectLoaderContext& parContext );

		protected:
			virtual void DispatchCall ( const duckmem::TypedMemoryView<ParamCoordinate>& parParams, ReturnType* parStoreRet, ObjectLoaderContext& parContext ) = 0;
		};

		template <>
		class AbstractCallableWithRetVal<bool> : public AbstractCallableWithNamedParams {
		public:
			typedef bool ReturnType;
			AbstractCallableWithRetVal ( void ) { }
			virtual ~AbstractCallableWithRetVal ( void ) { }
			virtual bool Call ( const duckmem::TypedMemoryView<ParamCoordinate>& parParams, ObjectLoaderContext& parContext );
		protected:
			virtual void DispatchCall ( const duckmem::TypedMemoryView<ParamCoordinate>& parParams, ReturnType* parStoreRet, ObjectLoaderContext& parContext ) = 0;
		};

		template <typename Callable, int ParamCount, typename R> class CallableWithNamedParams;
		CALLABLE_CLASS(0);
		CALLABLE_CLASS(1);
		CALLABLE_CLASS(2);
		CALLABLE_CLASS(3);
		CALLABLE_CLASS(4);
		CALLABLE_CLASS(5);

		class ParamAdapter {
		public:
			ParamAdapter ( const ParamCoordinate& parCoord, ObjectLoaderContext& parContext );
			~ParamAdapter ( void ) { }

			template <typename U>
			operator U ( void ) const;

		private:
			ObjectLoaderContext& m_context;
			const ParamCoordinate& m_coord;
		};

		template <typename T>
		class DefaultConverter {
		public:
			enum {
				IsNative = 0
			};
			DefaultConverter ( const ParamCoordinate& parCoord, ObjectLoaderContext& parContext );
			operator T ( void );
		private:
			const ParamCoordinate& m_coord;
			ObjectLoaderContext& m_context;
		};

		template <typename T> struct StringToType {
			typedef DefaultConverter<T> ConverterType;
		};

		template <typename T>
		class NativeTypeConverter {
		public:
			enum {
				IsNative = 1
			};
			NativeTypeConverter ( const ParamCoordinate& parCoord, ObjectLoaderContext& parContext );
			operator T ( void ) { return m_value; }
		private:
			T m_value;
		};

		template <>
		class NativeTypeConverter<std::string> {
		public:
			enum {
				IsNative = 1
			};
			NativeTypeConverter ( const ParamCoordinate& parCoord, ObjectLoaderContext& parContext );
			operator std::string ( void ) { return m_value; }
		private:
			std::string m_value;
		};

		template <> struct StringToType<u8>  { typedef NativeTypeConverter<u8>  ConverterType; };
		template <> struct StringToType<u16> { typedef NativeTypeConverter<u16> ConverterType; };
		template <> struct StringToType<u32> { typedef NativeTypeConverter<u32> ConverterType; };
		template <> struct StringToType<u64> { typedef NativeTypeConverter<u64> ConverterType; };
		template <> struct StringToType<i8>  { typedef NativeTypeConverter<i8>  ConverterType; };
		template <> struct StringToType<i16> { typedef NativeTypeConverter<i16> ConverterType; };
		template <> struct StringToType<i32> { typedef NativeTypeConverter<i32> ConverterType; };
		template <> struct StringToType<i64> { typedef NativeTypeConverter<i64> ConverterType; };
		template <> struct StringToType<float> { typedef NativeTypeConverter<float> ConverterType; };
		template <> struct StringToType<double> { typedef NativeTypeConverter<double> ConverterType; };
		template <> struct StringToType<std::string> { typedef NativeTypeConverter<std::string> ConverterType; };

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		inline ParamAdapter::ParamAdapter (const ParamCoordinate& parCoord, ObjectLoaderContext& parContext) :
			m_context(parContext),
			m_coord(parCoord)
		{
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <typename U>
		ParamAdapter::operator U() const {
			typedef typename duckcore::Iif<Loki::TypeTraits<U>::isReference, typename Loki::TypeTraits<U>::ReferredType, U>::Result CastToType;
			return typename StringToType<CastToType>::ConverterType(m_coord, m_context);
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <typename T>
		DefaultConverter<T>::DefaultConverter (const ParamCoordinate& parCoord, ObjectLoaderContext& parContext) :
			m_coord(parCoord),
			m_context(parContext)
		{
			Assert(NULL != GetStorageFromContext(m_coord.type, m_context));
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <typename T>
		DefaultConverter<T>::operator T() {
			RegisteredType* const store = GetStorageFromContext(m_coord.type, m_context);
			Assert(NULL != store);
			Assert(RegisteredTypeStorage<T>::StaticGetClassID() == m_coord.type);
			Assert(store->GetClassID() == m_coord.type);
			RegisteredTypeStorage<T>* const storeUpcasted = duckcore::checked_cast<RegisteredTypeStorage<T>*>(store);
			T retVal;
			storeUpcasted->GetStoredObject(m_coord.id, retVal);
			return retVal;
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <typename Callable, typename R>
		void CallableWithNamedParams<Callable, 0, R>::DispatchCall (const duckmem::TypedMemoryView<ParamCoordinate>& parParams, ReturnType* parStoreRet, ObjectLoaderContext& parContext) {
			AssertRelease(NULL != parStoreRet);
			AssertRelease(parParams.empty());
			*parStoreRet = m_callee();
		}
		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <typename Callable, typename R>
		void CallableWithNamedParams<Callable, 1, R>::DispatchCall (const duckmem::TypedMemoryView<ParamCoordinate>& parParams, ReturnType* parStoreRet, ObjectLoaderContext& parContext) {
			AssertRelease(NULL != parStoreRet);
			AssertRelease(1 == parParams.size());
			*parStoreRet = m_callee(
				ParamAdapter(parParams[0], parContext)
			);
		}
		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <typename Callable, typename R>
		void CallableWithNamedParams<Callable, 2, R>::DispatchCall (const duckmem::TypedMemoryView<ParamCoordinate>& parParams, ReturnType* parStoreRet, ObjectLoaderContext& parContext) {
			AssertRelease(NULL != parStoreRet);
			AssertRelease(2 == parParams.size());
			*parStoreRet = m_callee(
				ParamAdapter(parParams[0], parContext),
				ParamAdapter(parParams[1], parContext)
			);
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <int S, typename R, typename C>
		AbstractCallableWithNamedParams* MakeNewCallableWithNamedParams (C parCallable) {
			return new CallableWithNamedParams<C, S, R>(parCallable);
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <typename R>
		bool AbstractCallableWithRetVal<R>::Call (const duckmem::TypedMemoryView<ParamCoordinate>& parParams, ObjectLoaderContext& parContext) {
			DispatchCall(parParams, NULL, parContext);
			return true;
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		inline bool AbstractCallableWithRetVal<bool>::Call (const duckmem::TypedMemoryView<ParamCoordinate>& parParams, ObjectLoaderContext& parContext) {
			bool retVal;
			this->DispatchCall(parParams, &retVal, parContext);
			return retVal;
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		template <typename R>
		typename AbstractCallableWithRetVal<R>::ReturnType AbstractCallableWithRetVal<R>::Call_GetRetVal (const duckmem::TypedMemoryView<ParamCoordinate>& parParams, ObjectLoaderContext& parContext) {
			ReturnType retVal;
			DispatchCall(parParams, &retVal, parContext);
			return retVal;
		}
	} //namespace obload
} //namespace duckutil

#undef CALLABLE_CLASS

#endif
