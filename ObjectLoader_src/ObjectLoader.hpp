#ifndef id22FC8E51ACF643F885AA9501D6CD49C6
#define id22FC8E51ACF643F885AA9501D6CD49C6

#include <loki/Functor.h>
#include <loki/Pimpl.h>
#include "DuckCore/Functor.hpp"
#include <cstdarg>
#include "ObjectLoader/LoaderWrapper.hpp"
#include "ObjectLoader/RegisteredType.hpp"
#include "ObjectLoader/CallableWithNamedParams.hpp"
#include "ObjectLoader/ParamTypesFromFunctor.hpp"
#include "ObjectLoader/ParameterInfo.hpp"

namespace duckcore {
	class AbstractReader;
} //namespace duckcore

namespace duckutil {
	struct ObjectLoaderContext;
	class ObjectLoaderData;

	class ObjectLoader {
		typedef obload::RegisteredType*(*NewStorageFunc)(void);

	public:
		typedef Loki::Functor<duckcore::AbstractReader*, LOKI_TYPELIST_1(std::string)> ReaderFactoryType;

		explicit ObjectLoader ( const ReaderFactoryType& parReaderMaker );
		~ObjectLoader ( void );

		template <typename T>
		bool RegisterObject ( T parFactory, const char* parName, ... );
		bool Load ( const std::string& parPath );

	private:
		bool StoreTypeLoader ( const obload::ParameterInfo& parInfo );
		bool StoreStorageMaker ( NewStorageFunc parFunc, obload::TypeIDType parType );

		ReaderFactoryType m_readerMaker;
		ObjectLoaderData* const m_data;
	};
} //namespace duckutil

#include "ObjectLoader.inl"

#endif
