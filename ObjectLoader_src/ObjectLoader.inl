namespace duckutil {
	namespace Implem {
	} //namespace Implem

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	template <typename T>
	bool ObjectLoader::RegisterObject (T parFactory, const char* parName, ...) {
		typedef T CallbackType;
		const int ParametersCount = Loki::TL::Length<typename CallbackType::ParmList>::value;
		typedef typename T::ResultType ReturnType;
		typedef obload::LoaderWrapper<ReturnType, CallbackType, ParametersCount> LoaderWrapperType;

		const obload::TypeIDType typeID = obload::RegisteredTypeStorage<ReturnType>::StaticGetClassID();

		//Add a new storage creator if necessary
		this->StoreStorageMaker(&obload::MakeNewRegisteredTypeStorage<ReturnType>, typeID);

		obload::ParameterInfo paramInfo;
		paramInfo.nametype = parName;
		paramInfo.idtype = typeID;
		paramInfo.objectid = 0;
		paramInfo.loader = new LoaderWrapperType(parFactory, typeID);
		paramInfo.dependencies.reserve(ParametersCount);

		//This method is where we can gather most informations about the
		//functor's parameters, so let's do that.
		va_list ap;
		va_start(ap, parName);

		for (int z = 0; z < ParametersCount; ++z) {
			obload::ParameterDependency dep;
			dep.name = va_arg(ap, const char*);
			dep.info = NULL; //We're not sure we've got one yet so no pointer to the more detailed infos.
			paramInfo.dependencies.push_back(dep);
		}
		//Fill in the parameter's type info based on the functor's type
		if (ParametersCount > 0) {
			duckmem::CStridedTypedMemoryView<obload::TypeIDType> typeView(&paramInfo.dependencies[0].idtype, ParametersCount, sizeof(paramInfo.dependencies[0]));
			obload::ParamTypesFromFunctor<CallbackType, ParametersCount>::FillTypes(typeView);
		}
		va_end(ap);

		const bool stored = StoreTypeLoader(paramInfo);
		if (not stored) {
			delete paramInfo.loader;
		}

		return stored;
	}
} //namespace duckutil
