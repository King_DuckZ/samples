//CCaligari.cpp


#define _DEFINE_CALG_PROP

#define F_HOLE 0x08
#define F_BACKCULL 0x10
#define TXTR_OVERLAY 0x01
#define TXTR_USE 0x02
#define BUMP_USE 0x02
#define ENVR_CUBIC 0x01
#define ENVR_USE 0x02


#include "constants.h"
#include <windows.h>
#include "CCaligari.h"
#include <string.h>
#include "CMathEngine.h"

const char COB_SIGNATURE[] = "Caligari ";
const char COB_VERSION[] = "V00.01";
const char COB_ASCII[] = "A";
const char COB_BINARY[] = "B";
const char COB_LITTLEENDIAN[] = "LH";
const char COB_BIGENDIAN[] = "HL";
const char COB_BLANK[] = "             ";


//********************************************************************************
//Costruttore standard
//********************************************************************************
CCaligari::CCaligari () {
	m_Reset ();
	
	m_mtlDefault.fUOffset = m_mtlDefault.fVOffset = 0.0f;
	m_mtlDefault.fURepeat = m_mtlDefault.fVRepeat = 0.0f;
	m_mtlDefault.mpFlags = MP_NONE;
	m_mtlDefault.mtlNext = NULL;
	m_mtlDefault.mtlPrev = NULL;
	m_mtlDefault.nColor = 0xFFFFFFFF;
	m_mtlDefault.nIDCount = 0;
	m_mtlDefault.nMaterialIndex = 0;
	m_mtlDefault.nMeshID = 0;
	m_mtlDefault.uIDList = NULL;
}



//********************************************************************************
//Distruttore standard
//********************************************************************************
CCaligari::~CCaligari () {
	m_Dispose ();
}



//********************************************************************************
//Chiama m_Reset().
//********************************************************************************
void CCaligari::Reset () {
	m_Dispose (); //Libero la memoria usata
	m_Reset (); //Reimposto le variabili sui valori iniziali
}



//********************************************************************************
//Imposta tutte le propriet� sui valori iniziali.
//********************************************************************************
inline void CCaligari::m_Reset () {
	Properties = CALG_NONE;
	m_strFileName = NULL;
	m_nFileName = 0;
	m_bDisposed = true;
	m_CobChunk = NULL;
	m_mtMeshTree = NULL;
	m_mtMeshTreeLast = NULL;
	m_mtlList = NULL;
}



//********************************************************************************
//Chiama m_Dispose()
//********************************************************************************
void CCaligari::Dispose () {
	m_Dispose ();
}



//********************************************************************************
//Libera la memoria occupata e distrugge gli oggetti creati.
//********************************************************************************
inline void CCaligari::m_Dispose () {
	CPairList pairPtr;
	
	m_bDisposed = true;
	SAFE_DELETE (m_strFileName);
	SAFE_DELETE (m_CobChunk);
	
	//Cancellazione delle mesh caricate
	LPCOB_MESHTREE mtTmp = m_mtMeshTree, mtTmpPrev;
	LPCOB_FACELIST flTmp, flTmpPrev;
	
	if (mtTmp) {
		pairPtr.AddPair (NULL, 0, (UINT)mtTmp->vtxList);
		pairPtr.SortByID ();
	}
	
	while (mtTmp) {
		flTmp = mtTmp->flFaces;
		while (flTmp) {
			flTmpPrev = flTmp;
			flTmp = flTmp->flNext;
			SAFE_DELETE (flTmpPrev);
		}
		
		//Cancello - se ancora non � stata cancellata - la lista di vertici
		if (pairPtr.ExistID ((UINT)mtTmp->vtxList) == FALSE) {
			pairPtr.AddPair (NULL, 0, (UINT)mtTmp->vtxList);
			pairPtr.SortByID ();
			SAFE_DELETE (mtTmp->vtxList);
		}
		else {
			mtTmp->vtxList = NULL;
		}
		
		mtTmpPrev = mtTmp;
		mtTmp = mtTmp->mtNext;
		SAFE_DELETE (mtTmpPrev);
	}
	m_mtMeshTree = NULL;
	
	//Cancellazione dei materiali
	LPCOB_MATERIAL mtlTmp = m_mtlList, mtlTmpPrev;
	
	while (mtlTmp) {
		mtlTmpPrev = mtlTmp;
		mtlTmp = mtlTmp->mtlNext;
		
		SAFE_DELETE (mtlTmpPrev->uIDList);
		delete mtlTmpPrev;
	}
	m_mtlList = NULL;
	
	m_pairTexture.Dispose ();
}



//********************************************************************************
//Apre il file specificato e chiama m_LoadFile(), restituendone il valore
//di ritorno se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CCaligari::LoadFile (const char* strFileName) {
	if (strFileName == NULL)
		return NONZERO;
	
	FILE* f = fopen (strFileName, "rb");
	if (f == NULL)
		return NONZERO;
	
	//Cancello quello che � stato precedentemente caricato
	if (!m_bDisposed)
		m_Dispose();
	m_Reset ();
	
	//Conservo il nuovo nome di file
	m_nFileName = strlen(strFileName);
	m_strFileName = new char[m_nFileName + 1];
	if (m_strFileName == NULL) {
		fclose (f);
		m_nFileName = 0;	
		return NONZERO;
	}
	strcpy (m_strFileName, strFileName);
	
	const int nRet = m_LoadFile(f);
	fclose (f);
	return nRet;
}



//********************************************************************************
//Chiama m_LoadFile() e ne restituisce il valore di ritorno se va tutto
//bene, altrimenti NONZERO.
//********************************************************************************
int CCaligari::LoadFile (FILE* f) {
	if (f == NULL)
		return NONZERO;
	else {
		if (!m_bDisposed)
			m_Dispose();
		m_Reset ();
		
		return m_LoadFile(f);
	}
}



//********************************************************************************
//Carica le mesh contenute nel file specificato. Restituisce ZERO se va tutto
//bene, altrimenti NONZERO. Non effettua controlli sui puntatori ricevuti.
//********************************************************************************
inline int CCaligari::m_LoadFile (FILE* f) {
	//Validazione del file
	COB_HEADER cobHeader;
	COB_CHUNK cobChunk;
	
	fread (&cobHeader, sizeof(COB_HEADER), 1, f);
	if (m_CheckHeader (&cobHeader))
		return NONZERO;
	
	while (TRUE) {
		fread (&cobChunk, sizeof(COB_CHUNK), 1, f);
		if (m_ChunkCompare (cobChunk.strType, "END ", 4))
			break;
		else if (m_ChunkCompare (cobChunk.strType, "PolH", 4))
			m_ReadChunkPolH (f, &cobChunk);
		else if (m_ChunkCompare (cobChunk.strType, "Mat1", 4))
			m_ReadChunkMat1 (f, &cobChunk);
		else
			fseek (f, cobChunk.nChunkSize, SEEK_CUR);
	};
	
	m_bDisposed = false;
	return ZERO;
}




//********************************************************************************
//Confronta i valori dell'header specificato con quelli supportati dalla
//classe. Restituisce ZERO se l'header � valido, altrimenti NONZERO.
//********************************************************************************
int CCaligari::m_CheckHeader (const LPCOB_HEADER header) {
	if (header == NULL)
		return NONZERO;
	
	if (m_ChunkCompare (COB_SIGNATURE, header->strSignature, strlen(COB_SIGNATURE)) == FALSE)
		return NONZERO;
	if (m_ChunkCompare (COB_VERSION, header->strVersion, strlen(COB_VERSION)) == FALSE)
		return NONZERO;
	if (m_ChunkCompare (COB_BINARY, &header->cType, strlen(COB_BINARY)) == FALSE)
		return NONZERO;
	if (m_ChunkCompare (COB_LITTLEENDIAN, header->strByteOrder, strlen(COB_LITTLEENDIAN)) == FALSE)
		return NONZERO;
	if (m_ChunkCompare (COB_BLANK, header->strBlank, strlen(COB_BLANK)) == FALSE)
		return NONZERO;
	
	return ZERO;
}



//********************************************************************************
//Confronta i primi nLen byte dei due array specificati, e restituisce TRUE
//se sono uguali, altrimenti FALSE.
//********************************************************************************
BOOL CCaligari::m_ChunkCompare ( const char* chunk1, const char* chunk2, int nLen ) {
	if (nLen <= 0 || chunk1 == NULL || chunk2 == NULL)
		return FALSE;
	
//	const char* c1 = chunk1;
//	const char* c2 = chunk2;
	BOOL bRet = TRUE;
	
	for (int z = 0; z < nLen; z++) {
		if (*chunk1 != *chunk2) {
			bRet = FALSE;
			break;
		}
		chunk1++;
		chunk2++;
	}
	
	return bRet;
}



//********************************************************************************
//PolH
//Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CCaligari::m_ReadChunkPolH (FILE* f, const LPCOB_CHUNK cobNfo) {
	if (cobNfo->iMajor != 0 || cobNfo->iMinor != 8) {
		if (cobNfo->nChunkSize >= 0)
			fseek (f, cobNfo->nChunkSize, SEEK_CUR);
		return NONZERO;
	}
	
//	LPCOB_VERTEX vList; //Lista dei vertici
	LPMESH_VERTEX vList;
	LPCOB_TEXUV uvList; //Lista delle coordinate per le texture
	//LPMESH_VERTEX* vMesh; //Mesh caricata
	COB_OBJECTNAME cobObjName; //Non usato
	COB_LOCALAXES cobAxes; //Assi locali
	COB_MATRIX cobPosition; //Posizione
	int nVertCount; //Numero di elementi in vList
	int nTexCount; //Numero di elementi in uvList
	int nFaceCount; //Numero di face nella mesh
	char cFlags; //Flag della faccia in lettura
	short int iVertices; //Numero di vertici per la faccia in lettura
	short int iMaterialIndex; //Indice del material riferito alla faccia in lettura
	DWORD dwFace[3]; //Faccia in lettura
	MATH_3DVECTOR vecTmp; //Vettore temporaneo usato per applicare gli assi locali
//	MATH_3DVECTOR vecI, vecJ, vecK;
	CMathEngine math;
	MATH_QUATERNION qTmp;
	MATH_MATRIX16 matActual;
	MATH_3DVECTOR vecNormal;
	MATH_3DVECTOR vecU, vecV;
	bool bKeepList = false; //Determina se la funzione dovr� cancellare o no vList
	
	const int nFilePos = ftell(f); //Memorizzo la posizione di lettura nel file
	
	fread (&cobObjName.iDupecount, sizeof(short int), 1, f);
	fread (&cobObjName.cobstrName.iLen, sizeof(short int), 1, f);
	fseek (f, cobObjName.cobstrName.iLen, SEEK_CUR);
	
	fread (&cobAxes, sizeof(COB_LOCALAXES), 1, f);
	fread (&cobPosition, sizeof(COB_MATRIX), 1, f);
	
	//Matrice
	CopyMemory (&matActual, &cobPosition, sizeof(COB_MATRIX));
	MATH_SETQUATERNION (matActual.r3, 0.0f, 0.0f, 0.0f, 1.0f);
	
	//Vertici
	fread (&nVertCount, 4, 1, f);
	vList = new MESH_VERTEX[nVertCount];
	if (vList == NULL)
		goto Error;
//	fread (vList, sizeof(COB_VERTEX), nVertCount, f);


//Cancellare - inizio
	FILE* fDump = fopen("mech_anim.vtx", "rb");
//Cancellare - fine

	//Trasformazione dei vertici
	for (int nV = 0; nV < nVertCount; nV++) {
		fread (&vList[nV].vPos, 12, 1, f);
		MATH_INIT3DVECTOR (vList[nV].vNormal);
		
#ifdef _DEBUG
		MATH_MATRIX16 matScale;
		math.Mat16TransformScale (&matScale, 1.0f, 1.0f, 1.0f);
#endif
/*
		MATH_COPY3DVECTOR (vecI, cobAxes.vX);
		MATH_COPY3DVECTOR (vecJ, cobAxes.vY);
		MATH_COPY3DVECTOR (vecK, cobAxes.vZ);
		math.Vec3Scale (&vecI, vList[nV].x);
		math.Vec3Scale (&vecJ, vList[nV].y);
		math.Vec3Scale (&vecK, vList[nV].z);
		math.Vec3Sum (&vecTmp, &vecI, &vecJ);
		math.Vec3Sum (&vecTmp, &vecTmp, &vecK);
*/

		MATH_COPY3DVECTOR (vecTmp, vList[nV].vPos);
		MATH_SETQUATERNION (qTmp, vecTmp.x + cobAxes.vCenter.x, vecTmp.y + cobAxes.vCenter.y, vecTmp.z + cobAxes.vCenter.z, 1.0f);
		math.QuatTransform (&qTmp, &matActual);
#ifdef _DEBUG
		math.QuatTransform (&qTmp, &matScale);
#endif
		vList[nV].x = qTmp.x;
		vList[nV].y = qTmp.z;
		vList[nV].z = qTmp.y;
//Cancellare - inizio
		fread (&vList[nV].vPos, 12, 1, fDump);
//Cancellare - fine
	}
	
//Cancellare - inizio
	fclose (fDump);
	fDump = NULL;
//Cancellare - fine

	//Coordinate texture
	fread (&nTexCount, 4, 1, f);
	uvList = new COB_TEXUV[nTexCount];
	if (uvList == NULL)
		goto Error;
	fread (uvList, sizeof(COB_TEXUV), nTexCount, f);
	
	//Facce
	fread (&nFaceCount, 4, 1, f);
	for (int z = 0; z < nFaceCount; z++) {
		fread (&cFlags, 1, 1, f);
		fread (&iVertices, 2, 1, f);
		if (cFlags & F_HOLE) {
			fseek (f, (int)iVertices << 3, SEEK_CUR);
		}
		else {
			if (iVertices == 3) {
				int nTexIndex;
			
				//Leggo l'indice del materiale e preparo la faccia
				fread (&iMaterialIndex, 2, 1, f);
				
				for (int n = 0; n < 3; n++) {
					fread (dwFace + n, 4, 1, f);
					fread (&nTexIndex, 4, 1, f);
					
					vList[dwFace[n]].tu = uvList[nTexIndex].tu;
					vList[dwFace[n]].tv = uvList[nTexIndex].tv * -1.0f;
				}
				
				//Calcolo la normale a questa faccia, poi la
				//normalizzo e la sommo alla normale che gi� c'era
				LPMATH_3DVECTOR vecA = &(vList+dwFace[0])->vPos,
								vecB = &(vList+dwFace[1])->vPos,
								vecC = &(vList+dwFace[2])->vPos;
				
				math.Vec3Subtract (&vecU, vecB, vecA); //B - A
				math.Vec3Subtract (&vecV, vecC, vecB); //C - B
				
				math.Vec3Cross (&vecNormal, &vecU, &vecV);
				math.Vec3Normalize (&vecNormal);
				math.Vec3Sum (&(vList+dwFace[0])->vNormal, &(vList+dwFace[0])->vNormal, &vecNormal);
				math.Vec3Sum (&(vList+dwFace[1])->vNormal, &(vList+dwFace[1])->vNormal, &vecNormal);
				math.Vec3Sum (&(vList+dwFace[2])->vNormal, &(vList+dwFace[2])->vNormal, &vecNormal);
				
				//Memorizzo la faccia
				if (m_AddFace (dwFace, vList, nVertCount, cobNfo->nChunkID, (int)iMaterialIndex) == NONZERO)
					goto Error;
				bKeepList = true;
			}
			else
				fseek (f, (int)iVertices << 3, SEEK_CUR);
		}
	}
	
	SAFE_DELETE (uvList);
	if (!bKeepList) {
		SAFE_DELETE (vList);
	}
	fseek (f, nFilePos + cobNfo->nChunkSize, SEEK_SET);
	return ZERO;
Error:
	SAFE_DELETE (uvList);
	if (!bKeepList) {
		SAFE_DELETE (vList);
	}
	fseek (f, nFilePos + cobNfo->nChunkSize, SEEK_SET);
	return NONZERO;
}



//********************************************************************************
//Aggiunge alla lista interna la faccia specificata. Questa viene aggiunta
//ad una sotto-mesh esistente se ne esiste una con la texture specificata,
//oppure viene creata una nuova sotto-mesh per la texture specificata.
//restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CCaligari::m_AddFace (const LPDWORD dwInd, const LPMESH_VERTEX vtxList, int nVertexCount, int nMeshID, int nMaterialIndex) {
	if (dwInd == NULL || nMeshID == 0 || vtxList == NULL || nVertexCount == 0)
		return NONZERO;
	
	LPCOB_MESHTREE mtMesh = m_mtMeshTree;
	LPCOB_FACELIST flFace;
	LPCOB_FACELIST flNew;
	bool bFound = false;
	
	//Cerco se esiste gi� una sottomesh alla quale aggiungere la faccia
	while (mtMesh) {
		if (mtMesh->nMeshID == nMeshID && mtMesh->nMaterialIndex == nMaterialIndex) {
			bFound = true;
			break;
		}
		mtMesh = mtMesh->mtNext;
	}
	
	//Se non esiste una sotto-mesh per la texture specificata
	if (!bFound) {
		//Creo un nuovo elemento
		LPCOB_MESHTREE mtPrev = m_mtMeshTreeLast;
		LPCOB_MESHTREE mtNew = new COB_MESHTREE;
		if (mtNew == NULL)
			return NONZERO;
		
		//Se la lista � vuota
		if (m_mtMeshTreeLast == NULL) {
			mtMesh = m_mtMeshTree = m_mtMeshTreeLast = mtNew;
		}
		else {
			m_mtMeshTreeLast->mtNext = mtNew;
			mtMesh = m_mtMeshTreeLast = mtNew;
		}
		
		mtMesh->nMeshID = nMeshID;
		mtMesh->nMaterialIndex = nMaterialIndex;
		mtMesh->nFaceCount = 0;
		mtMesh->flFaces = NULL;
		mtMesh->mtPrev = mtPrev;
		mtMesh->mtNext = NULL;
		mtMesh->nVertexCount = nVertexCount;
		mtMesh->vtxList = vtxList;
	}
	
	//Creo la nuova faccia
	flNew = new COB_FACELIST;
	if (flNew == NULL) {
		//Ommamma...
		return NONZERO;
	}
	
	flNew->nPadd = 0;
//	flNew->flPrev = mtMesh->flFaces;
	flNew->flNext = NULL;
	
//	CopyMemory (flNew->dwIndices, dwInd, sizeof(DWORD) * 3);
	flNew->dwIndices[0] = dwInd[0];
	flNew->dwIndices[1] = dwInd[1];
	flNew->dwIndices[2] = dwInd[2];
	
	//Mi sposto sull'ultimo elemento
	if (mtMesh->flFaces) { //Se ci sono gi� delle facce, scorro la lista
		flFace = mtMesh->flFaces;
		while (flFace->flNext) {
			flFace = flFace->flNext;
		}
		flNew->flPrev = flFace;
		flFace->flNext = flNew;
	}
	else {
		flNew->flPrev = NULL;
		mtMesh->flFaces = flNew;
	}
	
	mtMesh->nFaceCount++;
	return ZERO;
}



//********************************************************************************
//
//********************************************************************************
int CCaligari::BuildMesh (LPDIRECT3DDEVICE9 dev, CMesh* mesh) {
	DWORD dwSubMesh = 0;
	LPCOB_MESHTREE mtTmp = m_mtMeshTree;
	LPDWORD dwList = NULL;
	CPairList pairList; //Lista di tutte le liste di vertici
	CMathEngine math;
	
	while (mtTmp) {
		dwSubMesh++;
		mtTmp = mtTmp->mtNext;
	}
	
	mesh->PreAllocSubMesh (dwSubMesh);
		
	mtTmp = m_mtMeshTree;
	while (mtTmp) {
		const int nVertexCount = mtTmp->nFaceCount + (mtTmp->nFaceCount << 1);
		LPCOB_FACELIST flTmp;
		DWORD z = 0;
		
		//Controllo se la lista di vertici di questa submesh � gi� in lista
		//Se non c'�, le normali vanno normalizzate e la lista di vertici
		//va aggiunta alla lista di liste.
		if (pairList.ExistID ((UINT)mtTmp->vtxList) == FALSE) {
			pairList.AddPair (NULL, 0, (UINT)mtTmp->vtxList);
			pairList.SortByID ();
			
			//Normalizzo le normali
			for (z = 0; z < mtTmp->nVertexCount; z++) {
				math.Vec3Normalize (&mtTmp->vtxList[z].vNormal);
				mtTmp->vtxList[z].color = D3DCOLOR_XRGB(30,100,200);
			}
		}
		
		dwList = new DWORD[nVertexCount];
		if (dwList == NULL)
			break;
		
		//Imposto il materiale per la mesh
		LPCOB_MATERIAL mtlMat = m_FindMaterial (mtTmp->nMeshID, mtTmp->nMaterialIndex, true);
		for (z = 0; z < mtTmp->nVertexCount; z++) {
			mtTmp->vtxList[z].tu *= mtlMat->fURepeat;
			mtTmp->vtxList[z].tv *= mtlMat->fVRepeat;
			
			mtTmp->vtxList[z].tu += mtlMat->fUOffset;
			mtTmp->vtxList[z].tv += mtlMat->fVOffset;
			
			mtTmp->vtxList[z].color = mtlMat->nColor;
		}
		
		//Crea l'array di indici
		flTmp = mtTmp->flFaces;
		z = 0;
		while (flTmp) {
			CopyMemory (dwList + z, flTmp->dwIndices, sizeof(DWORD) * 3);
//			dwList[z+0].color = D3DCOLOR_XRGB(30,100,200);
//			dwList[z+1].color = D3DCOLOR_XRGB(30,100,200);
//			dwList[z+2].color = D3DCOLOR_XRGB(30,100,200);
			
			z += 3;
			flTmp = flTmp->flNext;
		}
		
		m_pairTexture.SortByID ();
		
//		MATH_MATRIX16 mat;
//		math.Mat16TransformScale (&mat, 7.0f, 7.0f, 7.0f);
		mesh->SetVertices (dev, /*&mat*/NULL, mtTmp->vtxList, dwList, &m_pairTexture, mtlMat->uIDList[0], -1, -1, mtTmp->nVertexCount, z, mtlMat->mpFlags);
		
		//Pulisco e next
		SAFE_DELETE (dwList);
		mtTmp = mtTmp->mtNext;
	}
	
	return ZERO;
}



//********************************************************************************
//Cancella il percorso contenuto in strFileName, lasciando solo il nome
//del file. Restituisce in csFileName->iLen la lunghezza della nuova
//stringa. Se bNullTerminated � specificato, la funzione assume che la
//lunghezza della stringa � di iLen+1 e che sia NULL-terminated. Di conseguenza
//anche la nuova stringa sar� NULL-terminated.
//********************************************************************************
void CCaligari::m_DeletePath (LPCOB_STRING csFileName, bool bNullTerminated) {
	if (csFileName == NULL)
		return;
	if (csFileName->iLen <= 0)
		return;
	
	short int z;
	const int nNullChar = (bNullTerminated ? 1 : 0);
	
	for (z = (short int)(csFileName->iLen - 1); z >= 0; z--) {
		if (csFileName->strString[z] == '\\')
			break;
	}
	
	if (z > -1) { //Se � stato trovato un separatore backslash
		char* strTmp = new char[csFileName->iLen - z - 1 + nNullChar];
		if (strTmp == NULL)
			return;
		
		csFileName->iLen = (short int)(csFileName->iLen - z - 1);
//		for (short int n = 0; n < csFileName->iLen; n++) {
//			strTmp[n] = csFileName->strString[n + z + 1];
//		}
		CopyMemory (strTmp, csFileName->strString + z + 1, csFileName->iLen);
		CopyMemory (csFileName->strString, strTmp, csFileName->iLen);
		SAFE_DELETE (strTmp);
		
		if (bNullTerminated)
			csFileName->strString[csFileName->iLen] = 0;
	}
	
	return;
}



//********************************************************************************
//Mat1
//Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CCaligari::m_ReadChunkMat1 (FILE* f, const LPCOB_CHUNK cobNfo) {
	if (cobNfo->iMajor != 0 || cobNfo->iMinor != 6) {
		if (cobNfo->nChunkSize >= 0)
			fseek (f, cobNfo->nChunkSize, SEEK_CUR);
		return NONZERO;
	}
	
	LPCOB_MATERIAL mtlNew = new COB_MATERIAL;
	short int iMatIndex;
	const int nFilePos = ftell(f);
	BYTE ucShader;
	float fRed, fGreen, fBlue, fAlpha;
	
	if (mtlNew == NULL)
		goto Error;
	
	fread (&iMatIndex, 2, 1, f);
	mtlNew->nMaterialIndex = (int)iMatIndex;
	mtlNew->nMeshID = cobNfo->nParentID;
	mtlNew->nIDCount = 1;
	mtlNew->uIDList = new unsigned int[1];
	mtlNew->mtlNext = NULL;
	mtlNew->mpFlags = MP_NONE;
	mtlNew->fUOffset = mtlNew->fVOffset = 0.0f;
	mtlNew->fURepeat = mtlNew->fVRepeat = 0.0f;
	
	//Leggo lo shader da usare
	fread (&ucShader, 1, 1, f);
	if (ucShader == 'f')
		mtlNew->mpFlags = (MATERIAL_PROPERTIES)(mtlNew->mpFlags | MP_FLATSHADING);
	
	//Leggo il diffuse color
	fseek (f, 2, SEEK_CUR);
	fread (&fRed, 4, 1, f);
	fread (&fGreen, 4, 1, f);
	fread (&fBlue, 4, 1, f);
	fread (&fAlpha, 4, 1, f);
	mtlNew->nColor = ((int)(fAlpha * 255.0f) & 0xFF) |
					 (((int)(fRed * 255.0f) & 0xFF) << 8) |
					 (((int)(fGreen * 255.0f) & 0xFF) << 16) |
					 (((int)(fBlue * 255.0f) & 0xFF) << 24);
	
	//Salto altri valori che non mi interessano
	fseek (f, 16, SEEK_CUR);
	
	//Controllo se sono presenti i campi opzionali
	int nDataRead = 37;
	while (nDataRead < cobNfo->nChunkSize) {
		short int iIdentifier;
		fread (&iIdentifier, 2, 1, f);
		nDataRead += 2;
		
		if (iIdentifier == ':e') {
			//Environment map - da saltare
			fseek (f, 1, SEEK_CUR);
			fread (&iIdentifier, 2, 1, f);
			fseek (f, iIdentifier, SEEK_CUR);
			nDataRead += 3 + iIdentifier;
		}
		else if (iIdentifier == ':t') {
			//Texture
			BYTE ucTextureFlags;
			COB_STRING csTexturePath;
			
			fread (&ucTextureFlags, 1, 1, f); //Leggo i flag
			m_ReadString (f, &csTexturePath); //Leggo il percorso della texture
			fread (&mtlNew->fUOffset, 4, 4, f); //Leggo gli offset e i repeat
			
			if (ucTextureFlags & TXTR_OVERLAY)
				mtlNew->mpFlags = (MATERIAL_PROPERTIES)(mtlNew->mpFlags | MP_TEXTUREOVERLAY);
			
			nDataRead += 3 + csTexturePath.iLen + 4 * 4;
			m_DeletePath (&csTexturePath, true);
			
			//Aggiunta della texture in m_pairTexture
			if (m_pairTexture.ExistText (csTexturePath.strString) == FALSE) {
				*(mtlNew->uIDList) = m_pairTexture.AddPair (csTexturePath.strString, 0);
			}
			else {
				m_pairTexture.GetLastID (mtlNew->uIDList);
			}
		}
		if (iIdentifier == ':b') {
			//Bump map - da saltare
			fseek (f, 1, SEEK_CUR);
			fread (&iIdentifier, 2, 1, f);
			fseek (f, iIdentifier + 20, SEEK_CUR);
			nDataRead += 3 + iIdentifier + 20;
		}
	}
	
	//Aggiungo il materiale alla lista
	if (m_mtlList == NULL) {
		mtlNew->mtlPrev = NULL;
		m_mtlList = mtlNew;
	}
	else {
		LPCOB_MATERIAL mtlTmp = m_mtlList;
		while (mtlTmp->mtlNext) {
			mtlTmp = mtlTmp->mtlNext;
		}
		mtlNew->mtlPrev = mtlTmp;
		mtlTmp->mtlNext = mtlNew;
	}
	
	fseek (f, nFilePos + cobNfo->nChunkSize, SEEK_SET);
	return ZERO;
Error:
	if (mtlNew) {
		SAFE_DELETE (mtlNew->uIDList);
		delete mtlNew;
	}
	
	fseek (f, nFilePos + cobNfo->nChunkSize, SEEK_SET);
	return NONZERO;
}



//********************************************************************************
//Legge una stringa di tipo COB_STRING dal file specificato. Restituisce
//ZERO se va tutto bene, altrimenti NONZERO. Anche se non conforme alle
//specifiche Caligari, la funzione aggiunge un NULL char al termine della
//stringa caricata. La dimensione allocata � quindi quella indicata in
//csString->iLen + 1.
//********************************************************************************
int CCaligari::m_ReadString (FILE* f, LPCOB_STRING csString) {
	if ( (f && csString) == FALSE )
		return NONZERO;
	
	fread (&csString->iLen, 2, 1, f);
	if (csString->iLen <= 0)
		return NONZERO;
	
	csString->strString = new char[csString->iLen + 1];
	if (csString->strString == NULL)
		return NONZERO;
	
	fread (csString->strString, 1, csString->iLen, f);
	csString->strString[csString->iLen] = 0;

	return ZERO;
}



//********************************************************************************
//Cerca in m_mtlList il materiale appartenente alla mesh con ID nOwnerID
//e con indice nIndex. Restituisce un puntatore alla struttura desiderata
//se va tutto bene, altrimenti NULL. Se bForceMtl � true, in caso di
//insuccesso la funzione restituir� invece l'indirizzo di m_mtlDefault.
//********************************************************************************
inline LPCOB_MATERIAL CCaligari::m_FindMaterial (int nOwnerID, int nIndex, bool bForceMtl) {
	LPCOB_MATERIAL mtlTmp = m_mtlList;
	
	while (mtlTmp) {
		if (mtlTmp->nMeshID == nOwnerID && mtlTmp->nMaterialIndex == nIndex)
			break;
		mtlTmp = mtlTmp->mtlNext;
	}
	
	if (bForceMtl) {
		if (mtlTmp)
			return mtlTmp;
		else
			return &m_mtlDefault;
	}
	else
		return mtlTmp;
}