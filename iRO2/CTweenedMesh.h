//CTweenedMesh.h


#ifndef _INCL_CTWEENEDMESH_H
#define _INCL_CTWEENEDMESH_H

#include <d3d9.h>
#include "CMathEngine.h"
#include "CMesh.h"

/*
#ifdef MESH_VERTEXSTRUCTUREDEFINED
#if MESH_VERTEXSTRUCTUREDEFINED == 1
#pragma message(__FILE__ ": STRUCT_MESH_VERTEX already defined in CMesh class. Include CTweenedMesh.h first.")
#error
#else if MESH_VERTEXSTRUCTUREDEFINED != 2
#pragma message(__FILE__ ": STRUCT_MESH_VERTEX already defined in another class. Include CTweenedMesh.h first.")
#error
#endif
#else
*/
/*
#define MESH_VERTEXSTRUCTUREDEFINED 2
#endif


#ifdef _DEFINE_CMESH_FVF
#undef _DEFINE_CMESH_FVF
#endif
#ifdef D3DFVF_CMESH
#undef D3DFVF_CMESH
#endif
#define D3DFVF_CMESH ( D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1 )

//Va incluso dopo aver dichiarato STRUCT_MESH_VERTEX
#include "CMesh.h"
*/



typedef struct STRUCT_TMESH_VERTNORM {
	union {
		struct {
			float x, y, z;
		};
		MATH_3DVECTOR vPos;
	};
	union {
		struct {
			float nx, ny, nz;
		};
		MATH_3DVECTOR vNormal;
	};
} TMESH_VERTNORM, *LPTMESH_VERTNORM;
typedef struct STRUCT_TMESH_COLORTEX {
	D3DCOLOR color;
	union {
		struct {
			float tu, tv;
		};
		MATH_2DVECTOR vTex;
	};
} TMESH_COLORTEX, *LPTMESH_COLORTEX;
typedef struct STRUCT_TMESH_ANIMATION {
	float fStartFrame;
	float fFrameCount;
	float fLoopFrames;
	float fFPS;
} TMESH_ANIMATION, *LPTMESH_ANIMATION;
typedef struct STRUCT_TMESH_VERTEXDUMP { //Informazioni aggiuntive per ogni vertice
	LPMATH_3DVECTOR vecVert; //Lista delle posizioni per ogni keyframe
	LPMATH_3DVECTOR vecNorm; //Lista delle normali per ogni keyframe
//	MATH_2DVECTOR vecTexture; //Coordinate delle texture per questo vertice
} TMESH_VERTEXDUMP, *LPTMESH_VERTEXDUMP;



class CTweenedMesh : public CMesh {
public:
	CTweenedMesh ( void );
	~CTweenedMesh ( void );
	void Dispose ( void );
	
	int LoadRawVertices ( const char* strFileName, DWORD dwKeyFrameCount );
	int LoadRawVertices ( FILE* f, DWORD dwKeyFrameCount );
	int LoadRawAnimationInfo ( const char* strFileName, DWORD dwCount );
	int LoadRawAnimationInfo ( FILE* f, DWORD dwCount );
	DWORD SetVertices ( LPDIRECT3DDEVICE9 dev, const LPMATH_MATRIX16 matTransf, const LPMESH_VERTEX vert, const LPDWORD ind, CPairList* pairTextureList, UINT nTexIndex, UINT nBumpIndex, UINT nReflIndex, DWORD dwVert, DWORD dwInd, MATERIAL_PROPERTIES mp );
	int BuildAnimation ( LPDIRECT3DDEVICE9 dev );
	int SpawnClone ( CTweenedMesh &mesh );
	
	inline int SetActiveAnimation ( DWORD dwAnim );
	inline int AdvanceAnimation ( float fDelta );
	inline DWORD GetCurrentFrame ( void );
	inline void CompleteAnimation ( void );
	
	bool bAnimationComplete;
protected:
	inline void m_Reset ( void );
	inline void m_Dispose ( void );
	inline int m_LoadRawVertices ( FILE* f, DWORD dwKeyFrameCount );
	inline int m_LoadRawAnimation ( FILE* f, DWORD dwCount );
	int m_SwapVertices ( DWORD dwIndex1, DWORD dwIndex2 );
	int m_ExpandSubMeshList ( DWORD dwNewSize );
	int m_BuildNormals ( void );
	int m_BuildVBuffers ( void );
	inline int m_DeleteTemp ( void );
	inline int m_DeleteVBuffers ( void );
	int m_Render ( LPDIRECT3DDEVICE9 dev, DWORD dwIndex );
	int m_PreRenderSteps ( LPDIRECT3DDEVICE9 dev );
	int m_PostRenderSteps ( LPDIRECT3DDEVICE9 dev );
	friend int m_SpawnClone ( CTweenedMesh &src, CTweenedMesh &dst );

	//Copia degli indici di ogni submesh. Il numero di array
	//� pari a m_dwPreallocatedIndices, ed il numero di elementi in ogni
	//array � pari al valore specificato nel m_smMesh[].dwIndexCount
	//corrispondente. Il numero di array da considerare valido, comunque,
	//� m_dwSubMeshCount. Elementi successivi a tale numero, anche se
	//allocati, non sono da considerare puntatori validi.
	DWORD** m_ppdwIndices;
	//Ogni elemento dell'array identifica un vertice composto da posizione
	//e normale. La posizione � caricata dal dump dei vertici con LoadRawVertices(),
	//mentre le normali sono calcolate in seguito da m_BuildNormals(). vdVert �
	//composto da m_dwVertCount elementi, ed i puntatotri contenuti nella
	//struttura puntano ad array di m_dwKeyFrameCount elementi.
	TMESH_VERTEXDUMP* m_vdVert;
	//Contiene i vertex buffer finali usati per le animazioni. Il numero di
	//array puntati da m_vbList � pari a m_dwSubMeshCount, ed ogni array �
	//lungo m_dwKeyFrameCount. Ogni vertex buffer contiene la lista di vertici
	//del keyframe appropriato.
	LPDIRECT3DVERTEXBUFFER9* m_vbList;
	
	bool m_bIsClone; //Determina se i buffer vanno eliminati quando viene invocato Dispose()
	LPTMESH_ANIMATION m_anAnims;
	DWORD m_dwKeyFrameCount;
	DWORD m_dwAnimsCount;
	DWORD m_dwActiveAnim;
	DWORD m_dwIndex; //Copia intera di m_fIndex
	DWORD m_dwIndex2; //Indice del fotogramma che segue m_dwIndex
	DWORD m_dwPreallocatedIndices;
	float m_fIndex;
	float m_fIndexNow; //Usato da AdvanceAnimation - contiene l'intero di m_fIndex
	float m_fVertexBalance; // 0.0f <-- vertice 1, 1.0f <-- vertice 2
	
	//Static
	static D3DVERTEXELEMENT9 m_dwStreamInfo[];
	static LPDIRECT3DVERTEXDECLARATION9 m_vdeclShader;
	static int m_nRefCount;
};



//********************************************************************************
//Avanza l'animazione corrente. Restituisce ZERO, o NONZERO se non ci sono
//animazioni.
//********************************************************************************
inline int CTweenedMesh::AdvanceAnimation (float fDelta) {
	if (m_anAnims == NULL)
		return NONZERO;
	if (bAnimationComplete)
		return ZERO;
	
	const float fLoopOffset = m_anAnims[m_dwActiveAnim].fFrameCount - m_anAnims[m_dwActiveAnim].fLoopFrames;
	
	//Faccio progredire l'animazione
	m_fIndex += fDelta * m_anAnims[m_dwActiveAnim].fFPS;
	
	//Se l'indice supera il numero di fotogrammi
	//(il +1.0f serve a compensare il tween factor, che al penultimo fotogramma punta anche all'ultimo)
	if (m_fIndex + 1.0f > m_anAnims[m_dwActiveAnim].fStartFrame + m_anAnims[m_dwActiveAnim].fFrameCount) {
		//Se l'animazione non � un loop
		if (m_anAnims[m_dwActiveAnim].fLoopFrames == 0.0f) {
			//L'animazione � da considerare finita
			bAnimationComplete = true;
			m_fIndex = m_anAnims[m_dwActiveAnim].fStartFrame + m_anAnims[m_dwActiveAnim].fFrameCount;
		}
		//Se l'animazione � un loop
		else {
			m_fIndex = fmodf (m_fIndex - m_anAnims[m_dwActiveAnim].fStartFrame - fLoopOffset, m_anAnims[m_dwActiveAnim].fLoopFrames) + m_anAnims[m_dwActiveAnim].fStartFrame + fLoopOffset;
		}
		m_fIndexNow = m_fIndex;
	}
	
	//2 cast + 1 ad ogni loop dell'animazione
	if (m_fIndex >= m_fIndexNow /*|| m_fIndex + m_fIndexNow - m_anAnims[m_dwActiveAnim].fStartFrame >= m_anAnims[m_dwActiveAnim].fFrameCount*/) {
		m_dwIndex = (DWORD)m_fIndex;
		m_dwIndex2 = (m_fIndex + 1.0f >= m_anAnims[m_dwActiveAnim].fStartFrame + m_anAnims[m_dwActiveAnim].fFrameCount ? (int)m_anAnims[m_dwActiveAnim].fStartFrame : m_dwIndex + 1);
		//m_dwIndex2 = m_dwIndex % (DWORD)m_anAnims[m_dwActiveAnim].fFrameCount;
		m_fIndexNow = (float)m_dwIndex2;
	}
	
	m_fVertexBalance = fmodf(m_fIndex, 1.0f);
	return ZERO;
}



//********************************************************************************
//Imposta l'animazione da eseguire. Restituisce ZERO se va tutto bene, NONZERO
//se l'animazione specificata non esiste.
//********************************************************************************
inline int CTweenedMesh::SetActiveAnimation (DWORD dwAnim) {
	if (dwAnim == m_dwActiveAnim)
		return ZERO;
	else if (dwAnim >= m_dwAnimsCount)
		return NONZERO;
	
	m_fVertexBalance = 0.0f;
	m_dwActiveAnim = dwAnim;
	
	m_dwIndex2 = m_dwIndex = (DWORD)m_anAnims[m_dwActiveAnim].fStartFrame;
	m_fIndexNow = m_fIndex = m_anAnims[m_dwActiveAnim].fStartFrame;
	bAnimationComplete = false;
	
	return ZERO;
}



//********************************************************************************
//Restituisce m_dwIndex.
//********************************************************************************
inline DWORD CTweenedMesh::GetCurrentFrame () {
	return m_dwIndex;
}



//********************************************************************************
//Imposta l'animazione corrente sull'ultimo fotogramma.
//********************************************************************************
inline void CTweenedMesh::CompleteAnimation () {
	m_fVertexBalance = 0.0f;
	
	m_dwIndex2 = m_dwIndex = (DWORD)(m_anAnims[m_dwActiveAnim].fStartFrame + m_anAnims[m_dwActiveAnim].fFrameCount - 1.0f);
	m_fIndexNow = m_fIndex = m_anAnims[m_dwActiveAnim].fStartFrame + m_anAnims[m_dwActiveAnim].fFrameCount - 1.0f;
	bAnimationComplete = true;
}



#endif