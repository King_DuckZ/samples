//CPairList.cpp


#include "constants.h"
#include <windows.h>
#include "CPairList.h"
#include <stdio.h>
#include <string.h>

#define PREALLOC_SIZE 16
#define INVALID_INDEX 0xFFFFFFFF
#define DEF_LINE_LEN (1024 * 1)

//********************************************************************************
//Costruttore standard
//********************************************************************************
CPairList::CPairList () {
	m_strText = NULL;
	m_uID = NULL;
	m_uIndex = NULL;
	m_nParam = NULL;
	m_uLongestString = 0;
	m_uCount = 0;
	m_uPreallocated = 0;
	m_bTextSorted = false;
	m_bIDSorted = false;
	m_bLastIndexValid = false;
}



//********************************************************************************
//Distruttore standard
//********************************************************************************
CPairList::~CPairList () {
	m_Dispose ();
}



//********************************************************************************
//Chiama m_Dispose()
//********************************************************************************
void CPairList::Dispose () {
	m_Dispose ();
	m_uCount = 0;
	m_bTextSorted = false;
	m_bIDSorted = false;
	m_bLastIndexValid = false;
	m_uLongestString = 0;
	m_uPreallocated = 0;
}



//********************************************************************************
//Libera la memoria occupata e distrugge gli oggetti creati.
//********************************************************************************
inline void CPairList::m_Dispose () {
	if (m_strText) {
		for (unsigned int z = 0; z < m_uCount; z++) {
			SAFE_DELETE (m_strText[z]);
		}
	}
	SAFE_DELETE (m_strText);
	SAFE_DELETE (m_uID);
	SAFE_DELETE (m_uIndex);
}



//********************************************************************************
//Dispone i valori in m_uIndex in modo tale che se questi vengono usati come
//indici in m_strText, tale array risulta ordinato in modo crescente.
//********************************************************************************
void CPairList::SortByText () {
//shellsort(itemType a[], int l, int r)
	if (m_bTextSorted || m_uCount == 0)
		return;
	
	int i, j, k, h;
	char* v = new char[m_uLongestString + 1];
	int incs[16] = { 1391376, 463792, 198768, 86961, 33936,
		13776, 4592, 1968, 861, 336, 
		112, 48, 21, 7, 3, 1 };
	
	char** a = m_strText;
	int l = 0;
	int r = (int)m_uCount - 1;
	unsigned int tmp;

	for ( k = 0; k < 16; k++) {
		for (h = incs[k], i = l+h; i <= r; i++) {
			//v = a[i];
			strcpy (v, a[m_uIndex[i]]);
			tmp = m_uIndex[i];
			
			j = i;
			while (j > h && /*a[j-h] > v*/strcmp(a[m_uIndex[j-h]], v) > 0) {
				//a[j] = a[j-h];
				m_uIndex[j] = m_uIndex[j-h];
				j -= h;
			}
			//a[j] = v;
			m_uIndex[j] = tmp;
		}
	}
	
	SAFE_DELETE (v);
	m_bTextSorted = true;
	m_bIDSorted = false;

/*
	//Dump	
	FILE* f;
	if ( (f=fopen("textdump.txt", "wb")) != NULL ) {
		//const char strRet[] = { 13, 10 };
		const short int iRet = 0x0A0D;
		for (unsigned int z = 0; z < m_uCount; z++) {
			fwrite (m_strText[m_uIndex[z]], 1, strlen(m_strText[m_uIndex[z]]), f);
			fwrite (&iRet, 2, 1, f);
		}
		fclose (f);
	}
*/
}



//********************************************************************************
//Dispone i valori in m_uIndex in modo tale che se questi vengono usati come
//indici in m_uID, tale array risulta ordinato in modo crescente.
//********************************************************************************
void CPairList::SortByID () {
//shellsort(itemType a[], int l, int r)
	if (m_bIDSorted || m_uCount == 0)
		return;
	
	int i, j, k, h;
	unsigned int v;
	int incs[16] = { 1391376, 463792, 198768, 86961, 33936,
		13776, 4592, 1968, 861, 336, 
		112, 48, 21, 7, 3, 1 };

	int r = (int)m_uCount - 1;
	int l = 0;
	unsigned int* a = m_uID;
	unsigned int tmp;

	for ( k = 0; k < 16; k++) {
		for (h = incs[k], i = l+h; i <= r; i++) {
			v = a[m_uIndex[i]];
			tmp = m_uIndex[i];
			
			j = i;
			while (j > h && a[m_uIndex[j-h]] > v) {
				//a[j] = a[j-h];
				m_uIndex[j] = m_uIndex[j-h];
				j -= h;
			}
			//a[j] = v;
			m_uIndex[j] = tmp;
		}
	}
	
	m_bIDSorted = true;
	m_bTextSorted = false;

/*
	//Dump	
	FILE* f;
	if ( (f=fopen("textdump.txt", "wb")) != NULL ) {
		//const char strRet[] = { 13, 10 };
		const short int iRet = 0x0A0D;
		for (unsigned int z = 0; z < m_uCount; z++) {
			fwrite (m_strText[m_uIndex[z]], 1, strlen(m_strText[m_uIndex[z]]), f);
			fwrite (&iRet, 2, 1, f);
		}
		fclose (f);
	}
*/
}



//********************************************************************************
//Ripristina l'ordine iniziale della lista.
//********************************************************************************
void CPairList::Unsort () {
	for (unsigned int z = 0; z < m_uCount; z++) {
		m_uIndex[z] = z;
	}
	
	m_bIDSorted = false;
	m_bTextSorted = false;
}



//********************************************************************************
//Aggiunge una coppia testo-ID alla lista interna. L'inserimento causa
//l'invalidazione dell'ordinamento (se presente). Se uID non � specificato,
//o se uID � 0, viene scelto un ID dall'algoritmo. Restituisce l'ID assegnato
//all'elemento inserito in caso di successo, altrimenti 0. Se l'ID specificato
//� gi� presente, ne viene scelto un altro. Al termine dell'inserimento,
//m_uLastIndex punter� all'elemento appena inserito, o manterr� il suo valore
//originale in caso di fallimento.
//********************************************************************************
unsigned int CPairList::AddPair (const char* strText, int param, unsigned int uID) {
	if (m_uPreallocated == m_uCount) {
		char** tmp;
		unsigned int* tmpID;
		unsigned int* tmpIndex;
		int* tmpParam;
		
		m_uPreallocated += PREALLOC_SIZE;
		
		//Creo i nuovi array
		tmp = new char*[m_uPreallocated];
		tmpID = new unsigned int[m_uPreallocated];
		tmpIndex = new unsigned int[m_uPreallocated];
		tmpParam = new int[m_uPreallocated];
		if (tmp == NULL || tmpID == NULL || tmpIndex == NULL || tmpParam == NULL)
			goto Error;
		
		//Riempio i nuovi array
		for (unsigned int z = 0; z < m_uCount; z++) {
//			tmp[z] = new char[strlen(m_strText[z]) + 1];
			
/*			//In caso di errore di allocazione, l'intera lista va svuotata
			if (tmp[z] == NULL) {
				for (unsigned int u = 0; u < z; u++) {
					SAFE_DELETE (tmp[u]);
				}
				goto Error;
			}
*/			
			//In caso di successo, proseguo con la copia			
//			strcpy (tmp[z], m_strText[z]);
			tmp[z] = m_strText[z];
			tmpID[z] = m_uID[z];
			tmpIndex[z] = m_uIndex[z];
		}
		
		//Cancello i vecchi array e aggiorno i puntatori
//		for (unsigned int z = 0; z < m_uCount; z++) {
//			SAFE_DELETE (m_strText[z]);
//		}
		SAFE_DELETE (m_strText);
		SAFE_DELETE (m_uID);
		SAFE_DELETE (m_uIndex);
		SAFE_DELETE (m_nParam);
		m_strText = tmp;
		m_uID = tmpID;
		m_uIndex = tmpIndex;
		m_nParam = tmpParam;
		
		goto EndIf;
Error:
		SAFE_DELETE (tmp);
		SAFE_DELETE (tmpID);
		SAFE_DELETE (tmpIndex);
		SAFE_DELETE (tmpParam);
		m_uPreallocated -= PREALLOC_SIZE;
		return 0;
	}
EndIf:
	
	//Qui inizia l'aggiunta del nuovo elemento
//	if (strText == NULL)
//		return 0;
	
	if (strText) {
		//Duplicati non ammessi
		const UINT uLastInd = m_uLastIndex;
		if (this->ExistText (strText)) {
			m_uLastIndex = uLastInd;
			return 0;
		}
		m_uLastIndex = uLastInd;
		
		//Salvo la nuova stringa
		m_strText[m_uCount] = new char[strlen(strText) + 1];
		if (m_strText[m_uCount] == NULL)
			return 0;
	}
	else
		m_strText[m_uCount] = NULL;
	
	//Creo un uID valido, se necessario
	BOOL bSearch = false;
	if (uID != 0)
		bSearch = ExistID (uID);
	//Bisogna inventare un ID valido
	if (uID == 0 || bSearch) {
		uID = 0;
		for (unsigned int z = 0; z < m_uCount; z++) {
			if (m_uID[z] > uID)
				uID = m_uID[z];
		}
		uID++; //Se si verificano errori di overflow, correggere qui
	}
	
	m_bTextSorted = false;
	m_bIDSorted = false;
	if (strText) {
		m_uLongestString = MAX(m_uLongestString, strlen(strText));
		strcpy (m_strText[m_uCount], strText);
	}
	
	m_uIndex[m_uCount] = m_uCount;
	m_uID[m_uCount] = uID;
	m_nParam[m_uCount] = param;
	
	//Aggiorno m_uLastIndex
	m_bLastIndexValid = true;
	m_uLastIndex = m_uCount;
	
	m_uCount++;
	
	return uID;
}



//********************************************************************************
//
//********************************************************************************
unsigned int CPairList::DeletePair (unsigned int uID) {
	return 0;
}



//********************************************************************************
//Restituisce in *dst e come valore di ritorno il testo corrispondente all'ID
//specificato. In caso di insuccesso, il valore di ritorno � NULL. dst deve
//essere un puntatore valido.
//********************************************************************************
char* CPairList::GetText (char** dst, unsigned int uID) {
	if (dst == NULL)
		return NULL;
	
	const unsigned int uIndex = m_GetIndex (uID);
	
	//Indice non trovato
	if (uIndex == INVALID_INDEX) {
		m_bLastIndexValid = false;
		return NULL;
	}
	//Trovato
	else {
		m_bLastIndexValid = true;
		m_uLastIndex = uIndex;
		*dst = new char[strlen(m_strText[uIndex]) + 1];
		if (*dst == NULL)
			return NULL;
		strcpy (*dst, m_strText[uIndex]);
		return *dst;
	}
}



//********************************************************************************
//Restituisce in *dst e come valore di ritorno l'ID corrispondente al
//testo specificato. In caso di insuccesso, il valore di ritorno � 0. Se dst
//� NULL, � comunque possibile tenere in considerazione il valore di ritorno.
//********************************************************************************
unsigned int CPairList::GetID (unsigned int* dst, const char* strText) {
	const unsigned int uIndex = m_GetIndex (strText);
	unsigned int uRet;
	
	if (uIndex == INVALID_INDEX) {
		m_bLastIndexValid = false;
		uRet = 0;
	}
	else {
		m_bLastIndexValid = true;
		m_uLastIndex = uIndex;
		uRet = m_uID[uIndex];
	}
	
	if (dst)
		*dst = uRet;
	return uRet;
}



//********************************************************************************
//Restituisce TRUE se l'elemento specificato � presente in m_uID,
//altrimenti FALSE.
//********************************************************************************
BOOL CPairList::ExistID (unsigned int uID) {
	return (m_GetIndex(uID) == INVALID_INDEX ? FALSE : TRUE );
}



//********************************************************************************
//Restituisce TRUE se l'elemento specificato � presente in m_strText,
//altrimenti FALSE.
//********************************************************************************
BOOL CPairList::ExistText (const char* strText) {
	return (m_GetIndex(strText) == INVALID_INDEX ? FALSE : TRUE );
}



//********************************************************************************
//Restituisce l'indice dell'elemento di m_strText avente valore uguale
//a strText, oppure INVALID_INDEX in caso di errore o insuccesso.
//********************************************************************************
inline unsigned int CPairList::m_GetIndex (const char* strText) {
	unsigned int uRet = INVALID_INDEX;
	
	//Valori non validi
	if (strText == NULL)
		return uRet;
	if (strlen(strText) == 0)
		return uRet;
	if (m_uCount == 0)
		return uRet;
	
	if (m_bTextSorted) {
		//Faccio una ricerca binaria
		unsigned int z = m_uCount - 1;
		int nRet;
		
		while (z) {
			if (m_strText[m_uIndex[z]] != NULL) {
				if ((nRet = strcmp(strText, m_strText[m_uIndex[z]])) < 0) {
					break;
				}
			}
			z >>= 1;
		}

		if (z == 0 && nRet < 0)
			return INVALID_INDEX;

		for (unsigned int u = z; u < m_uCount; u++) {
			if (m_strText[m_uIndex[u]]) {
				if (strcmp(strText, m_strText[m_uIndex[u]]) == 0) {
					uRet = m_uIndex[u]; //Togliere m_uIndex per avere l'indice assoluto
					break;
				}
			}
		}
	}
	else {
		//Faccio una ricerca naive
		for (unsigned int z = 0; z < m_uCount; z++) {
			if (m_strText[z]) {
				if (strcmp (strText, m_strText[z]) == 0) {
					uRet = z;
					break;
				}
			}
		}
	}
	
	if (uRet != INVALID_INDEX) {
		m_uLastIndex = uRet;
		m_bLastIndexValid = true;
	}

	return uRet;
}



//********************************************************************************
//Restituisce l'indice dell'elemento di m_uID avente valore uguale
//a uID, oppure INVALID_INDEX in caso di errore o insuccesso.
//********************************************************************************
inline unsigned int CPairList::m_GetIndex (unsigned int uID) {
	unsigned int uRet = INVALID_INDEX;
	
	//Valore non valido
	if (uID == 0)
		return uRet;
	if (m_uCount == 0)
		return uRet;
	
	if (m_bIDSorted) {
		//Faccio una ricerca binaria
		unsigned int z = m_uCount - 1;
		
		while ( (uID < m_uID[m_uIndex[z]]) && (z != 0) ) {
			z >>= 1;
		}
		
		if (z == 0 && uID < m_uID[m_uIndex[z]])
			return INVALID_INDEX;
		
		for (unsigned int u = z; u < m_uCount; u++) {
			if (uID == m_uID[m_uIndex[u]]) {
				uRet = m_uIndex[u]; //Togliere m_uIndex per avere l'indice assoluto
				break;
			}
		}
	}
	else {
		//Faccio una ricerca naive
		for (unsigned int z = 0; z < m_uCount; z++) {
			if (uID == m_uID[z]) {
				uRet = z;
				break;
			}
		}
	}
	
	if (uRet != INVALID_INDEX) {
		m_uLastIndex = uRet;
		m_bLastIndexValid = true;
	}

	return uRet;
}



//********************************************************************************
//Carica una lista di coppie stringa-ID dal file specificato, che deve
//essere nel formato "%u %s\n". I valori corrispondenti alle coppie vengono
//impostati tutti su 0. Restituisce ZERO se � andato tutto bene, altrimenti
//NONZERO.
//********************************************************************************
int CPairList::LoadListFromFile (const char* strFileName) {
	if (strFileName == NULL)
		return NONZERO;
	if (strlen(strFileName) < 1)
		return NONZERO;
	
	FILE* f = NULL;
	char* strBuff = NULL;
	const char strFormat[] = "%u %s\n";
	int nRet = NONZERO;
	unsigned int uID;
	
	strBuff = new char[DEF_LINE_LEN];
	if (strBuff == NULL)
		goto Farewell;
	if ( (f=fopen(strFileName, "r")) == NULL )
		goto Farewell;
	
	while (fscanf (f, strFormat, &uID, strBuff) != EOF) {
		this->AddPair (strBuff, 0, uID);
	}
	
	nRet = ZERO;
Farewell:
	if (f)
		fclose (f);
	SAFE_DELETE (strBuff);
	return nRet;
}