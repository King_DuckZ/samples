//CInputDevice.h
//Questa classe non � multithread safe.
//La classe per funzionare ha bisogno di avere impostato in DI un indirizzo
//ad un oggetto DIRECTINPUT8 valido. Quando viengono invocati il metodo Dispose()
//o il distruttore, tale oggetto non viene rilasciato, e il valore del 
//puntatore viene semplicemente impostato su NULL. � quindi necessario
//rilasciare DI prima che l'oggetto che ne fa uso venga distrutto. Se pi�
//istanze di CInputDevice condividono lo stesso oggetto DIRECTINPUT8, � necessario
//comunque rilasciarne solo uno.


#ifndef _INCL_CINPUTDEVICE_H
#define _INCL_CINPUTDEVICE_H

#define DIRECTINPUT_VERSION 0x0800

#include "Constants.h"
#include <windows.h>
#include <dinput.h>

//Movimenti del personaggio
#define IDMOVE_FORWARD 0x01
#define IDMOVE_BACKWARD 0x02
#define IDMOVE_NONE 0x00
#define IDMOVE_LEFT 0x04
#define IDMOVE_RIGHT 0x08
#define IDMOVE_LEFTWARD 0x10
#define IDMOVE_RIGHTWARD 0x20
#define IDMOVE_RUNNING 0x40
//Movimenti combinati con running
#define IDMOVE_RUNNINGFORWARD 0x41
#define IDMOVE_RUNNINGBACKWARD 0x42
#define IDMOVE_RUNNINGLEFTWARD 0x50
#define IDMOVE_RUNNINGRIGHTWARD 0x60
//Tasti vari
#define IDBUTTON_NONE 0x00
#define IDBUTTON_EXIT 0x01
#define IDBUTTON_JUMP 0x02
#define IDBUTTON_PRINT 0x04

enum IDDEVICE_TYPE {
	IDDEVTYPE_JOYPAD,
	IDDEVTYPE_KEYBOARD,
	IDDEVTYPE_FORCEDWORD = 0xFFFFFFFF
};

typedef struct STRUCT_INPUTDATA {
	DWORD dwMovement;
	DWORD dwKeys;
} INPUTDATA, *LPINPUTDATA;

class CInputDevice {
public:
	CInputDevice ( void );
	~CInputDevice ( void );
	void Dispose ( void );
	inline int SetInitialData ( const LPINPUTDATA id );
	int CreateDevice ( IDDEVICE_TYPE iddt, HWND hDlg );
	inline int GetInput ( void );
	
	LPDIRECTINPUT8 DI;
	LPDIRECTINPUTDEVICE8 Device;
	
	INPUTDATA InputData;
private:
	inline void m_Reset ( void );
	inline void m_Dispose ( void );
	
	static BOOL CALLBACK m_EnumDevicesCallback ( const DIDEVICEINSTANCE* pdidInstance, void* pContext );
	static BOOL CALLBACK m_EnumObjectsCallback ( const DIDEVICEOBJECTINSTANCE* pdidoi, void* pContext );
	
	int m_GetKeyboardInput ( void );
	int m_GetJoypadInput ( void );
	
	int (CInputDevice::*m_GetInput)(void);
	
	BOOL m_bActive; //Indica se l'oggetto ha inizializzato tutto con successo, e quindi se pu� essere usato
	int m_nPOVCount;
};



//********************************************************************************
//Chiama la funzione puntata da m_GetInput() e ne restituisce il valore di
//ritorno, oppure restituisce NONZERO se l'oggetto non � stato correttamente
//inizializzato.
//********************************************************************************
inline int CInputDevice::GetInput () {
	if (m_bActive)
		return (this->*m_GetInput)();
	else
		return NONZERO;
}



//********************************************************************************
//Questa funzione va chiamata prima di GetInput(). La funzione azzera il
//contenuto della variabile membro InputData, oppure copia in InputData il
//contenuto della struttura specificata, se id <> NULL. Restituisce ZERO.
//********************************************************************************
inline int CInputDevice::SetInitialData (const LPINPUTDATA id) {
	if (id) {
		InputData = *id;
	}
	else {
		//ZeroMemory (&InputData, sizeof(INPUTDATA));
		InputData.dwMovement = InputData.dwKeys = 0;
	}
	return ZERO;
}


#endif