//Main.cpp


//Costanti
#define INITGUID
#define DEF_CLASS_NAME "IRO2_DEMO_WIN"
#define DEF_WIN_TITLE "iRO2 demo"
#define DIRECTINPUT_VERSION 0x0800

#define USE_TWEENING_CLASS

//Headers
#include "constants.h"
#include <windows.h>
#include <d3d9.h>
#include <dinput.h>
#include <d3dx9.h>
#include <stdio.h>
#ifdef WIN32_LEAN_AND_MEAN
#include <mmsystem.h>
#endif
#include "resource.h"

#include "CSpeech.h"
#include "CBitmapSarge.h"
#include "CMathEngine.h"
#include "Game.h"

//Prototipi
int WINAPI WinMain ( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow );
LRESULT CALLBACK MsgProc ( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam );
HRESULT InitD3D ( HWND hWnd );
HRESULT InitTextures ( void );
HRESULT InitGeometry ( void );
HRESULT InitLights ( void );
void EndIt ( void );
int Render ( void );
int ShowPicture ( const char* strFileName );
float GetFPS ( float fDelta );


//Variabili globali
LPDIRECT3D9 g_d3d = NULL;
LPDIRECT3DDEVICE9 g_dev = NULL;
D3DCAPS9 g_caps;
DWORD g_dwAnisotropy;
DWORD g_dwMinSimpleFilter;
DWORD g_dwMagSimpleFilter;
DWORD g_dwMagFilter;
DWORD g_dwMinFilter;
char g_strFPS[32];
CSpeech g_spcFPS;

extern int (*GameFunction)(float);
extern bool g_bShowFPS;
extern int (*GameClean)(void);
extern bool g_bRunning;

//Altre costanti
#ifndef WM_MOUSEWHEEL
#define WM_MOUSEWHEEL                   0x020A
#endif
#ifndef WHEEL_DELTA
#define WHEEL_DELTA 120
#endif
#ifndef GET_WHEEL_DELTA_WPARAM
#define GET_WHEEL_DELTA_WPARAM(wParam)  ((short)HIWORD(wParam))
#endif

//********************************************************************************
//WinMain
//********************************************************************************
int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
#pragma unused (hPrevInstance)
#pragma unused (lpCmdLine)
	WNDCLASSEX wnd;
	HWND hWnd;
	RECT rClient;
	char* strMess;
	char* strTitle;
	MSG msg;
	float t1, t2;
	bool bShowErr = false;
	BOOL bSuccess;
	HRESULT hrD3D = S_OK, hrGeometry = S_OK, hrDInput = S_OK,
			hrTexture = S_OK, hrLight = S_OK;
	
	strMess = new char[DEF_STR_LEN];
	strTitle = new char[32];
	if ( (strMess == NULL ) || (strTitle == NULL) )
		return 0;
	LoadString (hInstance, IDS_ERR_TITLE, strTitle, 32); 
	
	wnd.cbClsExtra = 0;
	wnd.cbSize = sizeof(WNDCLASSEX);
	wnd.cbWndExtra = 0;
	wnd.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wnd.hCursor = LoadCursor (NULL, IDC_ARROW);
	wnd.hIcon = LoadIcon (hInstance, MAKEINTRESOURCE(IDI_MAIN_ICON));
	wnd.hIconSm = LoadIcon (hInstance, MAKEINTRESOURCE(IDI_MAIN_ICON));
	wnd.hInstance = hInstance;
	wnd.lpfnWndProc = MsgProc;
	wnd.lpszClassName = DEF_CLASS_NAME;
	wnd.lpszMenuName = NULL;
	wnd.style = CS_VREDRAW | CS_HREDRAW | CS_DBLCLKS;
	
	RegisterClassEx (&wnd);
	hWnd = CreateWindowEx (0, DEF_CLASS_NAME, DEF_WIN_TITLE,
		WS_OVERLAPPEDWINDOW, 0, 0, 640, 480, NULL, NULL, wnd.hInstance, NULL);
	
	if (hWnd == NULL) {
		LoadString (hInstance, IDS_ERR_HWND, strMess, DEF_STR_LEN-1);
		MessageBox (NULL, strMess, strTitle, MB_OK | MB_ICONSTOP);
	}
	else {
		GetClientRect (hWnd, &rClient);
		const int nWidth = AREA_WIDTH + 640 - (rClient.right - rClient.left);
		const int nHeight = AREA_HEIGHT + 480 - (rClient.bottom - rClient.top);
		
		SetWindowPos (hWnd, NULL, 0, 0, nWidth, nHeight, SWP_NOMOVE | SWP_NOACTIVATE);
		ShowWindow (hWnd, nCmdShow);
		UpdateWindow (hWnd);
		
		bSuccess = true;
		if (bSuccess)
			bSuccess = SUCCEEDED(hrD3D = InitD3D(hWnd));
		if (bSuccess)
			bSuccess = SUCCEEDED(hrGeometry = InitGeometry());
		if (bSuccess)
			bSuccess = SUCCEEDED(hrTexture = InitTextures());
		if (bSuccess)
			bSuccess = SUCCEEDED(hrDInput = (GameInit (hWnd) ? E_FAIL : S_OK));
		if (bSuccess)
			bSuccess = SUCCEEDED(hrLight = InitLights ());
		if (bSuccess)
			bSuccess = (SelectGameFunction (GF_INGAME) ? FALSE : TRUE);
		
		ShowPicture ("Loading400.bmp");
		
		//Variabili varie
		g_dwAnisotropy = MIN(g_caps.MaxAnisotropy, 8);
		g_dwMinFilter = (g_caps.TextureFilterCaps & D3DPTFILTERCAPS_MINFANISOTROPIC ? D3DTEXF_ANISOTROPIC : D3DTEXF_POINT);
		g_dwMagFilter = (g_caps.TextureFilterCaps & D3DPTFILTERCAPS_MAGFANISOTROPIC ? D3DTEXF_ANISOTROPIC : D3DTEXF_POINT);
		g_dwMagSimpleFilter = (g_caps.TextureFilterCaps & D3DPTFILTERCAPS_MAGFLINEAR ? D3DTEXF_LINEAR : D3DTEXF_POINT);
		g_dwMinSimpleFilter = (g_caps.TextureFilterCaps & D3DPTFILTERCAPS_MINFLINEAR ? D3DTEXF_LINEAR : D3DTEXF_POINT);
		g_dev->SetSamplerState (0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);
		g_dev->SetSamplerState (0, D3DSAMP_MAXANISOTROPY, g_dwAnisotropy);
		
		//Inizializzazione oggetti
		g_spcFPS.LoadCharset (g_dev, "Charset01.dlg", "Charset01.bmp");
		g_spcFPS.SetReferenceArea (800.0f, 600.0f);
		g_spcFPS.SetDialoguePoint (5.0f, 3.0f);
		

		POINT p = {AREA_WIDTH >> 1, AREA_HEIGHT >> 1};
		ClientToScreen (hWnd, &p);
		SetCursorPos (p.x, p.y);
		
		if (bSuccess) {
			//g_keyboard->Acquire();
			t1 = 0.0f;
			t2 = timeGetTime() / 1000.0f;
			ZeroMemory (&msg, sizeof(MSG));
			while ( true /*msg.message != WM_QUIT*/ ) {
				if (!g_bRunning)
					PostQuitMessage (0);
				
				if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE)) {
					if (msg.message == WM_QUIT)
						break;
					TranslateMessage (&msg);
					DispatchMessage (&msg);
				}
				else {
					t1 = t2;
					t2 = (float)timeGetTime () / 1000.0f;
					
					if (g_bShowFPS) {
						sprintf (g_strFPS, "%g fps", GetFPS (t2 - t1));
						g_spcFPS.SetUntranslatedString (g_dev, g_strFPS);
						g_spcFPS.CompleteAnimation();
					}
					
					if ((*GameFunction)(t2 - t1)) { /*Render()*/
						bShowErr = true;
						LoadString (hInstance, IDS_ERR_RENDERING, strMess, DEF_STR_LEN-1);
						break;
					}
				}//EndIf
			}//Wend
		} //if (bSuccess)
		else {
			if (FAILED(hrD3D))
				LoadString (hInstance, IDS_ERR_D3DINIT, strMess, DEF_STR_LEN-1);
			else if (FAILED(hrGeometry))
				LoadString (hInstance, IDS_ERR_GEOMETRY, strMess, DEF_STR_LEN-1);
			else if (FAILED(hrDInput))
				LoadString (hInstance, IDS_ERR_DINPUT, strMess, DEF_STR_LEN-1);
			else if (FAILED(hrTexture))
				LoadString (hInstance, IDS_ERR_TEXTURE, strMess, DEF_STR_LEN-1);
			else if (FAILED(hrLight))
				LoadString (hInstance, IDS_ERR_LIGHT, strMess, DEF_STR_LEN-1);
			else
				LoadString (hInstance, IDS_ERR_GENERIC, strMess, DEF_STR_LEN-1);
			PostQuitMessage (0);
			bShowErr = true;
		}//if (bSuccess)
	}//if (hWnd == NULL)
	
	if (GameClean)
		(*GameClean)();
	GameEnd ();
	EndIt ();
	if (bShowErr) {
		MessageBox (NULL, strMess, strTitle, MB_OK | MB_ICONSTOP);
	}
	
	//Pulisco e ciao
	if (strMess != NULL) {
		delete[] strMess;
		strMess = NULL;
	}
	if (strTitle != NULL) {
		delete[] strTitle;
		strTitle = NULL;
	}
	UnregisterClass (DEF_CLASS_NAME, wnd.hInstance);
	return msg.wParam;
}



//********************************************************************************
//Event handler
//********************************************************************************
LRESULT CALLBACK MsgProc (HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	switch (uMsg) {
	case WM_DESTROY:
		PostQuitMessage(0);
		return wParam;
	
#ifndef _DEBUG
	case WM_SETCURSOR:
		SetCursor (NULL);
		return TRUE;
#endif
	}
	return DefWindowProc (hwnd, uMsg, wParam, lParam);
}



//********************************************************************************
//Inizializza Direct3D. Restituisce S_OK se va tuttto bene, altrimenti E_FAIL.
//********************************************************************************
HRESULT InitD3D (HWND hWnd) {
	D3DPRESENT_PARAMETERS pparams;
	
	//D3D9
	g_d3d = Direct3DCreate9 (D3D_SDK_VERSION);
	if (g_d3d == NULL)
		return E_FAIL;
	
	//Caps
	ZeroMemory (&g_caps, sizeof(D3DCAPS9));
	if (FAILED(g_d3d->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &g_caps)))
		return E_FAIL;
	
	//Device
	ZeroMemory (&pparams, sizeof(D3DPRESENT_PARAMETERS));
	pparams.AutoDepthStencilFormat = D3DFMT_D24S8;
	pparams.EnableAutoDepthStencil = TRUE;
	pparams.BackBufferWidth = AREA_WIDTH;
	pparams.BackBufferHeight = AREA_HEIGHT;
#ifdef FULL_SCREEN
	pparams.BackBufferFormat = D3DFMT_A8R8G8B8;
#else
	pparams.BackBufferFormat = D3DFMT_UNKNOWN;
	pparams.Windowed = TRUE;
#endif
	pparams.hDeviceWindow = hWnd;
	pparams.SwapEffect = D3DSWAPEFFECT_DISCARD;
	
	if (FAILED(g_d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
		D3DCREATE_SOFTWARE_VERTEXPROCESSING,
		&pparams, &g_dev)))
		
		return E_FAIL;
	
	g_dev->SetRenderState (D3DRS_ZENABLE, TRUE);
	return S_OK;
}



//********************************************************************************
//Crea la grafica del gioco. Restituisce S_OK se va tutto bene, altrimenti
//E_FAIL.
//********************************************************************************
HRESULT InitGeometry () {
	return S_OK;
}



//********************************************************************************
//Distrugge gli oggetti creati
//********************************************************************************
void EndIt () {
	SAFE_RELEASE (g_dev);
	SAFE_RELEASE (g_d3d);
}



//********************************************************************************
//Disegna su schermo la scena corrente. Restituisce ZERO se � andato tutto
//bene, altrimenti NONZERO.
//********************************************************************************
int Render () {
	g_dev->Clear (0, NULL, D3DCLEAR_ZBUFFER|D3DCLEAR_TARGET, D3DCOLOR_XRGB(10, 36, 106), 1.0f, 0);
	
	if (SUCCEEDED(g_dev->BeginScene ())) {
		g_spcFPS.Render (g_dev);
	
		g_dev->EndScene ();
	}
	
	g_dev->Present (NULL, NULL, NULL, NULL);
	return ZERO;
}



//********************************************************************************
//Carica le texture. Restituisce S_OK se va tutto bene, altrimenti E_FAIL.
//********************************************************************************
HRESULT InitTextures () {
	return S_OK;
}



//********************************************************************************
//Inizializza le luci della scena. Restituisce S_OK se va tutto bene,
//altrimenti E_FAIL.
//********************************************************************************
HRESULT InitLights () {
	D3DLIGHT9 light;
	MATH_3DVECTOR vDirection;
	CMathEngine math;

	D3DMATERIAL9 mtrl;
	ZeroMemory (&mtrl, sizeof(D3DMATERIAL9));
	mtrl.Diffuse.r = mtrl.Ambient.r = 1.0f;
	mtrl.Diffuse.g = mtrl.Ambient.g = 1.0f;
	mtrl.Diffuse.b = mtrl.Ambient.b = 1.0f;
	mtrl.Diffuse.a = mtrl.Ambient.a = 1.0f;
//	g_dev->SetMaterial( &mtrl );

	MATH_SET3DVECTOR (vDirection, -1.0f, -1.0f, 1.0f);
	math.Vec3Normalize (&vDirection);
//	MATH_SET3DVECTOR (vDirection, 0.0f, 2.0f, 0.0f);
	ZeroMemory (&light, sizeof(D3DLIGHT9));
	
	light.Type = D3DLIGHT_DIRECTIONAL;
	light.Diffuse.r = 1.0f;
	light.Diffuse.g = 1.0f;
	light.Diffuse.b = 1.0f;
	CopyMemory (&light.Direction, &vDirection, sizeof(MATH_3DVECTOR));
//	CopyMemory (&light.Position, &vDirection, sizeof(MATH_3DVECTOR));
	light.Specular = light.Diffuse;
	light.Attenuation0 = 1.0f;
	light.Range = 1000.0f; //sqrtf(FLT_MAX);
	
	g_dev->SetLight (0, &light);
	g_dev->LightEnable (0, TRUE);
	g_dev->SetRenderState (D3DRS_LIGHTING, TRUE);
	g_dev->SetRenderState (D3DRS_AMBIENT, 0x00202020);
	return S_OK;
}



//********************************************************************************
//Carica l'immagine specificata e la mostra a schermo intero sul device g_dev.
//Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int ShowPicture (const char* strFileName) {
	if ( (g_dev && strFileName) == FALSE )
		return NONZERO;
	
	CBitmapSarge bmp;
	LPDIRECT3DSURFACE9 surf = NULL;
	LPDIRECT3DSURFACE9 surfTmp = NULL;
	D3DLOCKED_RECT lrInfo;
	char* src = NULL, *dst;
	int nRet = NONZERO;
	D3DSURFACE_DESC desc;
	
	if (bmp.LoadPicture (strFileName))
		goto Error;
	
	bmp.ConvertTo32bit();
	bmp.ScaleImage (AREA_WIDTH, AREA_HEIGHT, BSSM_BLUR4X4);
	bmp.RGBAtoARGB ();
	
	if (FAILED(g_dev->GetBackBuffer (0, 0, D3DBACKBUFFER_TYPE_MONO, &surf)))
		goto Error;
	ZeroMemory (&desc, sizeof(D3DSURFACE_DESC));
	surf->GetDesc (&desc);
	
	//Se il formato dello schermo non � X8R8G8B8 o A8R8G8B8, la surface non la posso disegnare
	if (desc.Format != D3DFMT_A8R8G8B8 && desc.Format != D3DFMT_X8R8G8B8)
		goto Error;
	
	if (FAILED(g_dev->CreateOffscreenPlainSurface (AREA_WIDTH, AREA_HEIGHT, desc.Format, D3DPOOL_SYSTEMMEM, &surfTmp, NULL)))
		goto Error;
	
	ZeroMemory (&lrInfo, sizeof(D3DLOCKED_RECT));
	if (SUCCEEDED(surfTmp->LockRect(&lrInfo, NULL, D3DLOCK_DISCARD))) {
		bmp.GetDataPtr ((void**)&src);
		dst = (char*)lrInfo.pBits;
		const int nMinMemPitch = MIN(lrInfo.Pitch, bmp.GetPitch());
		
		for (int y = 0; y < bmp.GetPictureHeight(); y++) {
			CopyMemory (dst, src, nMinMemPitch);
			dst += lrInfo.Pitch;
			src += bmp.GetPitch();
		}
		
		surfTmp->UnlockRect ();
		
		if (FAILED(g_dev->UpdateSurface (surfTmp, NULL, surf, NULL)))
			goto Error;
		
		g_dev->Present (NULL, NULL, NULL, NULL);
	}
	else {
		goto Error;
	}
	
	nRet = ZERO;
Error:
	SAFE_RELEASE (surfTmp);
	SAFE_RELEASE (surf);
	return nRet;
}



//********************************************************************************
//Restituisce in numero di fotogrammi al secondo.
//********************************************************************************
float GetFPS (float fDelta) {
	static float fTime = 0.0f;
	static float fCount = 0.0f;
	static float fPrevCount = 0.0f;
	
	fTime += fDelta;
	if (fTime >= 1.0f) {
		fPrevCount = fCount / fTime;
		fCount = 0.0f;
		fTime = 0.0f;
	}
	else
		fCount += 1.0f;
	
	return fPrevCount;
}