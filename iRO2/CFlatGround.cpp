//CFlatGround.cpp


#include "constants.h"
#include <windows.h>
#include "CFlatGround.h"



//********************************************************************************
//Costruttore standard
//********************************************************************************
CFlatGround::CFlatGround () {
	m_Reset ();
}



//********************************************************************************
//Distruttore standard
//********************************************************************************
CFlatGround::~CFlatGround () {
	m_Dispose ();
}



//********************************************************************************
//Imposta tutte le proprietÓ sui valori iniziali.
//********************************************************************************
inline void CFlatGround::m_Reset () {
	m_fGravityAccel = GRAVITYACCEL;
	fGroundLevel = 0.0f;
	m_vb = NULL;
	m_ib = NULL;
	
	bAutoFreeTexture = true;
	texGround = NULL;
	texReflection = NULL;
	MATH_INITPLANE (plnEquation);
}



//********************************************************************************
//Chiama m_Dispose()
//********************************************************************************
void CFlatGround::Dispose () {
	m_Dispose ();
	m_Reset ();
}



//********************************************************************************
//Libera la memoria occupata e distrugge gli oggetti creati.
//********************************************************************************
inline void CFlatGround::m_Dispose () {
	SAFE_RELEASE (m_ib);
	SAFE_RELEASE (m_vb);
	
	if (bAutoFreeTexture) {
		SAFE_RELEASE (texGround);
	}
}



//********************************************************************************
//Costruisce un quadrato di dimensioni (fcx;fcz), ad altezza fy. Eventuali
//quadrati precedentemente creati vengono persi. Restituisce ZERO se va
//tutto bene, altrimenti NONZERO.
//********************************************************************************
int CFlatGround::BuildGround (LPDIRECT3DDEVICE9 dev, float fcx, float fcz, float fy, float fu, float fv) {
	if (dev == NULL)
		return NONZERO;
	
	void* vert = NULL;
	void* ind = NULL;
	const UINT uVertSize = sizeof(FLATGROUND_VERTEX) << 2;
	const UINT uIndSize = sizeof(WORD) << 2;
	MATH_2DVECTOR vTexSize;
	LPDIRECT3DSURFACE9 surf = NULL;
	D3DSURFACE_DESC desc;
	CMathEngine math;
	
	SAFE_RELEASE (m_ib);
	SAFE_RELEASE (m_vb);
	
	if (FAILED(dev->CreateVertexBuffer (uVertSize, D3DUSAGE_WRITEONLY, D3DFVF_FLATGROUND, D3DPOOL_MANAGED, &m_vb, NULL)))
		goto Error;
	if (FAILED(m_vb->Lock (0, uVertSize, &vert, 0)))
		goto Error;
	
	if (FAILED(dev->CreateIndexBuffer (uIndSize, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_ib, NULL)))
		goto Error;
	if (FAILED(m_ib->Lock (0, uIndSize, &ind, 0)))
		goto Error;
	
	MATH_SET2DVECTOR (vTexSize, 256.0f, 256.0f);
	if (texGround != NULL) {
		if (SUCCEEDED(texGround->GetSurfaceLevel (0, &surf))) {
			ZeroMemory (&desc, sizeof(D3DSURFACE_DESC));
			surf->GetDesc (&desc);
			MATH_SET2DVECTOR (vTexSize, (float)desc.Width, (float)desc.Height);
			SAFE_RELEASE (surf);
		}
	}
	
	m_MakeQuad (fcx, fcz, fy, fu, fv, &vTexSize, 0xFFFFFFFF, (LPFLATGROUND_VERTEX)vert, (LPWORD)ind);
	
	m_ib->Unlock ();
	m_vb->Unlock ();
	
	//Equazione del piano
	MATH_3DVECTOR vecNormal, vecPoint;
	MATH_SET3DVECTOR (vecNormal, 0.0f, 1.0f, 0.0f);
	MATH_SET3DVECTOR (vecPoint, fcx, fy, fcz);
	math.PlaneBuildEquation (&plnEquation, &vecNormal, &vecPoint);
	
	//Variabili
	fGroundLevel = fy;
	
	return ZERO;
Error:
	if (ind)
		m_ib->Unlock ();
	if (vert)
		m_vb->Unlock ();
	
	SAFE_RELEASE (m_ib);
	SAFE_RELEASE (m_vb);
	return NONZERO;
}



//********************************************************************************
//Renderizza il quadrato creato, applicando, se presente, la texture texGround.
//Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CFlatGround::Render (LPDIRECT3DDEVICE9 dev) {
	if ( (dev && m_ib && m_vb) == FALSE )
		return NONZERO;
	
	dev->SetStreamSource (0, m_vb, 0, sizeof(FLATGROUND_VERTEX));
	dev->SetIndices (m_ib);
	dev->SetTexture (0, texGround);
	dev->SetTexture (1, texReflection);
	dev->SetFVF (D3DFVF_FLATGROUND);

	if (texGround) {
		dev->SetTextureStageState (0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		dev->SetTextureStageState (0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
		dev->SetTextureStageState (0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
		dev->SetTextureStageState (0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
		
		if (texReflection) {
			dev->SetTextureStageState (1, D3DTSS_COLORARG1, D3DTA_CURRENT);
			dev->SetTextureStageState (1, D3DTSS_COLORARG2, D3DTA_TEXTURE);
			dev->SetTextureStageState (1, D3DTSS_COLOROP, D3DTOP_BLENDFACTORALPHA);
			dev->SetTextureStageState (1, D3DTSS_ALPHAARG1, D3DTA_CURRENT);
			dev->SetTextureStageState (1, D3DTSS_ALPHAARG2, D3DTA_TEXTURE);
			dev->SetTextureStageState (1, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
			dev->SetRenderState (D3DRS_TEXTUREFACTOR, D3DCOLOR_ARGB (120, 0xFF, 0xFF, 0xFF));
		}
		else {
			dev->SetTextureStageState (1, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
			dev->SetTextureStageState (1, D3DTSS_COLOROP, D3DTOP_DISABLE);
		}
	}
	else {
		dev->SetTextureStageState (0, D3DTSS_COLORARG1, D3DTA_DIFFUSE);
		dev->SetTextureStageState (0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
		dev->SetTextureStageState (0, D3DTSS_ALPHAARG1, D3DTA_DIFFUSE);
		dev->SetTextureStageState (0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	}
	
	dev->DrawIndexedPrimitive (D3DPT_TRIANGLEFAN, 0, 0, 4, 0, 2);
	
	dev->SetTextureStageState (1, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
	dev->SetTextureStageState (1, D3DTSS_COLOROP, D3DTOP_DISABLE);
	dev->SetStreamSource (0, NULL, 0, 0);
	dev->SetIndices (NULL);
	dev->SetTexture (0, NULL);
	
	return ZERO;
}



//********************************************************************************
//Riempie pfgvDst e pwDst rispettivamente con i vertici e gli indici per
//il piano. Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CFlatGround::m_MakeQuad (float fcx, float fcz, float fy, float fu, float fv, const LPMATH_2DVECTOR pvTexSize, D3DCOLOR d3dclrColor, LPFLATGROUND_VERTEX pfgvDst, LPWORD pwDst) {
	if (!(pfgvDst && pwDst && pvTexSize))
		return NONZERO;
	if (fcx < 1.0f || fcz < 1.0f)
		return NONZERO;
	
	const float fHalfWidth = fcx / 2.0f;
	const float fHalfDepth = fcz / 2.0f;
	const float fTexU = (fu == 0.0f ? 256.0f * fcx / pvTexSize->x : fu);
	const float fTexV = (fv == 0.0f ? 256.0f * fcz / pvTexSize->y : fv);
	MATH_3DVECTOR vNorm;
	
	MATH_SET3DVECTOR (vNorm, 0.0f, 1.0f, 0.0f);
	
	pfgvDst[0].color = d3dclrColor;
	pfgvDst[0].tu = 0.0f;
	pfgvDst[0].tv = fTexV;
	pfgvDst[0].tu2 = 0.0f;
	pfgvDst[0].tv2 = 1.0f;
	pfgvDst[0].vNormal = vNorm;
	MATH_SET3DVECTOR (pfgvDst[0].vPos, -fHalfWidth, fy, -fHalfDepth);
	//-
	pfgvDst[1].color = d3dclrColor;
	pfgvDst[1].tu = fTexU;
	pfgvDst[1].tv = fTexV;
	pfgvDst[1].tu2 = 1.0f;
	pfgvDst[1].tv2 = 1.0f;
	pfgvDst[1].vNormal = vNorm;
	MATH_SET3DVECTOR (pfgvDst[1].vPos, fHalfWidth, fy, -fHalfDepth);
	//-
	pfgvDst[2].color = d3dclrColor;
	pfgvDst[2].tu = fTexU;
	pfgvDst[2].tv = 0.0f;
	pfgvDst[2].tu2 = 1.0f;
	pfgvDst[2].tv2 = 0.0f;
	pfgvDst[2].vNormal = vNorm;
	MATH_SET3DVECTOR (pfgvDst[2].vPos, fHalfWidth, fy, fHalfDepth);
	//-
	pfgvDst[3].color = d3dclrColor;
	pfgvDst[3].tu = 0.0f;
	pfgvDst[3].tv = 0.0f;
	pfgvDst[3].tu2 = 0.0f;
	pfgvDst[3].tv2 = 0.0f;
	pfgvDst[3].vNormal = vNorm;
	MATH_SET3DVECTOR (pfgvDst[3].vPos, -fHalfWidth, fy, fHalfDepth);
	
	pwDst[0] = 0;
	pwDst[1] = 3;
	pwDst[2] = 2;
	pwDst[3] = 1;
//	pwDst[4] = 0;
//	pwDst[5] = 2;
	
	return ZERO;
}