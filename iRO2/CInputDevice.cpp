//CInputDevice.cpp


#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include "CInputDevice.h"


//Costanti per il joypad
#define PSXCTRL_POSITIVETHROTTLE +1000
#define PSXCTRL_NEGATIVETHROTTLE -1000
#define PSXCTRL_POSITIVEDEADZONE  230
#define PSXCTRL_NEGATIVEDEADZONE -230
#define PSXCTRL_POSITIVEHIGHZONE  909
#define PSXCTRL_NEGATIVEHIGHZONE -909

//Tasti del joypad
#define PSXCTRL_EXIT 8
#define PSXCTRL_RUN 2
#define PSXCTRL_JUMP 1

//Tasti della tastiera
#define KEYB_EXIT DIK_ESCAPE
#define KEYB_PRINT /*DIK_S*/DIK_SYSRQ
#define KEYB_FORWARD DIK_UPARROW
#define KEYB_BACKWARD DIK_DOWNARROW
#define KEYB_LEFT DIK_LEFTARROW
#define KEYB_RIGHT DIK_RIGHTARROW
#define KEYB_RUN DIK_RCONTROL
#define KEYB_JUMP DIK_RSHIFT

//Puntatore this globale
CInputDevice* g_this = NULL;
int g_nSliderCount;
int g_nPOVCount;


//********************************************************************************
//Costruttore standard
//********************************************************************************
CInputDevice::CInputDevice () {
	m_Reset ();
}



//********************************************************************************
//Distruttore standard
//********************************************************************************
CInputDevice::~CInputDevice () {
	m_Dispose ();
}



//********************************************************************************
//Imposta tutte le propriet� sui valori iniziali.
//********************************************************************************
inline void CInputDevice::m_Reset () {
	DI = NULL;
	Device = NULL;
	m_bActive = FALSE;
}



//********************************************************************************
//Chiama m_Dispose()
//********************************************************************************
void CInputDevice::Dispose () {
	m_Dispose ();
	m_Reset ();
}



//********************************************************************************
//Libera la memoria occupata e distrugge gli oggetti creati.
//********************************************************************************
inline void CInputDevice::m_Dispose () {
	if (Device)
		Device->Unacquire ();
	SAFE_RELEASE (Device);
}



//********************************************************************************
//Called once for each enumerated Joystick. If we find one, create a
//device interface on it so we can play with it.
//********************************************************************************
BOOL CALLBACK CInputDevice::m_EnumDevicesCallback (const DIDEVICEINSTANCE* pdidInstance, void* pContext) {
    HRESULT hr;

    //Obtain an interface to the enumerated Joystick.
    hr = g_this->DI->CreateDevice (pdidInstance->guidInstance, &g_this->Device, NULL);

    //If it failed, then we can't use this Joystick. (Maybe the user unplugged
    //it while we were in the middle of enumerating it.)
    if (FAILED(hr)) 
        return DIENUM_CONTINUE;

    //Stop enumeration. Note: we're just taking the first Joystick we get. You
    //could store all the enumerated Joysticks and let the user pick.
	return DIENUM_STOP; //DIENUM_CONTINUE;
}



//********************************************************************************
//Callback function for enumerating objects (axes, buttons, POVs) on a 
//Joystick. This function enables user interface elements for objects
//that are found to exist, and scales axes min/max values.
//********************************************************************************
BOOL CALLBACK CInputDevice::m_EnumObjectsCallback (const DIDEVICEOBJECTINSTANCE* pdidoi, VOID* pContext) {
    //static int nSliderCount = 0;  //Number of returned slider controls
    //static int nPOVCount = 0;     //Number of returned POV controls

    //For axes that are returned, set the DIPROP_RANGE property for the
    //enumerated axis in order to scale min/max values.
    if (pdidoi->dwType & DIDFT_AXIS) {
        DIPROPRANGE diprg; 
        diprg.diph.dwSize = sizeof(DIPROPRANGE); 
        diprg.diph.dwHeaderSize = sizeof(DIPROPHEADER); 
        diprg.diph.dwHow = DIPH_BYID; 
        diprg.diph.dwObj = pdidoi->dwType; //Specify the enumerated axis
        diprg.lMin = PSXCTRL_NEGATIVETHROTTLE; 
        diprg.lMax = PSXCTRL_POSITIVETHROTTLE; 
    
        //Set the range for the axis
        if (FAILED (g_this->Device->SetProperty (DIPROP_RANGE, &diprg.diph))) {
            return DIENUM_STOP;
		}
    }

/*
    //Set the UI to reflect what objects the Joystick supports
	if (pdidoi->guidType == GUID_XAxis) {
	}
	if (pdidoi->guidType == GUID_YAxis) {
	}
	if (pdidoi->guidType == GUID_ZAxis) {
	}
	if (pdidoi->guidType == GUID_RxAxis) {
	}
	if (pdidoi->guidType == GUID_RyAxis) {
	}
	if (pdidoi->guidType == GUID_RzAxis) {
	}
	if (pdidoi->guidType == GUID_Slider) {
		switch (g_nSliderCount++) {
		case 0:
			break;

		case 1:
			break;
		}
	}
	if (pdidoi->guidType == GUID_POV) {
		switch (g_nPOVCount++) {
		case 0 :
			break;

		case 1 :
			break;

		case 2 :
			break;

		case 3 :
			break;
		}
	}
*/
	if (pdidoi->guidType == GUID_POV)
		g_nPOVCount++;
	
	return DIENUM_CONTINUE;
}



//********************************************************************************
//Inizializza l'oggetto in modo che la variabile membro Device sia un
//dispositivo del tipo indicato in iddt. Restituisce ZERO se va tutto bene,
//altrimenti NONZERO. La funzione si occupa anche di impostare i puntatori
//alle funzioni interne relative al tipo di dispositivo creato.
//********************************************************************************
int CInputDevice::CreateDevice (IDDEVICE_TYPE iddt, HWND hDlg) {
	if (DI == NULL)
		return NONZERO;
	
	LPDIRECTINPUT8 diTmp = DI; //Salvo una copia dell'indirizzo di DI perch� sto per resettare
	DWORD dwDevClass;
	
	m_Dispose ();
	m_Reset ();
	
	DI = diTmp;
	diTmp = NULL;
	
	g_nSliderCount = g_nPOVCount = 0;
	g_this = this;
	
	switch (iddt) {
	case IDDEVTYPE_JOYPAD:
		dwDevClass = DI8DEVCLASS_GAMECTRL;
		m_GetInput = &m_GetJoypadInput;
		break;
	case IDDEVTYPE_KEYBOARD:
		dwDevClass = DI8DEVCLASS_KEYBOARD;
		m_GetInput = &m_GetKeyboardInput;
		break;
	default:
		return NONZERO;
	}

	//La funzione di callback si preoccupa di creare l'oggetto Device
	if (FAILED (DI->EnumDevices (dwDevClass, &m_EnumDevicesCallback, NULL, DIEDFL_ATTACHEDONLY))) {
		return NONZERO;
	}
	if (Device == NULL)
		return NONZERO;
	
	if (FAILED (Device->SetCooperativeLevel (hDlg, DISCL_EXCLUSIVE | DISCL_FOREGROUND))) {
		SAFE_RELEASE (Device);
		return NONZERO;
	}
	
	//Se il device � un joypad
	if (iddt == IDDEVTYPE_JOYPAD) {
		if (FAILED (Device->SetDataFormat (&c_dfDIJoystick)))
			return NONZERO;
		
		if (FAILED (Device->EnumObjects (m_EnumObjectsCallback, NULL, DIDFT_AXIS | DIDFT_POV))) {
			SAFE_RELEASE (Device);
			return NONZERO;
		}
		m_nPOVCount = g_nPOVCount;
	}
	else if (iddt == IDDEVTYPE_KEYBOARD) {
		if (FAILED (Device->SetDataFormat (&c_dfDIKeyboard))) {
			SAFE_RELEASE (Device);
			return NONZERO;
		}
		m_nPOVCount = 0;
	}
	
	m_bActive = TRUE;
	return ZERO;
}



//********************************************************************************
//Ottiene l'input dal joypad e restituisce il risultato nella variabile membro
//InputData. Restituisce ZERO se l'input � stato letto e analizzato con
//successo, altrimenti NONZERO.
//********************************************************************************
int CInputDevice::m_GetKeyboardInput () {
    if (m_bActive == FALSE) 
        return NONZERO;
	
	BYTE ks[256];
	
	//Poll the device to read the current state
    if (FAILED (Device->Poll())) {
		HRESULT hr;

		//DInput is telling us that the input stream has been
		//interrupted. We aren't tracking any state between polls, so
		//we don't have any special reset that needs to be done. We
		//just re-acquire and try again.
		hr = Device->Acquire();
		while (hr == DIERR_INPUTLOST) 
			hr = Device->Acquire();
		
		//hr may be DIERR_OTHERAPPHASPRIO or other errors. This
		//may occur when the app is minimized or in the process of 
		//switching, so just try again later 
		return ZERO; 
	}
	
	//Get the input's device state
	if (FAILED (Device->GetDeviceState (256, (void*)ks)))
		return NONZERO;
	
	//Movimento
	//Tasti direzionali
	if (ks[KEYB_FORWARD] & 128)
		InputData.dwMovement |= IDMOVE_FORWARD;
	else if (ks[KEYB_BACKWARD] & 128)
		InputData.dwMovement |= IDMOVE_BACKWARD;
	if (ks[KEYB_LEFT] & 128)
		InputData.dwMovement |= IDMOVE_LEFT;
	else if (ks[KEYB_RIGHT] & 128)
		InputData.dwMovement |= IDMOVE_RIGHT;
	
	//Altri tasti
	if (ks[KEYB_EXIT] & 128)
		InputData.dwKeys |= IDBUTTON_EXIT;
	if (ks[KEYB_RUN] & 128)
		InputData.dwMovement |= IDMOVE_RUNNING;
	if (ks[KEYB_JUMP] & 128)
		InputData.dwKeys |= IDBUTTON_JUMP;
	if (ks[KEYB_PRINT] & 128)
		InputData.dwKeys |= IDBUTTON_PRINT;
	return ZERO;
}



//********************************************************************************
//Ottiene l'input dalla tastiera e restituisce il risultato nella variabile
//membro InputData. Restituisce ZERO se l'input � stato letto e analizzato
//con successo, altrimenti NONZERO.
//********************************************************************************
int CInputDevice::m_GetJoypadInput () {
	DIJOYSTATE js;
	
    if (m_bActive == FALSE) 
        return NONZERO;
	
	//Poll the device to read the current state
    if (FAILED (Device->Poll())) {
		HRESULT hr;

		//DInput is telling us that the input stream has been
		//interrupted. We aren't tracking any state between polls, so
		//we don't have any special reset that needs to be done. We
		//just re-acquire and try again.
		hr = Device->Acquire();
		while (hr == DIERR_INPUTLOST) 
			hr = Device->Acquire();
		
		//hr may be DIERR_OTHERAPPHASPRIO or other errors. This
		//may occur when the app is minimized or in the process of 
		//switching, so just try again later 
		return ZERO; 
	}
	
	//Get the input's device state
	if (FAILED (Device->GetDeviceState (sizeof(DIJOYSTATE), &js)))
		return NONZERO;
	
	//Leva analogica
	//Rotazione verso destra/sinistra
	if (js.lX > PSXCTRL_POSITIVEDEADZONE)
		InputData.dwMovement |= IDMOVE_RIGHT;
	else if (js.lX < PSXCTRL_NEGATIVEDEADZONE)
		InputData.dwMovement |= IDMOVE_LEFT;
	//Pad direzionale
	for (int z = 0; z < m_nPOVCount; z++) {
		//Destra/sinistra
		if (js.rgdwPOV[z] >= 4500 && js.rgdwPOV[z] <= 13500)
			InputData.dwMovement |= IDMOVE_RIGHT;
		else if (js.rgdwPOV[z] >= 22500 && js.rgdwPOV[z] <= 31500)
			InputData.dwMovement |= IDMOVE_LEFT;
		//Avanti/indietro
		if (js.rgdwPOV[z] >= 0 && js.rgdwPOV[z] <= 4500 || js.rgdwPOV[z] >= 31500 && js.rgdwPOV[z] < 36000)
			InputData.dwMovement |= IDMOVE_FORWARD;
		else if (js.rgdwPOV[z] >= 13500 && js.rgdwPOV[z] <= 22500)
			InputData.dwMovement |= IDMOVE_BACKWARD;
	}
	//Movimento
	//Leva analogica
	if (js.lY < PSXCTRL_NEGATIVEHIGHZONE)
		InputData.dwMovement |= IDMOVE_RUNNINGFORWARD;
	else if (js.lY < PSXCTRL_NEGATIVEDEADZONE)
		InputData.dwMovement |= IDMOVE_FORWARD;
	else if (js.lY > PSXCTRL_POSITIVEHIGHZONE)
		InputData.dwMovement |= IDMOVE_RUNNINGBACKWARD;
	else if (js.lY > PSXCTRL_POSITIVEDEADZONE)
		InputData.dwMovement |= IDMOVE_BACKWARD;
	
	//Altri tasti
	if (js.rgbButtons[PSXCTRL_EXIT] & 128)
		InputData.dwKeys |= IDBUTTON_EXIT;
	if (js.rgbButtons[PSXCTRL_RUN] & 128)
		InputData.dwMovement |= IDMOVE_RUNNING;
	if (js.rgbButtons[PSXCTRL_JUMP] & 128)
		InputData.dwKeys |= IDBUTTON_JUMP;
	return ZERO;
}