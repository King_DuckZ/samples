//CSolidCube.h


#ifndef _INCL_CSOLIDCUBE_H
#define _INCL_CSOLIDCUBE_H

#include "CSimpleSolid.h"

class CSolidCube : public CSimpleSolid {
public:
	CSolidCube ( void );
	~CSolidCube ( void );
	int BuildSolid ( LPDIRECT3DDEVICE9 dev, float fScale, D3DCOLOR color, bool bFlipNormals = false, unsigned int param = 0 );
	
private:
	static SIMPLESOLID_VERTEX m_vList[];
};

#endif