//CTweenedMesh.cpp


#include "constants.h"
#include <windows.h>
#include "CTweenedMesh.h"


#define INVALID_INDEX CMESH_INVALIDINDEX


D3DVERTEXELEMENT9 CTweenedMesh::m_dwStreamInfo[] = {
	{ 0,  0, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR,  0 },
	{ 0,  4, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
	{ 1,  0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
	{ 1, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL,   0 },
	{ 2,  0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 1 },
	{ 2, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL,   1 },
	D3DDECL_END()
};
LPDIRECT3DVERTEXDECLARATION9 CTweenedMesh::m_vdeclShader = NULL;
int CTweenedMesh::m_nRefCount = 0;


//********************************************************************************
//Costruttore standard
//********************************************************************************
CTweenedMesh::CTweenedMesh () {
	m_nRefCount++;
	m_Reset ();
}



//********************************************************************************
//Distruttore standard
//********************************************************************************
CTweenedMesh::~CTweenedMesh () {
	m_nRefCount--;
	if (m_nRefCount == 0) {
		SAFE_RELEASE (m_vdeclShader);
	}

	m_Dispose ();
}



//********************************************************************************
//Imposta tutte le propriet� sui valori iniziali.
//********************************************************************************
inline void CTweenedMesh::m_Reset () {
	m_ppdwIndices = NULL;
	m_vdVert = NULL;
	m_vbList = NULL;
	m_anAnims = NULL;
	
	m_dwKeyFrameCount = 0;
	m_dwAnimsCount = 0;
	m_dwActiveAnim = 0;
	m_dwIndex = 0;
	m_dwIndex2 = 0;
	m_dwPreallocatedIndices = 0;
	m_fIndex = 0.0f;
	m_fIndexNow = 0.0f;
	m_fVertexBalance = 0.0f;
	m_uStride = sizeof(TMESH_COLORTEX);
	m_bIsClone = false;
	
	bAnimationComplete = true;
}



//********************************************************************************
//Chiama m_Dispose()
//********************************************************************************
void CTweenedMesh::Dispose () {
	m_Dispose ();
	CMesh::Dispose ();
	m_Reset ();
}



//********************************************************************************
//Libera la memoria occupata e distrugge gli oggetti creati.
//********************************************************************************
inline void CTweenedMesh::m_Dispose () {
	//m_ppdwIndices
	if ((m_ppdwIndices != NULL) && (!m_bIsClone)) {
		for (DWORD z = 0; z < m_dwSubMeshCount; z++) {
			SAFE_DELETE (m_ppdwIndices[z]);
		}
		SAFE_DELETE (m_ppdwIndices);
	}

	//m_vbList
	m_DeleteVBuffers ();
		
	//m_vdVert
	m_DeleteTemp ();

	//m_anAnims
	SAFE_DELETE (m_anAnims);
}



//********************************************************************************
//Apre il file specificato e chiama m_LoadRawVertices(). Restituisce il valore
//di ritorno di m_LoadRawVertices() se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CTweenedMesh::LoadRawVertices (const char* strFileName, DWORD dwKeyFrameCount) {
	if (dwKeyFrameCount == 0 || strFileName == NULL)
		return NONZERO;
	
	FILE* f;
	if ( (f=fopen(strFileName, "rb")) == NULL )
		return NONZERO;
	
	const int nRet = m_LoadRawVertices (f, dwKeyFrameCount);
	fclose (f);
	return nRet;
}



//********************************************************************************
//Chiama m_LoadRawVertices() e ne restituisce il valore di ritorno.
//********************************************************************************
int CTweenedMesh::LoadRawVertices (FILE* f, DWORD dwKeyFrameCount) {
	if (f == NULL || dwKeyFrameCount == 0)
		return NONZERO;
	else
		return m_LoadRawVertices (f, dwKeyFrameCount);
}



//********************************************************************************
//Carica i vertici necessari all'animazione. Il caricamento deve essere
//effettuato necessariamente dopo che la mesh originale � stata caricata con
//successo nella classe base. Restituisce ZERO se va tutto bene, altrimenti
//NONZERO.
//********************************************************************************
inline int CTweenedMesh::m_LoadRawVertices (FILE* f, DWORD dwKeyFrameCount) {
	if (m_smMesh == NULL || m_dwVertCount == 0)
		return NONZERO;
	
	m_DeleteTemp ();
	
	m_vdVert = new TMESH_VERTEXDUMP[m_dwVertCount];
	if (m_vdVert == NULL)
		return NONZERO;
	
/*	
	for (DWORD dwVert = 0; dwVert < m_dwVertCount; dwVert++) {
		m_vdVert[dwVert].vecVert = new MATH_3DVECTOR[dwKeyFrameCount];
		if (m_vdVert[dwVert].vecVert == NULL)
			goto Error;
		
		//Imposto anche i puntatori delle normali su NULL, perch�
		//m_BuildNormals() lo richiede per funzionare correttamente
		m_vdVert[dwVert].vecNorm = NULL;
	
		if (fread (m_vdVert[dwVert].vecVert, sizeof(MATH_3DVECTOR), dwKeyFrameCount, f) != dwKeyFrameCount) {
			goto Error;
		}
	}
*/
	for (DWORD dwVert = 0; dwVert < m_dwVertCount; dwVert++) {
		m_vdVert[dwVert].vecVert = new MATH_3DVECTOR[dwKeyFrameCount];
		if (m_vdVert[dwVert].vecVert == NULL)
			goto Error;
		//Imposto anche i puntatori delle normali su NULL, perch�
		//m_BuildNormals() lo richiede per funzionare correttamente
		m_vdVert[dwVert].vecNorm = NULL;
	}

	Bounding.SetFigureCount (dwKeyFrameCount, BFSFC_SPHERE | BFSFC_BOX);
	
	for (DWORD dwFrame = 0; dwFrame < dwKeyFrameCount; dwFrame++) {
		Bounding.BeginCalc (dwFrame);
		
		for (DWORD dwVert = 0; dwVert < m_dwVertCount; dwVert++) {			
			fread (m_vdVert[dwVert].vecVert + dwFrame, sizeof(MATH_3DVECTOR), 1, f);
			Bounding << *(m_vdVert[dwVert].vecVert + dwFrame);
		}
		
		Bounding.EndCalc ();
	}
		
	m_dwKeyFrameCount = dwKeyFrameCount;
	return ZERO;
Error:
	m_DeleteTemp ();
	m_dwKeyFrameCount = 0;
	return NONZERO;
}



//********************************************************************************
//Apre il file specificato e chiama m_LoadRawAnimation(). Restituisce il valore
//di ritorno di m_LoadRawAnimation() se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CTweenedMesh::LoadRawAnimationInfo (const char* strFileName, DWORD dwCount) {
	if (dwCount == 0 || strFileName == NULL)
		return NONZERO;
	
	FILE* f;
	if ( (f=fopen(strFileName, "rb")) == NULL )
		return NONZERO;
	
	const int nRet = m_LoadRawAnimation (f, dwCount);
	fclose (f);
	return nRet;
}



//********************************************************************************
//Chiama m_LoadRawAnimation() e ne restituisce il valore di ritorno.
//********************************************************************************
int CTweenedMesh::LoadRawAnimationInfo (FILE* f, DWORD dwCount) {
	if (f == NULL || dwCount == 0)
		return NONZERO;
	else
		return m_LoadRawAnimation (f, dwCount);
	return ZERO;
}



//********************************************************************************
//Carica le informazioni necessarie ad interpetare correttamente i vertici
//caricati con m_LoadRawVertices(). dwCount deve specificare il numero
//totale di animazioni presenti nel file, che � uguale alla dimensione del
//file in byte / 16. I dati nel file devono rispecchiare il formato
//definito dalla struttura TMESH_ANIMATION. Resituisce ZERO se va tutto
//bene, altrimenti NONZERO.
//********************************************************************************
inline int CTweenedMesh::m_LoadRawAnimation (FILE* f, DWORD dwCount) {
	int nRet = ZERO;
	
	SAFE_DELETE (m_anAnims);
	m_anAnims = new TMESH_ANIMATION[dwCount];
	if (m_anAnims == NULL)
		return NONZERO;
	
	if (fread (m_anAnims, sizeof(TMESH_ANIMATION), dwCount, f) != dwCount) {
		nRet = NONZERO;
		m_dwAnimsCount = 0;
	}
	else
		m_dwAnimsCount = dwCount;
	
	m_dwActiveAnim = 0;
	m_dwIndex = 0;
	m_dwIndex2 = 0;
	m_fIndex = 0.0f;
	m_fIndexNow = 0.0f;
	
	return nRet;
}



//********************************************************************************
//Crea una nuova submesh in memoria. A differenza della versione della stessa
//funzione nella classe base, � obbligatorio specificare degli indici
//validi, mentre per i vertici le informazioni sulla posizione non sono
//richieste. La funzione memorizza gli indici ricevuti e le coordinate delle
//texture, se specificate. Restituisce un indice che identifica la nuova
//submesh in caso di successo, o INVALID_INDEX in caso d'errore. Non � possibile
//utilizzare questo metodo se l'oggetto � un clone (cf. m_SpawnClone())
//********************************************************************************
DWORD CTweenedMesh::SetVertices (LPDIRECT3DDEVICE9 dev, const LPMATH_MATRIX16 matTransf, const LPMESH_VERTEX vert, const LPDWORD ind, CPairList* pairTextureList, UINT nTexIndex, UINT nBumpIndex, UINT nReflIndex, DWORD dwVert, DWORD dwInd, MATERIAL_PROPERTIES mp) {
//LPDIRECT3DDEVICE9 dev
//const LPMATH_MATRIX16 matTransf
//const LPMESH_VERTEX vert
//const LPDWORD ind
//CPairList* pairTextureList
//const int* TextureIndices
//int nBumpIndex
//int nReflIndex
//DWORD dwVert
//DWORD dwInd
//int nTexInd
//MATERIAL_PROPERTIES mp
	if (m_bIsClone)
		return INVALID_INDEX;
	if ( (dev && pairTextureList && ind && vert) == FALSE )
		return INVALID_INDEX;
	if (dwInd == 0 || dwVert == 0)
		return INVALID_INDEX;

	const UINT uVertSize = dwVert * sizeof(TMESH_COLORTEX);
	LPWORD wIndices = NULL;
	const bool b32bitIndices = ((dwVert > 0xFFFF) && (m_caps.MaxVertexIndex > 0xFFFF) ? true : false);
	const UINT uIndSize = (b32bitIndices ? dwInd << 2: dwInd << 1);
	const WORD wMaxIndex = MIN((WORD)0xFFFF, (WORD)dwVert);
	LPDWORD dwIndicesCpy = new DWORD[dwInd];
	CMathEngine math;
	char* strTexName = NULL;
	
	if (dwIndicesCpy == NULL)
		goto Error;
	CopyMemory (dwIndicesCpy, ind, dwInd * sizeof(DWORD));
	
	//Se devo usare gli indici a 16 bit devo convertirli qua
	//Gli indici > 0xFFFF vengono troncati
	if (!b32bitIndices && dwInd) {
		wIndices = new WORD[dwInd];
		if (wIndices == NULL)
			goto Error;
		
		for (DWORD z = 0; z < dwInd; z++) {
			wIndices[z] = (WORD)(ind[z] & 0xFFFF);
		}
	}
	
	//Alloco memoria per un'altra submesh
	if (m_ExpandSubMeshList(m_dwSubMeshCount + 1))
		goto Error;
	m_smMesh[m_dwSubMeshCount].vb = NULL;
	m_smMesh[m_dwSubMeshCount].ib = NULL;
	
	//Creo il vertex buffer. Questo conterr� solo le coordinate delle texture e il diffuse color
	if (FAILED(dev->CreateVertexBuffer(uVertSize, D3DUSAGE_WRITEONLY, 0, D3DPOOL_SYSTEMMEM, &m_smMesh[m_dwSubMeshCount].vb, NULL))) {
		goto Error;
	}
	//Creo l'index buffer
	if (FAILED(dev->CreateIndexBuffer(uIndSize, D3DUSAGE_WRITEONLY, (b32bitIndices ? D3DFMT_INDEX32 : D3DFMT_INDEX16), D3DPOOL_MANAGED, &m_smMesh[m_dwSubMeshCount].ib, NULL))) {
		goto Error;
	}
	
	//Se � possibile, conservo gli indici
	if (m_dwPreallocatedIndices > m_dwSubMeshCount)
		m_ppdwIndices[m_dwSubMeshCount] = dwIndicesCpy;
	else
		goto Error;
	
	m_smMesh[m_dwSubMeshCount].nTexIndexCount = 1/*nTexInd*/;
	m_smMesh[m_dwSubMeshCount].nBumpIndex = nBumpIndex;
	m_smMesh[m_dwSubMeshCount].nRefMapIndex = nReflIndex;
	m_smMesh[m_dwSubMeshCount].mpProperties = mp;
	m_smMesh[m_dwSubMeshCount].dwIndexCount = dwInd;
	m_smMesh[m_dwSubMeshCount].dwVertexCount = dwVert;
	
	if (matTransf == NULL) {
		math.Mat16Identity (&m_smMesh[m_dwSubMeshCount].matTransform);
		math.Mat16Identity (&m_smMesh[m_dwSubMeshCount].matBase);
	}
	else {
		CopyMemory (&m_smMesh[m_dwSubMeshCount].matTransform, matTransf, sizeof(MATH_MATRIX16));
		CopyMemory (&m_smMesh[m_dwSubMeshCount].matBase, matTransf, sizeof(MATH_MATRIX16));
	}
	
	//Imposto i riferimenti alle texture
	//(nonostante la struttura sia predisposta al multitexturing, qui ne imposto solo una)
	if (nTexIndex <= 0) //Se non ci sono texture
		m_smMesh[m_dwSubMeshCount].pnTextureIndex = NULL;
	else { //Se ci sono texture
		m_smMesh[m_dwSubMeshCount].pnTextureIndex = new int[1];
		if (m_smMesh[m_dwSubMeshCount].pnTextureIndex == NULL)
			m_smMesh[m_dwSubMeshCount].nTexIndexCount = nTexIndex = 0;
		else { //Memoria allocata con successo
			m_smMesh[m_dwSubMeshCount].pnTextureIndex[0] = 0; /*nTexIndex*/;
			if (pairTextureList->ExistID ((UINT)nTexIndex)) {
				pairTextureList->GetLastText (&strTexName);
				
				//Se � stata impostata una lista per evitare il doppio caricamento
				//delle texture, la consulto ora
				if (m_pairTexture) {
					if (!m_pairTexture->ExistText (strTexName)) {
						const int nTexAddr = (int)(this->*m_LoadTexture)(dev, strTexName);
						
						m_pairTexture->AddPair (strTexName, nTexAddr);
						m_smMesh[m_dwSubMeshCount].pnTextureIndex[0] = nTexAddr;
						
						m_pairTexture->SortByText ();
					}
					else { //La texture non va caricata, gi� esiste
						m_smMesh[m_dwSubMeshCount].pnTextureIndex[0] = m_pairTexture->GetLastParam (NULL);
					}
				}
				else {
					const int nTexAddr = (int)(this->*m_LoadTexture)(dev, strTexName);
					m_smMesh[m_dwSubMeshCount].pnTextureIndex[0] = nTexAddr;
				}
			}
		}
	}
	
	//Copio vertici e indici
	LPTMESH_COLORTEX VertDst = NULL;
	void* IndDst = NULL;
	if (SUCCEEDED(m_smMesh[m_dwSubMeshCount].vb->Lock(0, uVertSize, (void**)&VertDst, 0))) {
		for (DWORD z = 0; z < dwVert; z++) {
			VertDst[z].color = vert[z].color;
			VertDst[z].tu = vert[z].tu;
			VertDst[z].tv = vert[z].tv;
		}
		m_smMesh[m_dwSubMeshCount].vb->Unlock();
	}
	if (dwInd) {
		if (SUCCEEDED(m_smMesh[m_dwSubMeshCount].ib->Lock(0, uIndSize, &IndDst, 0))) {
			if (b32bitIndices)
				CopyMemory (IndDst, ind, uIndSize);
			else
				CopyMemory (IndDst, wIndices, uIndSize);
			m_smMesh[m_dwSubMeshCount].ib->Unlock();
			m_dwIndCount += dwInd;
		}
	}
	
	m_dwVertCount += dwVert;
	dwIndicesCpy = NULL; //Non va deallocato, � usato da m_ppdwIndices[m_dwSubMeshCount]
	SAFE_DELETE (strTexName);
	SAFE_DELETE (wIndices);
	m_dwSubMeshCount++;
	return (m_dwSubMeshCount - 1);

Error:
	SAFE_DELETE (dwIndicesCpy);
	SAFE_DELETE (strTexName);
	SAFE_RELEASE (m_smMesh[m_dwSubMeshCount].ib);
	SAFE_RELEASE (m_smMesh[m_dwSubMeshCount].vb);
	SAFE_DELETE (m_smMesh[m_dwSubMeshCount].pnTextureIndex);
	SAFE_DELETE (wIndices);
	return INVALID_INDEX;
}



//********************************************************************************
//Chiama la versione base di m_ExpandSubMeshList(). Se la funzione restituisce
//ZERO, viene allocato spazio anche per m_vdVert. Se quest'ultima
//operazione dovesse fallire, la funzione restituisce ZERO comunque, � quindi
//opportuno fare riferimento a m_dwPreallocatedVD per conoscere il numero
//di elementi effetivamente disponibili.
//********************************************************************************
int CTweenedMesh::m_ExpandSubMeshList (DWORD dwNewSize) {
	const int nRet = CMesh::m_ExpandSubMeshList (dwNewSize);
	
	if (nRet)
		return NONZERO;
	if (dwNewSize <= m_dwPreallocatedIndices)
		return ZERO;
	
	LPDWORD* ppdwInd = new LPDWORD[m_dwPreallocatedSubMesh];
	if (ppdwInd == NULL)
		return ZERO;
	
	CopyMemory (ppdwInd, m_ppdwIndices, sizeof(LPDWORD) * m_dwSubMeshCount);
	ZeroMemory (ppdwInd + sizeof(LPDWORD) * m_dwSubMeshCount, sizeof(LPDWORD) * (dwNewSize - m_dwPreallocatedIndices));
	
	SAFE_DELETE (m_ppdwIndices);
	m_ppdwIndices = ppdwInd;
	ppdwInd = NULL;
	m_dwPreallocatedIndices = m_dwPreallocatedSubMesh;
	
	return ZERO;
}



//********************************************************************************
//Alloca la memoria necessaria a contenere tutte le normali, e calcola
//le normali stesse.
//Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CTweenedMesh::m_BuildNormals () {
	if (m_dwVertCount == 0 || m_dwSubMeshCount == 0 || m_vdVert == NULL)
		return NONZERO;
	
	LPTMESH_VERTEXDUMP v = m_vdVert;
	MATH_3DVECTOR vecU, vecV, vecNormal;
	CMathEngine math;
	
	//Scorro tutte le submesh
	for (DWORD dwMesh = 0; dwMesh < m_dwSubMeshCount; dwMesh++) {
	
		//Per ogni submesh scorro tutte le facce
		for (DWORD dwIndex = 0; dwIndex < m_smMesh[dwMesh].dwIndexCount; dwIndex+=3) {
			
			//Per prima cosa, se � necessario, alloco la memoria per l'array di normali
			for (DWORD n = 0; n < 3; n++) {
				if ((v[m_ppdwIndices[dwMesh][dwIndex + n]].vecNorm) == NULL) {
					v[m_ppdwIndices[dwMesh][dwIndex + n]].vecNorm = new MATH_3DVECTOR[m_dwKeyFrameCount];
					if (v[m_ppdwIndices[dwMesh][dwIndex + n]].vecNorm == NULL)
						goto Error;
				}
			}
			
			//Ora per comodit� mi salvo un po' di alias
			const TMESH_VERTEXDUMP &vdA = v[m_ppdwIndices[dwMesh][dwIndex + 0]];
			const TMESH_VERTEXDUMP &vdB = v[m_ppdwIndices[dwMesh][dwIndex + 1]];
			const TMESH_VERTEXDUMP &vdC = v[m_ppdwIndices[dwMesh][dwIndex + 2]];
			
			//Per ogni faccia, scorro tutti i fotogrammi
			for (DWORD dwFrame = 0; dwFrame < m_dwKeyFrameCount; dwFrame++) {
				//Se i vertici non ci sono, non posso calcolare un tubo
				if ( (vdA.vecVert && vdB.vecVert && vdC.vecVert) == FALSE )
					break;
				
				//Non resta che calcolare le normali
				math.Vec3Subtract (&vecU, vdB.vecVert + dwFrame, vdA.vecVert + dwFrame);
				math.Vec3Subtract (&vecV, vdC.vecVert + dwFrame, vdB.vecVert + dwFrame);
				math.Vec3Cross (&vecNormal, &vecU, &vecV);
				math.Vec3Normalize (&vecNormal);
				
				math.Vec3Sum (vdA.vecNorm + dwFrame, vdA.vecNorm + dwFrame, &vecNormal);
				math.Vec3Sum (vdB.vecNorm + dwFrame, vdB.vecNorm + dwFrame, &vecNormal);
				math.Vec3Sum (vdC.vecNorm + dwFrame, vdC.vecNorm + dwFrame, &vecNormal);
				
				math.Vec3Normalize (vdA.vecNorm + dwFrame);
				math.Vec3Normalize (vdB.vecNorm + dwFrame);
				math.Vec3Normalize (vdC.vecNorm + dwFrame);
			}
		}
		v += m_smMesh[dwMesh].dwVertexCount;
	}
	
	return ZERO;
Error:
	m_DeleteTemp ();
	return NONZERO;
}



//********************************************************************************
//Costruisce i vertex buffer in base ai vertici caricati in m_xyzVert. Al
//termine della funzione, lo spazio occupato dalle normali, dalla lista di
//vertici e dagli array di indici viene liberato. m_vbList conterr� un array i
//cui elementi sono array di fotogrammi chiave per ogni submesh gestita dalla
//classe. Restituisce ZERO se va tutto bene, altrimenti NONZERO. Se si sta
//chiamando questa funzione per la seconda volta, � necessario chiamare prima
//il metodo Dispose().
//In caso di fallimento della funzione, � preferibile chiamare comunque
//il metodo Dispose(). Questo potrebbe essere obbligatorio qualora si voglia
//ritentare la costruzione dei vertex buffer.
//********************************************************************************
int CTweenedMesh::BuildAnimation (LPDIRECT3DDEVICE9 dev) {
	if ( (dev && m_ppdwIndices) == FALSE )
		return NONZERO;
	
	LPTMESH_COLORTEX ColorTex = NULL;
	LPTMESH_VERTNORM VertNorm = NULL;
	LPTMESH_VERTEXDUMP vd = m_vdVert;
	DWORD dwBuffOffset = 0;
	
	//Allocazione della memoria per i puntatori ai vertex buffer
	if (m_BuildVBuffers ())
		return NONZERO;

	//Creo le normali
	if (m_BuildNormals())
		return NONZERO;
	
	//Ora creo i vertex buffer veri e propri
	for (DWORD i = 0; i < m_dwSubMeshCount; i++) {
		//Questi sono i vertex buffer che rappresentano il primo e il secondo stream,
		//quelli cio� che contengono informazioni su vertici e normali
		//Qui creo un vertex buffer per la submesh i, in grado di contenere
		//i vertici di tutti i fotogrammi di questa submesh.
		if (FAILED(dev->CreateVertexBuffer (sizeof(TMESH_VERTNORM) * m_smMesh[i].dwVertexCount * m_dwKeyFrameCount, D3DUSAGE_WRITEONLY, 0, D3DPOOL_SYSTEMMEM, m_vbList + i, NULL))) {
			return NONZERO;
		}
		//Ora riempio il vertex buffer
		if (SUCCEEDED(m_vbList[i]->Lock (0, 0, (void**)&VertNorm, 0))) {
			for (DWORD j = 0; j < m_dwKeyFrameCount; j++) {
				
				for (DWORD dwVertNorm = 0; dwVertNorm < m_smMesh[i].dwVertexCount; dwVertNorm++) {
					VertNorm[dwBuffOffset + dwVertNorm].vPos = vd[dwVertNorm].vecVert[j];
					VertNorm[dwBuffOffset + dwVertNorm].vNormal = vd[dwVertNorm].vecNorm[j];
				}
				
				dwBuffOffset += m_smMesh[i].dwVertexCount;
			}
			
			dwBuffOffset = 0;
			m_vbList[i]->Unlock();
			VertNorm = NULL;
		}
		vd += m_smMesh[i].dwVertexCount;
	}
	
	m_DeleteTemp ();
	
	for (DWORD z = 0; z < m_dwSubMeshCount; z++) {
		SAFE_DELETE (m_ppdwIndices[z]);
	}
	SAFE_DELETE (m_ppdwIndices);
	
	//Se � NULL, creo lo shader per il tweening
	if (m_vdeclShader == NULL) {
		dev->CreateVertexDeclaration (m_dwStreamInfo, &m_vdeclShader);
	}
	
	return ZERO;
}



//********************************************************************************
//Alloca la memoria necessaria e crea i vertex buffer. Restituisce ZERO se
//ve tutto bene, altrimenti NONZERO.
//********************************************************************************
int CTweenedMesh::m_BuildVBuffers () {
	if (m_dwSubMeshCount == 0 || m_dwKeyFrameCount == 0)
		return NONZERO;
	
	m_DeleteVBuffers ();

	m_vbList = new LPDIRECT3DVERTEXBUFFER9[m_dwSubMeshCount];
	if (m_vbList == NULL)
		return NONZERO;
	return ZERO;
}



//********************************************************************************
//Libera la memoria occupata da m_ppdwIndices e da m_vdVert. Restituisce ZERO.
//� necessario chiamare questa funzione prima che il distruttore o il
//Dispose() della classe base siano invocati.
//********************************************************************************
inline int CTweenedMesh::m_DeleteTemp () {
	if (m_vdVert) {
		for (DWORD z = 0; z < m_dwVertCount; z++) {
			SAFE_DELETE (m_vdVert[z].vecNorm);
			SAFE_DELETE (m_vdVert[z].vecVert);
		}
		SAFE_DELETE (m_vdVert);
	}
	return ZERO;
}



//********************************************************************************
//Distrugge i vertex buffer e libera la memoria occupata dai puntatori.
//Restituisce ZERO. � necessario chiamare questa funzione prima che il
//distruttore o il Dispose() della classe base siano invocati.
//********************************************************************************
inline int CTweenedMesh::m_DeleteVBuffers () {
	if (m_vbList) {
		for (DWORD x = 0; x < m_dwSubMeshCount; x++) {
			SAFE_RELEASE (m_vbList[x]);
		}
		SAFE_DELETE (m_vbList);
	}
	return ZERO;
}



//********************************************************************************
//********************************************************************************
int CTweenedMesh::m_Render (LPDIRECT3DDEVICE9 dev, DWORD dwIndex) {
	if (m_anAnims == NULL)
		return NONZERO;
	
	dev->SetFVF (0);
	
	dev->SetStreamSource (1, m_vbList[dwIndex], m_dwIndex * m_smMesh[dwIndex].dwVertexCount * sizeof(TMESH_VERTNORM), sizeof(TMESH_VERTNORM));
	dev->SetStreamSource (2, m_vbList[dwIndex], m_dwIndex2 * m_smMesh[dwIndex].dwVertexCount * sizeof(TMESH_VERTNORM), sizeof(TMESH_VERTNORM));
	
	int nRet = CMesh::m_Render (dev, dwIndex);
	return nRet;
}



//********************************************************************************
//********************************************************************************
int CTweenedMesh::m_PreRenderSteps (LPDIRECT3DDEVICE9 dev) {
	dev->SetFVF (0);
	if (m_vdeclShader)
		dev->SetVertexDeclaration (m_vdeclShader);
	dev->SetRenderState (D3DRS_VERTEXBLEND, D3DVBF_TWEENING);
	dev->SetRenderState (D3DRS_TWEENFACTOR, *((DWORD*)&m_fVertexBalance));
	return ZERO;
}



//********************************************************************************
//********************************************************************************
int CTweenedMesh::m_PostRenderSteps (LPDIRECT3DDEVICE9 dev) {
	CMesh::m_PostRenderSteps (dev);
	
//	dev->SetVertexDeclaration (NULL);
	dev->SetStreamSource (1, NULL, 0, 0);
	dev->SetStreamSource (2, NULL, 0, 0);
	dev->SetRenderState (D3DRS_VERTEXBLEND, D3DVBF_DISABLE);
	return ZERO;
}



//********************************************************************************
//Imposta la classe specificata in dst in modo che contenga le stesse
//informazioni contenute in src. I dati come i vertici e gli indici non sono
//copiati, ma sono condivisi con l'oggetto dst. � pertanto opportuno
//distruggere i cloni prima di distruggere l'oggetto base (src), o comunque
//non utilizzare i cloni dopo che l'oggetto base � stato distrutto.
//Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int m_SpawnClone (CTweenedMesh &src, CTweenedMesh &dst) {
	dst.Dispose ();
	dst.m_bIsClone = true;
	
//	dst.m_dwKeyFrameCount = src.m_dwKeyFrameCount;
/*
	LPTMESH_ANIMATION m_anAnims;
	DWORD m_dwKeyFrameCount;
	DWORD m_dwAnimsCount;
	DWORD m_dwActiveAnim;
	DWORD m_dwIndex; //Copia intera di m_fIndex
	DWORD m_dwIndex2; //Indice del fotogramma che segue m_dwIndex
	DWORD m_dwPreallocatedIndices;
	float m_fIndex;
	float m_fIndexNow; //Usato da AdvanceAnimation - contiene l'intero di m_fIndex
	float m_fVertexBalance; // 0.0f <-- vertice 1, 1.0f <-- vertice 2
*/
	return ZERO;
}



//********************************************************************************
//Chiama m_SpawnClone() e ne restituisce il valore di ritorno.
//********************************************************************************
int CTweenedMesh::SpawnClone (CTweenedMesh &mesh) {
	return m_SpawnClone (*this, mesh);
}