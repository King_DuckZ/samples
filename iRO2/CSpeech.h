//CSpeech.h


#ifndef _INCL_CSPEECH_H
#define _INCL_CSPEECH_H

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>
#include <d3d9.h>


enum SPEECH_STATUS {
	SPEECHS_WRITING = 0, //La classe sta eseguendo l'animazione del testo
	SPEECHS_IDLE = 1, //La classe non sta facendo nulla
	SPEECHS_COMPLETED = 2 //La classe sta visualizzando un testo la cui animazione � terminata
};

typedef struct STRUCT_CHARSETHEADER { //56 byte
	unsigned int chSignature; //Deve valere "DG"
	int chSize; //Dimensione della struttura
	int chReserved; //Deve valere 0
	int chRectCount; //Numero di RECT presenti
	unsigned int chFileSize; //Dimensione totale del file in byte
	unsigned int chRectDataStart; //Offset dei dati riguardanti i RECT
	unsigned int chSpacePixel; //Spazio consigliato da lasciare per il carattere 32
	unsigned int chAverageWidth; //Larghezza media
	unsigned int chAverageHeight; //Altezza media
	unsigned int chDistance; //Distanza consigliata da usare fra un carattere e l'altro
	unsigned short int chAppMajor; //Versione minima necessaria a leggere il file
	unsigned short int chAppMinor; //Versione minima necessaria a leggere il file
	short int chDefinesCharset; //Specifica se il file contiene i caratteri ASCII
	short int chDefinesBitmap; //Specifica se il file contiene la bitmap cui i RECT fanno riferimento
	short int chDefinesTop; //Specifica se il file contiene un array di int che contiene i top consigliati per il blitting
	char chPadd[6]; //Non usato
} CHARSETHEADER, *LPCHARSETHEADER;
typedef struct STRUCT_POINTF {
	float fx;
	float fy;
} POINTF, *LPPOINTF;


class CSpeech {
public:
	CSpeech ( void );
	~CSpeech ( void );
	void Dispose ( void );
	int LoadCharset ( LPDIRECT3DDEVICE9 dev, const char* strCharset, const char* strSurface );
	int Render ( LPDIRECT3DDEVICE9 dev );
	int UpdateScreenSize ( int cx, int cy );
	int SetDialoguePoint ( float x, float y );
	int SetTranslatedString ( LPDIRECT3DDEVICE9 dev, const unsigned char* Text, int nLenght );
	int SetUntranslatedString ( LPDIRECT3DDEVICE9 dev, const char* Text );
	int AdvanceAnimation ( float fDelta );
	int SetReferenceArea ( float cx, float cy );
	inline void CompleteAnimation ( void );
	
	SPEECH_STATUS Status;
private:
	inline void m_Dispose ( void );
	int m_PreBuildDialogue ( LPDIRECT3DDEVICE9 dev, float fCount );
	
	float* m_pfTops;
	char* m_strCharset; //Alfabeto
	char* m_strUntranslated; //Stringa ASCII impostata, se disponibile
	LPRECT m_prList;
	CHARSETHEADER m_chInfo;
	LPDIRECT3DTEXTURE9 m_texCharset;
	LPDIRECT3DVERTEXBUFFER9 m_vb;
	LPDIRECT3DINDEXBUFFER9 m_ib;
	float m_fText; //Lunghezza totale della stringa
	float m_fStripedText; //Numero di caratteri da visualizzare
	float m_fIndex; //Lettera in visualizzazione
	float m_fVB; //Numero di caratteri rappresentabili con il vb
	float m_fScreenWidth; //Larghezza in pixel dello schermo
	float m_fScreenHeight; //Altezza in pixel dello schermo
	float m_fReferenceWidth;
	float m_fReferenceHeight;
	float m_fLeft; //Destinazione x in pixel del dialogo
	float m_fTop; //Destinazione y in pixel del dialogo
	BOOL m_bEnabled;
};



//********************************************************************************
//Imposta m_fIndex sul suo valore finale, in modo che il testo venga
//visualizzato completamente, saltando l'animazione.
//********************************************************************************
inline void CSpeech::CompleteAnimation () {
	m_fIndex = m_fStripedText;
	Status = SPEECHS_COMPLETED;
}


#endif