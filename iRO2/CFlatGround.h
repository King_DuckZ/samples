//CFlatGround.h


#ifndef _INCL_CFLATGROUND_H
#define _INCL_CFLATGROUND_H

#include <d3d9.h>
#include "CMathEngine.h"

#ifndef GRAVITYACCEL
#define GRAVITYACCEL 9.80665f
#endif
#define D3DFVF_FLATGROUND (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1 | D3DFVF_TEX2)

typedef struct STRUCT_FLATGROUND_VERTEX {
	MATH_3DVECTOR vPos;
	MATH_3DVECTOR vNormal;
	D3DCOLOR color;
	float tu, tv;
	float tu2, tv2;
} FLATGROUND_VERTEX, *LPFLATGROUND_VERTEX;


class CFlatGround {
public:
	CFlatGround ( void );
	~CFlatGround ( void );
	void Dispose ( void );
	int BuildGround ( LPDIRECT3DDEVICE9 dev, float fcx, float fcz, float fy, float fu = 0.0f, float fv = 0.0f );
	inline float GetFallingPosition ( float fV0, float fTime );
	int Render ( LPDIRECT3DDEVICE9 dev );
	
	bool bAutoFreeTexture;
	float fGroundLevel;
	LPDIRECT3DTEXTURE9 texGround;
	LPDIRECT3DTEXTURE9 texReflection;
	MATH_PLANE plnEquation;
private:
	inline void m_Reset ( void );
	inline void m_Dispose ( void );
	int m_MakeQuad ( float fcx, float fcz, float fy, float fu, float fv, const LPMATH_2DVECTOR pvTexSize, D3DCOLOR d3dclrColor, LPFLATGROUND_VERTEX pfgvDst, LPWORD pwDst );
	
	float m_fGravityAccel;
	LPDIRECT3DVERTEXBUFFER9 m_vb;
	LPDIRECT3DINDEXBUFFER9 m_ib;
};



//********************************************************************************
//Restituisce la posizione di un corpo data la sua posizione iniziale
//fRefOrdinate all'istante fTime.
//********************************************************************************
inline float CFlatGround::GetFallingPosition (float fV0, float fTime) {
	const float fRet = -(fV0 * fTime - 0.5f * m_fGravityAccel * fTime * fTime);
//	if (fRet > m_fGroundY) //m_fGroundY cambiato in fGroundLevel
		return fRet;
//	else
//		return m_fGroundY;
}



#endif