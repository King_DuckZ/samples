VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   4890
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7020
   LinkTopic       =   "Form1"
   ScaleHeight     =   4890
   ScaleWidth      =   7020
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   375
      Left            =   1680
      TabIndex        =   2
      Top             =   4440
      Width           =   3375
   End
   Begin VB.ListBox List1 
      Height          =   3960
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   6975
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   0
      TabIndex        =   0
      Top             =   3960
      Width           =   6975
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Type ANIM
    fStartFrame As Single
    fFrameCount As Single
    fLoopFrames As Single
    fFPS As Single
End Type

Dim List() As ANIM
Dim AppPath As String

Private Sub Command1_Click()
    Dim mystr() As String
    Dim a(0) As ANIM
    Dim n As Long
    
    mystr = Split(Text1.Text, Chr(32))
    
    a(n).fStartFrame = CSng(Val(mystr(0)))
    a(n).fFrameCount = CSng(Val(mystr(1)))
    a(n).fLoopFrames = CSng(Val(mystr(2)))
    a(n).fFPS = CSng(Val(mystr(3)))
    
    List1.AddItem CStr(a(n).fStartFrame) & vbTab & _
                  CStr(a(n).fFrameCount) & vbTab & _
                  CStr(a(n).fLoopFrames) & vbTab & _
                  CStr(a(n).fFPS) & vbTab
    
    Text1.Text = ""
    Call Text1.SetFocus
    n = n + 1
End Sub

Private Sub Form_Click()
    Dim FileNum As Integer
    Dim z As Long
    Dim mystr() As String
    
    ReDim List(List1.ListCount - 1) As ANIM
    For z = 0 To List1.ListCount - 1
        mystr = Split(List1.List(z), vbTab)
        List(z).fStartFrame = CSng(Val(mystr(0)))
        List(z).fFrameCount = CSng(Val(mystr(1)))
        List(z).fLoopFrames = CSng(Val(mystr(2)))
        List(z).fFPS = CSng(Val(mystr(3)))
    Next
    
    FileNum = FreeFile()
    Open AppPath & "dump_anim.bin" For Binary Access Write As #FileNum
    Put #FileNum, , List
    Close #FileNum
    Erase List()
    
    MsgBox "Dump salvato!"
End Sub

Private Sub Form_Load()
    AppPath = App.Path & IIf(Right(App.Path, 1) = "\", "", "\")
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Call Command1_Click
End Sub
