//CCaligari.h


#ifndef _INCL_CCALIGARI_H
#define _INCL_CCALIGARI_H

#include <stdio.h>
#include "CMathEngine.h"
#include "CMesh.h"
#include "CPairList.h"

enum CALIGARI_PROP {
	CALG_NONE = 0
};

typedef struct STRUCT_COB_HEADER { //32
	char strSignature[9];
	char strVersion[6];
	char cType;
	char strByteOrder[2];
	char strBlank[13];
	char cNewLine;
} COB_HEADER, *LPCOB_HEADER;
typedef struct STRUCT_COB_CHUNK { //20
	char strType[4];
	short int iMajor;
	short int iMinor;
	int nChunkID;
	int nParentID;
	int nChunkSize;
} COB_CHUNK, *LPCOB_CHUNK;
typedef struct STRUCT_COB_STRING { //?
	short int iLen;
	char* strString;
} COB_STRING, *LPCOB_STRING;
typedef struct STRUCT_COB_OBJECTNAME { //2+sizeof(COB_STRING)
	short int iDupecount;
	COB_STRING cobstrName;
} COB_OBJECTNAME, *LPCOB_OBJECTNAME;
/*typedef struct STRUCT_COB_LOCALAXES {
	float cx, cy, cz; //Centro degli assi locali
	float xx, xy, xz; //Direzione dell'asse x
	float yx, yy, yz; //Direzione dell'asse y
	float zx, zy, zz; //Direzione dell'asse z
} COB_LOCALAXES, *LPCOB_LOCALAXES;*/
typedef struct STRUCT_COB_LOCALAXES { //48
	MATH_3DVECTOR vCenter; //Centro degli assi locali
	MATH_3DVECTOR vX; //Direzione dell'asse x
	MATH_3DVECTOR vY; //Direzione dell'asse y
	MATH_3DVECTOR vZ; //Direzione dell'asse z
} COB_LOCALAXES, *LPCOB_LOCALAXES;
typedef struct STRUCT_COB_MATRIX { //48
	MATH_QUATERNION qRow1;
	MATH_QUATERNION qRow2;
	MATH_QUATERNION qRow3;
} COB_MATRIX, *LPCOB_MATRIX;
typedef struct STRUCT_COB_VERTEX {
	float x, y, z;
} COB_VERTEX, *LPCOB_VERTEX;
typedef struct STRUCT_COB_TEXUV {
	float tu, tv;
} COB_TEXUV, *LPCOB_TEXUV;
typedef struct STRUCT_COB_MATERIAL {
	MATERIAL_PROPERTIES mpFlags;
	int nMeshID; //ID della mesh cui il materiale fa riferimento
	int nMaterialIndex; //Indice interno di questo materiale
	unsigned int* uIDList; //Lista degli ID in m_pairTexture delle texture usate
	int nIDCount; //Numero di elementi in uIDList;
	int nColor; //Colore diffuse in formato ARGB
	float fUOffset, fVOffset;
	float fURepeat, fVRepeat;
	STRUCT_COB_MATERIAL* mtlPrev;
	STRUCT_COB_MATERIAL* mtlNext;
} COB_MATERIAL, *LPCOB_MATERIAL;
typedef struct STRUCT_COB_FACELIST { //Le facce hanno tutte 3 vertici
	DWORD dwIndices[3]; //Indici
	int nPadd;
	STRUCT_COB_FACELIST* flPrev;
	STRUCT_COB_FACELIST* flNext;
} COB_FACELIST, *LPCOB_FACELIST;
typedef struct STRUCT_COB_MESHTREE {
	int nMeshID; //ID della mesh
	int nMaterialIndex; //Indice del materiale per questa mesh
	int nFaceCount; //Numero di facce in questa mesh
	LPCOB_FACELIST flFaces; //Facce
	LPMESH_VERTEX vtxList; //Lista dei vertici cui gli indici nelle facce fanno riferimento
	int nVertexCount; //Numero di vertici nella lista
	STRUCT_COB_MESHTREE* mtPrev;
	STRUCT_COB_MESHTREE* mtNext;
} COB_MESHTREE, *LPCOB_MESHTREE;
	

#ifdef _DEFINE_CALG_PROP
#ifdef CALG_PROP
#undef CALG_PROP
#endif
#define CALG_PROP
#endif


class CCaligari {
public:
	CCaligari ( void );
	~CCaligari ( void );
	void Reset ( void );
	void Dispose ( void );
	
	int LoadFile ( const char* strFileName );
	int LoadFile ( FILE* f );
	int BuildMesh ( LPDIRECT3DDEVICE9 dev, CMesh* mesh );
	
	CALIGARI_PROP Properties;
private:
	inline void m_Reset ( void );
	inline void m_Dispose ( void );
	inline int m_LoadFile ( FILE* f );
	int m_CheckHeader ( const LPCOB_HEADER header );
	BOOL m_ChunkCompare ( const char* chunk1, const char* chunk2, int nLen );
	int m_AddFace ( const LPDWORD dwInd, const LPMESH_VERTEX vtxList, int nVertexCount, int nMeshID, int nMaterialIndex );
	void m_DeletePath ( LPCOB_STRING csFileName, bool bNullTerminated = false );
	int m_ReadString ( FILE* f, LPCOB_STRING csString );
	inline LPCOB_MATERIAL m_FindMaterial ( int nOwnerID, int nIndex, bool bForceMtl = false );
	
	//Funzioni di lettura dei chunk
	int m_ReadChunkPolH ( FILE* f, const LPCOB_CHUNK cobNfo );
	int m_ReadChunkMat1 ( FILE* f, const LPCOB_CHUNK cobNfo );
	
	bool m_bDisposed; //Indica se � stato chiamato il metodo Dispose().
	//Per conservare il nome del file, se disponibile
	char* m_strFileName;
	int m_nFileName;
	//Informazioni sui dati letti
	LPCOB_CHUNK m_CobChunk;
	LPCOB_MESHTREE m_mtMeshTree;
	LPCOB_MESHTREE m_mtMeshTreeLast; //Tengo traccia dell'ultimo elemento della lista
	LPCOB_MATERIAL m_mtlList; //Lista dei materiali
	CPairList m_pairTexture;
	COB_MATERIAL m_mtlDefault;
};

#endif