//CBitmapSarge.cpp


#define WIN32_LEAN_AMD_MEAN
#include "Constants.h"
#include <stdlib.h>
#include <windows.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include "CBitmapSarge.h"


#define BITMAP_SIGNATURE 0x4D42
#define PAL_SIGNATURE 0x46464952
#define RIFF_TYPE_PAL 0x204C4150
#define RIFF_CHUNK_SIGNATURE 0x61746164

enum CSPALETTE_TYPE {
	CSPT_UNKNOWN,
	CSPT_PAL,
	CSPT_BMP
};

//********************************************************************************
//Costruttore standard
//********************************************************************************
CBitmapSarge::CBitmapSarge () {
	m_ResetValues ();
}



//********************************************************************************
//Overload del costruttore. Permette di caricare subito un'immagine.
//********************************************************************************
CBitmapSarge::CBitmapSarge (const char* strFileName) {
	m_ResetValues ();
	LoadPicture (strFileName);
}



//********************************************************************************
//Distruttore standard
//********************************************************************************
CBitmapSarge::~CBitmapSarge () {
	m_Dispose();
}



//********************************************************************************
//Chiama m_Dispose()
//********************************************************************************
void CBitmapSarge::Dispose () {
	m_Dispose ();
}



//********************************************************************************
//Libera la memoria occupata e riporta l'oggetto allo stato iniziale.
//********************************************************************************
inline void CBitmapSarge::m_Dispose () {
	if (m_strFileName)
		free (m_strFileName);
	if (m_data)
		free (m_data);
	if (m_pal)
		free (m_pal);
	if (m_strPaletteName)
		free (m_strPaletteName);
	
	m_ResetValues ();
}



//********************************************************************************
//Imposta tutte le propriet� sui valori iniziali, senza effettuare nessun
//controllo sui puntatori.
//********************************************************************************
inline void CBitmapSarge::m_ResetValues () {
	m_strFileName = NULL;
	m_data = NULL;
	m_pal = NULL;
	m_strPaletteName = NULL;
	m_nPal = 0;
	ZeroMemory (&m_fH, sizeof (BITMAPFILEHEADER));
	ZeroMemory (&m_iH, sizeof (BITMAPINFOHEADER));
	ZeroMemory (&m_BitMask, sizeof (BITMASK));
	m_nPitch = 0;
	bPicture = false;
	m_bIsARGB = true;
	BitmapType = BTYPE_UNKNOWN;
}



//********************************************************************************
//Apre il file specificato e passa lo stream a LoadPicture(FILE*). Restituisce
//il valore di ritorno di LoadPicture(FILE*) se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CBitmapSarge::LoadPicture (const char* strFileName) {
	if (strFileName == NULL)
		return NONZERO;
	
	FILE* f;
	
	if ( (f=fopen(strFileName, "rb")) == NULL ) {
		return NONZERO;
	}
	
	const int nRet = LoadPicture (f);
	fclose (f);
	
	if (nRet == ZERO) {
		bPicture = true;
		if (m_strFileName) {
			if (strcmp(m_strFileName, strFileName)) {
				free (m_strFileName);
				m_strFileName = NULL;
			}
		}
		
		if (m_strFileName == NULL) {
			if ( (ha=m_strFileName=(char*)malloc(strlen(strFileName)+1)) != NULL ) {
				strcpy (m_strFileName, strFileName);
			}
		}
	}
	return nRet;
}


//********************************************************************************
//Restituisce l'indirizzo dei dati dell'immagine. Restituisce NULL se
//l'immagine non � presente in memoria.
//********************************************************************************
void *CBitmapSarge::GetDataPtr (void** ptr) {
	if (ptr)
		*ptr = (void*)m_data;
	return m_data;
}



//********************************************************************************
//Libera la memoria occupata dalla bitmap caricata, ma non rimuove il
//riferimento in m_strFileName. � quindi possibile ricaricare la bitmap
//facendo uso della funzione Reload(). Le altre informazioni sull'immagine
//restano disponibili.
//********************************************************************************
void CBitmapSarge::Unload () {
	if (m_data) {
		free (m_data);
		m_data = NULL;
	}
	if (m_pal) {
		free (m_pal);
		m_pal = NULL;
		m_nPal = 0;
	}
}



//********************************************************************************
//Ricarica in memoria la bitmap caricata in precedenza. Restituisce ZERO se
//il caricamento � avvenuto con successo, altrimenti NONZERO.
//********************************************************************************
int CBitmapSarge::Reload () {
	return (LoadPicture (m_strFileName));
}



//********************************************************************************
//Ricarica in memoria la palette caricata in precedenza. Restituisce ZERO se
//il caricamento � avvenuto con successo, altrimenti NONZERO.
//********************************************************************************
int CBitmapSarge::ReloadPalette () {
	return (LoadPalette (m_strPaletteName));
}



//********************************************************************************
//Carica una palette da un file bitmap o .pal. Se � gi� presente un'altra
//palette, questa viene rimpiazzata. Restituisce ZERO se � andato tutto
//bene, altrimenti NONZERO.
//********************************************************************************
int CBitmapSarge::LoadPalette (const char* strFileName) {
	if (strFileName == NULL)
		return NONZERO;
	FILE* f;
	
	if ( (f=fopen(strFileName, "rb")) == NULL )
		return NONZERO;
	
	const int nRet = LoadPalette (f);
	fclose (f);
	
	if (nRet == ZERO) {
		if (m_strPaletteName) {
			if (strcmp(m_strPaletteName, strFileName)) {
				free (m_strPaletteName);
				m_strPaletteName = NULL;
			}
		}
		
		if (m_strPaletteName == NULL) {
			if ( (m_strPaletteName=(char*)malloc(strlen(strFileName)+1)) != NULL ) {
				strcpy (m_strPaletteName, strFileName);
			}
		}
	}
	return ZERO;
}



//********************************************************************************
//Restituisce l'indirizzo della memoria relativa alla palette caricata, o NULL
//se questa non � disponibile.
//********************************************************************************
void *CBitmapSarge::GetPalettePtr (void** ptr) {
	if (ptr)
		*ptr = (void*)m_pal;
	return m_pal;
}



//********************************************************************************
//Converte l'immagine nel buffer in un'immagine a 32 bit. La palette, se
//presente, viene persa. Restituisce ZERO se � andato tutto bene, altrimenti
//NONZERO.
//********************************************************************************
int CBitmapSarge::ConvertTo32bit () {
	if (m_data == NULL)
		return NONZERO;
	if (m_iH.biBitCount == 32)
		return ZERO;
	
	int nRet;
	switch (m_iH.biBitCount) {
	case 1:
		nRet = m_Conv1to32();
		break;
	case 4:
		nRet = m_Conv4to32();
		break;
	case 8:
		nRet = m_Conv8to32();
		break;
	case 16:
		nRet = m_Conv16to32();
		break;
	case 24:
		nRet = m_Conv24to32();
		break;
	default:
		nRet = NONZERO;
		break;
	}
	
	BitmapType = BTYPE_R8G8B8A8;
	return nRet;
}



//********************************************************************************
//Ridimensiona l'immagine caricata alle dimensioni specificate. bssm permette
//di specificare un filtro di ridimensionamenteo. L'immagine deve essere
//convertita a 32bit prima di poter essere ridimensionata. Restituisce ZERO
//se � andato tutto bene, altrimenti NONZERO.
//********************************************************************************
int CBitmapSarge::ScaleImage (int cx, int cy, BSSCALE_MODE bssm) {
	if (m_data == NULL)
		return NONZERO;
	if (m_iH.biBitCount != 32) {
		return NONZERO;
	}
	if ( (cx < 4) || (cy < 4) )
		return NONZERO;
	if (m_iH.biWidth == cx && m_iH.biHeight == cy)
		return ZERO;
	
	const float fWidth = (float)m_iH.biWidth;
	const float fHeight = (float)m_iH.biHeight;
    const float fXRatio = fWidth / (float)cx;
    const float fYRatio = fHeight / (float)cy;
    int DstX = 0;
    int DstY = 0;
    LPPOINT p;
    DWORD* dwPel1;
    DWORD* dwPel2;
    DWORD* dwPel3;
    DWORD* dwPel4;
	RGBQUAD rgbPixel;
	const int nPitchNew = (((cx << 5) + 31) >> 5) << 2;
	BYTE* NewBuffer;
	BYTE* DummyNewBuffer;
	
	if ( (NewBuffer=(BYTE*)malloc(nPitchNew*cy)) == NULL )
		return NONZERO;
	DummyNewBuffer = NewBuffer;
	
	switch (bssm) {
	case BSSM_BILINEAR: {
	    if ( (p=(LPPOINT)malloc(sizeof(POINT)<<2)) == NULL) 
			return NONZERO;
	    
	    float fWeight1, fWeight2, fWeight3, fWeight4;
//	    rgbPixel.rgbReserved = 0xFF;
	    
	    for (float SrcY = 0.0f; SrcY < fHeight; SrcY += fYRatio) {
	    	for (float SrcX = 0.0f; SrcX < fWidth; SrcX += fXRatio) {
            	p[0].x = (int)(SrcX + 0.5f);
            	p[1].x = (int)(SrcX - 0.5f);
            	p[2].x = (int)(SrcX - 0.5f);
            	p[3].x = (int)(SrcX + 0.5f);
            	//-
            	p[0].y = (int)(SrcY + 0.5f);
	            p[1].y = (int)(SrcY + 0.5f);
	            p[2].y = (int)(SrcY - 0.5f);
	            p[3].y = (int)(SrcY - 0.5f);
            
	            //# 1. Uf * Vf
	            //# 2. (1 - Uf) * Vf
	            //# 3. (1 - Uf) * (1 - Vf)
	            //# 4. Uf * (1 - Vf)
            	fWeight1 =       (SrcX - (int)SrcX) * (SrcY - (int)SrcY);
	            fWeight2 = (1 - (SrcX - (int)SrcX)) * (SrcY - (int)SrcY);
	            fWeight3 = (1 - (SrcX - (int)SrcX)) * (1 - (SrcY - (int)SrcY));
	            fWeight4 =       (SrcX - (int)SrcX) * (1 - (SrcY - (int)SrcY));
            
	            dwPel1 = (DWORD*)((m_nPitch * p[0].y + (p[0].x << 2)) + m_data);
	            dwPel2 = (DWORD*)((m_nPitch * p[1].y + (p[1].x << 2)) + m_data);
	            dwPel3 = (DWORD*)((m_nPitch * p[2].y + (p[2].x << 2)) + m_data);
	            dwPel4 = (DWORD*)((m_nPitch * p[3].y + (p[3].x << 2)) + m_data);
            
	            rgbPixel.rgbRed = (BYTE)(
	            	(float)((*dwPel1 >> 16) & 0xFF) * fWeight1 +
	                (float)((*dwPel2 >> 16) & 0xFF) * fWeight2 +
	                (float)((*dwPel3 >> 16) & 0xFF) * fWeight3 +
	                (float)((*dwPel4 >> 16) & 0xFF) * fWeight4);
	            rgbPixel.rgbGreen = (BYTE)(
	            	(float)((*dwPel1 >> 8) & 0xFF) * fWeight1 +
	                (float)((*dwPel2 >> 8) & 0xFF) * fWeight2 +
	                (float)((*dwPel3 >> 8) & 0xFF) * fWeight3 +
	                (float)((*dwPel4 >> 8) & 0xFF) * fWeight4);
	            rgbPixel.rgbBlue = (BYTE)(
	            	(float)((*dwPel1 >> 0) & 0xFF) * fWeight1 +
	                (float)((*dwPel2 >> 0) & 0xFF) * fWeight2 +
	                (float)((*dwPel3 >> 0) & 0xFF) * fWeight3 +
	                (float)((*dwPel4 >> 0) & 0xFF) * fWeight4);
	            rgbPixel.rgbReserved = (BYTE)(
	            	(float)((*dwPel1 >> 24) & 0xFF) * fWeight1 +
	                (float)((*dwPel2 >> 24) & 0xFF) * fWeight2 +
	                (float)((*dwPel3 >> 24) & 0xFF) * fWeight3 +
	                (float)((*dwPel4 >> 24) & 0xFF) * fWeight4);
                
					DummyNewBuffer[(DstX<<2)+3] = rgbPixel.rgbReserved;
					DummyNewBuffer[(DstX<<2)+2] = rgbPixel.rgbRed;
					DummyNewBuffer[(DstX<<2)+1] = rgbPixel.rgbGreen;
					DummyNewBuffer[(DstX<<2)+0] = rgbPixel.rgbBlue;
            	DstX++;
	    	}
	    	DstX = 0;
	    	DstY++;
	    	DummyNewBuffer += nPitchNew;
	    }
	    
	    free (p);
		p = NULL;
	    
	}	break;
	case BSSM_BLUR4X4:
	    if ( (p=(LPPOINT)malloc(sizeof(POINT)<<2)) == NULL) 
			return NONZERO;
//	    rgbPixel.rgbReserved = 0xFF;
	    
	    for (float SrcY = 0.0f; SrcY < fHeight; SrcY += fYRatio) {
	    	for (float SrcX = 0.0f; SrcX < fWidth; SrcX += fXRatio) {
            	p[0].x = (int)(SrcX + 0.5f);
            	p[1].x = (int)(SrcX - 0.5f);
            	p[2].x = (int)(SrcX - 0.5f);
            	p[3].x = (int)(SrcX + 0.5f);
            	//-
            	p[0].y = (int)(SrcY + 0.5f);
	            p[1].y = (int)(SrcY + 0.5f);
	            p[2].y = (int)(SrcY - 0.5f);
	            p[3].y = (int)(SrcY - 0.5f);
            
	            dwPel1 = (DWORD*)((m_nPitch * p[0].y + (p[0].x << 2)) + m_data);
	            dwPel2 = (DWORD*)((m_nPitch * p[1].y + (p[1].x << 2)) + m_data);
	            dwPel3 = (DWORD*)((m_nPitch * p[2].y + (p[2].x << 2)) + m_data);
	            dwPel4 = (DWORD*)((m_nPitch * p[3].y + (p[3].x << 2)) + m_data);
            
	            rgbPixel.rgbRed = (BYTE)((
	            	(float)((*dwPel1 >> 16) & 0xFF) +
	                (float)((*dwPel2 >> 16) & 0xFF) +
	                (float)((*dwPel3 >> 16) & 0xFF) +
	                (float)((*dwPel4 >> 16) & 0xFF)) / 4.0f);
	            rgbPixel.rgbGreen = (BYTE)((
	            	(float)((*dwPel1 >> 8) & 0xFF) +
	                (float)((*dwPel2 >> 8) & 0xFF) +
	                (float)((*dwPel3 >> 8) & 0xFF) +
	                (float)((*dwPel4 >> 8) & 0xFF)) / 4.0f);
	            rgbPixel.rgbBlue = (BYTE)((
	            	(float)((*dwPel1 >> 0) & 0xFF) +
	                (float)((*dwPel2 >> 0) & 0xFF) +
	                (float)((*dwPel3 >> 0) & 0xFF) +
	                (float)((*dwPel4 >> 0) & 0xFF)) / 4.0f);
	            rgbPixel.rgbReserved = (BYTE)((
	            	(float)((*dwPel1 >> 24) & 0xFF) +
	                (float)((*dwPel2 >> 24) & 0xFF) +
	                (float)((*dwPel3 >> 24) & 0xFF) +
	                (float)((*dwPel4 >> 24) & 0xFF)) / 4.0f);
                
					DummyNewBuffer[(DstX<<2)+3] = rgbPixel.rgbReserved;
					DummyNewBuffer[(DstX<<2)+2] = rgbPixel.rgbRed;
					DummyNewBuffer[(DstX<<2)+1] = rgbPixel.rgbGreen;
					DummyNewBuffer[(DstX<<2)+0] = rgbPixel.rgbBlue;
            	DstX++;
	    	}
	    	DstX = 0;
	    	DstY++;
	    	DummyNewBuffer += nPitchNew;
	    }
	    
	    free (p);
		p = NULL;
	    
		break;
	default:
		return NONZERO;
	}
	
	//Aggiorno le varibili interne
    free (m_data);
	m_data = (char*)NewBuffer;
	NewBuffer = NULL;
	m_iH.biSizeImage = nPitchNew * cy;
	m_nPitch = nPitchNew;
	m_iH.biWidth = cx;
	m_iH.biHeight = cy;
	
	return ZERO;
}



//********************************************************************************
//Carica l'immagine specificata. Se ce n'� un'altra gi� caricata, questa viene
//rimpiazzata. Restituisce ZERO se � andato tutto bene, altrimenti NONZERO.
//In caso di errore, la vecchia immagine non resta disponibile.
//********************************************************************************
int CBitmapSarge::LoadPicture (FILE* f) {
	if (f == NULL)
		return NONZERO;
	const unsigned int nPos = ftell (f);
	//m_Dispose();
	Unload ();
	
	fread (&m_fH, sizeof(BITMAPFILEHEADER), 1, f);
	if (m_fH.bfType != BITMAP_SIGNATURE) {
		fclose (f);
		return NONZERO;
	}
	fread (&m_iH, sizeof(BITMAPINFOHEADER), 1, f);
	
	//Controllo le bitmask
	if (sizeof(BITMAPINFOHEADER) + sizeof(BITMASK) == m_iH.biSize) {
		fread (&m_BitMask, sizeof(BITMASK), 1, f);
	}
	else {
		if (m_iH.biBitCount == 16) { //Vado con la mask di default - X1B5G5R5
			m_BitMask.dwAlpha = 0x8000;
			m_BitMask.dwBlue =  0x7C00;
			m_BitMask.dwGreen = 0x3E0;
			m_BitMask.dwRed =   0x1F;
		}
		else {
			m_BitMask.dwAlpha = 0xFF000000;
			m_BitMask.dwBlue =  0x00FF0000;
			m_BitMask.dwGreen = 0x0000FF00;
			m_BitMask.dwRed =   0x000000FF;
		}
	}
	
	//Controllo della palette
	if (m_iH.biBitCount <= 8) {
		if (m_iH.biClrUsed > 0)
			m_nPal = m_iH.biClrUsed;
		else
			m_nPal = (int)pow(2, m_iH.biBitCount);
		if ( (m_pal = (LPRGBQUAD)malloc(sizeof(RGBQUAD)*m_nPal)) == NULL ) {
			fclose (f);
			return NONZERO;
		}
		fread (m_pal, sizeof(RGBQUAD), m_nPal, f);
	}
	
	//Controllo del tipo di immagine
	switch (m_iH.biBitCount) {
	case 1:
		BitmapType = BTYPE_MONOCHROME;
		break;
	case 4:
		BitmapType = BTYPE_4BIT;
		break;
	case 8:
		BitmapType = BTYPE_8BIT;
		break;
	case 16:
		if (m_BitMask.dwAlpha == 0x8000 && m_BitMask.dwBlue == 0x7C00 && m_BitMask.dwGreen == 0x3E0 && m_BitMask.dwRed == 0x1F)
			BitmapType = BTYPE_A1R5G5B5;
		else if (m_BitMask.dwBlue == 0xF800 && m_BitMask.dwGreen == 0x7E0 && m_BitMask.dwRed == 0x1F)
			BitmapType = BTYPE_R5G6B5;
		else
			BitmapType = BTYPE_UNKNOWN;
		break;
	case 24:
		BitmapType = BTYPE_24BIT;
		break;
	case 32:
		BitmapType = BTYPE_A8R8G8B8;
		break;
	default:
		BitmapType = BTYPE_UNKNOWN;
	}
	
	//Pitch e imagesize
	m_nPitch = (((m_iH.biWidth * m_iH.biBitCount) + 31) >> 5) << 2;
	if (m_iH.biSizeImage == 0) {
		m_iH.biSizeImage = m_nPitch * m_iH.biHeight;
	}
	else {
		if (m_iH.biSizeImage != m_nPitch * m_iH.biHeight) {
			fclose (f);
			return NONZERO;
		}
	}
	
	//Preparo la memoria
	if ( (m_data=(char*)malloc(m_nPitch*abs(m_iH.biHeight))) == NULL ) {
		//Tragedia!!! :0
		fclose (f);
		return NONZERO;
	}
	
	//Leggo il file
	fseek (f, m_fH.bfOffBits + nPos, SEEK_SET);
	if (m_iH.biHeight < 0) {
		m_iH.biHeight = -m_iH.biHeight;
		fread (m_data, 1, m_nPitch * m_iH.biHeight, f);
	}
	else {
		for (int y = 0; y < m_iH.biHeight; y++) {
			fread (m_data + m_nPitch * (m_iH.biHeight - 1 - y), 1, m_nPitch, f);
		}
	}
	
	m_bIsARGB = true;
	return ZERO;
}



//********************************************************************************
//Carica una palette dallo stream specificato. La palette deve essere in
//formato .pal o contenuta in un file bitmap. Restituisce ZERO se �
//andato tutto bene, altrimenti NONZERO. In caso di errore, la palette vecchia,
//se presente, viene mantenuta, tranne nel caso in cui l'errore � causato
//da un out of memory.
//********************************************************************************
int CBitmapSarge::LoadPalette (FILE* f) {
	if (f == NULL)
		return NONZERO;
	
//	const unsigned int nPos = ftell (f);
	DWORD dwSignature;
	DWORD dwChunkSignature;
	DWORD dwRiffType;
	long int nChunkSize;
	BITMAPFILEHEADER fH;
	BITMAPINFOHEADER iH;
	CSPALETTE_TYPE csPalType = CSPT_UNKNOWN;
	
	//Identifico il tipo di file
	fread (&dwSignature, sizeof(DWORD), 1, f);
	fseek (f, -sizeof(DWORD), SEEK_CUR);
	if (dwSignature != PAL_SIGNATURE) {
		fread (&fH, sizeof(BITMAPFILEHEADER), 1, f);
		fseek (f, -sizeof(BITMAPFILEHEADER), SEEK_CUR);
		if (fH.bfSize == BITMAP_SIGNATURE)
			csPalType = CSPT_BMP;
	}
	else
		csPalType = CSPT_PAL;
	
	switch (csPalType) {
	case CSPT_PAL:
		fseek (f, sizeof(DWORD) << 1, SEEK_CUR);
		fread (&dwRiffType, sizeof(DWORD), 1, f);
		
		if (dwRiffType == RIFF_TYPE_PAL) {
			fread (&dwChunkSignature, sizeof(DWORD), 1, f);
			if (dwChunkSignature != RIFF_CHUNK_SIGNATURE)
				return NONZERO;
			fread (&nChunkSize, sizeof(long int), 1, f);
			
			if (m_pal)
				free (m_pal);
			//m_nPal = (nChunkSize - 4) >> 2;
			fread (&m_nPal, sizeof(int), 1, f);
			m_nPal >>= 16;
			
			if ( (m_pal=(LPRGBQUAD)malloc(sizeof(RGBQUAD)*m_nPal)) == NULL )
				return NONZERO;
			fread (m_pal, sizeof(RGBQUAD), m_nPal, f);
		}
		break;
		
	case CSPT_BMP:
		fread (&fH, sizeof(BITMAPFILEHEADER), 1, f);
		fread (&iH, sizeof(BITMAPINFOHEADER), 1, f);
		
		if (iH.biBitCount <= 8) {
			if (m_pal) {
				free (m_pal);
			}
			if (iH.biClrUsed == 0)
				m_nPal = (int)pow(2, iH.biBitCount);
			else
				m_nPal = iH.biClrUsed;
			if ( (m_pal=(LPRGBQUAD)malloc(sizeof(RGBQUAD)*m_nPal)) == NULL )
				return NONZERO;
			fread (m_pal, sizeof(RGBQUAD), m_nPal, f);
		}
		break;
		
	default:
		return NONZERO;
	}
	
	return ZERO;
}



//********************************************************************************
//Scompone il colore specificato nelle componenti RGB (supponendo che
//dwStartColor abbia il formato X8B8G8R8) e chiama
//CreatePalette(BYTE,BYTE,BYTE), restituendone il valore di ritorno.
//********************************************************************************
int CBitmapSarge::CreatePalette (DWORD dwStartColor, DWORD dwEndColor) {
	const BYTE r1 = dwStartColor & 0xFF;
	const BYTE g1 = (dwStartColor >> 8) & 0xFF;
	const BYTE b1 = (dwStartColor >> 16) & 0xFF;
	const BYTE r2 = dwEndColor & 0xFF;
	const BYTE g2 = (dwEndColor >> 8) & 0xFF;
	const BYTE b2 = (dwEndColor >> 16) & 0xFF;
	
	return CreatePalette (r1, g1, b1, r2, g2, b2);
}



//********************************************************************************
//Crea una palette gradiente da xStart a xEnd, di 256 elementi. Se � gi�
//presente una palette, questa viene sostituita. Restituisce ZERO se va
//tutto bene, altrimenti NONZERO.
//********************************************************************************
int CBitmapSarge::CreatePalette (BYTE rStart, BYTE gStart, BYTE bStart, BYTE rEnd, BYTE gEnd, BYTE bEnd) {
	if (m_pal == NULL) {
		if ( (m_pal=(LPRGBQUAD)malloc(sizeof(RGBQUAD)<<8)) == NULL )
			return NONZERO;
		m_nPal = 256;
	}
	else {
		if (m_nPal != 256) {
			free (m_pal);
			if ( (m_pal=(LPRGBQUAD)malloc(sizeof(RGBQUAD)<<8)) == NULL )
				return NONZERO;
			m_nPal = 256;
		}
	}	
	
	//ZeroMemory (m_pal, sizeof(RGBQUAD) << 8;
	
	float fR = (float)rStart;
	float fG = (float)gStart;
	float fB = (float)bStart;
	const float fRedRatio = (float)(rEnd - rStart + 1) / 256.0f;
	const float fGreenRatio = (float)(gEnd - gStart + 1) / 256.0f;
	const float fBlueRatio = (float)(bEnd - bStart + 1) / 256.0f;
	
	for (int i = 0; i < 256; i++) {
		m_pal[i].rgbRed = (BYTE)fR;
		m_pal[i].rgbGreen = (BYTE)fG;
		m_pal[i].rgbBlue = (BYTE)fB;
		m_pal[i].rgbReserved = 0xFF;
		
		fR += fRedRatio;
		fB += fBlueRatio;
		fG += fGreenRatio;
	}
	
	return ZERO;
}



//********************************************************************************
//Crea una palette di 256 elementi gradienti, partendo dal nero ed arrivando
//al bianco. � pi� veloce rispetto alla CreatePalette(). Restituisce ZERO
//se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CBitmapSarge::CreateGrayscalePalette () {
	if (m_pal) {
		free (m_pal);
		m_nPal = 0;
	}
	if ( (m_pal=(LPRGBQUAD)malloc(sizeof(RGBQUAD)<<8)) == NULL )
		return NONZERO;
	m_nPal = 256;
	
	LPRGBQUAD ptr = m_pal;
	
	for (register int z = 0; z < 256; z++) {
		ptr->rgbRed = z;
		ptr->rgbGreen = z;
		ptr->rgbBlue = z;
		ptr->rgbReserved = 0xFF;
		ptr++;
	}
	return ZERO;
}



//********************************************************************************
//Converte l'immagine caricata da 1 a 32 bit. Restituisce ZERO se va tutto
//bene, altrimenti NONZERO.
//********************************************************************************
int CBitmapSarge::m_Conv1to32 () {
	return ZERO;
}



//********************************************************************************
//Converte l'immagine caricata da 4 a 32 bit. Restituisce ZERO se va tutto
//bene, altrimenti NONZERO.
//********************************************************************************
int CBitmapSarge::m_Conv4to32 () {
	return ZERO;
}



//********************************************************************************
//Converte l'immagine caricata da 8 a 32 bit. Restituisce ZERO se va tutto
//bene, altrimenti NONZERO.
//********************************************************************************
int CBitmapSarge::m_Conv8to32 () {
	const int nPitch = (((m_iH.biWidth << 5) + 31) >> 5) << 2;
	const int nPaddArr = m_nPitch - ((m_iH.biBitCount >> 3) * m_iH.biWidth);
	const int nPaddDst = nPitch - (m_iH.biWidth << 2);
	BYTE* arr = (BYTE*)m_data;
	BYTE* dst;
	BYTE* DummyDst;
	
	if (m_pal == NULL)
		return NONZERO;
	if (m_iH.biBitCount != 8)
		return NONZERO;
	
	if ( (dst=(BYTE*)malloc(nPitch * m_iH.biHeight)) == NULL )
		return NONZERO;
	
	DummyDst = dst;
	for (int y = 0; y < m_iH.biHeight; y++) {
		for (int x = 0; x < m_iH.biWidth; x++) {
			DummyDst[3] = m_pal[*arr].rgbRed; //rosso
			DummyDst[2] = m_pal[*arr].rgbGreen; //verde
			DummyDst[1] = m_pal[*arr].rgbBlue; //blu
			DummyDst[0] = m_pal[*arr].rgbReserved;
			
			DummyDst += 4;
			arr++;
		}
		DummyDst += nPaddDst;
		arr += nPaddArr;
	}
	
	//Libero la memoria
	free (m_data);
	m_data = (char*)dst;
	dst = NULL;
	
	//Aggiorno le variabili interne
	m_iH.biSizeImage = nPitch * m_iH.biHeight;
	m_nPitch = nPitch;
	m_iH.biBitCount = 32;
	
	//Via la palette
	free (m_pal);
	m_pal = NULL;
	m_nPal = 0;
	
	//Ciao
	return ZERO;
}



//********************************************************************************
//Converte l'immagine caricata da 16 a 32 bit. Restituisce ZERO se va tutto
//bene, altrimenti NONZERO.
//********************************************************************************
int CBitmapSarge::m_Conv16to32 () {
	const int nPitch = (((m_iH.biWidth << 5) + 31) >> 5) << 2;
	const int nPaddArr = (m_nPitch - ((m_iH.biBitCount >> 3) * m_iH.biWidth)) >> 1;// / sizeof(WORD);
	const int nPaddDst = (nPitch - (m_iH.biWidth << 2)) >> 2; // / sizeof(DWORD);
	WORD* arr = (WORD*)m_data;
	DWORD* dst;
	DWORD* DummyDst;
	int nShiftRed = 0, nShiftBlue = 0, nShiftGreen = 0, nShiftAlpha = 0;
	
	if (m_iH.biBitCount != 16)
		return NONZERO;
	
	if ( (dst=(DWORD*)malloc(nPitch * m_iH.biHeight)) == NULL )
		return NONZERO;
	
	//Calcolo lo shift per ogni canale
	if (m_BitMask.dwAlpha) {
		while ((m_BitMask.dwAlpha & (1 << nShiftAlpha)) == 0)
			nShiftAlpha++;
	}
	if (m_BitMask.dwRed) {
		while ((m_BitMask.dwRed & (1 << nShiftRed)) == 0)
			nShiftRed++;
	}
	if (m_BitMask.dwBlue) {
		while ((m_BitMask.dwBlue & (1 << nShiftBlue)) == 0)
			nShiftBlue++;
	}
	if (m_BitMask.dwGreen) {
		while ((m_BitMask.dwGreen & (1 << nShiftGreen)) == 0)
			nShiftGreen++;
	}
	
	const float fMaxRed = (float)(m_BitMask.dwRed >> nShiftRed);
	const float fMaxGreen = (float)(m_BitMask.dwGreen >> nShiftGreen);
	const float fMaxBlue = (float)(m_BitMask.dwBlue >> nShiftBlue);
	const float fMaxAlpha = (float)(m_BitMask.dwAlpha >> nShiftAlpha);
	
	//Loop!
	DummyDst = dst;
	for (int y = 0; y < m_iH.biHeight; y++) {
		for (int x = 0; x < m_iH.biWidth; x++) {
			
			*DummyDst = ( (((DWORD)((float)((*arr & m_BitMask.dwAlpha) >> nShiftAlpha) / fMaxAlpha * 255.0f)) << 24) |
						  (((DWORD)((float)((*arr & m_BitMask.dwRed  ) >> nShiftRed  ) / fMaxRed   * 255.0f)) <<  0) |
						  (((DWORD)((float)((*arr & m_BitMask.dwGreen) >> nShiftGreen) / fMaxGreen * 255.0f)) <<  8) |
						  (((DWORD)((float)((*arr & m_BitMask.dwBlue ) >> nShiftBlue ) / fMaxBlue  * 255.0f)) << 16) );
			
//			*DummyDst = 0x00000000;
			
			DummyDst++;
			arr++;
		}
		DummyDst += nPaddDst;
		arr += nPaddArr;
	}
	
	//Libero la memoria
	free (m_data);
	m_data = (char*)dst;
	dst = NULL;
	
	//Aggiorno le variabili interne
	m_iH.biSizeImage = nPitch * m_iH.biHeight;
	m_nPitch = nPitch;
	m_iH.biBitCount = 32;
	m_bIsARGB = true;
	BitmapType = BTYPE_A8R8G8B8;
	
	//Via la palette
	free (m_pal);
	m_pal = NULL;
	m_nPal = 0;
	
	//Ciao
	return ZERO;
}



//********************************************************************************
//Converte l'immagine caricata da 24 a 32 bit. Restituisce ZERO se va tutto
//bene, altrimenti NONZERO.
//********************************************************************************
int CBitmapSarge::m_Conv24to32 () {
	const int nPitch = (((m_iH.biWidth << 5) + 31) >> 5) << 2;
	const int nPaddArr = m_nPitch - ((m_iH.biBitCount >> 3) * m_iH.biWidth);
	const int nPaddDst = nPitch - (m_iH.biWidth << 2);
	BYTE* arr = (BYTE*)m_data;
	BYTE* dst;
	BYTE* DummyDst;
	
	if (m_iH.biBitCount != 24)
		return NONZERO;
	
	if ( (dst=(BYTE*)malloc(nPitch * m_iH.biHeight)) == NULL )
		return NONZERO;
	
	DummyDst = dst;
	for (int y = 0; y < m_iH.biHeight; y++) {
		for (int x = 0; x < m_iH.biWidth; x++) {
			DummyDst[0] = 0xFF;
			DummyDst[1] = arr[0];
			DummyDst[2] = arr[1];
			DummyDst[3] = arr[2];
			
			DummyDst += 4;
			arr += 3;
		}
		DummyDst += nPaddDst;
		arr += nPaddArr;
	}
	
	//Libero la memoria
	free (m_data);
	m_data = (char*)dst;
	dst = NULL;
	
	//Aggiorno le variabili interne
	m_iH.biSizeImage = nPitch * m_iH.biHeight;
	m_nPitch = nPitch;
	m_iH.biBitCount = 32;
	
	//Via la palette
	free (m_pal);
	m_pal = NULL;
	m_nPal = 0;
	m_bIsARGB = false;
	
	//Ciao
	return ZERO;
}



//********************************************************************************
//Cambia l'ordine dei byte nei pixel da RGBA a ARGB. L'immagine deve essere
//a 32 bit. Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CBitmapSarge::RGBAtoARGB () {
	if (m_iH.biBitCount != 32 || m_data == NULL || m_bIsARGB)
		return NONZERO;

	char* arr = m_data;
	char tmp;
//	char a, r, g, b;
	
	for (int y = 0; y < m_iH.biHeight; y++) {
		for (int x = 0; x < m_iH.biWidth; x++) {
			tmp = arr[(x<<2)+0];
			arr[(x<<2)+0] = arr[(x<<2)+1];
			arr[(x<<2)+1] = arr[(x<<2)+2];
			arr[(x<<2)+2] = arr[(x<<2)+3];
			arr[(x<<2)+3] = tmp;

/*			a = arr[(x<<2)+0];
			b = arr[(x<<2)+1];
			g = arr[(x<<2)+2];
			r = arr[(x<<2)+3];
			
			arr[(x<<2)+0] = b;
			arr[(x<<2)+1] = g;
			arr[(x<<2)+2] = r;
			arr[(x<<2)+3] = a;
*/
		}
		arr += m_nPitch;
	}
	
	m_bIsARGB = true;
	BitmapType = BTYPE_A8R8G8B8;
	return ZERO;
}



//********************************************************************************
//Cambia l'ordine dei byte nei pixel da ARGB a RGBA. L'immagine deve essere
//a 32 bit. Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CBitmapSarge::ARGBtoRGBA () {
	if (m_iH.biBitCount != 32 || m_data == NULL || !m_bIsARGB)
		return NONZERO;
	
	char* arr = m_data;
	char tmp;
	
	for (int y = 0; y < m_iH.biHeight; y++) {
		for (int x = 0; x < m_iH.biWidth; x++) {
			tmp = arr[(x<<2)+3];
			arr[(x<<2)+3] = arr[(x<<2)+2];
			arr[(x<<2)+2] = arr[(x<<2)+1];
			arr[(x<<2)+1] = arr[(x<<2)+0];
			arr[(x<<2)+0] = tmp;
		}
		arr += m_nPitch;
	}
	
	m_bIsARGB = false;
	BitmapType = BTYPE_R8G8B8A8;
	return ZERO;
}



//********************************************************************************
//Carica una bitmap dalla locazione di memoria specificata. Restituisce
//ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CBitmapSarge::LoadPicture (const void* MemPtr) {
	if (MemPtr == NULL)
		return NONZERO;
	BYTE* byPtr = (BYTE*)MemPtr;
	
	Unload ();
	
	CopyMemory (&m_fH, byPtr, sizeof(BITMAPFILEHEADER));
	byPtr += sizeof(BITMAPFILEHEADER);
	if (m_fH.bfType != BITMAP_SIGNATURE) {
		return NONZERO;
	}
	
	
	CopyMemory (&m_iH, byPtr, sizeof(BITMAPINFOHEADER));
	byPtr += sizeof(BITMAPINFOHEADER);
	
	//Controllo le bitmask
	if (sizeof(BITMAPINFOHEADER) + sizeof(BITMASK) == m_iH.biSize) {
		CopyMemory (&m_BitMask, byPtr, sizeof(BITMASK));
		byPtr += sizeof(BITMASK);
	}
	else {
		if (m_iH.biBitCount == 16) { //Vado con la mask di default - X1B5G5R5
			m_BitMask.dwAlpha = 0x8000;
			m_BitMask.dwBlue =  0x7C00;
			m_BitMask.dwGreen = 0x3E0;
			m_BitMask.dwRed =   0x1F;
		}
		else {
			m_BitMask.dwAlpha = 0xFF000000;
			m_BitMask.dwBlue =  0x00FF0000;
			m_BitMask.dwGreen = 0x0000FF00;
			m_BitMask.dwRed =   0x000000FF;
		}
	}
	
	//Controllo della palette
	if (m_iH.biBitCount <= 8) {
		if (m_iH.biClrUsed > 0)
			m_nPal = m_iH.biClrUsed;
		else
			m_nPal = (int)pow(2, m_iH.biBitCount);
		if ( (m_pal = (LPRGBQUAD)malloc(sizeof(RGBQUAD)*m_nPal)) == NULL ) {
			return NONZERO;
		}
		CopyMemory (m_pal, byPtr, sizeof(RGBQUAD) * m_nPal);
		byPtr += sizeof(RGBQUAD) * m_nPal;
	}
	
	//Pitch e imagesize
	m_nPitch = (((m_iH.biWidth * m_iH.biBitCount) + 31) >> 5) << 2;
	if (m_iH.biSizeImage == 0) {
		m_iH.biSizeImage = m_nPitch * m_iH.biHeight;
	}
	else {
		if (m_iH.biSizeImage != m_nPitch * m_iH.biHeight) {
			return NONZERO;
		}
	}
	
	//Preparo la memoria
	if ( (m_data=(char*)malloc(m_nPitch*abs(m_iH.biHeight))) == NULL ) {
		//Tragedia!!! :0
		return NONZERO;
	}
	
	//Leggo il file
	byPtr = (BYTE*)MemPtr + m_fH.bfOffBits;
	if (m_iH.biHeight < 0) {
		m_iH.biHeight = -m_iH.biHeight;
		CopyMemory (m_data, byPtr, m_nPitch * m_iH.biHeight);
	}
	else {
		for (int y = 0; y < m_iH.biHeight; y++) {
			CopyMemory (m_data + m_nPitch * (m_iH.biHeight - 1 - y), byPtr, m_nPitch);
			byPtr += m_nPitch;
		}
	}
	
	m_bIsARGB = false;
	return ZERO;
}



//********************************************************************************
//Converte il buffer interno in scala di grigio. Il buffer deve essere a 32
//bit. Se bTo8Bit � true, il buffer viene convertito in un formato a 8
//bit, in cui il valore del grigio rappresenta un indice della palette.
//Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CBitmapSarge::ConvertToGrayScale (bool bTo8Bit) {
	//L = 0.299R + 0.587G + 0.114B
	if (m_iH.biBitCount != 32 || m_data == NULL || m_bIsARGB)
		return NONZERO;
	
	BYTE* data = (BYTE*)m_data;
	int Index;
	//Usati solo se bTo8Bit == true
	BYTE* indices = NULL;
	BYTE* DummyIndices;
	const int nNewPitch = (((m_iH.biWidth << 3) + 31) >> 5) << 2;
	
	if (bTo8Bit) {
		DummyIndices = indices = (BYTE*)malloc(nNewPitch * m_iH.biHeight);
		if (indices == NULL)
			return NONZERO;
	
		for (int y = 0; y < m_iH.biHeight; y++) {
			Index = 0;
			for (int x = 0; x < m_iH.biWidth; x++) {
				indices[Index] = (BYTE)((float)data[(Index << 2) + 1] * 0.114f +
										(float)data[(Index << 2) + 2] * 0.587f +
										(float)data[(Index << 2) + 3] * 0.299f);
				Index++;
			}
			DummyIndices += nNewPitch;
			data += m_nPitch;
		}
		
		free (m_data);
		m_data = (char*)indices;
		m_iH.biSizeImage = nNewPitch * m_iH.biHeight;
		m_iH.biBitCount = 8;
		m_nPitch = nNewPitch;
	}
	else {
		BYTE clr;
		for (int y = 0; y < m_iH.biHeight; y++) {
			Index = 0;
			for (int x = 0; x < m_iH.biWidth; x++) {
				clr = (BYTE)((float)(data[(Index << 2) + 1]) * 0.114f +
							 (float)(data[(Index << 2) + 2]) * 0.587f +
							 (float)(data[(Index << 2) + 3]) * 0.299f);
				data[(Index << 2) + 1] = clr;
				data[(Index << 2) + 2] = clr;
				data[(Index << 2) + 3] = clr;
				Index++;
			}
			data += m_nPitch;
		}
	}
	return ZERO;
}



//********************************************************************************
//********************************************************************************
int CBitmapSarge::LoadPalette (const void* MemPtr) {
	if (MemPtr == NULL)
		return NONZERO;
	
	DWORD dwSignature;
	DWORD dwChunkSignature;
	DWORD dwRiffType;
	long int nChunkSize;
	BITMAPFILEHEADER fH;
	BITMAPINFOHEADER iH;
	CSPALETTE_TYPE csPalType = CSPT_UNKNOWN;
	char* ptr = (char*)MemPtr;
	
	//Identifico il tipo di file
	CopyMemory (&dwSignature, ptr, sizeof(DWORD));
	//fseek (f, -sizeof(DWORD), SEEK_CUR);
	//Non incremento il puntatore, qui
	
	if (dwSignature != PAL_SIGNATURE) {
		CopyMemory (&fH, ptr, sizeof(BITMAPFILEHEADER));
		//fseek (f, -sizeof(BITMAPFILEHEADER), SEEK_CUR);
		//Non incremento il puntatore
		
		if (fH.bfSize == BITMAP_SIGNATURE)
			csPalType = CSPT_BMP;
	}
	else
		csPalType = CSPT_PAL;
	
	switch (csPalType) {
	case CSPT_PAL:
		ptr += (sizeof(DWORD) << 1);
		CopyMemory (&dwRiffType, ptr, sizeof(DWORD));
		ptr += sizeof(DWORD);
		
		if (dwRiffType == RIFF_TYPE_PAL) {
			CopyMemory (&dwChunkSignature, ptr, sizeof(DWORD));
			ptr += sizeof(DWORD);
			
			if (dwChunkSignature != RIFF_CHUNK_SIGNATURE)
				return NONZERO;
			CopyMemory (&nChunkSize, ptr, sizeof(long int));
			ptr += sizeof(long int);
			
			if (m_pal)
				free (m_pal);
			CopyMemory (&m_nPal, ptr, sizeof(int));
			m_nPal >>= 16;
			if ( (m_pal=(LPRGBQUAD)malloc(sizeof(RGBQUAD)*m_nPal)) == NULL )
				return NONZERO;
			CopyMemory (m_pal, ptr, sizeof(RGBQUAD) * m_nPal);
			ptr += sizeof(RGBQUAD) * m_nPal;
		}
		break;
		
	case CSPT_BMP:
		CopyMemory (&fH, ptr, sizeof(BITMAPFILEHEADER));
		ptr += sizeof(BITMAPFILEHEADER);
		CopyMemory (&iH, ptr, sizeof(BITMAPINFOHEADER));
		ptr += sizeof(BITMAPINFOHEADER);
		
		if (iH.biBitCount <= 8) {
			if (m_pal) {
				free (m_pal);
			}
			if (iH.biClrUsed == 0)
				m_nPal = (int)pow(2, iH.biBitCount);
			else
				m_nPal = iH.biClrUsed;
			if ( (m_pal=(LPRGBQUAD)malloc(sizeof(RGBQUAD)*m_nPal)) == NULL )
				return NONZERO;
			CopyMemory (m_pal, ptr, sizeof(RGBQUAD) * m_nPal);
			ptr += sizeof(RGBQUAD) * m_nPal;
		}
		break;
		
	default:
		return NONZERO;
	}
	
	return ZERO;
}