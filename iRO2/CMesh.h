//CMesh.h
//Un oggetto CMesh � in grado di contenere e renderizzare una mesh
//che conta anche pi� di una texture. 

#ifndef _INCL_CMESH_H
#define _INCL_CMESH_H

#include "d3d9.h"
#include "CMathEngine.h"
#include "CPairList.h"
#include "CBitmapSarge.h"
#include "CBoundingFigure.h"

#ifdef _DEFINE_CMESH_FVF
#ifdef D3DFVF_CMESH
#undef D3DFVF_CMESH
#endif
#define D3DFVF_CMESH (D3DFVF_DIFFUSE | D3DFVF_TEX1 | D3DFVF_XYZ | D3DFVF_NORMAL)
#endif
#define CMESH_INVALIDINDEX 0xFFFFFFFF

enum MATERIAL_PROPERTIES {
	MP_NONE = 0,
	MP_FLATSHADING = 1,
	MP_ALPHABLENDED = 2,
	MP_USETEXTURE = 4,
	MP_TEXTUREOVERLAY = 8,
	MP_BUMPMAPPED = 16,
	MP_FORCEDWORD = 0xFFFFFFFF
};
enum CMESH_TEXTURESOURCE {
	MTS_STRAIGHTFILE,
	MTS_GIGAPAQ,
	MTS_FORCEDWORD = 0xFFFFFFFF
};
enum CMESH_BOUNDINGFIGURE {
	MBF_BOX,
	MBF_CYLINDER,
	MBF_SPHERE,
	MBF_FORCEDWORD = 0xFFFFFFFF
};


typedef struct STRUCT_MESH_VERTEX {
	union {
		struct {
			float x, y, z;
		};
		MATH_3DVECTOR vPos;
	};
	union {
		struct {
			float nx, ny, nz;
		};
		MATH_3DVECTOR vNormal;
	};
	D3DCOLOR color;
	float tu, tv;
} MESH_VERTEX, *LPMESH_VERTEX;
typedef struct STRUCT_MESH_SUBMESH {
	MATH_MATRIX16 matBase;
	MATH_MATRIX16 matTransform;
	int* pnTextureIndex;
	int nTexIndexCount;
	int nBumpIndex;
	int nRefMapIndex;
	int dwIndexCount;
	int dwVertexCount;
	LPDIRECT3DVERTEXBUFFER9 vb;
	LPDIRECT3DINDEXBUFFER9 ib;
	MATERIAL_PROPERTIES mpProperties;
} MESH_SUBMESH, *LPMESH_SUBMESH;

class CMesh {
public:
	CMesh ( void );
	~CMesh ( void );
	virtual void Dispose ( void );
	void SetDeviceCaps ( const D3DCAPS9* caps );
	virtual DWORD SetVertices ( LPDIRECT3DDEVICE9 dev, const LPMATH_MATRIX16 matTransf, const LPMESH_VERTEX vert, const LPDWORD ind, CPairList* pairTextureList, UINT nTexIndex, UINT nBumpIndex, UINT nReflIndex, DWORD dwVert, DWORD dwInd, MATERIAL_PROPERTIES mp );
	int PreAllocSubMesh ( DWORD dwCount );
	int Render ( LPDIRECT3DDEVICE9 dev, DWORD dwIndex = CMESH_INVALIDINDEX );
	int SetTextureSource ( CMESH_TEXTURESOURCE src );
	int SetPairList ( CPairList* pair, bool bDelOld = true );
	
	bool bSetVrtKeepCopy;
	CBoundingFigure Bounding;
protected:
	inline void m_Reset ( void );
	inline void m_Dispose ( void );
	virtual int m_ExpandSubMeshList ( DWORD dwNewSize );
	inline void m_DeviceSetup ( LPDIRECT3DDEVICE9 dev, MATERIAL_PROPERTIES p );

	LPDIRECT3DTEXTURE9 m_LoadTextureFromFile ( LPDIRECT3DDEVICE9 dev, const char* strFileName );
	LPDIRECT3DTEXTURE9 m_LoadTextureFromArchive ( LPDIRECT3DDEVICE9 dev, const char* strFileName );
	inline LPDIRECT3DTEXTURE9 m_LoadTextureFunc ( LPDIRECT3DDEVICE9 dev, FILE* f );
	virtual int m_Render ( LPDIRECT3DDEVICE9 dev, DWORD dwIndex );
	virtual int m_PreRenderSteps ( LPDIRECT3DDEVICE9 dev );
	virtual int m_PostRenderSteps ( LPDIRECT3DDEVICE9 dev );

	//Puntatore alla funzione di caricamento delle texture
	IDirect3DTexture9* (CMesh::*m_LoadTexture)(LPDIRECT3DDEVICE9, const char*);
	
	bool m_bGotCaps;
	LPMESH_SUBMESH m_smMesh;
//	LPDIRECT3DTEXTURE9* m_tex;
//	LPDIRECT3DTEXTURE9* m_bump;
//	LPDIRECT3DTEXTURE9* m_refl;
	LPMESH_VERTEX m_vert;
	CPairList* m_pairTexture;
	DWORD m_dwVertCount; //Numero complessivo di vertici di tutte le submesh
	DWORD m_dwIndCount; //Numero complessivo di indici di tutte le submesh
	DWORD m_dwTexCount;
	DWORD m_dwBumpCount;
	DWORD m_dwReflCount;
	DWORD m_dwSubMeshCount;
	DWORD m_dwPreallocatedSubMesh;
	UINT m_uStride; //Dimensione della struttura vertici per lo stream 0
	D3DCAPS9 m_caps;
};

#endif