//CBoundingFigure.cpp


#include "constants.h"
#include <windows.h>
#include "CBoundingFigure.h"


#define STATE_LIESPOSITIVE 0x01
#define STATE_LIESNEGATIVE 0x02
#define STATE_LIESOVER 0x04


//********************************************************************************
//Costruttore standard
//********************************************************************************
CBoundingFigure::CBoundingFigure () {
	m_Reset ();
}



//********************************************************************************
//Distruttore standard
//********************************************************************************
CBoundingFigure::~CBoundingFigure () {
	m_Dispose ();
}



//********************************************************************************
//Imposta tutte le propriet� sui valori iniziali.
//********************************************************************************
inline void CBoundingFigure::m_Reset () {
	CMathEngine math;
	
	math.Mat16Identity (&matTransform);
	
	UsingFigure (BFTYPE_BOX);
	m_uCount = 0;
	m_uCurrentFig = 0;
	m_dwFiguresUsed = BFSFC_NONE;
	m_bCalculating = FALSE;
	
	ZeroMemory (&m_bfSphereVoid, sizeof(BFIGURE_SPHERE));
	ZeroMemory (&m_bfBoxVoid, sizeof(BFIGURE_BOX));
	ZeroMemory (&m_bfCylinderVoid, sizeof(BFIGURE_CYLINDER));
}



//********************************************************************************
//Chiama m_Dispose()
//********************************************************************************
void CBoundingFigure::Dispose () {
	m_Dispose ();
	m_Reset ();
}



//********************************************************************************
//Libera la memoria occupata e distrugge gli oggetti creati.
//********************************************************************************
inline void CBoundingFigure::m_Dispose () {
}



//********************************************************************************
//Imposta i puntatori alle funzioni da usare per il tipo di bounding figure
//specificato in bfType. Restituisce NONZERO se bfType non � un valore
//valido, altrimenti ZERO.
//********************************************************************************
int CBoundingFigure::UsingFigure (BFIGURE_BOUNDINGTYPE bfType) {
	switch (bfType) {
	case BFTYPE_SPHERE:
		m_GetFigure = (BFIGURE_BASEFIGURE& (CBoundingFigure::*)(unsigned int))&m_GetSphere;
		m_IntersectPlane = &m_SphereIntersectPlane;
		break;
	
	case BFTYPE_BOX:
		m_GetFigure = (BFIGURE_BASEFIGURE& (CBoundingFigure::*)(unsigned int))&m_GetBox;
		m_IntersectPlane = &m_BoxIntersectPlane;
		break;
	
	case BFTYPE_CYLINDER:
		m_GetFigure = (BFIGURE_BASEFIGURE& (CBoundingFigure::*)(unsigned int))&m_GetCylinder;
		m_IntersectPlane = &m_CylinderIntersectPlane;
		break;
	
	default:
		return NONZERO;
	}
	
	m_bftCurrent = bfType;
	return ZERO;
}



//********************************************************************************
//Restituisce l'elemento u della lista m_SphereList.
//********************************************************************************
BFIGURE_SPHERE& CBoundingFigure::m_GetSphere (unsigned int u) {
	if (u < m_uCount && m_SphereList) {
		return m_SphereList[u];
	}
	else {
		return m_bfSphereVoid;
	}
}



//********************************************************************************
//Restituisce l'elemento u della lista m_BoxList.
//********************************************************************************
BFIGURE_BOX& CBoundingFigure::m_GetBox (unsigned int u) {
	if (u < m_uCount && m_BoxList) {
		return m_BoxList[u];
	}
	else {
		return m_bfBoxVoid;
	}
}



//********************************************************************************
//Restituisce l'elemento u della lista m_CylinderList.
//********************************************************************************
BFIGURE_CYLINDER& CBoundingFigure::m_GetCylinder (unsigned int u) {
	if (u < m_uCount && m_CylinderList) {
		return m_CylinderList[u];
	}
	else {
		return m_bfCylinderVoid;
	}
}



//********************************************************************************
//Imposta il numero di bounding figures che l'oggetto dovr� contenere. Se
//viene specificato 0, le liste di bounding figure vengono rimosse. Tutta la
//memoria allocata da questa funzione viene impostata su 0, pertanto il
//ridimensionamento delle liste interne risulter� in una perdita di dati.
//Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CBoundingFigure::SetFigureCount (unsigned int uCount, DWORD dwFigs) {
	SAFE_DELETE (m_SphereList);
	SAFE_DELETE (m_BoxList);
	SAFE_DELETE (m_CylinderList);
	
	m_uCount = uCount;
	if (uCount == 0)
		return ZERO;
	
	if (dwFigs & BFSFC_SPHERE) {
		m_SphereList = new BFIGURE_SPHERE[uCount];
		if (m_SphereList == NULL)
			goto Error;
	}
	if (dwFigs & BFSFC_BOX) {
		m_BoxList = new BFIGURE_BOX[uCount];
		if (m_BoxList == NULL)
			goto Error;
	}
	if (dwFigs & BFSFC_CYLINDER) {
		m_CylinderList = new BFIGURE_CYLINDER[uCount];
		if (m_CylinderList == NULL)
			goto Error;
	}
	
	m_uCount = uCount;
	m_dwFiguresUsed = dwFigs;
	return ZERO;
Error:
	SetFigureCount (0, 0);
	return NONZERO;
}



//********************************************************************************
//Vedi IntersectPlane()
//********************************************************************************
BFIGURE_INTERSECTION CBoundingFigure::m_SphereIntersectPlane (unsigned int u, const LPMATH_PLANE plane) {
	if (plane == NULL || u >= m_uCount)
		return BFINTS_POSITIVESIDE;
	
	MATH_QUATERNION quatCenter;
	MATH_SETQUATERNIONXYZ (quatCenter, m_SphereList[u].vecCenter.x, m_SphereList[u].vecCenter.y, m_SphereList[u].vecCenter.z);
	m_math.QuatTransform (&quatCenter, &matTransform);
	
	const float fDist = plane->d +
						plane->a * quatCenter.x +
						plane->b * quatCenter.y +
						plane->c * quatCenter.z;
	
	const float fAbsDist = fabs(fDist);
	
	//Intersezione
	if (fAbsDist < m_SphereList[u].radius) {
		return BFINTS_INTERSECTION;
	}
	//Nessuna intersezione
	else if (fAbsDist > m_SphereList[u].radius) {
		if (fDist > 0.0f)
			return BFINTS_POSITIVESIDE;
		else
			return BFINTS_NEGATIVESIDE;
	}
	//Sfera e piano si toccano
	else {
		if (fDist > 0.0f)
			return BFINTS_POSITIVETOUCH;
		else
			return BFINTS_NEGATIVETOUCH;
	}
}



//********************************************************************************
//Vedi IntersectPlane()
//********************************************************************************
BFIGURE_INTERSECTION CBoundingFigure::m_BoxIntersectPlane (unsigned int u, const LPMATH_PLANE plane) {
	if (plane == NULL || u >= m_uCount)
		return BFINTS_POSITIVESIDE;
	
	const float fHalfWidth = m_BoxList[u].width / 2.0f;
	const float fHalfHeight = m_BoxList[u].height / 2.0f;
	const float fHalfDepth = m_BoxList[u].depth / 2.0f;
//	MATH_QUATERNION quatVertex;
	MATH_3DVECTOR vecVertex;
	int n;
	float fDist;
	DWORD dwState = 0;
	
//	quatVertex.w = 1.0f;
	for (DWORD dwSgn = 0; dwSgn < 8; dwSgn++) {
		n = *(int*)&fHalfWidth;
		n |= ((dwSgn & 1) << 31);
		vecVertex.x = *(float*)&n + m_BoxList[u].vecCenter.x;
		
		n = *(int*)&fHalfHeight;
		n |= ((dwSgn & 2) << 30);
		vecVertex.y = *(float*)&n + m_BoxList[u].vecCenter.y;
		
		n = *(int*)&fHalfDepth;
		n |= ((dwSgn & 4) << 29);
		vecVertex.z = *(float*)&n + m_BoxList[u].vecCenter.z;
/*		//Calcolo un vertice del solido
		MATH_SET3DVECTOR (vecVertex, m_BoxList[u].vecCenter.x + fHalfWidth,
									 m_BoxList[u].vecCenter.y + fHalfHeight,
									 m_BoxList[u].vecCenter.z + fHalfDepth);
		//Calcolo il segno celle coordinate per questa iterazione del for
		//Il valore calcolato lo copio direttamente in quatVertex
		n = *(int*)&vecVertex.x;
		n |= ((dwSgn & 1) << 31);
		quatVertex.x = *(float*)&n;
		n = *(int*)&vecVertex.y;
		n |= ((dwSgn & 2) << 30);
		quatVertex.y = *(float*)&n;
		n = *(int*)&vecVertex.z;
		n |= ((dwSgn & 4) << 29);
		quatVertex.z = *(float*)&n;
*/		
		m_math.Vec3Transform (&vecVertex, &matTransform);
		
		fDist = plane->a * vecVertex.x + plane->b * vecVertex.y + plane->c * vecVertex.z + plane->d;
		if (fDist > 0.0f)
			dwState |= STATE_LIESPOSITIVE;
		else if (fDist < 0.0f)
			dwState |= STATE_LIESNEGATIVE;
		else
			dwState |= STATE_LIESOVER;
	}
	
	//Intersezione
	if ((dwState & ~STATE_LIESOVER) == (STATE_LIESPOSITIVE | STATE_LIESNEGATIVE))
		return BFINTS_INTERSECTION;
	//Si toccano
	else if (dwState & STATE_LIESOVER) {
		if (dwState & STATE_LIESPOSITIVE)
			return BFINTS_POSITIVETOUCH;
		else
			return BFINTS_NEGATIVETOUCH;
	}
	//Nessuna intersezione
	else {
		if (dwState & STATE_LIESPOSITIVE)
			return BFINTS_POSITIVESIDE;
		else
			return BFINTS_NEGATIVESIDE;
	}
}



//********************************************************************************
//Vedi IntersectPlane()
//********************************************************************************
BFIGURE_INTERSECTION CBoundingFigure::m_CylinderIntersectPlane (unsigned int u, const LPMATH_PLANE plane) {
	if (plane == NULL || u >= m_uCount)
		return BFINTS_POSITIVESIDE;
	return BFINTS_POSITIVESIDE;
}



//********************************************************************************
//Informa la classe che l'elemento u della lista interna sta venendo aggiornato.
//� necessario chiamare questa funzione prima di poter assegnare un valore
//tramite l'operatore di assegnazione. La funzione azzera anche i valori
//correntemente impostati nelle bounding figures che si stanno per aggiornare, a
//meno che non venga specificato diversamente in bReset.
//Restituisce ZERO se va tutto bene,
//NONZERO se l'elemento u non esiste in elenco o se � gi� in corso
//un'altra operazione di aggiornamento. 
//********************************************************************************
int CBoundingFigure::BeginCalc (unsigned int u, bool bReset) {
	if (m_bCalculating || u >= m_uCount)
		return NONZERO;
	
	//FLT_MAX
	//FLT_MIN
	
	m_bCalculating = TRUE;
	m_uCurrentFig = u;
	MATH_SET3DVECTOR (m_vecMax, -INFINITY, -INFINITY, -INFINITY);
	MATH_SET3DVECTOR (m_vecMin, INFINITY, INFINITY, INFINITY);
	return ZERO;
}



//********************************************************************************
//Informa la classe che l'operazione di aggiornamento in corso termina.
//Restituisce ZERO.
//********************************************************************************
int CBoundingFigure::EndCalc () {
	MATH_3DVECTOR vecTmp;
	MATH_3DVECTOR vecCent;
	CMathEngine math;
	
	//Il centro della figura � (vecMax - vecMin) / 2 + vecMin
	math.Vec3Subtract (&vecTmp, &m_vecMax, &m_vecMin);
	math.Vec3Scale (&vecTmp, 0.5f);
	math.Vec3Sum (&vecCent, &vecTmp, &m_vecMin);
	
	if (m_bCalculating) {
		if (m_dwFiguresUsed & BFSFC_SPHERE) {
/*			m_SphereList[m_uCurrentFig].radius = 
				MATH_LENGHT3DVECTOR (*math.Vec3Scale (
				math.Vec3Subtract (&vecTmp, &m_vecMax, &m_vecMin),
				0.5f));
*/
			const float i = MATH_LENGHT3DVECTOR (*math.Vec3Subtract (&vecTmp, &m_vecMax, &m_vecMin));
			// = i * sqrtf(2.0f) / 4.0f;
			m_SphereList[m_uCurrentFig].radius = i * 0.3535533f;
			MATH_COPY3DVECTOR (m_SphereList[m_uCurrentFig].vecCenter, vecCent);
		}
		
		if (m_dwFiguresUsed & BFSFC_BOX) {
			m_BoxList[m_uCurrentFig].width = m_vecMax.x - m_vecMin.x;
			m_BoxList[m_uCurrentFig].height = m_vecMax.y - m_vecMin.y;
			m_BoxList[m_uCurrentFig].depth = m_vecMax.z - m_vecMin.z;
			MATH_COPY3DVECTOR (m_BoxList[m_uCurrentFig].vecCenter, vecCent);
		}
		
		if (m_dwFiguresUsed & BFSFC_CYLINDER) {
			MATH_2DVECTOR vecMin, vecMax;
			MATH_2DVECTOR vecTmp;
			
			MATH_SET2DVECTOR (vecMin, m_vecMin.x, m_vecMin.z);
			MATH_SET2DVECTOR (vecMax, m_vecMax.x, m_vecMax.z);
			
			const float i = MATH_LENGHT2DVECTOR (*math.Vec2Subtract (&vecTmp, &vecMax, &vecMin));
			m_CylinderList[m_uCurrentFig].height = m_vecMax.y - m_vecMin.y;
			m_CylinderList[m_uCurrentFig].radius = i * 0.3535533f;
			MATH_COPY3DVECTOR (m_CylinderList[m_uCurrentFig].vecCenter, vecCent);
		}
		
		m_bCalculating = FALSE;
	}
	
	return ZERO;
}


//TODO: Eliminare il switch
//********************************************************************************
//Restituisce larghezza, altezza e profondit� della bounding figure corrente.
//Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CBoundingFigure::GetMeasures (unsigned int u, float* fWidth, float* fHeight, float* fDepth) {
	if (u >= m_uCount)
		return NONZERO;
	
	LPBFIGURE_BOX pbfBox;
	LPBFIGURE_SPHERE pbfSphere;
//	LPBFIGURE_CYLINDER pbfCylinder;
	
	switch (m_bftCurrent) {
	case BFTYPE_BOX:
		pbfBox = (LPBFIGURE_BOX)&(this->*m_GetFigure)(u);
		if (fWidth)
			*fWidth = pbfBox->width;
		if (fHeight)
			*fHeight = pbfBox->height;
		if (fDepth)
			*fDepth = pbfBox->depth;
		break;
	
	case BFTYPE_SPHERE:
		pbfSphere = (LPBFIGURE_SPHERE)&(this->*m_GetFigure)(u);
		if (fWidth)
			*fWidth = pbfSphere->radius * 2.0f;
		if (fHeight)
			*fHeight = pbfSphere->radius * 2.0f;
		if (fDepth)
			*fDepth = pbfSphere->radius * 2.0f;
		break;
	
	default:
		return NONZERO;
	}
	return ZERO;
}