//Constants.h

//Definire USERES_640, USERES_800 o USE_RES1024 per compilare facendo uso
//della risoluzione indicata dalla costante

#ifndef _INCL_CONSTANTS_H
#define _INCL_CONSTANTS_H

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef MIN
#define MIN(a,b) ( (a) < (b) ? (a) : (b) )
#endif
#ifndef MAX
#define MAX(a,b) ( (a) > (b) ? (a) : (b) )
#endif

#define DEF_STR_LEN 1024

//#ifndef FULL_SCREEN
//#define FULL_SCREEN
//#endif

//1024x768 (valido solo se FULL_SCREEN � definito)
#ifdef USERES_1024
#ifdef FULL_SCREEN
#define AREA_WIDTH 1024
#define AREA_HEIGHT 768
#define AREA_WIDTH_FLOAT 1024.0f
#define AREA_HEIGHT_FLOAT 768.0f
//FULL_SCREEN non definito
#else
#warning "Unable to use 1024x768 resolution, since FULL_SCREEN is undefined. Switching to 800x600."
#define AREA_WIDTH 800
#define AREA_HEIGHT 600
#define AREA_WIDTH_FLOAT 800.0f
#define AREA_HEIGHT_FLOAT 600.0f
#endif //ifdef FULL_SCREEN
//640x480
#elif defined USERES_640
#define AREA_WIDTH 640
#define AREA_HEIGHT 480
#define AREA_WIDTH_FLOAT 640.0f
#define AREA_HEIGHT_FLOAT 480.0f
//800x600
#elif defined USERES_800
#define AREA_WIDTH 800
#define AREA_HEIGHT 600
#define AREA_WIDTH_FLOAT 800.0f
#define AREA_HEIGHT_FLOAT 600.0f
#else
#warning "No resolution defined. Defaulting to 800x600."
#define AREA_WIDTH 800
#define AREA_HEIGHT 600
#define AREA_WIDTH_FLOAT 800.0f
#define AREA_HEIGHT_FLOAT 600.0f
#endif

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(obj) { if ((obj) != NULL) { (obj)->Release(); (obj) = NULL; } }
#endif
#ifndef SAFE_DELETE
#define SAFE_DELETE(arr) { if ((arr) != NULL) { delete[] (arr); (arr) = NULL; } }
#endif

#ifndef ZERO
#define ZERO 0
#endif
#ifndef NONZERO
#define NONZERO (-1)
#endif

#endif