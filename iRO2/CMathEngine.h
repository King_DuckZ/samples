//CMathEngine.h


#ifndef _INCL_CMATHENGINE_H
#define _INCL_CMATHENGINE_H

#include <math.h>

#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

typedef struct STRUCT_MATH_2DVECTOR {
	union {
		struct {
			float x, y;
		};
		float f[2];
	};
} MATH_2DVECTOR, *LPMATH_2DVECTOR;
typedef struct STRUCT_MATH_3DVECTOR {
	union {
		struct {
			float x, y, z;
		};
		float f[3];
	};
} MATH_3DVECTOR, *LPMATH_3DVECTOR;
typedef struct STRUCT_MATH_QUATERNION {
	union {
		struct {
			float x, y, z, w; //<x,y,z> rappresenta la parte immaginaria, w la parte reale
		};
		struct {
			MATH_3DVECTOR qv; //Parte immaginaria
			float q0; //Parte reale
		};
		float f[4];
	};
} MATH_QUATERNION, *LPMATH_QUATERNION;
typedef struct STRUCT_MATH_SPHERICAL {
	float rho;
	float phi;
	float theta;
} MATH_SPHERICAL, *LPMATH_SPHERICAL;
typedef struct STRUCT_MATH_MATRIX16 {
	union {
		struct {
			float _00, _01, _02, _03;
			float _10, _11, _12, _13;
			float _20, _21, _22, _23;
			float _30, _31, _32, _33;
		};
		float M[4][4];
		struct {
			MATH_QUATERNION r0, r1, r2, r3;
		};
	};
} MATH_MATRIX16, *LPMATH_MATRIX16;
typedef struct STRUCT_MATH_PLANE {
	float a, b, c, d;
} MATH_PLANE, *LPMATH_PLANE;


//Angoli
#define MATH_PI (3.14159265359f)
#define MATH_2PI (6.28318530718f)
#define MATH_DEGTORAD(deg) ( (deg) * (MATH_PI / 180.0f) )
#define MATH_RADTODEG(rad) ( (rad) * (180.0f / MATH_PI) )

//Inizializzazione struct
#define MATH_INIT2DVECTOR(vector) ( (vector).x = (vector).y = 0.0f )
#define MATH_INIT3DVECTOR(vector) ( (vector).x = (vector).y = (vector).z = 0.0f )
#define MATH_INITQUATERNION(quat) { (quat). x = (quat).y = (quat).z = 0.0f; (quat).w = 1.0f; }
#define MATH_INITSPHERICAL(s) ( (s).rho = (s).phi = (s).theta = 0.0f )
#define MATH_INITMATRIX16(m) { (m)._00 = (m)._01 = (m)._02 = (m)._03 = 0.0f; (m)._10 = (m)._11 = (m)._12 = (m)._13 = 0.0f; (m)._20 = (m)._21 = (m)._22 = (m)._23 = 0.0f; (m)._30 = (m)._31 = (m)._32 = (m)._33 = 0.0f; }
#define MATH_INITPLANE(p) { (p).a = (p).b = (p).c = (p).d = 0.0f; }

//Valorizzazione variabili
#define MATH_SET2DVECTOR(vector,vx,vy) { (vector).x = (vx); (vector).y = (vy); }
#define MATH_SET3DVECTOR(vector,vx,vy,vz) { (vector).x = (vx); (vector).y = (vy); (vector).z = (vz); }
#define MATH_SETQUATERNION(quat,q0,q1,q2,q3) { (quat).x = (q0); (quat).y = (q1); (quat).z = (q2); (quat).w = (q3); }
#define MATH_SETQUATERNIONXYZ(quat,x,y,z) MATH_SETQUATERNION(quat,x,y,z,1.0f)
#define MATH_SETSPHERICAL(s,r,f,t) { (s).rho = (r); (s).phi = (f); (s).theta = t; }
#define MATH_SETPLANE(p,a1,b1,c1,d1) { (p).a = a1; (p).b = b1; (p).c = c1; (p).d = d1; }

#define MATH_COPY2DVECTOR(dst,src) { (dst).x = (src).x; (dst).y = (src).y; }
#define MATH_COPY3DVECTOR(dst,src) { (dst).x = (src).x; (dst).y = (src).y; (dst).z = (src).z; }
#define MATH_COPYQUATERNION(dst,src) { (dst).x = (src).x; (dst).y = (src).y; (dst).z = (src).z; (dst).w = (src).w; }
#define MATH_COPYSPHERICAL(dst,src) { (dst).rho = (src).rho; (dst).phi = (src).phi; (dst).theta = (src).theta; }
#define MATH_COPYPLANE(dst,src) { (dst). a = (src).a; (dst).b = (src).b; (dst).c = (src).c; (dst).d = (src).d; }

//Lunghezza di un vettore
#define MATH_LENGHT2DVECTOR(vector) (sqrtf( (vector).x * (vector).x + (vector).y * (vector).y ))
#define MATH_LENGHT3DVECTOR(vector) (sqrtf( (vector).x * (vector).x + (vector).y * (vector).y + (vector).z * (vector).z ))
#define MATH_LENGHTQUATERNION(quat) (sqrtf( (quat).x * (quat).x + (quat).y * (quat).y + (quat).z * (quat).z + (quat).w * (quat).w) )

//Varie
#define MATH_SWITCH(a,b,type) { const type math_switch_tmp = (a); (a) = (b); (b) = math_switch_tmp; }

class CMathEngine {
public:
	CMathEngine ( void );
	~CMathEngine ( void );
	
	static float* CosTable;
	static float* SinTable;
	static unsigned int nTableEntries;
	
	inline void Vec2Normalize ( LPMATH_2DVECTOR v );
	inline float Vec2Dot ( const LPMATH_2DVECTOR u, const LPMATH_2DVECTOR v );
	inline LPMATH_2DVECTOR Vec2Subtract ( LPMATH_2DVECTOR dst, const LPMATH_2DVECTOR v1, const LPMATH_2DVECTOR v2 );
	inline LPMATH_2DVECTOR Vec2Sum ( LPMATH_2DVECTOR dst, const LPMATH_2DVECTOR v1, const LPMATH_2DVECTOR v2 );
	inline LPMATH_2DVECTOR Vec2Scale ( LPMATH_2DVECTOR v, float f );
	inline LPMATH_2DVECTOR Vec2Multiply ( LPMATH_2DVECTOR dst, const LPMATH_2DVECTOR v1, const LPMATH_2DVECTOR v2 );
	inline LPMATH_2DVECTOR Vec2Divide ( LPMATH_2DVECTOR dst, const LPMATH_2DVECTOR v1, const LPMATH_2DVECTOR v2 );
	
	inline void Vec3Normalize ( LPMATH_3DVECTOR v );
	inline LPMATH_3DVECTOR Vec3Cross ( LPMATH_3DVECTOR dst, const LPMATH_3DVECTOR u, const LPMATH_3DVECTOR v );
	inline float Vec3Dot ( const LPMATH_3DVECTOR u, const LPMATH_3DVECTOR v );
	inline LPMATH_3DVECTOR Vec3Subtract ( LPMATH_3DVECTOR dst, const LPMATH_3DVECTOR v1, const LPMATH_3DVECTOR v2 );
	inline LPMATH_3DVECTOR Vec3Sum ( LPMATH_3DVECTOR dst, const LPMATH_3DVECTOR v1, const LPMATH_3DVECTOR v2 );
	inline LPMATH_3DVECTOR Vec3Scale ( LPMATH_3DVECTOR v, float f );
	inline LPMATH_3DVECTOR Vec3Multiply ( LPMATH_3DVECTOR dst, const LPMATH_3DVECTOR v1, const LPMATH_3DVECTOR v2 );
	inline LPMATH_3DVECTOR Vec3Divide ( LPMATH_3DVECTOR dst, const LPMATH_3DVECTOR v1, const LPMATH_3DVECTOR v2 );
	LPMATH_3DVECTOR Vec3Transform ( LPMATH_3DVECTOR vecDst, const LPMATH_MATRIX16 mat );
	
	LPMATH_QUATERNION QuatTransform ( LPMATH_QUATERNION dst, const LPMATH_MATRIX16 matTransf );
	inline LPMATH_QUATERNION QuatGetConjugate ( LPMATH_QUATERNION dst, const LPMATH_QUATERNION quat );
	inline LPMATH_QUATERNION QuatNormalize ( LPMATH_QUATERNION quatDst );
	inline LPMATH_QUATERNION QuatMultiply ( LPMATH_QUATERNION quatDst, const LPMATH_QUATERNION quatA, const LPMATH_QUATERNION quatB );
	inline LPMATH_QUATERNION QuatRotation ( LPMATH_QUATERNION quatDst, float fYaw, float fPitch, float fRoll );
	inline LPMATH_QUATERNION QuatRotationY ( LPMATH_QUATERNION quatDst, float fYaw );
	
	inline void Mat16Identity ( LPMATH_MATRIX16 m );
	LPMATH_MATRIX16 Mat16Multiply ( LPMATH_MATRIX16 matDst, const LPMATH_MATRIX16 mat1, const LPMATH_MATRIX16 mat2 );
	inline LPMATH_MATRIX16 Mat16Translation ( LPMATH_MATRIX16 dst, float x, float y, float z );
	inline LPMATH_MATRIX16 Mat16TransformScale ( LPMATH_MATRIX16, float x, float y, float z );
	int Mat16ProjectionLH ( LPMATH_MATRIX16 m, float width, float height, float NearPlane, float FarPlane );
	inline void Mat16Copy ( LPMATH_MATRIX16 matDst, const LPMATH_MATRIX16 matSrc );
	inline LPMATH_MATRIX16 Mat16TransformRotateX ( LPMATH_MATRIX16 matDst, float fRad );
	inline LPMATH_MATRIX16 Mat16TransformRotateY ( LPMATH_MATRIX16 matDst, float fRad );
	inline LPMATH_MATRIX16 Mat16TransformRotateZ ( LPMATH_MATRIX16 matDst, float fRad );
	
	LPMATH_3DVECTOR PlaneIntersectLine ( LPMATH_3DVECTOR pt, const LPMATH_PLANE plane, const LPMATH_3DVECTOR v1, const LPMATH_3DVECTOR v2 );
	inline LPMATH_PLANE PlaneBuildEquation ( LPMATH_PLANE dst, const LPMATH_3DVECTOR n, const LPMATH_3DVECTOR p );
	inline LPMATH_PLANE PlaneBuildEquation ( LPMATH_PLANE dst, const LPMATH_3DVECTOR p1, const LPMATH_3DVECTOR p2, const LPMATH_3DVECTOR p3 );
	
	float GetInterpolatedSine ( float fRad );
	float GetInterpolatedCosine ( float fRad );
	int SphericalToCartesian ( const LPMATH_SPHERICAL s, LPMATH_3DVECTOR c );
	inline void SwapDouble ( double* a, double* b );
	inline void SwapDWord ( void* a, void* b );
	
private:
	static unsigned int m_nRefCount;
	
	inline float m_GetInterpolatedFunc ( float fRad, const float* fTable );
};



//********************************************************************************
//Normalizza un vettore 2D.
//********************************************************************************
inline void CMathEngine::Vec2Normalize (LPMATH_2DVECTOR v) {
	const float fLen = MATH_LENGHT2DVECTOR (*v);
	
	v->x /= fLen;
	v->y /= fLen;
}



//********************************************************************************
//Normalizza un vettore 3D.
//********************************************************************************
inline void CMathEngine::Vec3Normalize (LPMATH_3DVECTOR v) {
	const float fLen = MATH_LENGHT3DVECTOR (*v);
	
	v->x /= fLen;
	v->y /= fLen;
	v->z /= fLen;
}



//********************************************************************************
//Imposta la matrice specificata in modo che diventi una identity matrix.
//********************************************************************************
inline void CMathEngine::Mat16Identity (LPMATH_MATRIX16 m) {
	m->_01 = m->_02 = m->_03 = m->_10 = 0.0f;
	m->_12 = m->_13 = m->_20 = m->_21 = 0.0f;
	m->_23 = m->_30 = m->_31 = m->_32 = 0.0f;

	m->_00 = m->_11 = m->_22 = m->_33 = 1.0f;
}



//********************************************************************************
//Scambia i valori di a e b, facendo uso dell'FPU.
//Non effettua controlli sui puntatori.
//********************************************************************************
inline void CMathEngine::SwapDouble (double* a, double* b) {
	asm {
		MOV EAX, a
		MOV EBX, b
		FLD dword ptr [EAX]
		FLD dword ptr [EBX]
		FSTP dword ptr [EAX]
		FSTP dword ptr [EBX]
	}
}



//********************************************************************************
//Scambia i valori di a e b. Non effettua controlli sui puntatori, e presuppone
//che entrambe le zone di memoria siano lunghe almeno 4 byte.
//********************************************************************************
inline void CMathEngine::SwapDWord (void* a, void* b) {
	asm {
		MOV EAX, a
		MOV EBX, b
		PUSH dword ptr [EBX]
		PUSH dword ptr [EAX]
		POP dword ptr [EBX]
		POP dword ptr [EAX]
	}
}



//********************************************************************************
//Calcola il cross product fra i vettori specificati e restituisce in *dst
//il risultato. Restituisce dst se va tutto bene, altrimenti NULL.
//********************************************************************************
inline LPMATH_3DVECTOR CMathEngine::Vec3Cross (LPMATH_3DVECTOR dst, const LPMATH_3DVECTOR u, const LPMATH_3DVECTOR v) {
	if ( (dst && u && v) == FALSE)
		return NULL;
	dst->x = u->y * v->z - v->y * u->z;
	dst->y = v->x * u->z - u->x * v->z;
	dst->z = u->x * v->y - v->x * u->y;
	
	return dst;
}



//********************************************************************************
//Calcola il dot product fra i vettori specificati e restituisce il risultato.
//Restituisce 0.0f in caso di errore.
//********************************************************************************
inline float CMathEngine::Vec3Dot (const LPMATH_3DVECTOR u, const LPMATH_3DVECTOR v) {
	if ( (u && v) == FALSE)
		return 0.0f;
	
	return (u->x * v->x + u->y * v->y + u->z * v->z);
}



//********************************************************************************
//Calcola il dot product fra i vettori specificati e restituisce il risultato.
//Restituisce 0.0f in caso di errore.
//********************************************************************************
inline float CMathEngine::Vec2Dot (const LPMATH_2DVECTOR u, const LPMATH_2DVECTOR v) {
	if ( (u && v) == FALSE)
		return 0.0f;
	
	return (u->x * v->x + u->y * v->y);
}



//********************************************************************************
//Calcola la differenza fra i vettori specificati e restituisce in *dst
//il risultato. Restituisce dst se va tutto bene, altrimenti NULL.
//********************************************************************************
inline LPMATH_3DVECTOR CMathEngine::Vec3Subtract (LPMATH_3DVECTOR dst, const LPMATH_3DVECTOR v1, const LPMATH_3DVECTOR v2) {
	if ( (dst && v1 && v2) == FALSE )
		return NULL;
	
	dst->x = v1->x - v2->x;
	dst->y = v1->y - v2->y;
	dst->z = v1->z - v2->z;
	return dst;
}



//********************************************************************************
//Calcola la differenza fra i vettori specificati e restituisce in *dst
//il risultato. Restituisce dst se va tutto bene, altrimenti NULL.
//********************************************************************************
inline LPMATH_2DVECTOR CMathEngine::Vec2Subtract (LPMATH_2DVECTOR dst, const LPMATH_2DVECTOR v1, const LPMATH_2DVECTOR v2) {
	if ( (dst && v1 && v2) == FALSE )
		return NULL;
	
	dst->x = v1->x - v2->x;
	dst->y = v1->y - v2->y;
	return dst;
}



//********************************************************************************
//Calcola i coefficienti e il termine noto della forma generale dell'equazione
//del piano identificato dalla normale al piano stesso e da un punto
//appartenente al piano. Restituisce in *dst il piano e restituisce dst se
//va tutto bene, altrimenti restituisce NULL.
//********************************************************************************
inline LPMATH_PLANE CMathEngine::PlaneBuildEquation (LPMATH_PLANE dst, const LPMATH_3DVECTOR n, const LPMATH_3DVECTOR p) {
	if ( (dst && n && p) == FALSE )
		return NULL;
	
	dst->a = n->x;
	dst->b = n->y;
	dst->c = n->z;
	dst->d = -n->x * p->x - n->y * p->y - n->z * p->z;
	
	return dst;
}



//********************************************************************************
//Scala il vettore specificato per lo scalare f e restituisce in *v
//il risultato. Restituisce v se va tutto bene, altrimenti NULL.
//********************************************************************************
inline LPMATH_3DVECTOR CMathEngine::Vec3Scale (LPMATH_3DVECTOR v, float f) {
	if (v == NULL)
		return NULL;
	
	v->x *= f;
	v->y *= f;
	v->z *= f;
	return v;
}



//********************************************************************************
//Scala il vettore specificato per lo scalare f e restituisce in *v
//il risultato. Restituisce v se va tutto bene, altrimenti NULL.
//********************************************************************************
inline LPMATH_2DVECTOR CMathEngine::Vec2Scale (LPMATH_2DVECTOR v, float f) {
	if (v == NULL)
		return NULL;
	
	v->x *= f;
	v->y *= f;
	return v;
}



//********************************************************************************
//Calcola la somma fra i vettori specificati e restituisce in *dst
//il risultato. Restituisce dst se va tutto bene, altrimenti NULL.
//********************************************************************************
inline LPMATH_3DVECTOR CMathEngine::Vec3Sum (LPMATH_3DVECTOR dst, const LPMATH_3DVECTOR v1, const LPMATH_3DVECTOR v2) {
	if ( (dst && v1 && v2) == FALSE	)
		return NULL;
	
	dst->x = v1->x + v2->x;
	dst->y = v1->y + v2->y;
	dst->z = v1->z + v2->z;
	
	return dst;
}



//********************************************************************************
//Calcola la somma fra i vettori specificati e restituisce in *dst
//il risultato. Restituisce dst se va tutto bene, altrimenti NULL.
//********************************************************************************
inline LPMATH_2DVECTOR CMathEngine::Vec2Sum (LPMATH_2DVECTOR dst, const LPMATH_2DVECTOR v1, const LPMATH_2DVECTOR v2) {
	if ( (dst && v1 && v2) == FALSE	)
		return NULL;
	
	dst->x = v1->x + v2->x;
	dst->y = v1->y + v2->y;
	
	return dst;
}



//********************************************************************************
//Calcola i coefficienti e il termine noto della forma generale dell'equazione
//del piano identificato da tre punti appartenenti al piano. Restituisce
//in *dst il piano e restituisce dst se va tutto bene, altrimenti
//restituisce NULL.
//********************************************************************************
inline LPMATH_PLANE CMathEngine::PlaneBuildEquation (LPMATH_PLANE dst, const LPMATH_3DVECTOR p1, const LPMATH_3DVECTOR p2, const LPMATH_3DVECTOR p3) {
	if ( (dst && p1 && p2 && p3) ==FALSE )
		return NULL;
	
	MATH_3DVECTOR n;
	
	Vec3Cross (&n, p1, p2);
	dst->a = n.x;
	dst->b = n.y;
	dst->c = n.z;
	dst->d = -n.x * p3->x - n.y * p3->y - n.z * p3->z;
	
	return dst;
}



//********************************************************************************
//Calcola il prodotto fra i vettori specificati e restituisce in *dst
//il risultato. Restituisce dst se va tutto bene, altrimenti NULL.
//********************************************************************************
inline LPMATH_3DVECTOR CMathEngine::Vec3Multiply (LPMATH_3DVECTOR dst, const LPMATH_3DVECTOR v1, const LPMATH_3DVECTOR v2) {
	if ( (dst && v1 && v2) == FALSE )
		return NULL;
	
	dst->x = v1->x * v2->x;
	dst->y = v1->y * v2->y;
	dst->z = v1->z * v2->z;
	
	return dst;
}



//********************************************************************************
//Calcola il prodotto fra i vettori specificati e restituisce in *dst
//il risultato. Restituisce dst se va tutto bene, altrimenti NULL.
//********************************************************************************
inline LPMATH_2DVECTOR CMathEngine::Vec2Multiply (LPMATH_2DVECTOR dst, const LPMATH_2DVECTOR v1, const LPMATH_2DVECTOR v2) {
	if ( (dst && v1 && v2) == FALSE )
		return NULL;
	
	dst->x = v1->x * v2->x;
	dst->y = v1->y * v2->y;
	
	return dst;
}



//********************************************************************************
//Calcola il rapporto fra i vettori specificati e restituisce in *dst
//il risultato. Restituisce dst se va tutto bene, altrimenti NULL.
//********************************************************************************
inline LPMATH_3DVECTOR CMathEngine::Vec3Divide (LPMATH_3DVECTOR dst, const LPMATH_3DVECTOR v1, const LPMATH_3DVECTOR v2) {
	if ( (dst && v1 && v2) == FALSE )
		return NULL;
	
	dst->x = (v2->x != 0.0f ? v1->x / v2->x : 0.0f);
	dst->y = (v2->y != 0.0f ? v1->y / v2->y : 0.0f);
	dst->z = (v2->z != 0.0f ? v1->z / v2->z : 0.0f);
	
	return dst;
}



//********************************************************************************
//Calcola il rapporto fra i vettori specificati e restituisce in *dst
//il risultato. Restituisce dst se va tutto bene, altrimenti NULL.
//********************************************************************************
inline LPMATH_2DVECTOR CMathEngine::Vec2Divide (LPMATH_2DVECTOR dst, const LPMATH_2DVECTOR v1, const LPMATH_2DVECTOR v2) {
	if ( (dst && v1 && v2) == FALSE )
		return NULL;
	
	dst->x = (v2->x != 0.0f ? v1->x / v2->x : 0.0f);
	dst->y = (v2->y != 0.0f ? v1->y / v2->y : 0.0f);
	
	return dst;
}



//********************************************************************************
//Calcola il coniugato complesso di *quat e restituisce in *dst il risultato.
//Restituisce dst se � andato tutto bene, altrimenti NULL.
//********************************************************************************
inline LPMATH_QUATERNION CMathEngine::QuatGetConjugate (LPMATH_QUATERNION dst, const LPMATH_QUATERNION quat) {
	if ( (dst && quat) == FALSE )
		return NULL;
	
	dst->q0 = quat->q0;
	dst->qv.x = -quat->qv.x;
	dst->qv.y = -quat->qv.y;
	dst->qv.z = -quat->qv.z;
	return dst;
}



//********************************************************************************
//Imposta la matrice specificata in modo che rappresenti una traslazione come
//specificato in <x,y,z>. Restituisce dst, o NULL se il puntatore ricevuto
//non � valido.
//********************************************************************************
inline LPMATH_MATRIX16 CMathEngine::Mat16Translation (LPMATH_MATRIX16 dst, float x, float y, float z) {
	if (dst == NULL)
		return NULL;
	
	MATH_SETQUATERNION (dst->r0, 1.0f, 0.0f, 0.0f, 0.0f);
	MATH_SETQUATERNION (dst->r1, 0.0f, 1.0f, 0.0f, 0.0f);
	MATH_SETQUATERNION (dst->r2, 0.0f, 0.0f, 1.0f, 0.0f);
	MATH_SETQUATERNION (dst->r3,    x,    y,    z, 1.0f);
	
	return dst;
}



//********************************************************************************
//Imposta la matrice specificata in modo che rappresenti una scalatura
//come specificato in x, y e z. Restituisce mat se va tutto bene, altrimenti
//NULL.
//********************************************************************************
inline LPMATH_MATRIX16 CMathEngine::Mat16TransformScale (LPMATH_MATRIX16 mat, float x, float y, float z) {
	if (mat == NULL)
		return NULL;
	
	Mat16Identity (mat);
	mat->M[0][0] = x;
	mat->M[1][1] = y;
	mat->M[2][2] = z;
	
	return mat;
}



//********************************************************************************
//Copia la matrice matSrc in matDst.
//********************************************************************************
inline void CMathEngine::Mat16Copy (LPMATH_MATRIX16 matDst, const LPMATH_MATRIX16 matSrc) {
	if ( (matDst && matSrc) == FALSE )
		return;
	
	asm {
		MOV ECX, 0
		MOV EAX, matSrc
		MOV EBX, matDst
		Loop:
		
		PUSH dword ptr [EAX]
		POP dword ptr [EBX]
		
		ADD EAX, 4
		ADD EBX, 4
		
		INC ECX
		CMP ECX, 16
		JB Loop
	}
	return;
}



//********************************************************************************
//Crea una matrice di rotazione attorno all'asse delle ascisse. Restituisce
//matDst se va tutto bene, altrimenti NULL.
//********************************************************************************
inline LPMATH_MATRIX16 CMathEngine::Mat16TransformRotateX (LPMATH_MATRIX16 matDst, float fRad) {
	if (matDst == NULL)
		return NULL;
	Mat16Identity (matDst);
	matDst->_11 = matDst->_22 = GetInterpolatedCosine (fRad);
	matDst->_12 = GetInterpolatedSine (fRad);
	matDst->_21 = -matDst->_12;
	return matDst;
}



//********************************************************************************
//Crea una matrice di rotazione attorno all'asse delle ordinate. Restituisce
//matDst se va tutto bene, altrimenti NULL.
//********************************************************************************
inline LPMATH_MATRIX16 CMathEngine::Mat16TransformRotateY (LPMATH_MATRIX16 matDst, float fRad) {
	if (matDst == NULL)
		return NULL;
	Mat16Identity (matDst);
	matDst->_00 = matDst->_22 = GetInterpolatedCosine (fRad);
	matDst->_20 = GetInterpolatedSine (fRad);
	matDst->_02 = -matDst->_20;
	return matDst;
}



//********************************************************************************
//Crea una matrice di rotazione attorno all'asse delle oblique. Restituisce
//matDst se va tutto bene, altrimenti NULL.
//********************************************************************************
inline LPMATH_MATRIX16 CMathEngine::Mat16TransformRotateZ (LPMATH_MATRIX16 matDst, float fRad) {
	if (matDst == NULL)
		return NULL;
	Mat16Identity (matDst);
	matDst->_11 = matDst->_00 = GetInterpolatedCosine (fRad);
	matDst->_01 = GetInterpolatedSine (fRad);
	matDst->_10 = -matDst->_01;
	return matDst;
}



//********************************************************************************
//Normalizza un quaternione. Restituisce quatDst se va tutto bene,
//altrimenti NULL.
//********************************************************************************
inline LPMATH_QUATERNION CMathEngine::QuatNormalize (LPMATH_QUATERNION quatDst) {
	if (quatDst == NULL)
		return NULL;
	
	const float fLen = MATH_LENGHTQUATERNION (*quatDst);
	quatDst->x /= fLen;
	quatDst->y /= fLen;
	quatDst->z /= fLen;
	quatDst->w /= fLen;
	
	return quatDst;
}



//********************************************************************************
//Calcola il prodotto fra i quaternioni specificati e restituisce in *quatDst
//il risultato. Restituisce quatDst se va tutto bene, altrimenti NULL.
//********************************************************************************
inline LPMATH_QUATERNION CMathEngine::QuatMultiply (LPMATH_QUATERNION quatDst, const LPMATH_QUATERNION quatA, const LPMATH_QUATERNION quatB) {
	if ( (quatDst && quatA && quatB) == FALSE )
		return NULL;
	
	quatDst->q0 = (quatA->q0 * quatB->q0 - Vec3Dot(&quatA->qv, &quatB->qv));
	MATH_3DVECTOR vecA, vecB, vecCross;
	
	MATH_COPY3DVECTOR (vecA, quatA->qv);
	MATH_COPY3DVECTOR (vecB, quatB->qv);
	Vec3Scale (&vecA, quatB->q0);
	Vec3Scale (&vecB, quatA->q0);
	Vec3Cross (&vecCross, &quatA->qv, &quatB->qv);
	
	Vec3Sum (&quatDst->qv, &vecA, &vecB);
	Vec3Sum (&quatDst->qv, &quatDst->qv, &vecCross);
	
	return quatDst;
}



//********************************************************************************
//Crea un quaternione che rappresenta una rotazione definita dai tre
//angoli specificati. Restituisce quatDst se va tutto bene, altrimenti NULL.
//********************************************************************************
inline LPMATH_QUATERNION CMathEngine::QuatRotation (LPMATH_QUATERNION quatDst, float fYaw, float fPitch, float fRoll) {
	if (quatDst == NULL)
		return NULL;
	
	const float fCosHalfZ = GetInterpolatedCosine (fRoll / 2.0f);
	const float fSinHalfZ = GetInterpolatedSine (fRoll / 2.0f);
	const float fCosHalfX = GetInterpolatedCosine (fPitch / 2.0f);
	const float fSinHalfX = GetInterpolatedSine (fPitch / 2.0f);
	const float fCosHalfY = GetInterpolatedCosine (fYaw / 2.0f);
	const float fSinHalfY = GetInterpolatedSine (fYaw / 2.0f);
	
	quatDst->w = fCosHalfZ * fCosHalfY * fCosHalfX - fSinHalfZ * fSinHalfY * fSinHalfX;
	quatDst->x = fCosHalfZ * fCosHalfY * fSinHalfX + fSinHalfZ * fSinHalfY * fCosHalfX;
	quatDst->y = fCosHalfZ * fSinHalfY * fCosHalfX - fSinHalfZ * fCosHalfY * fSinHalfX;
	quatDst->z = fSinHalfZ * fCosHalfY * fCosHalfX + fCosHalfZ * fSinHalfY * fSinHalfX;
	
	return quatDst;
}



//********************************************************************************
//Crea un quaternione che rappresenta una rotazione attorno all'asse
//delle ordinate. Restituisce quatDst se va tutto bene, altrimenti NULL.
//********************************************************************************
inline LPMATH_QUATERNION CMathEngine::QuatRotationY (LPMATH_QUATERNION quatDst, float fYaw) {
	if (quatDst == NULL)
		return NULL;
	
	quatDst->w = GetInterpolatedCosine(fYaw / 2.0f);
	quatDst->x = 0.0f;
	quatDst->y = GetInterpolatedSine(fYaw / 2.0f);
	quatDst->z = 0.0f;
	
	return quatDst;
}


#endif