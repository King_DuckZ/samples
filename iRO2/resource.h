//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by res.rc
//
#define IDS_ERR_HWND                    1
#define IDS_ERR_D3DINIT                 2
#define IDS_ERR_TITLE                   3
#define IDS_ERR_GEOMETRY                4
#define IDS_ERR_RENDERING               5
#define IDS_ERR_DINPUT                  6
#define IDS_ERR_GENERIC                 7
#define IDS_ERR_VERTEXSHADER            8
#define IDS_ERR_TEXTURE                 9
#define IDS_ERR_LIGHT                   10
#define IDI_MAIN_ICON                   101

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
