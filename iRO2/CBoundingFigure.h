//CBoundingFigure.h


#ifndef _INCL_CBOUNDINGFIGURE_H
#define _INCL_CBOUNDINGFIGURE_H

#include "CMathEngine.h"

#define BFSFC_SPHERE 0x04
#define BFSFC_BOX 0x01
#define BFSFC_CYLINDER 0x02
#define BFSFC_NONE 0x00


struct BFIGURE_BASEFIGURE {
	MATH_3DVECTOR vecCenter;
};
typedef struct STRUCT_BFIGURE_BOX : public BFIGURE_BASEFIGURE {
	union {
		struct {
			float width;
			float height;
			float depth;
		};
		MATH_3DVECTOR vec;
	};
} BFIGURE_BOX, *LPBFIGURE_BOX;
typedef struct STRUCT_BFIGURE_SPHERE : public BFIGURE_BASEFIGURE {
	float radius;
} BFIGURE_SPHERE, *LPBFIGURE_SPHERE;
typedef struct STRUCT_BFIGURE_CYLINDER : public BFIGURE_BASEFIGURE {
	float radius;
	float height;
} BFIGURE_CYLINDER, *LPBFIGURE_CYLINDER;


enum BFIGURE_BOUNDINGTYPE {
	BFTYPE_SPHERE,
	BFTYPE_BOX,
	BFTYPE_CYLINDER,
	BFTYPE_FORCEDWORD = 0xFFFFFFFF
};
enum BFIGURE_INTERSECTION {
	BFINTS_POSITIVESIDE, //nessuna intersezione
	BFINTS_NEGATIVESIDE,
	BFINTS_INTERSECTION, //intersezione
	BFINTS_POSITIVETOUCH, //bounding figure e piano si toccano
	BFINTS_NEGATIVETOUCH,
	BFINTS_FORCEDWORD = 0xFFFFFFFF
};

class CBoundingFigure {
public:
	CBoundingFigure ( void );
	~CBoundingFigure ( void );
	void Dispose ( void );
	
	int UsingFigure ( BFIGURE_BOUNDINGTYPE bfType );
	inline BFIGURE_BASEFIGURE& operator[] ( unsigned int u );
	int SetFigureCount ( unsigned int uCount, DWORD dwFigs );
	int BeginCalc ( unsigned int u, bool bReset = true );
	inline MATH_3DVECTOR& operator<< ( MATH_3DVECTOR& v );
	int EndCalc ( void );
	inline BFIGURE_INTERSECTION IntersectPlane ( unsigned int u, const LPMATH_PLANE plane );
	inline int GetCentre ( unsigned int u, LPMATH_3DVECTOR vCentre );
	int GetMeasures ( unsigned int u, float* fWidth, float* fHeight, float* fDepth );
	
	MATH_MATRIX16 matTransform;
private:
	inline void m_Reset ( void );
	inline void m_Dispose ( void );
	
	BFIGURE_SPHERE& m_GetSphere ( unsigned int u );
	BFIGURE_BOX& m_GetBox ( unsigned int u );
	BFIGURE_CYLINDER& m_GetCylinder ( unsigned int u );
	BFIGURE_INTERSECTION m_SphereIntersectPlane ( unsigned int u, const LPMATH_PLANE plane );
	BFIGURE_INTERSECTION m_BoxIntersectPlane ( unsigned int u, const LPMATH_PLANE plane );
	BFIGURE_INTERSECTION m_CylinderIntersectPlane ( unsigned int u, const LPMATH_PLANE plane );
	
	//Puntatori alle funzioni
	BFIGURE_BASEFIGURE& (CBoundingFigure::*m_GetFigure)(unsigned int);
	BFIGURE_INTERSECTION (CBoundingFigure::*m_IntersectPlane)(unsigned int, const LPMATH_PLANE);
	
	LPBFIGURE_SPHERE m_SphereList;
	LPBFIGURE_BOX m_BoxList;
	LPBFIGURE_CYLINDER m_CylinderList;
	
	//Variabili usate durante il calcolo delle bounding figures
	BOOL m_bCalculating;
	MATH_3DVECTOR m_vecMax; //Valori massimi incontrati per x, y e z
	MATH_3DVECTOR m_vecMin; //Valori minimi incontrati per x, y e z
	
	BFIGURE_BOUNDINGTYPE m_bftCurrent;
	unsigned int m_uCount;
	unsigned int m_uCurrentFig;
	DWORD m_dwFiguresUsed;
	
	BFIGURE_SPHERE m_bfSphereVoid;
	BFIGURE_BOX m_bfBoxVoid;
	BFIGURE_CYLINDER m_bfCylinderVoid;
	CMathEngine m_math;
};



//********************************************************************************
//Dopo aver chiamato BeginCalc(), � possibile assegnare alla classe un valore
//di tipo MATH_3DVECTOR tramite l'operatore =. Il vettore v viene interpretato
//come vertice, e viene usato per calcolare le bounding figures richieste
//tramite SetFigureCount(). Restituisce v.
//********************************************************************************
inline MATH_3DVECTOR& CBoundingFigure::operator<< (MATH_3DVECTOR& v) {
	if (v.x > m_vecMax.x)
		m_vecMax.x = v.x;
	if (v.y > m_vecMax.y)
		m_vecMax.y = v.y;
	if (v.z > m_vecMax.z)
		m_vecMax.z = v.z;
	
	if (v.x < m_vecMin.x)
		m_vecMin.x = v.x;
	if (v.y < m_vecMin.y)
		m_vecMin.y = v.y;
	if (v.z < m_vecMin.z)
		m_vecMin.z = v.z;
	
	return v;
}



//********************************************************************************
//Restituisce la bounding figure del tipo specificato tramite UsingFigure().
//Il tipo di default � BFTYPE_BOX. Restituisce una figura con tutti i valori
//impostati su 0 nel caso in cui l'indice specificato non sia valido.
//********************************************************************************
inline BFIGURE_BASEFIGURE& CBoundingFigure::operator[] (unsigned int u) {
	return (this->*m_GetFigure)(u);
}



//********************************************************************************
//Verifica l'intersezione fra l'elemento u della lista specificata tramite
//UsingFigure() e il piano specificato, restituendo uno dei valori
//BFIGURE_INTERSECTION.
//********************************************************************************
inline BFIGURE_INTERSECTION CBoundingFigure::IntersectPlane (unsigned int u, const LPMATH_PLANE plane) {
	return (this->*m_IntersectPlane)(u, plane);
}



//********************************************************************************
//Restiuisce in vCentre il centro della figura correntemente in uso.
//********************************************************************************
inline int CBoundingFigure::GetCentre (unsigned int u, LPMATH_3DVECTOR vCentre) {
	if (vCentre == NULL || u >= m_uCount)
		return NONZERO;
	
	*vCentre = ((this->*m_GetFigure)(u)).vecCenter;
	return ZERO;
}

#endif