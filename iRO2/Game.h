//Game.h


#ifndef _INCL_GAME_H
#define _INCL_GAME_H

enum GAME_FUNCTION {
	GF_UNDEFINED,
	GF_INGAME,
	GF_FORCEDWORD = 0xFFFFFFFF
};

int SelectGameFunction ( GAME_FUNCTION func, bool bReset = true );
int GameInit ( HWND hWnd );
int GameEnd ( void );

#endif