//CSolidCube.cpp

#define _DEFINE_FVF_SIMPLESOLID

#include "constants.h"
#include <windows.h>
#include "CSolidCube.h"


const DWORD VERTEX_COUNT = 24;
const DWORD INDEX_COUNT = 36;

SIMPLESOLID_VERTEX CSolidCube::m_vList[] = {
/*	1.0f, -1.0f, 1.0f, 0, 1.000488f, -0.997905f,
	-1.0f, 1.0f, 1.0f, 0, -0.000488f, -0.507640f,
	1.0f, 1.0f, 1.0f, 0, 1.000488f, -0.507640f,
	-1.0f, 1.0f, 1.0f, 0, -0.000488f, -0.507640f,
	1.0f, -1.0f, 1.0f, 0, 1.000488f, -0.997905f,
	-1.0f, -1.0f, 1.0f, 0, -0.000488f, -0.997905f,
	1.0f, -1.0f, -1.0f, 0, 0.999686f, -0.997905f,
	1.0f, 1.0f, 1.0f, 0, -0.001290f, -0.507640f,
	1.0f, 1.0f, -1.0f, 0, 0.999686f, -0.507640f,
	1.0f, 1.0f, 1.0f, 0, -0.001290f, -0.507640f,
	1.0f, -1.0f, -1.0f, 0, 0.999686f, -0.997905f,
	1.0f, -1.0f, 1.0f, 0, -0.001290f, -0.997905f,
	-1.0f, -1.0f, -1.0f, 0, 1.000488f, -0.997717f,
	1.0f, 1.0f, -1.0f, 0, -0.000488f, -0.507452f,
	-1.0f, 1.0f, -1.0f, 0, 1.000488f, -0.507452f,
	1.0f, 1.0f, -1.0f, 0, -0.000488f, -0.507452f,
	-1.0f, -1.0f, -1.0f, 0, 1.000488f, -0.997717f,
	1.0f, -1.0f, -1.0f, 0, -0.000488f, -0.997717f,
	-1.0f, -1.0f, 1.0f, 0, 1.001465f, -0.998026f,
	-1.0f, 1.0f, -1.0f, 0, 0.000488f, -0.507761f,
	-1.0f, 1.0f, 1.0f, 0, 1.001465f, -0.507761f,
	-1.0f, 1.0f, -1.0f, 0, 0.000488f, -0.507761f,
	-1.0f, -1.0f, 1.0f, 0, 1.001465f, -0.998026f,
	-1.0f, -1.0f, -1.0f, 0, 0.000488f, -0.998026f,
	1.0f, 1.0f, 1.0f, 0, 0.999878f, -0.496094f,
	-1.0f, 1.0f, -1.0f, 0, 0.000122f, 0.003906f,
	1.0f, 1.0f, -1.0f, 0, 0.999878f, 0.003906f,
	-1.0f, 1.0f, -1.0f, 0, 0.000122f, 0.003906f,
	1.0f, 1.0f, 1.0f, 0, 0.999878f, -0.496094f,
	-1.0f, 1.0f, 1.0f, 0, 0.000122f, -0.496094f,
	1.0f, -1.0f, -1.0f, 0, 0.999878f, -0.496094f,
	-1.0f, -1.0f, 1.0f, 0, 0.000122f, 0.003906f,
	1.0f, -1.0f, 1.0f, 0, 0.999878f, 0.003906f,
	-1.0f, -1.0f, 1.0f, 0, 0.000122f, 0.003906f,
	1.0f, -1.0f, -1.0f, 0, 0.999878f, -0.496094f,
	-1.0f, -1.0f, -1.0f, 0, 0.000122f, -0.496094f
*/
	// z = -1
	-1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0, 0.0f, 0.0f,
	 1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0, 1.0f, 0.0f,
	-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0, 0.0f, 1.0f,
	 1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0, 1.0f, 1.0f,
	// z = 1
	-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0, 1.0f, 1.0f,
	 1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0, 0.0f, 0.0f,
	-1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0, 1.0f, 0.0f,
	 1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0, 0.0f, 1.0f,
	// x = 1
	 1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0, 0.0f, 0.0f,
	 1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0, 1.0f, 0.0f,
	 1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0, 0.0f, 1.0f,
	 1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0, 1.0f, 1.0f,
	// x = -1
	-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 0, 0.0f, 0.0f,
	-1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0, 1.0f, 0.0f,
	-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0, 1.0f, 1.0f,
	-1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 0, 0.0f, 1.0f,
	// y = 1
	-1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0, 0.0f, 0.0f,
	 1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0, 1.0f, 0.0f,
	-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0, 0.0f, 1.0f,
	 1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0, 1.0f, 1.0f,
	// y = -1
	-1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0, 0.0f, 1.0f,
	-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0, 0.0f, 0.0f,
	 1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0, 1.0f, 1.0f,
	 1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0, 1.0f, 0.0f
};


//********************************************************************************
//Costruttore standard
//********************************************************************************
CSolidCube::CSolidCube () {
}



//********************************************************************************
//Distruttore standard
//********************************************************************************
CSolidCube::~CSolidCube () {
}



//********************************************************************************
//Costruisce in memoria un cubo avente origine al centro, colore color e
//dimensione fScale. Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CSolidCube::BuildSolid (LPDIRECT3DDEVICE9 dev, float fScale, D3DCOLOR color, bool bFlipNormals, unsigned int param) {
	LPSIMPLESOLID_VERTEX vList = new SIMPLESOLID_VERTEX[VERTEX_COUNT];
	LPWORD iList = new WORD[INDEX_COUNT];
	const float fHalfScale = fabs(fScale) / 2.0f;
	void* dst;
	const float fFlipNormals = (bFlipNormals ? -1.0f : 1.0f);
	
	if ((vList && iList) == FALSE) {
		SAFE_DELETE (vList);
		SAFE_DELETE (iList);
		return NONZERO;
	}
	
	SAFE_RELEASE (m_ib);
	SAFE_RELEASE (m_vb);
	m_nVertexCount = VERTEX_COUNT;
	m_nIndexCount = INDEX_COUNT;
	
	//Lista hardcoded dei vertici e degli indici
	for (int z = 0; z < VERTEX_COUNT; z++) {
		vList[z].color = color;
		vList[z].x = m_vList[z].x * fHalfScale;
		vList[z].y = m_vList[z].y * fHalfScale;
		vList[z].z = m_vList[z].z * fHalfScale;
		
		vList[z].nx = m_vList[z].nx * fFlipNormals;
		vList[z].ny = m_vList[z].ny * fFlipNormals;
		vList[z].nz = m_vList[z].nz * fFlipNormals;
		
		vList[z].tu = m_vList[z].tu;
		vList[z].tv = m_vList[z].tv;
//		iList[z] = (WORD)(VERTEX_COUNT - z - 1);
	}
	
	iList[0] = 0;
	iList[1] = 1;
	iList[2] = 2;
	iList[3] = 1;
	iList[4] = 3;
	iList[5] = 2;
	iList[6] = 4;
	iList[7] = 5;
	iList[8] = 6;
	iList[9] = 4;
	iList[10] = 7;
	iList[11] = 5;
	iList[12] = 8;
	iList[13] = 9;
	iList[14] = 10;
	iList[15] = 10;
	iList[16] = 9;
	iList[17] = 11;
	iList[18] = 12;
	iList[19] = 13;
	iList[20] = 14;
	iList[21] = 12;
	iList[22] = 14;
	iList[23] = 15;
	iList[24] = 16;
	iList[25] = 17;
	iList[26] = 19;
	iList[27] = 16;
	iList[28] = 19;
	iList[29] = 18;
	iList[30] = 20;
	iList[31] = 21;
	iList[32] = 22;
	iList[33] = 21;
	iList[34] = 23;
	iList[35] = 22;
	
	if (FAILED(dev->CreateVertexBuffer (sizeof(SIMPLESOLID_VERTEX) * VERTEX_COUNT, D3DUSAGE_WRITEONLY, D3DFVF_SIMPLESOLID, D3DPOOL_MANAGED, &m_vb, NULL)))
		return NONZERO;
	dst = NULL;
	if (FAILED(m_vb->Lock (0, sizeof(SIMPLESOLID_VERTEX) * VERTEX_COUNT, &dst, 0)))
		return NONZERO;
	CopyMemory (dst, vList, sizeof(SIMPLESOLID_VERTEX) * VERTEX_COUNT);
	m_vb->Unlock();
	
	if (FAILED(dev->CreateIndexBuffer (sizeof(WORD) * INDEX_COUNT, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_ib, NULL)))
		return NONZERO;
	dst = NULL;
	if (FAILED(m_ib->Lock (0, sizeof(WORD) * INDEX_COUNT, &dst, 0)))
		return NONZERO;
	CopyMemory (dst, iList, sizeof(WORD) * INDEX_COUNT);
	m_ib->Unlock ();

	SAFE_DELETE (vList);
	SAFE_DELETE (iList);
	
	return ZERO;
}