//CCamera.h


#ifndef _INCL_CCAMERA_H
#define _INCL_CCAMERA_H

#include "CMathEngine.h"

class CCamera {
public:
	CCamera ( void );
	~CCamera ( void );
	void Reset ( void );
	void Dispose ( void );
	
	int Rotate ( float fY, float fZ);
	int Zoom ( float f );
	
	int OffsetRotate ( float fY, float fZ);
	int OffsetZoom ( float f );
	int SetTarget ( float fX, float fY, float fZ );
	int SetTarget ( const LPMATH_3DVECTOR v );
	int SetZoomRange ( float fMin, float fMax );
	int SetPitchRange ( float fMin, float fMax );
	int SetFov ( float f );
	int InterpolateAnimation ( float fDelta, float fRad );
	
	int BuildDefaultProjection ( void );
	void BuildCamMatrix ( void );
	
	inline float GetRotationY ( void );
	inline float GetRotationZ ( void );
	inline float GetZoom ( void );
	
	MATH_MATRIX16 matProj;
	MATH_MATRIX16 matCam;

private:
	inline void m_Reset ( void );
	inline void m_Dispose ( void );
	inline int m_Rotate ( float fY, float fZ);
	inline int m_Zoom ( float f );
	
	MATH_SPHERICAL m_sphPosition;
	MATH_3DVECTOR m_vUp;
	MATH_3DVECTOR m_vEye;
	MATH_3DVECTOR m_vLookAt;
	float m_fZoomMin;
	float m_fZoomMax;
	float m_fFOV;
	float m_fPitchMin;
	float m_fPitchMax;
	bool m_bChanged; //Flag che indica se le coordinate cartesiane vanno ricalcolate
};



//********************************************************************************
//Restituisce m_fRotX.
//********************************************************************************
inline float CCamera::GetRotationZ () {
	return m_sphPosition.theta;
}



//********************************************************************************
//Restituisce m_fRotY.
//********************************************************************************
inline float CCamera::GetRotationY () {
	return m_sphPosition.phi - MATH_PI / 2.0f;
}



//********************************************************************************
//Restituisce m_fZoom.
//********************************************************************************
inline float CCamera::GetZoom () {
	return m_sphPosition.rho;
}



#endif