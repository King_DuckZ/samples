//CPairList.h


#ifndef _INCL_CPAIRLIST_H
#define _INCL_CPAIRLIST_H

#include <string.h>


class CPairList {
public:
	CPairList ( void );
	~CPairList ( void );
	void Dispose ( void );
	
	unsigned int AddPair ( const char* strText, int param, unsigned int uID = 0 );
	unsigned int DeletePair ( unsigned int uID );
	void SortByText ( void );
	void SortByID ( void );
	void Unsort ( void );
	char* GetText ( char** dst, unsigned int uID );
	unsigned int GetID ( unsigned int* dst, const char* strText );
	BOOL ExistID ( unsigned int uID );
	BOOL ExistText ( const char* strText );
	inline char* GetLastText ( char** dst );
	inline unsigned int GetLastID ( unsigned int* dst );
	inline int GetLastParam ( int* dst );
	int LoadListFromFile ( const char* strFileName );
	inline int UpdateParam ( int nNewParam );
	inline unsigned int GetListCount ( void );
	inline unsigned int GetListIndex ( unsigned int uIndex, char** strDst, int* nParam, unsigned int* uID );
	
private:
	inline void m_Dispose ( void );
	inline unsigned int m_GetIndex ( const char* strText );
	inline unsigned int m_GetIndex ( unsigned int uID );
	
	bool m_bTextSorted; //Indica se gli indici sono ordinati per m_strText
	bool m_bIDSorted; //Indica se gli indici sono ordinati per m_uID
	bool m_bLastIndexValid; //Indica se m_uLastIndex � un valore valido
	char** m_strText; //Stringhe
	unsigned int* m_uID; //ID
	unsigned int* m_uIndex; //Indici per l'accesso ordinato a m_uID e m_uText
	int* m_nParam; //Valore aggiuntivo abbinato alle coppie
	unsigned int m_uCount; //Numero di coppie stringa-ID
	unsigned int m_uPreallocated; //Numero di elementi degli array allocati
	unsigned int m_uLastIndex; //Indice dell'ultimo elemento trovato da GetText() o GetID()
	unsigned int m_uLongestString; //Numero di caratteri (escluso il NULL_char) nella stringa pi� lunga
};


//********************************************************************************
//Se m_bLastIndexValid � true, la funzione restituisce in **dst l'ultimo
//valore trovato da GetText() o cercato con GetID(), altrimenti NULL.
//Se dst vale NULL, la funzione restituisce NULL.
//********************************************************************************
inline char* CPairList::GetLastText (char** dst) {
	if (dst == NULL)
		return NULL;
	
	if (m_bLastIndexValid) {
		*dst = new char[strlen(m_strText[m_uLastIndex]) + 1];
		if (*dst == NULL)
			return NULL;
		strcpy (*dst, m_strText[m_uLastIndex]);
		return *dst;
	}
	else {
		return NULL;
	}
}



//********************************************************************************
//Se m_bLastIndexValid � true, la funzione restituisce in *dst l'ultimo
//valore trovato da GetID() o cercato con GetText(), altrimenti 0.
//dst pu� essere NULL.
//********************************************************************************
inline unsigned int CPairList::GetLastID (unsigned int* dst) {
	if (m_bLastIndexValid) {
		if (dst != NULL)
			*dst = m_uID[m_uLastIndex];
		return m_uID[m_uLastIndex];
	}
	else {
		return 0;
	}
}



//********************************************************************************
//Restituisce il parametro associato all'ultimo elemento trovato. Per
//ottenerlo � possibile forzare una ricerca (ad esempio tramite ExistID() o
//GetText()) e chiamare questa funzione. Restituisce 0 in caso di insuccesso.
//********************************************************************************
inline int CPairList::GetLastParam (int* dst) {
	if (m_bLastIndexValid) {
		if (dst)
			*dst = m_nParam[m_uLastIndex];
		return m_nParam[m_uLastIndex];
	}
	else {
		return 0;
	}
}



//********************************************************************************
//Modifica il valore di m_nParam[m_uLastIndex]. Restituisce ZERO se il
//valore � stato modificato con successo, NONZERO se ci sono stati problemi.
//********************************************************************************
inline int CPairList::UpdateParam (int nNewParam) {
	if (!m_bLastIndexValid)
		return NONZERO;
	m_nParam[m_uLastIndex] = nNewParam;
	return ZERO;
}



//********************************************************************************
//Restituisce il numero di elementi in lista, inclusi i valori cancellati
//o con campi NULL. Restituisce 0 se la lista � vuota.
//********************************************************************************
inline unsigned int CPairList::GetListCount () {
	return m_uCount;
}



//********************************************************************************
//Restituisce nei rispettivi puntatori i valori interni corrispondenti
//all'indice richiesto. L'indirizzo della stringa restituita corrisponde
//all'indirizzo interno, pertanto la stringa non dovrebbe essere modificata.
//Inoltre, il puntatore restituito diventer� non valido se vengono chiamati
//il metodo Dispose() o il distruttore. Restituisce 0.
//********************************************************************************
inline unsigned int CPairList::GetListIndex (unsigned int uIndex, char** strDst, int* nParam, unsigned int* uID) {
	if (uIndex < m_uCount) {
		if (strDst)
			*strDst = m_strText[uIndex];
		if (nParam)
			*nParam = m_nParam[uIndex];
		if (uID)
			*uID = m_uID[uIndex];
	}
	
	return 0;
}



#endif