//CBitmapSarge.h


#ifndef _INCL_CBITMAPSARGE_H
#define _INCL_CBITMAPSARGE_H

#include <stdio.h>

typedef struct STRUCT_BITMASK {
	DWORD dwBlue;
	DWORD dwGreen;
	DWORD dwRed;
	DWORD dwAlpha;
} BITMASK, *LPBITMASK;
enum BSSCALE_MODE {
	BSSM_BILINEAR,
	BSSM_BLUR4X4
};
enum BITMAP_TYPE {
	BTYPE_UNKNOWN = 0,
	BTYPE_MONOCHROME = 1,
	BTYPE_4BIT = 2,
	BTYPE_8BIT = 3,
	BTYPE_A1R5G5B5 = 4,
	BTYPE_R5G6B5 = 5,
	//BTYPE_X1R5G5B5 = 6,
	BTYPE_24BIT = 7,
	BTYPE_A8R8G8B8 = 8,
	BTYPE_R8G8B8A8 = 9,
	BTYPE_DWORD = 0xFFFFFFFF
};

class CBitmapSarge {
public:
	CBitmapSarge ( void );
	CBitmapSarge ( const char* strFileName );
	~CBitmapSarge ( void );
	
	int LoadPicture ( const char* strFileName );
	int LoadPicture ( FILE* f );
	int LoadPicture ( const void* MemPtr );
	int LoadPalette ( const char* strFileName );
	int LoadPalette ( FILE* f );
	int LoadPalette ( const void* MemPtr );
	int Reload ( void );
	int ReloadPalette ( void );
	void *GetDataPtr ( void** ptr );
	void *GetPalettePtr ( void** ptr );
	void Unload ( void );
	void Dispose ( void );
	int ConvertTo32bit ( void );
	int ScaleImage ( int cx, int cy, BSSCALE_MODE bssm );
	int CreatePalette ( DWORD dwStartColor, DWORD dwEndColor );
	int CreatePalette ( BYTE rStart, BYTE gStart, BYTE bStart, BYTE rEnd, BYTE gEnd, BYTE bEnd );
	int CreateGrayscalePalette ( void );
	int RGBAtoARGB ( void );
	int ARGBtoRGBA ( void );
	inline int GetImageSize ( void );
	inline int GetPitch ( void );
	inline int GetPictureWidth ( void );
	inline int GetPictureHeight ( void );
	int ConvertToGrayScale ( bool bTo8Bit );
	int AttachPalette ( void );
	
	bool bPicture; //true se � stata caricata una bitmap. Reimpostata su false con Dispose()
	BITMAP_TYPE BitmapType;
private:
	char* m_strFileName;
	char* m_strPaletteName;
	char* m_data;
	char* ha;
	LPRGBQUAD m_pal;
	int m_nPal;
	BITMAPFILEHEADER m_fH;
	BITMAPINFOHEADER m_iH;
	BITMASK m_BitMask;
	int m_nPitch;
	bool m_bIsARGB;
	
	inline void m_Dispose ( void );
	inline void m_ResetValues ( void );
	int m_Conv1to32 ( void );
	int m_Conv4to32 ( void );
	int m_Conv8to32 ( void );
	int m_Conv16to32 ( void );
	int m_Conv24to32 ( void );
};



//********************************************************************************
//Restituisce m_nPitch
//********************************************************************************
inline int CBitmapSarge::GetPitch () {
	return m_nPitch;
}



//********************************************************************************
//Restituisce m_iH.biWidth
//********************************************************************************
inline int CBitmapSarge::GetPictureWidth () {
	return m_iH.biWidth;
}



//********************************************************************************
//Restituisce m_iH.biSizeImage
//********************************************************************************
inline int CBitmapSarge::GetImageSize () {
	return m_iH.biSizeImage;
}



//********************************************************************************
//Restituisce m_iH.biHeight
//********************************************************************************
inline int CBitmapSarge::GetPictureHeight () {
	return m_iH.biHeight;
}



#endif