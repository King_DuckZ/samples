//CMesh.cpp

#define _DEFINE_CMESH_FVF
#define INVALID_INDEX CMESH_INVALIDINDEX

#include "constants.h"
#include <windows.h>
#include "CMesh.h"

namespace Mesh {
CMathEngine g_math;
};

using namespace Mesh;


//********************************************************************************
//Costruttore standard
//********************************************************************************
CMesh::CMesh () {
	m_Reset ();
}



//********************************************************************************
//Distruttore standard
//********************************************************************************
CMesh::~CMesh () {
	m_Dispose ();
}



//********************************************************************************
//Imposta tutte le propriet� sui valori iniziali.
//********************************************************************************
inline void CMesh::m_Reset () {
	m_smMesh = NULL;
	m_vert = NULL;
//	m_tex = NULL;
//	m_bump = NULL;
//	m_refl = NULL;
	
	m_pairTexture = NULL;
	
	m_dwVertCount = 0;
	m_dwIndCount = 0;
	m_dwTexCount = 0;
	m_dwBumpCount = 0;
	m_dwReflCount = 0;
	m_dwSubMeshCount = 0;
	m_dwPreallocatedSubMesh = 0;
	m_uStride = sizeof(MESH_VERTEX);
	
	bSetVrtKeepCopy = false;
	m_bGotCaps = false;
	
//	ZeroMemory (&m_bbox, sizeof(CMESH_BOUNDINGBOX));
//	ZeroMemory (&m_bsphere, sizeof(CMESH_BOUNDINGSPHERE));
//	ZeroMemory (&m_bcylinder, sizeof(CMESH_BOUNDINGCYLINDER));
	ZeroMemory (&m_caps, sizeof(D3DCAPS9));

	m_LoadTexture = &this->m_LoadTextureFromFile;
}



//********************************************************************************
//Chiama m_Dispose()
//********************************************************************************
void CMesh::Dispose () {
	m_Dispose ();
	m_Reset ();
}



//********************************************************************************
//Libera la memoria occupata e distrugge gli oggetti creati.
//********************************************************************************
inline void CMesh::m_Dispose () {
/*	DWORD z;
	
	for (z = 0; z < m_dwTexCount; z++) {
		SAFE_RELEASE (m_tex[z]);
	}
	for (z = 0; z < m_dwBumpCount; z++) {
		SAFE_RELEASE (m_bump[z]);
	}
	for (z = 0; z < m_dwReflCount; z++) {
		SAFE_RELEASE (m_refl[z]);
	}
	
	SAFE_DELETE (m_bump);
	SAFE_DELETE (m_refl);
	SAFE_DELETE (m_tex);
*/	
	SAFE_DELETE (m_vert);
	SetPairList (NULL, false);
	for (DWORD z = 0; z < m_dwSubMeshCount; z++) {
		SAFE_RELEASE (m_smMesh[z].ib);
		SAFE_RELEASE (m_smMesh[z].vb);
		SAFE_DELETE (m_smMesh[z].pnTextureIndex);
	}
	SAFE_DELETE (m_smMesh);
}



//********************************************************************************
//Crea una nuova submesh in memoria, e restituisce un indice che
//identifica la nuova submesh in caso di successo, o INVALID_INDEX in
//caso d'errore.
//********************************************************************************
DWORD CMesh::SetVertices (LPDIRECT3DDEVICE9 dev, const LPMATH_MATRIX16 matTransf, const LPMESH_VERTEX vert, const LPDWORD ind, CPairList* pairTextureList, UINT nTexIndex, UINT nBumpIndex, UINT nReflIndex, DWORD dwVert, DWORD dwInd, MATERIAL_PROPERTIES mp) {
//LPDIRECT3DDEVICE9 dev
//const LPMATH_MATRIX16 matTransf
//const LPMESH_VERTEX vert
//const LPDWORD ind
//CPairList* pairTextureList
//const int* TextureIndices
//int nBumpIndex
//int nReflIndex
//DWORD dwVert
//DWORD dwInd
//int nTexInd
//MATERIAL_PROPERTIES mp

	if ( (dev && vert && pairTextureList) == FALSE )
		return INVALID_INDEX;
	if ( dwVert == 0 )
		return INVALID_INDEX;
	if (ind == NULL) {
		dwInd = 0;
	}
	
	const UINT uVertSize = dwVert * sizeof(MESH_VERTEX);
	CMathEngine math;
	LPWORD wIndices = NULL;
	const bool b32bitIndices = ((dwVert > 0xFFFF) && (m_caps.MaxVertexIndex > 0xFFFF) ? true : false);
	const UINT uIndSize = (b32bitIndices ? dwInd << 2: dwInd << 1);
	const WORD wMaxIndex = MIN((WORD)0xFFFF, (WORD)dwVert);
	char* strTexName = NULL;
	
	//Se devo usare gli indici a 16 bit devo convertirli qua
	//Gli indici > 0xFFFF vengono troncati
	if (!b32bitIndices && dwInd) {
		wIndices = new WORD[dwInd];
		if (wIndices == NULL)
			goto Error;
		
		for (DWORD z = 0; z < dwInd; z++) {
			wIndices[z] = (WORD)(ind[z] & 0xFFFF);
		}
	}
	
	if (m_ExpandSubMeshList(m_dwSubMeshCount + 1))
		return INVALID_INDEX;
	
	if (FAILED(dev->CreateVertexBuffer(uVertSize, D3DUSAGE_WRITEONLY, D3DFVF_CMESH, D3DPOOL_MANAGED, &m_smMesh[m_dwSubMeshCount].vb, NULL))) {
		goto Error;
	}
	if (dwInd) {
		if (FAILED(dev->CreateIndexBuffer(uIndSize, D3DUSAGE_WRITEONLY, (b32bitIndices ? D3DFMT_INDEX32 : D3DFMT_INDEX16), D3DPOOL_MANAGED, &m_smMesh[m_dwSubMeshCount].ib, NULL))) {
			goto Error;
		}
	}
	else
		m_smMesh[m_dwSubMeshCount].ib = NULL;

	m_smMesh[m_dwSubMeshCount].nTexIndexCount = 1/*nTexInd*/;
	m_smMesh[m_dwSubMeshCount].nBumpIndex = nBumpIndex;
	m_smMesh[m_dwSubMeshCount].nRefMapIndex = nReflIndex;
	m_smMesh[m_dwSubMeshCount].mpProperties = mp;
	m_smMesh[m_dwSubMeshCount].dwIndexCount = dwInd;
	m_smMesh[m_dwSubMeshCount].dwVertexCount = dwVert;
	
	if (matTransf == NULL) {
		math.Mat16Identity (&m_smMesh[m_dwSubMeshCount].matTransform);
		math.Mat16Identity (&m_smMesh[m_dwSubMeshCount].matBase);
	}
	else {
		CopyMemory (&m_smMesh[m_dwSubMeshCount].matTransform, matTransf, sizeof(MATH_MATRIX16));
		CopyMemory (&m_smMesh[m_dwSubMeshCount].matBase, matTransf, sizeof(MATH_MATRIX16));
	}
	
	//Imposto i riferimenti alle texture
	//(nonostante la struttura sia predisposta al multitexturing, qui ne imposto solo una)
	if (nTexIndex <= 0) //Se non ci sono texture
		m_smMesh[m_dwSubMeshCount].pnTextureIndex = NULL;
	else { //Se ci sono texture
		m_smMesh[m_dwSubMeshCount].pnTextureIndex = new int[1];
		if (m_smMesh[m_dwSubMeshCount].pnTextureIndex == NULL)
			m_smMesh[m_dwSubMeshCount].nTexIndexCount = nTexIndex = 0;
		else { //Memoria allocata con successo
			m_smMesh[m_dwSubMeshCount].pnTextureIndex[0] = 0; /*nTexIndex*/;
			if (pairTextureList->ExistID ((UINT)nTexIndex)) {
				pairTextureList->GetLastText (&strTexName);
				
				//Se � stata impostata una lista per evitare il doppio caricamento
				//delle texture, la consulto ora
				if (m_pairTexture) {
					if (!m_pairTexture->ExistText (strTexName)) {
						const int nTexAddr = (int)(this->*m_LoadTexture)(dev, strTexName);
						
						m_pairTexture->AddPair (strTexName, nTexAddr);
						m_smMesh[m_dwSubMeshCount].pnTextureIndex[0] = nTexAddr;
						
						m_pairTexture->SortByText ();
					}
					else { //La texture non va caricata, gi� esiste
						m_smMesh[m_dwSubMeshCount].pnTextureIndex[0] = m_pairTexture->GetLastParam (NULL);
					}
				}
				else {
					const int nTexAddr = (int)(this->*m_LoadTexture)(dev, strTexName);
					m_smMesh[m_dwSubMeshCount].pnTextureIndex[0] = nTexAddr;
				}
			}
		}
	}

	Bounding.SetFigureCount (1, BFSFC_BOX | BFSFC_SPHERE);
	
	//Copio vertici e indici
	MESH_VERTEX* VertDst = NULL;
	void* IndDst = NULL;
	if (SUCCEEDED(m_smMesh[m_dwSubMeshCount].vb->Lock(0, uVertSize, (void**)&VertDst, 0))) {
		//CopyMemory (VertDst, vert, uVertSize);
		Bounding.BeginCalc (0);
		for (DWORD z = 0; z < dwVert; z++) {
			VertDst[z].vPos = vert[z].vPos;
			Bounding << vert[z].vPos;
		}
		Bounding.EndCalc ();
		
		m_smMesh[m_dwSubMeshCount].vb->Unlock();
		m_dwVertCount += dwVert;
	}
	if (dwInd) {
		if (SUCCEEDED(m_smMesh[m_dwSubMeshCount].ib->Lock(0, uIndSize, &IndDst, 0))) {
			if (b32bitIndices)
				CopyMemory (IndDst, ind, uIndSize);
			else
				CopyMemory (IndDst, wIndices, uIndSize);
			m_smMesh[m_dwSubMeshCount].ib->Unlock();
			m_dwIndCount += dwInd;
		}
	}
	
	SAFE_DELETE (strTexName);
	SAFE_DELETE (wIndices);
	m_dwSubMeshCount++;
	return (m_dwSubMeshCount - 1);

Error:
	SAFE_DELETE (strTexName);
	SAFE_RELEASE (m_smMesh[m_dwSubMeshCount].ib);
	SAFE_RELEASE (m_smMesh[m_dwSubMeshCount].vb);
	SAFE_DELETE (m_smMesh[m_dwSubMeshCount].pnTextureIndex);
	SAFE_DELETE (wIndices);
	return INVALID_INDEX;
}



//********************************************************************************
//********************************************************************************
int CMesh::PreAllocSubMesh (DWORD dwCount) {
	return m_ExpandSubMeshList (dwCount + m_dwPreallocatedSubMesh);
}



//********************************************************************************
//Renderizza la submesh specificata in dwIndex, o tutte le submesh disponibili
//se dwIndex vale INVALID_INDEX.
//Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CMesh::Render (LPDIRECT3DDEVICE9 dev, DWORD dwIndex) {	
	if (dev == NULL)
		return NONZERO;
	
	DWORD dwFirstSubMesh;
	MATH_MATRIX16 matWorld;
	
	//Decisione della submesh da renderizzare
	if (dwIndex == INVALID_INDEX) {
		dwFirstSubMesh = 0;
		dwIndex = m_dwSubMeshCount;
	}
	else {
		dwFirstSubMesh = MIN(dwIndex, m_dwSubMeshCount - 1);
		dwIndex = dwFirstSubMesh + 1;
	}
	
	//Step 1
	//Impostazioni per il rendering generale
	m_PreRenderSteps (dev);
	
	for (DWORD z = dwFirstSubMesh; z < dwIndex; z++) {
		//Step 2
		//Setta il device
		m_DeviceSetup (dev, m_smMesh[z].mpProperties);
		
		//Imposto la texture
		dev->SetTexture (0, (LPDIRECT3DTEXTURE9)(m_smMesh[z].pnTextureIndex[0]));
		
		dev->SetStreamSource (0, m_smMesh[z].vb, 0, m_uStride/*sizeof(MESH_VERTEX)*/);
		
		dev->GetTransform (D3DTS_WORLD, (D3DMATRIX*)&matWorld);
		g_math.Mat16Multiply (&matWorld, &m_smMesh[z].matTransform, &matWorld);
		dev->SetTransform (D3DTS_WORLD, (D3DMATRIX*)&matWorld);
		
		//Step 3
		//Rendering
		m_Render (dev, z);
	}
	
	//Step 4
	//Impostazioni da ripristinare dopo il rendering
	m_PostRenderSteps (dev);

	return ZERO;
}



//********************************************************************************
//Incrementa il numero di elementi allocati per m_smMesh, in modo che siano
//tanti quanti specificati in dwNewSize. Restituisce ZERO se va tutto bene,
//altrimenti NONZERO.
//********************************************************************************
int CMesh::m_ExpandSubMeshList (DWORD dwNewSize) {
	if (dwNewSize <= m_dwPreallocatedSubMesh)
		return ZERO;
	
	LPMESH_SUBMESH smTmp = new MESH_SUBMESH[dwNewSize];
	if (smTmp == NULL)
		return NONZERO;
	
	m_dwPreallocatedSubMesh = dwNewSize;
	CopyMemory (smTmp, m_smMesh, sizeof(MESH_SUBMESH) * m_dwSubMeshCount);
	
	SAFE_DELETE (m_smMesh);
	m_smMesh = smTmp;
	
	return ZERO;
}



//********************************************************************************
//Imposta se in fase di loading la classe caricher� le texture da singoli
//file su disco o da un archivio GigaPaq. Restituisce ZERO.
//********************************************************************************
int CMesh::SetTextureSource (CMESH_TEXTURESOURCE src) {
	if (src == MTS_STRAIGHTFILE)
		m_LoadTexture = &this->m_LoadTextureFromFile;
	else
		m_LoadTexture = &this->m_LoadTextureFromArchive;
	return ZERO;
}



//********************************************************************************
//Restituisce il valore di ritorno di m_LoadTextureFunc() se va tutto
//bene, altrimenti NULL.
//********************************************************************************
LPDIRECT3DTEXTURE9 CMesh::m_LoadTextureFromFile (LPDIRECT3DDEVICE9 dev, const char* strFileName) {
	FILE *f;
	
	if ( (f=fopen(strFileName, "rb")) == NULL )
		return NULL;
	
	LPDIRECT3DTEXTURE9 texRet = m_LoadTextureFunc (dev, f);
	fclose (f);
	return texRet;
}



//********************************************************************************
//Restituisce il valore di ritorno di m_LoadTextureFunc() se va tutto
//bene, altrimenti NULL.
//********************************************************************************
LPDIRECT3DTEXTURE9 CMesh::m_LoadTextureFromArchive (LPDIRECT3DDEVICE9 dev, const char* strFileName) {
	return ZERO;
}



//********************************************************************************
//Imposta i caps per il device in uso.
//********************************************************************************
void CMesh::SetDeviceCaps (const D3DCAPS9* caps) {
	if (caps == NULL)
		return;
	
	CopyMemory (&m_caps, caps, sizeof(D3DCAPS9));
	m_bGotCaps = true;
	return;
}



//********************************************************************************
//Carica in memoria la texture specificata, tenendo conto dei limiti
//del device specificati in m_caps. Restituisce l'indirizzo della nuova
//texture se va tutto bene, altrimenti NULL.
//********************************************************************************
inline LPDIRECT3DTEXTURE9 CMesh::m_LoadTextureFunc (LPDIRECT3DDEVICE9 dev, FILE* f) {
	CBitmapSarge bmp;
	LPDIRECT3DTEXTURE9 texRet = NULL;
	bool bResize = false;
	int nWidth, nHeight;
	int nSide;
	D3DFORMAT BitmapFormat;
	D3DLOCKED_RECT LockedRect;
	BYTE* src = NULL;
	BYTE* dst = NULL;
	LPDIRECT3DSURFACE9 surf = NULL;
	
	if (bmp.LoadPicture (f))
		return NULL;
	nWidth = bmp.GetPictureWidth ();
	nHeight = bmp.GetPictureHeight ();
	
	//Ora ci sono i vari test per vedere se la texture � compatibile col device
	//Controllo che sia quadrata
	if (m_caps.TextureCaps & D3DPTEXTURECAPS_SQUAREONLY) {
		if (nWidth != nHeight) {
			bResize = true;
			nWidth = nHeight = MAX (nWidth, nHeight);
		}
	}
	
	//Controllo che le dimensioni siano potenze di 2
	if (m_caps.TextureCaps & D3DPTEXTURECAPS_POW2) {
		//Width
		nSide = 1;
		while (nSide < nWidth)
			nSide <<= 1;
		if (nSide != nWidth) { //Non potenza di 2
			bResize = true;
			nWidth = ( nSide - nWidth < nWidth - (nSide >> 1) ? nSide : nSide >> 1 );
		}
		
		//Height
		nSide = 1;
		while (nSide < nHeight)
			nSide <<= 1;
		if (nSide != nHeight) { //Non potenza di 2
			bResize = true;
			nHeight = ( nSide - nHeight < nHeight - (nSide >> 1) ? nSide : nSide >> 1 );
		}
	}
	
	//Controllo che le dimensioni massime siano rispettate
	if (nWidth > (m_caps.MaxTextureWidth ? m_caps.MaxTextureWidth : 256)) {
		bResize = true;
		nWidth = (m_caps.MaxTextureWidth ? m_caps.MaxTextureWidth : 256);
	}
	if (nHeight > (m_caps.MaxTextureHeight ? m_caps.MaxTextureHeight : 256)) {
		bResize = true;
		nHeight = (m_caps.MaxTextureHeight ? m_caps.MaxTextureHeight : 256);
	}
	
	//Test finiti
	//Se l'immagine va ridimensionata, lo faccio
	if (bResize) {
		bmp.ConvertTo32bit ();
		bmp.ScaleImage (nWidth, nHeight, BSSM_BILINEAR);
		bmp.RGBAtoARGB ();
	}
	
	//Ora posso creare la texture
	switch (bmp.BitmapType) {
	case BTYPE_24BIT:
		bmp.ConvertTo32bit ();
		bmp.RGBAtoARGB ();
		BitmapFormat = D3DFMT_A8R8G8B8;
//		BitmapFormat = D3DFMT_R8G8B8;
		break;
	case BTYPE_A8R8G8B8:
		BitmapFormat = D3DFMT_A8R8G8B8;
		break;
	case BTYPE_R8G8B8A8:
		bmp.RGBAtoARGB();
		BitmapFormat = D3DFMT_A8R8G8B8;
		break;
	case BTYPE_A1R5G5B5:
		BitmapFormat = D3DFMT_A1R5G5B5;
		break;
	case BTYPE_R5G6B5:
		BitmapFormat = D3DFMT_R5G6B5;
		break;
	case BTYPE_8BIT:
		bmp.ConvertTo32bit ();
		bmp.RGBAtoARGB ();
		BitmapFormat = D3DFMT_A8R8G8B8;
		break;
	default:
		goto ByeBye;
	}
	
	if (FAILED(dev->CreateTexture (nWidth, nHeight, 1, 0, BitmapFormat, D3DPOOL_MANAGED, &texRet, NULL)))
		goto ByeBye;
	
	if (FAILED(texRet->GetSurfaceLevel (0, &surf))) {
		SAFE_RELEASE (texRet);
		goto ByeBye;
	}
	ZeroMemory (&LockedRect, sizeof(D3DLOCKED_RECT));
	if (SUCCEEDED(surf->LockRect (&LockedRect, NULL, 0))) {
		bmp.GetDataPtr((void**)&src);
		dst = (BYTE*)LockedRect.pBits;
		const int nMinPitch = MIN(LockedRect.Pitch, bmp.GetPitch());
		
		for (int y = 0; y < bmp.GetPictureHeight(); y++) {
			CopyMemory (dst, src, nMinPitch);
			src += bmp.GetPitch();
			dst += LockedRect.Pitch;
		}
		
		surf->UnlockRect ();
	}
	
ByeBye:
	SAFE_RELEASE (surf);
	return texRet;
}



//********************************************************************************
//Imposta il puntatore m_pairTexture per questa classe. Per un uso pi�
//efficiente, il puntatore dovrebbe essere lo stesso per tutte le istanze di
//questa classe. Se bDelOld � true o non � specificato, le texture nella
//vecchia lista vengono rilasciate e la lista viene svuotata. Se sono gi�
//state caricate delle texture, ma senza una lista, queste vengono rilasciate
//sempre, indipendentemente da bDelOld. Un valore NULL in pair cause la
//rimozione della lista. Questa funzione � anche usata dal metodo m_Dispose().
//Restituisce ZERO.
//********************************************************************************
int CMesh::SetPairList (CPairList* pair, bool bDelOld) {
	DWORD z;
	LPDIRECT3DTEXTURE9 tex;
	
	//Caso 1:
	//Non c'era nessuna lista e ne sto impostando una
	if (m_pairTexture == NULL) {
		for (z = 0; z < m_dwSubMeshCount; z++) {
			if (m_smMesh[z].pnTextureIndex) {
				tex = (LPDIRECT3DTEXTURE9)m_smMesh[z].pnTextureIndex[0];
				SAFE_RELEASE (tex);
				m_smMesh[z].pnTextureIndex[0] = 0;
			}
		}
	}
	//Caso 2:
	//Una lista gi� c'�
	else {
		//Cancello la vecchia lista
		if (bDelOld) {
			for (z = 0; z < m_pairTexture->GetListCount(); z++) {
				m_pairTexture->GetListIndex (z, NULL, (int*)&tex, NULL);
				SAFE_RELEASE (tex);
			}
			m_pairTexture->Dispose ();
		}
	}
	
	m_pairTexture = pair;
	return ZERO;
}



//********************************************************************************
//Imposta il device in base ai parametri specificati in p.
//********************************************************************************
inline void CMesh::m_DeviceSetup (LPDIRECT3DDEVICE9 dev, MATERIAL_PROPERTIES p) {
	if (TRUE/*m_smMesh[z].nTexIndexCount == 0*/) {
		dev->SetTextureStageState (0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		dev->SetTextureStageState (0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
		dev->SetTextureStageState (0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
		dev->SetTextureStageState (0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
		dev->SetTextureStageState (1, D3DTSS_COLOROP, D3DTOP_DISABLE);
		dev->SetTextureStageState (1, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
	}
	dev->SetRenderState (D3DRS_SHADEMODE, D3DSHADE_GOURAUD);
	return;
}



//********************************************************************************
//Renderizza le mesh specificate. Restituisce ZERO se va tutto bene,
//altrimenti NONZERO.
//********************************************************************************
int CMesh::m_Render (LPDIRECT3DDEVICE9 dev, DWORD dwIndex) {
	UINT uFaceCount;
	UINT uDraw;
	UINT uProgress;
	const UINT uMaxVertBy3 = (m_bGotCaps ? m_caps.MaxPrimitiveCount : 0xFFFF);
//	const DWORD& z = dwIndex;

	if (m_smMesh[dwIndex].ib) {
		dev->SetIndices (m_smMesh[dwIndex].ib);
		uFaceCount = m_smMesh[dwIndex].dwIndexCount / 3;
		uProgress = 0;
		while (uFaceCount) {
			uDraw = MIN(uMaxVertBy3, uFaceCount);
			dev->DrawIndexedPrimitive (D3DPT_TRIANGLELIST, 0, 0, m_smMesh[dwIndex].dwVertexCount, uProgress, uDraw);
			uFaceCount -= uDraw;
			uProgress += uDraw * 3;
		}
	}
	else {
		uFaceCount = m_smMesh[dwIndex].dwVertexCount / 3;
		uProgress = 0;
		while (uFaceCount) {
			uDraw = MIN(uMaxVertBy3, uFaceCount);
			dev->DrawPrimitive (D3DPT_TRIANGLELIST, uProgress, uDraw);
			uFaceCount -= uDraw;
			uProgress += uDraw * 3;
		}
	}

	return ZERO;
}



//********************************************************************************
//Restituisce ZERO.
//********************************************************************************
int CMesh::m_PreRenderSteps (LPDIRECT3DDEVICE9 dev) {
	dev->SetFVF (D3DFVF_CMESH);
	return ZERO;
}



//********************************************************************************
//Restituisce ZERO.
//********************************************************************************
int CMesh::m_PostRenderSteps (LPDIRECT3DDEVICE9 dev) {
	dev->SetTexture (0, NULL);
	dev->SetStreamSource (0, NULL, 0, 0);
	dev->SetIndices (NULL);
	return ZERO;
}