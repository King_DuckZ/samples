//CSimpleSolid.cpp


#define _DEFINE_FVF_SIMPLESOLID

#include "Constants.h"
#include <windows.h>
#include "CSimpleSolid.h"



//********************************************************************************
//Costruttore standard
//********************************************************************************
CSimpleSolid::CSimpleSolid () {
	m_Reset();
}



//********************************************************************************
//Imposta tutte le variabili sui valori iniziali. Usata dal costruttore e
//da Dispose().
//********************************************************************************
inline void CSimpleSolid::m_Reset () {
	CMathEngine math;
	
	bApplyTransform = false;
	//MATH_INITMATRIX16 (matTransform);
	math.Mat16Identity (&matTransform);
	texTexture = NULL;
	
	m_nVertexCount = 0;
	m_nIndexCount = 0;
	Flags = DEF_FLAGS;
	m_vb = NULL;
	m_ib = NULL;
}	



//********************************************************************************
//Distruttore standard
//********************************************************************************
CSimpleSolid::~CSimpleSolid () {
	m_Dispose ();
}



//********************************************************************************
//Chiama m_Dispose()
//********************************************************************************
void CSimpleSolid::Dispose () {
	m_Dispose ();
	m_Reset ();
}



//********************************************************************************
//Libera la memoria occupata e distrugge gli oggetti creati.
//********************************************************************************
inline void CSimpleSolid::m_Dispose () {
	SAFE_RELEASE (texTexture);
	SAFE_RELEASE (m_ib);
	SAFE_RELEASE (m_vb);
}



//********************************************************************************
//Imposta il device secondo i flag definiti in this->Flags, e renderizza
//il buffer corrente. Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CSimpleSolid::Render (LPDIRECT3DDEVICE9 dev) {
	if ( (dev && m_vb && m_ib) == false )
		return NONZERO;
	
	DWORD dwOldCullMode;
	dev->GetRenderState (D3DRS_CULLMODE, &dwOldCullMode);
	
	 //Se la texture interna non � applicabile
	if (texTexture == NULL) {
		Flags = (SOLID_FLAGS)(Flags & ~SOLIDF_TEXTUREOVERDIFFUSE);
		Flags = (SOLID_FLAGS)(Flags & ~SOLIDF_SETOBJECTTEXTURE);
	}
	//Se non si vogliono applicare texture
	if ((Flags && SOLIDF_TEXTURIZED) == 0)
		Flags = (SOLID_FLAGS)(Flags & ~SOLIDF_SETOBJECTTEXTURE);
	//Se non � specificata la sorgente per il canale alpha, questo non pu� essere applicato
	if (Flags & (SOLIDF_USETEXTUREALPHA | SOLIDF_USEDIFFUSEALPHA) == FALSE)
		Flags = (SOLID_FLAGS)(Flags & ~SOLIDF_ALPHABLEND);
		
	if (bApplyTransform)
		dev->SetTransform (D3DTS_WORLD, (D3DMATRIX*)&matTransform);
	dev->SetFVF (D3DFVF_SIMPLESOLID);
	
	if ((Flags & SOLIDF_UNTOUCHED) == 0) {
		dev->SetRenderState (D3DRS_ALPHATESTENABLE, (Flags & SOLIDF_ALPHATEST ? TRUE : FALSE));
		dev->SetRenderState (D3DRS_SHADEMODE, (Flags & SOLIDF_GOURAUD ? D3DSHADE_GOURAUD : D3DSHADE_FLAT));
		dev->SetRenderState (D3DRS_ZENABLE, !(Flags & SOLIDF_IGNOREZ));
		
		if (Flags & SOLIDF_2SIDED)
			dev->SetRenderState (D3DRS_CULLMODE, D3DCULL_NONE);
		else if (Flags & SOLIDF_REVERSEFACES)
			dev->SetRenderState (D3DRS_CULLMODE, D3DCULL_CW);
		else
			dev->SetRenderState (D3DRS_CULLMODE, D3DCULL_CCW);
		
		if (Flags & SOLIDF_SETOBJECTTEXTURE)
			dev->SetTexture (0, texTexture);
		dev->SetTextureStageState (0, D3DTSS_COLORARG1, (Flags & SOLIDF_TEXTURIZED ? D3DTA_TEXTURE : D3DTA_DIFFUSE));
		dev->SetTextureStageState (0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
		
		if (Flags & SOLIDF_ALPHABLEND) {
			dev->SetRenderState (D3DRS_ALPHABLENDENABLE, TRUE);
			dev->SetRenderState (D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
			dev->SetRenderState (D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);

			if ((Flags & (SOLIDF_USETEXTUREALPHA | SOLIDF_USEDIFFUSEALPHA)) == (SOLIDF_USETEXTUREALPHA | SOLIDF_USEDIFFUSEALPHA)) {
				//Usa l'alpha della texture e del colore
				dev->SetTextureStageState (0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
				dev->SetTextureStageState (0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);
				dev->SetTextureStageState (0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
			}
			else if (Flags & SOLIDF_USETEXTUREALPHA) {
				//Usa solo l'alpha della texture
				dev->SetTextureStageState (0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
				dev->SetTextureStageState (0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
			}
			else if (Flags & SOLIDF_USEDIFFUSEALPHA) {
				//Usa solo l'alpha del colore
				dev->SetTextureStageState (0, D3DTSS_ALPHAARG1, D3DTA_DIFFUSE);
				dev->SetTextureStageState (0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
			}
		}
		else {
			dev->SetRenderState (D3DRS_ALPHABLENDENABLE, FALSE);
		}
		
		if (Flags & SOLIDF_TEXTUREOVERDIFFUSE) {
			dev->SetTextureStageState (0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
			dev->SetTextureStageState (0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
			dev->SetTextureStageState (0, D3DTSS_COLOROP, D3DTOP_BLENDTEXTUREALPHA);
		}
		
	}
	
	dev->SetStreamSource (0, m_vb, 0, sizeof(SIMPLESOLID_VERTEX));
	dev->SetIndices (m_ib);
	dev->DrawIndexedPrimitive (D3DPT_TRIANGLELIST, 0, 0, m_nVertexCount, 0, m_nIndexCount / 3);
	
	dev->SetRenderState (D3DRS_CULLMODE, dwOldCullMode);
	dev->SetTexture (0, NULL);

	return ZERO;
}