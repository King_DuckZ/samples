iRO2 - demo notes
Launch release.exe for an 800x600 resolution, or releaseHI.exe for a 1024x768
resolution.


________________________________________________________________________________________

   Game controls
________________________________________________________________________________________

The software is able to gather input either from keyboard or joypad, or any other
gaming device, if attached and installed. The control system has been developed for
Sony DualShock, and it has been tested with both Sony DualShock and Microsoft
SideWinder Freestyle Pro. It is possible to use both digital and analog controls.
Following, are described game controls for Sony DualShock and keyboard.

Sony DualShock
Left analog lever (up): walk/run forward
Left analog lever (left): rotate counter clockwise
Left analog lever (right): rotate clockwise
Left analog lever (down): walk backward

Directional pad (analog mode): the character moves like described for the left lever.

X button: hold to run
O button: jump
Start button: quit the program


Keyboard
Up arrow: walk forward
Left arrow: rotate counter clockwise
Right arrow: rotate clockwise
Down arrow: walk backward

Right ctrl key: hold to run
Right shift key: jump


Controls are character-relative: walking directions forward and backward are
relative to its facing direction.



________________________________________________________________________________________

   Development notes
________________________________________________________________________________________

Unfortunately, I couldn't get rid of the D3DX library. Although I've written a
math class which takes care of every matrix, vector and quaternion operations,
and I made a class for loading bitmaps, I still needed D3DX for few tasks, like
building the projection matrix.
This demo is based on the previous project "iRO". Although the idea was to simply
replace the bidimensional sprite with a 3D model, during the development it became
necessary to change and reorganize many pieces of code. What is left of the original
"iRO" in this demo is the camera management class and the text class (only used to
show FPS). Those classes, however, have been modified, since they lacked functions
I needed in this second demo and they needed a few bugs' correction.
Following, there is a list a softwares I used during the development:

3D Studio MAX
As usual it turned out to be a very useful program, either for viewing 3D models and
for applying textures.

Right Hemisphere Deep Exploration
The former character's model was in MD3 fileformat. I needed to change it into a
COB, since that's the fileformat I decided to use for the demo. Althought the many
difficulties in identifying the right keyframes, this program was indispensable for
the development progress (unfortunately the glitch in the idle character's pose
is due to the fact I couldn't find the right keyframe sequence within the original
MD3 file).

Caligari TrueSpace
Having to develop a class for loading COB files, I found appropriate to use the
latest specifications available, foreseeing an eventual reuse of the class.
TrueSpace was important not only to apply textures to the model exported from
DeepExploration, but also for scaling, rotating and converting the model into the
most recent COB fileformat, therefore into a format supported from my loading class.

DialogueMaker
It's an old tool I wrote time ago, which I updated few months ago. It's able to
generate a list of RECT structures from a bitmap image. Those RECTs are used to
identify areas containing text characters into the bitmap file. The generated file
(.dlg) also holds informations on the ASCII alphabet corresponding to the one onto
the bitmap. This is very important, since it allows dynamic conversion from ASCII
strings into indices to generated RECTs.

Microsoft Visual Studio 6
I used the Microsoft environment only to create the resource file embedded into
the final exe, and to test certain algorithms. Sadly it is not compatible with
the DirectX SDK I used.

Metrowerks CodeWarrior
It's a good IDE and an excellent compiler. I chosed this environment to develop
DirectX9 applications. This demo has been completely written and compiled within
this environment.



________________________________________________________________________________________

   Special thanks
________________________________________________________________________________________

I'd like to thank first of all the members of GameProg.it, especially programmer
and giuseppemag, for answering to my questions, not only relatively the development
of this demo.
I'd like to thank Brothers Grim, the authors of the model's animations I used
(Q4Gladiator) and for answering quickly to my e-mails. I would like to thank the
others who took part in the model's realization, even if I never spoke to them:
Per Abrahamsen
Paul Greveson
Fran�ois-Xavier Delmotte
Mat Ferguson
Mike Winder
Dinko Pavicic
Gutboy
Glen
Timex

I want to thank DH as well, Ensemble Studios' artist, of whom I don't know the name,
for allowing me to use his 3D model (Samjai). Although his model and textures were
stunning, for many reasons I decided to use Q4Gladiator, but I want to thank him
anyways for his kindness. However, Samjai is still visible in the loading screen.
You probably won't notice it, since loading is really quick, but it's there.
I would thank Gravity staff again for creating the wonderful Ragnarok Online.
Without that game I would have never created the first demo "iRO", and surely
it would have affected the development of this second demo, as well.

Last, a special thanks goes to Giuliano and Francesco, who supported me during
the development and who helped me testing the software on different platforms.



________________________________________________________________________________________

   Contacts
________________________________________________________________________________________

You can reach me on the MSN at the address

kneelion@hotspider.net


You can also contact me via e-mail at the same MSN address, or preferibly to
this e-mail address:

michelesantullo@virgilio.it
