
//Game.cpp


#include "Constants.h"
#include <windows.h>
#include "Game.h"
#include <d3d9.h>
#include <stdio.h>

#include "CBitmapSarge.h"
#include "CCamera.h"
#include "CCaligari.h"
#include "CTweenedMesh.h"
#include "CPairList.h"
#include "CSpeech.h"
#include "CMathEngine.h"
#include "CInputDevice.h"
#include "CFlatGround.h"


//Costanti
#define GROUND_LEVEL 0.0f
#define GROUND_WIDTH 200.0f
#define GROUND_DEPTH 130.0f
#define SKY_HEIGHT 100.0f
#define GROUND_TEXTUREU 12.5f
#define GROUND_TEXTUREV 8.125f
#define WALK_SPEED 8.5f
#define RUN_SPEED 22.0f

#define MECH_FRAMECOUNT 165
#define MECH_ANIMCOUNT 7

//Nessuno stato
#define GCHAR_NONE 0x00
//Il personaggio � in aria
#define GCHAR_FLOATING 0x01
//Il personaggio sta atterrando
#define GCHAR_LANDING 0x02
//Il personaggio sta saltando
#define GCHAR_JUMPING 0x04
//Il personaggio sta cadendo
#define GCHAR_FREEFALLING 0x08
//Il personaggio si sta muovendo
#define GCHAR_STROLLING 0x10
//Il personaggio sta camminando in avanti
#define GCHAR_FORWARD 0x20
//Il personaggio sta camminando all'indietro
#define GCHAR_BACKWARD 0x40
//Il personaggio sta correndo
#define GCHAR_RUNNING 0x80
//Il personaggio sta girando su s� stesso
#define GCHAR_TURNING 0x100
//Il personaggio sta girando in senso orario
#define GCHAR_TURNINGCW 0x200
//Il personaggio sta girando in senso antiorario
#define GCHAR_TURNINGCCW 0x400
//Il personaggio deve iniziare l'animazione di atterraggio
#define GCHAR_LANDINGBEGIN 0x800
//Il personaggio non deve essere allineato col terreno
#define GCHAR_DETACHED 0x1000

enum CHARACTER_ACTION {
	CACT_WALK = 0,
	CACT_RUN = 1,
	CACT_WALKBACK = 2,
	CACT_IDLE = 3,
	CACT_JUMP = 4,
	CACT_LAND = 5,
	CACT_TURN = 6,
	CACT_FORCEDWORD = 0xFFFFFFFF
};

//Strutture
typedef struct STRUCT_GAME_CHARACTER {
	BOOL bRebuildMatrix;
	DWORD dwFlags;
	CHARACTER_ACTION caAction;
	float fJumpTime;
	float fJumpVelocity;
	float fJumpHeight;
	float fRotation;
	float fWalkVelocity;
	
	MATH_3DVECTOR vecFacing;
	MATH_3DVECTOR vecPosition;
	MATH_3DVECTOR vecPositionOld;
	MATH_3DVECTOR vecScale;
	
	MATH_MATRIX16 matTransform;
	MATH_MATRIX16 matFinal;
	
	CTweenedMesh tmMesh;
} GAME_CHARACTER, *LPGAME_CHARACTER;
typedef struct STRUCT_GAME_TITLE {
	BOOL bVisible;
	BOOL bAvailable;
	float fLifeLeft;
	float fAngle;
	MATH_3DVECTOR vecPos;
	MATH_MATRIX16 matTransform;
	MATH_MATRIX16 matFinal;
	CFlatGround fgQuad;
} GAME_TITLE, *LPGAME_TITLE;
typedef struct STRUCT_GAME_OBJECT {
	BOOL bVisible;
	CMesh mMesh;
	MATH_MATRIX16 matTransform;
	MATH_3DVECTOR vecScale;
} GAME_OBJECT, *LPGAME_OBJECT;

//Prototipi
int Ingame ( float fDelta );
int LoadPictureToTexture ( LPDIRECT3DTEXTURE9* tex, LPCTSTR strFileName );
int InitIngame ( void );
int CleanIngame ( void );
inline void GameReadInput ( void );
int CreateGameTitle ( LPGAME_TITLE gt, const char* strTexture, float fWidth, float fHeight, const LPMATH_3DVECTOR vecPos );
int RenderIngame ( LPMATH_MATRIX16 matRef = NULL, bool bRenderAlphaItems = false, bool bKeepRenderState = false );
int UpdateCharacter ( const LPINPUTDATA id, LPGAME_CHARACTER c, float fDelta, bool bAllowExit = false );
BOOL CheckCollision ( LPGAME_CHARACTER c );
inline void RebuildCharacterMatrix ( LPGAME_CHARACTER c );
inline void AlignToGround ( LPGAME_CHARACTER c );
int SaveScreenshot ( void );


//Variabili globali esterne
extern LPDIRECT3DDEVICE9 g_dev;
extern CSpeech g_spcFPS;
extern D3DCAPS9 g_caps;
extern DWORD g_dwAnisotropy;
extern DWORD g_dwMagFilter;
extern DWORD g_dwMinFilter;
extern DWORD g_dwMinSimpleFilter;
extern DWORD g_dwMagSimpleFilter;

//Varibili globali
namespace Game {

GAME_CHARACTER g_gcTheMan;
LPDIRECTINPUT8 g_DI;
LPINPUTDATA g_pidData = NULL;
MATH_MATRIX16 g_matIdentity;
GAME_TITLE g_gtWelcome;
MATH_PLANE g_plnWalls[4];
CCamera g_cam;
CCaligari g_calgMeshLoader;
CPairList g_plTexturePairs;
CInputDevice g_idevKeyboard;
CInputDevice g_idevJoypad;
CMathEngine g_math;
CFlatGround g_fgGround;

};

//Variabili per uso esterno
int (*GameFunction)(float) = NULL;
int (*GameClean)(void) = NULL;
bool g_bShowFPS = true;
bool g_bRunning = true;
bool g_bPrinted = false; //Traccia la pressione del tasto stamp

using namespace Game;

//********************************************************************************
//Funzione di gioco principale
//********************************************************************************
int Ingame (float fDelta) {
	if (g_dev == NULL)
		return NONZERO;
	
	MATH_MATRIX16 matTmp;
	D3DVIEWPORT9 vpViewOld, vpView;
	
	//Lettura dell'input e movimento personaggi
	GameReadInput ();
	UpdateCharacter (g_pidData, &g_gcTheMan, fDelta, true);
	
	//Screenshot
	if ( (g_pidData->dwKeys & IDBUTTON_PRINT) && !g_bPrinted ) {
		g_bPrinted = true;
		SaveScreenshot();
	}
	else if ((g_pidData->dwKeys & IDBUTTON_PRINT) == 0)
		g_bPrinted = false;
	
	//Controllo collisioni e costruzione della matrice del personaggio
	RebuildCharacterMatrix (&g_gcTheMan);
	if (CheckCollision (&g_gcTheMan))
		RebuildCharacterMatrix (&g_gcTheMan);
	g_gcTheMan.bRebuildMatrix = FALSE;
	
	//Animazione
	g_gcTheMan.tmMesh.AdvanceAnimation (fDelta);
	g_gcTheMan.tmMesh.SetActiveAnimation (g_gcTheMan.caAction);
	
	//Animazione dei titoli
	if (g_gtWelcome.bAvailable && g_gtWelcome.bVisible) {
		g_gtWelcome.fLifeLeft -= fDelta;
		if (g_gtWelcome.fLifeLeft < 0.0f) {
			g_gtWelcome.bVisible = FALSE;
			SAFE_RELEASE (g_gtWelcome.fgQuad.texGround);
		}
		else {
//			MATH_MATRIX16 matTmp;
			
			g_gtWelcome.fAngle += 2.5f * fDelta;
			g_gtWelcome.vecPos.z += fDelta * 1.6;
			
			g_math.Mat16TransformRotateY (&g_gtWelcome.matFinal, g_gtWelcome.fAngle);
			g_math.Mat16Multiply (&g_gtWelcome.matFinal, &g_gtWelcome.matTransform, &g_gtWelcome.matFinal);
			g_math.Mat16Translation (&matTmp, g_gtWelcome.vecPos.x, g_gtWelcome.vecPos.y, g_gtWelcome.vecPos.z);
			g_math.Mat16Multiply (&g_gtWelcome.matFinal, &g_gtWelcome.matFinal, &matTmp);
		}
	}
	
	//Orientamento della telecamera verso il personaggio
	MATH_3DVECTOR vecCentre;
	g_gcTheMan.tmMesh.Bounding.UsingFigure (BFTYPE_BOX);
	g_gcTheMan.tmMesh.Bounding.GetCentre (g_gcTheMan.tmMesh.GetCurrentFrame(), &vecCentre);
	g_math.Vec3Transform (&vecCentre, &g_gcTheMan.matFinal);
	g_cam.SetTarget (&vecCentre);
	
	//Rendering
	g_cam.BuildCamMatrix();
	g_dev->SetTransform (D3DTS_VIEW, (D3DMATRIX*)&g_cam.matCam);
	
	g_dev->Clear (0, NULL, D3DCLEAR_ZBUFFER | D3DCLEAR_TARGET | D3DCLEAR_STENCIL, 0/*D3DCOLOR_XRGB(10, 36, 106)*/, 1.0f, 0);
	
	if (SUCCEEDED(g_dev->BeginScene ())) {
	
		//Per i poligoni con alpha blending, il terreno serve subito
		g_dev->SetRenderState (D3DRS_CULLMODE, D3DCULL_CCW);
		g_dev->SetRenderState (D3DRS_STENCILENABLE, FALSE);
		g_dev->SetRenderState (D3DRS_ZWRITEENABLE, FALSE);
		g_dev->SetTransform (D3DTS_WORLD, (D3DMATRIX*)&g_matIdentity);
		g_fgGround.Render (g_dev);
		g_dev->SetRenderState (D3DRS_ZWRITEENABLE, TRUE);
		
		//Renderizzo la scena normale
		g_dev->SetRenderState (D3DRS_STENCILENABLE, FALSE);
		g_dev->SetRenderState (D3DRS_CULLMODE, D3DCULL_CCW);
		RenderIngame (NULL, true);

		//Renderizzo gli oggetti riflettenti
		g_dev->SetRenderState (D3DRS_STENCILENABLE, TRUE);
		g_dev->SetRenderState (D3DRS_STENCILPASS, D3DSTENCILOP_REPLACE);
		g_dev->SetRenderState (D3DRS_STENCILREF, 1);
		g_dev->SetRenderState (D3DRS_STENCILFUNC, D3DCMP_ALWAYS);
		//Disabilito il color buffer
		g_dev->SetRenderState (D3DRS_ALPHABLENDENABLE, TRUE);
		g_dev->SetRenderState (D3DRS_SRCBLEND, D3DBLEND_ZERO);
		g_dev->SetRenderState (D3DRS_DESTBLEND, D3DBLEND_ONE);
		g_dev->SetTransform (D3DTS_WORLD, (D3DMATRIX*)&g_matIdentity);
		g_fgGround.Render (g_dev);
		
		g_dev->GetViewport (&vpViewOld);
		vpView = vpViewOld;
		vpView.MinZ = 1.0f;
		vpView.MaxZ = 1.0f;
		g_dev->SetViewport (&vpView);
		g_dev->SetRenderState (D3DRS_ZFUNC, D3DCMP_ALWAYS);
		g_dev->SetRenderState (D3DRS_STENCILFUNC, D3DCMP_EQUAL);
		g_dev->SetRenderState (D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);
		g_dev->SetRenderState (D3DRS_STENCILZFAIL, D3DSTENCILOP_KEEP);
		g_dev->SetRenderState (D3DRS_STENCILPASS, D3DSTENCILOP_KEEP);
		g_fgGround.Render (g_dev);
		g_dev->SetRenderState (D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
		g_dev->SetRenderState (D3DRS_ALPHABLENDENABLE, FALSE);
		g_dev->SetViewport (&vpViewOld);
		
		//Renderizzo i riflessi
		g_math.Mat16TransformScale (&matTmp, 1.0f, -1.0f, 1.0f);
		//g_dev->SetRenderState (D3DRS_STENCILPASS, D3DSTENCILOP_INCR);
		g_dev->SetRenderState (D3DRS_CULLMODE, D3DCULL_CW);
		g_dev->SetRenderState (D3DRS_ALPHABLENDENABLE, TRUE);
		g_dev->SetRenderState (D3DRS_BLENDOP, D3DBLENDOP_ADD);
		g_dev->SetRenderState (D3DRS_DESTBLEND, D3DBLEND_ONE);
		g_dev->SetRenderState (D3DRS_SRCBLEND, D3DBLEND_ONE);
		RenderIngame (&matTmp, true, true);
		g_dev->SetRenderState (D3DRS_BLENDOP, D3DTOP_DISABLE);
		g_dev->SetRenderState (D3DRS_ALPHABLENDENABLE, FALSE);
		
		//Geometria T&L
		g_dev->SetRenderState (D3DRS_CULLMODE, D3DCULL_CCW);
		g_dev->SetRenderState (D3DRS_STENCILENABLE, FALSE);
		g_spcFPS.Render (g_dev);
		
		g_dev->EndScene ();
	}
	
	g_dev->Present (NULL, NULL, NULL, NULL);
	return ZERO;
}



//********************************************************************************
//Imposta GameFunction in modo che punti alla funzione che si desidera
//utilizzare. Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int SelectGameFunction (GAME_FUNCTION func, bool bReset) {
	switch (func) {
	case GF_INGAME:
		GameFunction = &Ingame;
		GameClean = &CleanIngame;
		if (bReset)
			InitIngame ();
		return ZERO;
	
	case GF_UNDEFINED:
		GameFunction = NULL;
		GameClean = NULL;
		return ZERO;
	
	default:
		return NONZERO;
	}
}



//********************************************************************************
//Carica la bitmap specificata nella texture specificata. Restituisce
//ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int LoadPictureToTexture (LPDIRECT3DTEXTURE9* tex, LPCTSTR strFileName) {
	CBitmapSarge bmp;
	D3DFORMAT BitmapFormat;
	LPDIRECT3DSURFACE9 surf = NULL;
	D3DLOCKED_RECT LockedRect;
	BYTE* src = NULL;
	BYTE* dst = NULL;
	
	if (strFileName == NULL || tex == NULL)
		return NONZERO;
	
	SAFE_RELEASE (*tex);
	bmp.LoadPicture (strFileName);
	
	switch (bmp.BitmapType) {
	case BTYPE_24BIT:
		BitmapFormat = D3DFMT_R8G8B8;
		break;
	case BTYPE_A8R8G8B8:
		BitmapFormat = D3DFMT_A8R8G8B8;
		break;
	case BTYPE_R8G8B8A8:
		bmp.RGBAtoARGB();
		BitmapFormat = D3DFMT_A8R8G8B8;
		break;
	case BTYPE_A1R5G5B5:
		BitmapFormat = D3DFMT_A1R5G5B5;
		break;
	case BTYPE_R5G6B5:
		BitmapFormat = D3DFMT_R5G6B5;
		break;
	case BTYPE_8BIT:
		bmp.ConvertTo32bit ();
		BitmapFormat = D3DFMT_A8R8G8B8;
		break;
	default:
		return NONZERO;
	}
	
	if (FAILED(g_dev->CreateTexture (bmp.GetPictureWidth(), bmp.GetPictureHeight(), 1,
		0, BitmapFormat, D3DPOOL_MANAGED, tex, NULL)))
		return NONZERO;
	
	if (FAILED((*tex)->GetSurfaceLevel (0, &surf)))
		goto Error;
	ZeroMemory (&LockedRect, sizeof(D3DLOCKED_RECT));
	if (SUCCEEDED(surf->LockRect (&LockedRect, NULL, 0))) {
		bmp.GetDataPtr((void**)&src);
		dst = (BYTE*)LockedRect.pBits;
		const int nMinPitch = MIN(LockedRect.Pitch, bmp.GetPitch());
		
		for (int y = 0; y < bmp.GetPictureHeight(); y++) {
			CopyMemory (dst, src, nMinPitch);
			src += bmp.GetPitch();
			dst += LockedRect.Pitch;
		}
		
		surf->UnlockRect ();
	}
	
	SAFE_RELEASE (surf);
	return ZERO;
	
Error:
	SAFE_RELEASE (surf);
	SAFE_RELEASE (*tex);
	return NONZERO;
}



//********************************************************************************
//Inizializza oggetti e variabili utilizzati da Ingame(). Restituisce ZERO
//se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int InitIngame () {
//	g_tmTheMan.SetDeviceCaps (&g_caps);
	
	//Impostazione telecamera
	g_cam.BuildDefaultProjection ();
	g_cam.SetTarget (0.0f, 1.0f, 0.0f);
	g_cam.SetZoomRange (2.0f, 40.0f);
	g_cam.SetPitchRange (0.0f, MATH_PI / 3.0f);
	g_cam.Rotate (/*MATH_PI / -3.33f*/0.0f, MATH_PI / 4.5f);
	g_cam.Zoom (7.3f);
	g_cam.BuildCamMatrix ();

	g_dev->SetTransform (D3DTS_PROJECTION, (D3DMATRIX*)&g_cam.matProj);
	g_dev->SetTransform (D3DTS_VIEW, (D3DMATRIX*)&g_cam.matCam);

	//Pulizia
	CleanIngame();
	
	//Caricamento mesh
	g_gcTheMan.tmMesh.SetPairList (&g_plTexturePairs, false);
	g_calgMeshLoader.LoadFile ("mech.cob");
	g_calgMeshLoader.BuildMesh (g_dev, &g_gcTheMan.tmMesh);
	//Creazione terreno
	g_fgGround.BuildGround (g_dev, GROUND_WIDTH, GROUND_DEPTH, GROUND_LEVEL, GROUND_TEXTUREU, GROUND_TEXTUREV);
	LoadPictureToTexture (&g_fgGround.texGround, "marble.bmp");
//	LoadPictureToTexture (&g_fgGround.texReflection, "CLOUD2.BMP");
	//Creazione scritte
	MATH_3DVECTOR vecTitlePos;
	MATH_SET3DVECTOR (vecTitlePos, 0.0f, 2.25, -10.0f);
	CreateGameTitle (&g_gtWelcome, "welcome.bmp", 5.0f, 2.5f, &vecTitlePos);
	
	//Caricamento animazione
	g_gcTheMan.tmMesh.LoadRawVertices ("mech_anim.vtx", MECH_FRAMECOUNT);
	g_gcTheMan.tmMesh.BuildAnimation (g_dev);
	g_gcTheMan.tmMesh.LoadRawAnimationInfo ("mech_anim.bin", MECH_ANIMCOUNT);
	
	//Impostazione variabili
	MATH_SET3DVECTOR (g_gcTheMan.vecFacing, 1.0f, 0.0f, 0.0f);
	MATH_SET3DVECTOR (g_gcTheMan.vecScale, 2.2f, 2.2f, 2.2f);
	MATH_INIT3DVECTOR (g_gcTheMan.vecPosition);
	MATH_INIT3DVECTOR (g_gcTheMan.vecPositionOld);
	g_math.Mat16Identity (&g_gcTheMan.matFinal);
	g_math.Mat16TransformScale (&g_gcTheMan.matTransform, g_gcTheMan.vecScale.x, g_gcTheMan.vecScale.y, g_gcTheMan.vecScale.z);
	g_gcTheMan.caAction = CACT_JUMP;
	g_gcTheMan.tmMesh.SetActiveAnimation (g_gcTheMan.caAction);
	g_gcTheMan.tmMesh.CompleteAnimation ();
	g_gcTheMan.dwFlags = GCHAR_FREEFALLING | GCHAR_FLOATING;//GCHAR_NONE;
	g_gcTheMan.fJumpTime = 0.0f;
	g_gcTheMan.fJumpVelocity = 0.0f;
	g_gcTheMan.fRotation = 0.0f;
	g_gcTheMan.fJumpHeight = 25.0f;//0.0f;
	g_gcTheMan.bRebuildMatrix = TRUE;
	g_gcTheMan.fWalkVelocity = 0.0f;
	
	//Creazione dei muri
	MATH_3DVECTOR vecPoint, vecNorm;
	MATH_SET3DVECTOR (vecNorm, 0.0f, 0.0f, -1.0f); //Muro nord
	MATH_SET3DVECTOR (vecPoint, 0.0f, 0.0f, GROUND_DEPTH / 2.0f);
	g_math.PlaneBuildEquation (g_plnWalls + 0, &vecNorm, &vecPoint);
	MATH_SET3DVECTOR (vecNorm, -1.0f, 0.0f, 0.0f); //Muro est
	MATH_SET3DVECTOR (vecPoint, GROUND_WIDTH / 2.0f, 0.0f, 0.0f);
	g_math.PlaneBuildEquation (g_plnWalls + 1, &vecNorm, &vecPoint);
	MATH_SET3DVECTOR (vecNorm, 0.0f, 0.0f, 1.0f); //Muro sud
	MATH_SET3DVECTOR (vecPoint, 0.0f, 0.0f, -GROUND_DEPTH / 2.0f + 1.0f);
	g_math.PlaneBuildEquation (g_plnWalls + 2, &vecNorm, &vecPoint);
	MATH_SET3DVECTOR (vecNorm, 1.0f, 0.0f, 0.0f); //Muro ovest
	MATH_SET3DVECTOR (vecPoint, -GROUND_WIDTH / 2.0f + 1.0f, 0.0f, 0.0f);
	g_math.PlaneBuildEquation (g_plnWalls + 3, &vecNorm, &vecPoint);
	
	//Cancellare
	MATH_SET3DVECTOR (g_gcTheMan.vecPosition, 0.0f, 25.0f, 0.0f);

	//Libero la memoria
	g_calgMeshLoader.Dispose ();
	
	//Ciao
	return ZERO;
}



//********************************************************************************
//Libera la memoria occupata dagli elementi creati da InitGame(). Restituisce
//ZERO.
//********************************************************************************
int CleanIngame () {
	//Pulizia delle texture caricate
	SAFE_RELEASE (g_fgGround.texReflection);
	SAFE_RELEASE (g_fgGround.texGround);
	for (unsigned int z = 0; z < g_plTexturePairs.GetListCount(); z++) {
		LPDIRECT3DTEXTURE9 tex;
		
		g_plTexturePairs.GetListIndex (z, NULL, (int*)&tex, NULL);
		SAFE_RELEASE (tex);
	}
	g_plTexturePairs.Dispose ();
	
	g_gcTheMan.tmMesh.Dispose ();
	
	g_fgGround.Dispose ();
	
	SAFE_RELEASE (g_gtWelcome.fgQuad.texGround);
	g_gtWelcome.fgQuad.Dispose ();
	return ZERO;
}



//********************************************************************************
//Inizializza gli oggetti DirectInput e le varibili generali.
//Restituisce ZERO se l'inizializzazione di almeno un dispositivo va a
//buon fine, altrimenti NONZERO.
//********************************************************************************
int GameInit (HWND hWnd) {
	GameEnd ();
	
	if (FAILED(DirectInput8Create(GetModuleHandle(NULL), DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&g_DI, NULL)))
		return NONZERO;
	
	g_idevJoypad.DI = g_DI;
	g_idevKeyboard.DI = g_DI;
	
	const BOOL bGotJoypad = !g_idevJoypad.CreateDevice (IDDEVTYPE_JOYPAD, hWnd);
	const BOOL bGotKeyboard = !g_idevKeyboard.CreateDevice (IDDEVTYPE_KEYBOARD, hWnd);
	
	//Se ho il joypad, voglio l'indirizzo della struttura InputDevice perch�
	//GameReadInput() legge i dati dal joypad per ultimo, se presente.
	if (bGotJoypad)
		g_pidData = &g_idevJoypad.InputData;
	//Se ho la tastiera, invece, voglio il suo indirizzo
	else if (bGotKeyboard)
		g_pidData = &g_idevKeyboard.InputData;
	//Beh, niente da fare ^^
	else
		g_pidData = NULL;
	
	//Variabili
	g_math.Mat16Identity (&g_matIdentity);
	
	if (!(bGotJoypad || bGotKeyboard)) {
		GameEnd ();
		return NONZERO;
	}
	else
		return ZERO;
}



//********************************************************************************
//Distrugge gli oggetti creati da GameInit(). Restituisce ZERO.
//********************************************************************************
int GameEnd () {
	g_idevJoypad.Dispose ();
	g_idevKeyboard.Dispose ();
	
	SAFE_RELEASE (g_DI);
	
	return ZERO;
}



//********************************************************************************
//Legge l'input dai dispositivi inizializzati con successo.
//********************************************************************************
inline void GameReadInput () {
	//Se la tastiera � disponibile
	if (g_idevKeyboard.Device != NULL) {
		g_idevKeyboard.SetInitialData (NULL);
		g_idevKeyboard.GetInput ();
		
		//Se c'� anche il joypad
		if (g_idevJoypad.Device != NULL) {
			g_idevJoypad.SetInitialData (&g_idevKeyboard.InputData);
			g_idevJoypad.GetInput ();
		}
	}
	//La tastiera non c'� (?), controllo il joypad
	else if (g_idevJoypad.Device != NULL) {
		g_idevJoypad.SetInitialData (NULL);
		g_idevJoypad.GetInput ();
	}
}



//********************************************************************************
//Crea un quadrato utilizzabile per mostrare del testo o delle texture in
//generale. Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CreateGameTitle (LPGAME_TITLE gt, const char* strTexture, float fWidth, float fHeight, const LPMATH_3DVECTOR vecPos) {
	if (gt == NULL || vecPos == NULL)
		return NONZERO;
	
	SAFE_RELEASE (gt->fgQuad.texGround);
	gt->fgQuad.Dispose ();
	
	if (strTexture)
		LoadPictureToTexture (&gt->fgQuad.texGround, strTexture);
	
	gt->bAvailable = TRUE;
	gt->bVisible = TRUE;
	gt->fLifeLeft = 12.0f;
	gt->fAngle = 0.0f;
	gt->vecPos = *vecPos;
	
	if (gt->fgQuad.BuildGround (g_dev, fWidth, fHeight, 0.0f, 1.0f, 1.0f)) {
		SAFE_RELEASE (gt->fgQuad.texGround);
		gt->bAvailable = FALSE;
	}
	
	MATH_MATRIX16 matRotation;
	g_math.Mat16TransformRotateX (&matRotation, -MATH_PI / 2.0f);
	//g_math.Mat16Translation (&gt->matTransform, vecPos->x, vecPos->y, vecPos->z);
	//g_math.Mat16Multiply (&gt->matTransform, &matRotation, &gt->matTransform);
	g_math.Mat16Copy (&gt->matTransform, &matRotation);
	g_math.Mat16Copy (&gt->matFinal, &gt->matTransform);
	
	return ZERO;
}



//********************************************************************************
//Renderizza gli oggetti nella scena. Restituisce ZERO se va tutto
//bene, altrimenti NONZERO.
//********************************************************************************
int RenderIngame (LPMATH_MATRIX16 matRef, bool bRenderAlphaItems, bool bKeepRenderState) {
	MATH_MATRIX16 matTmp;
	D3DCULL cullMode;
	
	g_dev->GetRenderState (D3DRS_CULLMODE, (DWORD*)&cullMode);
	if (matRef == NULL) {
		matRef = &g_matIdentity;
	}
	
	//Filtro normale
	g_dev->SetSamplerState (0, D3DSAMP_MAGFILTER, g_dwMagSimpleFilter);
	g_dev->SetSamplerState (0, D3DSAMP_MINFILTER, g_dwMinSimpleFilter);
	
	g_math.Mat16Multiply (&matTmp, &g_gcTheMan.matFinal, matRef);
	
	g_dev->SetTransform (D3DTS_WORLD, (D3DMATRIX*)&matTmp);
	g_gcTheMan.tmMesh.Render (g_dev);
	
	//Filtro anisotropico
	g_dev->SetSamplerState (0, D3DSAMP_MAGFILTER, g_dwMagFilter);
	g_dev->SetSamplerState (0, D3DSAMP_MINFILTER, g_dwMinFilter);
	g_dev->SetSamplerState (0, D3DSAMP_MAXANISOTROPY, g_dwAnisotropy);
	g_dev->SetSamplerState (1, D3DSAMP_MAGFILTER, g_dwMagFilter);
	g_dev->SetSamplerState (1, D3DSAMP_MINFILTER, g_dwMinFilter);
	g_dev->SetSamplerState (1, D3DSAMP_MAXANISOTROPY, g_dwAnisotropy);
	
	//Oggetti 2-sided
	g_dev->SetRenderState (D3DRS_CULLMODE, D3DCULL_NONE);
	if (g_gtWelcome.bAvailable && g_gtWelcome.bVisible && bRenderAlphaItems) {
		g_math.Mat16Multiply (&matTmp, &g_gtWelcome.matFinal, matRef);
		
		g_dev->SetTransform (D3DTS_WORLD, (D3DMATRIX*)&matTmp);
		if (!bKeepRenderState) {
			g_dev->SetRenderState (D3DRS_ALPHABLENDENABLE, TRUE);
			g_dev->SetRenderState (D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
			g_dev->SetRenderState (D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			g_gtWelcome.fgQuad.Render (g_dev);
			g_dev->SetRenderState (D3DRS_ALPHABLENDENABLE, FALSE);
		}
		else
			g_gtWelcome.fgQuad.Render (g_dev);
	}
	g_dev->SetRenderState (D3DRS_CULLMODE, cullMode);
	return ZERO;
}



//********************************************************************************
//Aggiorna il personaggio specificato in base ai flag impostati in *id e in
//c->dwFlags. Se bAllowExit � true, la funzione ha l'autorizzazione a terminare
//la fase di gioco nel caso in cui ci� fosse specificato in *id. Restituisce
//ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int UpdateCharacter (const LPINPUTDATA id, LPGAME_CHARACTER c, float fDelta, bool bAllowExit) {
	if ( (c && id) == FALSE)
		return NONZERO;
	
	//Uscita dalla demo
	if (id->dwKeys & IDBUTTON_EXIT)
		g_bRunning = !bAllowExit;
	
	//Interpretazione dei dati in *id
	//Il personaggio � in aria
	if (c->dwFlags & GCHAR_FLOATING) {
		if (c->dwFlags & GCHAR_JUMPING) {
			//Se l'animazione di salto � finita, il personaggio � da considerare in caduta
//			if (c->tmMesh.bAnimationComplete) {
				c->dwFlags ^= GCHAR_JUMPING;
				c->dwFlags |= GCHAR_FREEFALLING | GCHAR_DETACHED;
//			}
		}
		else if (c->dwFlags & GCHAR_LANDING) {
			//Se l'animazione di atterraggio � finita, il personaggio non � pi� in aria
			if (c->tmMesh.bAnimationComplete) {
				c->dwFlags ^= GCHAR_FLOATING | GCHAR_LANDING | GCHAR_DETACHED;
			}
		}
	}
	//Il personaggio non � in aria
	else {
		//Presuppongo che il personaggio sia fermo
		c->dwFlags &= ~(GCHAR_FLOATING | GCHAR_STROLLING | GCHAR_TURNING);
		c->caAction = CACT_IDLE;
		
		//Salto
		if (id->dwKeys & IDBUTTON_JUMP)
			c->dwFlags |= GCHAR_FLOATING | GCHAR_JUMPING;
		else {
			//Movimento in avanti
			if (id->dwMovement & IDMOVE_FORWARD) {
				c->dwFlags |= GCHAR_STROLLING | GCHAR_FORWARD;
				c->dwFlags &= ~GCHAR_BACKWARD;
			}
			//Movimento all'indietro
			else if (id->dwMovement & IDMOVE_BACKWARD) {
				c->dwFlags |= GCHAR_STROLLING | GCHAR_BACKWARD;
				c->dwFlags &= ~GCHAR_FORWARD;
			}
			
			//Giro su s� stesso in senso orario
			if (id->dwMovement & IDMOVE_RIGHT) {
				c->dwFlags |= GCHAR_TURNING | GCHAR_TURNINGCW;
				c->dwFlags &= ~GCHAR_TURNINGCCW;
			}
			//Giro su s� stesso in senso antiorario
			else if (id->dwMovement & IDMOVE_LEFT) {
				c->dwFlags |= GCHAR_TURNING | GCHAR_TURNINGCCW;
				c->dwFlags &= ~GCHAR_TURNINGCW;
			}
		}
		
		//Corsa
		if (id->dwMovement & IDMOVE_RUNNING)
			c->dwFlags |= GCHAR_RUNNING;
		else
			c->dwFlags &= ~GCHAR_RUNNING;
	}
	
	//Faccio una copia del vecPosition
	MATH_COPY3DVECTOR (c->vecPositionOld, c->vecPosition);
	
	//Fall/Jump/Land
	if (c->dwFlags & GCHAR_FLOATING) {
		//Il personaggio si sta staccando da terra
		if (c->dwFlags & GCHAR_JUMPING) {
			c->fJumpTime = 0.0f;
			c->fJumpVelocity = 10.0f;
			c->fJumpHeight = 0.0f;
			c->caAction = CACT_JUMP;
			AlignToGround (c);
		}
		//Il personaggio sta per atterrare
		else if (c->dwFlags & GCHAR_LANDINGBEGIN) {
			c->dwFlags ^= GCHAR_LANDINGBEGIN;
			c->dwFlags |= GCHAR_LANDING;
			c->caAction = CACT_LAND;
		}
		//Il personaggio sta atterrando
		else if (c->dwFlags & GCHAR_LANDING) {
			c->fWalkVelocity = 0.0f;
			c->caAction = CACT_LAND;
			AlignToGround (c);
		}
		//Il personaggio � in caduta
		else {
			AlignToGround (c);
			
//			if (c->dwFlags & GCHAR_DETACHED)
//				c->vecPosition.y -= g_fgGround.GetFallingPosition(c->fJumpVelocity, c->fJumpTime);
//			else
				c->vecPosition.y =
					c->vecPosition.y + c->fJumpHeight -
					g_fgGround.GetFallingPosition(c->fJumpVelocity, c->fJumpTime) /*-
					fHeight * c->vecScale.y / 2.0f*/;
			
			c->fJumpTime += fDelta;
			c->caAction = CACT_JUMP;
		}
		
		//Sposta il personaggio nella direzione di salto
		MATH_3DVECTOR vecInc;
		MATH_COPY3DVECTOR (vecInc, c->vecFacing);
		g_math.Vec3Scale (&vecInc, fDelta * c->fWalkVelocity / 1.5f);
		g_math.Vec3Sum (&c->vecPosition, &c->vecPosition, &vecInc);
		
		c->bRebuildMatrix = TRUE;
	}
	//Turn left/right
	if ( c->dwFlags & GCHAR_TURNING ) {
		MATH_QUATERNION quatFacing;
		MATH_QUATERNION quatConjugate;
		MATH_QUATERNION quatRotation;
		
		//Verso di rotazione
		if (c->dwFlags & GCHAR_TURNINGCCW) {
			c->fRotation -= fDelta * 1.4f;
			c->caAction = CACT_TURN;
		}
		else {
			c->fRotation += fDelta * 1.4f;
			c->caAction = CACT_TURN;
		}
		
		//Calcolo del vettore verso cui il personaggio � rivolto
		MATH_SET3DVECTOR (c->vecFacing, 1.0f, 0.0f, 0.0f);
		MATH_SETQUATERNION (quatFacing, c->vecFacing.x, c->vecFacing.y, c->vecFacing.z, 0.0f);
		g_math.QuatRotationY (&quatRotation, c->fRotation);
		g_math.QuatGetConjugate (&quatConjugate, &quatRotation);
		
		g_math.QuatMultiply (&quatFacing, &quatRotation, &quatFacing);
		g_math.QuatMultiply (&quatFacing, &quatFacing, &quatConjugate);
		
		MATH_COPY3DVECTOR (c->vecFacing, quatFacing.qv);
		g_math.Vec3Normalize (&c->vecFacing);
		
		if (c->fRotation >= MATH_2PI)
			c->fRotation -= MATH_2PI;
		else if (c->fRotation <= -MATH_2PI)
			c->fRotation += MATH_2PI;
		
		c->bRebuildMatrix = TRUE;
	}
	//Walk/WalkBack/Run
	if (c->dwFlags & GCHAR_STROLLING) {
		MATH_3DVECTOR vecInc;
		
		if (c->dwFlags & GCHAR_FORWARD) {
			if (c->dwFlags & GCHAR_RUNNING) {
				c->fWalkVelocity = RUN_SPEED;
				c->caAction = CACT_RUN;
			}
			else {
				c->fWalkVelocity = WALK_SPEED;
				c->caAction = CACT_WALK;
			}
		}
		else if (c->dwFlags & GCHAR_BACKWARD) {
			c->fWalkVelocity = -WALK_SPEED;
			c->caAction = CACT_WALKBACK;
		}
		
		MATH_COPY3DVECTOR (vecInc, c->vecFacing);
		g_math.Vec3Scale (&vecInc, fDelta * c->fWalkVelocity);
		g_math.Vec3Sum (&c->vecPosition, &c->vecPosition, &vecInc);
		c->bRebuildMatrix = TRUE;
	}
	if (!(c->dwFlags & (GCHAR_STROLLING | GCHAR_FLOATING)))
		c->fWalkVelocity = 0.0f;

	return ZERO;
}



//********************************************************************************
//Verifica che la posizione corrente del personaggio sia valida. Se non lo �,
//la funzione imposta come posizione del personaggio quella specificata in
//vecPositionOld. Restituisce TRUE se � stata rilevata una collisione,
//altrimenti FALSE.
//********************************************************************************
BOOL CheckCollision (LPGAME_CHARACTER c) {
	if (c == NULL)
		return FALSE;
	BOOL bCollides = FALSE;
	
	//Controllo la collisione con i muri
	for (int nWall = 0; nWall < 4; nWall++) {
		bCollides |= (c->tmMesh.Bounding.IntersectPlane (c->tmMesh.GetCurrentFrame(), g_plnWalls + nWall) == BFINTS_INTERSECTION);
	}
	if (bCollides)
		//Riprendo il vettore posizione vecchio
		MATH_COPY3DVECTOR (c->vecPosition, c->vecPositionOld);

	//Controllo l'intersezione col terreno
	const BFIGURE_INTERSECTION bfiFall =
		c->tmMesh.Bounding.IntersectPlane (c->tmMesh.GetCurrentFrame(), &g_fgGround.plnEquation);
	
	//Se il personaggio tocca il terreno
	if ( (bfiFall == BFINTS_INTERSECTION || bfiFall == BFINTS_NEGATIVESIDE) ) {
		//Se sta cadendo da un salto, bisogna impostare il flag di atterraggio
		if ( (c->dwFlags & GCHAR_FREEFALLING) && !(c->dwFlags & GCHAR_DETACHED) ) {
			//!(c->dwFlags & GCHAR_DETACHED)
			c->dwFlags ^= GCHAR_FREEFALLING;
			c->dwFlags |= GCHAR_LANDINGBEGIN;
		}
		
		//Il personaggio va allineato col terreno
//		if ( !(c->dwFlags & GCHAR_FLOATING) || (c->dwFlags & GCHAR_LANDING) )
		if (!(c->dwFlags & GCHAR_DETACHED))
			AlignToGround (c);
		bCollides = TRUE;
	}
	else if (bfiFall == BFINTS_POSITIVESIDE) {
		if (c->dwFlags & GCHAR_DETACHED)
			c->dwFlags ^= GCHAR_DETACHED;
	}
	
	if (bCollides) {
		if (!(c->dwFlags & GCHAR_TURNING))
			c->bRebuildMatrix = FALSE;
	}
	return bCollides;
}



//********************************************************************************
//Costruisce la matrice per il personaggio specificato, in base ai parametri
//specificati nella struttura stessa.
//********************************************************************************
inline void RebuildCharacterMatrix (LPGAME_CHARACTER c) {
	MATH_MATRIX16 matTranslation;
	
	g_math.Mat16TransformRotateY (&c->matFinal, c->fRotation);
	g_math.Mat16Translation (&matTranslation, c->vecPosition.x, c->vecPosition.y, c->vecPosition.z);
	g_math.Mat16Multiply (&c->matFinal, &c->matTransform, &c->matFinal);
	g_math.Mat16Multiply (&c->matFinal, &c->matFinal, &matTranslation);
	g_math.Mat16Copy (&c->tmMesh.Bounding.matTransform, &c->matFinal);
	return;
}



//********************************************************************************
//Allinea il personaggio specificato con il livello del terreno.
//********************************************************************************
inline void AlignToGround (LPGAME_CHARACTER c) {
	MATH_3DVECTOR vecCentre;
	float fHeight;
	
	c->tmMesh.Bounding.GetMeasures (c->tmMesh.GetCurrentFrame(), NULL, &fHeight, NULL);
	c->tmMesh.Bounding.GetCentre (c->tmMesh.GetCurrentFrame(), &vecCentre);
	
	vecCentre.y = -vecCentre.y;
	
	c->vecPosition.y =
		g_math.Vec3Transform(&vecCentre, &c->matTransform)->y +
		g_fgGround.fGroundLevel +
		fHeight * c->vecScale.y / 2.0f;
	
	return;
}



//********************************************************************************
//Ottiene il contenuto del frontbuffer e lo salva su un file predefinito.
//Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int SaveScreenshot() {
	LPDIRECT3DSURFACE9 surfTemp = NULL;
	LPDIRECT3DSURFACE9 surfBackBuff = NULL;
	D3DSURFACE_DESC desc;
	FILE* f = NULL;
	int nRet = NONZERO;
	
	if (FAILED(g_dev->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &surfBackBuff)))
		goto Error;
	surfBackBuff->GetDesc(&desc);
	SAFE_RELEASE(surfBackBuff);
	
	//The buffer pointed to by pDestSurface will be filled with a
	//representation of the front buffer, converted to the standard
	//32 bits per pixel format D3DFMT_A8R8G8B8.
	if (FAILED(g_dev->CreateOffscreenPlainSurface(desc.Width, desc.Height, D3DFMT_A8R8G8B8, D3DPOOL_SYSTEMMEM, &surfTemp, NULL)))
		goto Error;
	
	if (FAILED(g_dev->GetFrontBufferData(0, surfTemp)))
		goto Error;
	
	BITMAPFILEHEADER bH;
	BITMAPINFOHEADER iH;
	f = fopen("screen.bmp", "wb");
	D3DLOCKED_RECT LockedRect;
	
	if (f == NULL)
		goto Error;
	
	bH.bfSize = sizeof(BITMAPFILEHEADER);
	bH.bfReserved1 = bH.bfReserved2 = 0;
	bH.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
	bH.bfType = 'MB';
	iH.biBitCount = 32;
	iH.biClrImportant = iH.biClrUsed = iH.biCompression = 0;
	iH.biHeight = desc.Height;
	iH.biPlanes = 1;
	iH.biSize = sizeof(BITMAPINFOHEADER);
	iH.biSizeImage = iH.biXPelsPerMeter = iH.biYPelsPerMeter = 0;
	iH.biWidth = desc.Width;
	
	ZeroMemory(&LockedRect, sizeof(D3DLOCKED_RECT));
	if (FAILED(surfTemp->LockRect(&LockedRect, NULL, 0)))
		goto Error;
	
	fwrite(&bH, sizeof(BITMAPFILEHEADER), 1, f);
	fwrite(&iH, sizeof(BITMAPINFOHEADER), 1, f);
	
	for (UINT z = desc.Height; z > 0; z--)
		fwrite((BYTE*)LockedRect.pBits + (z - 1) * LockedRect.Pitch, 1, LockedRect.Pitch, f);
	
	surfTemp->UnlockRect();
	
	nRet = ZERO;
Error:
	if (f != NULL) {
		fclose(f);
		f = NULL;
	}
	SAFE_RELEASE(surfTemp);
	return nRet;
}