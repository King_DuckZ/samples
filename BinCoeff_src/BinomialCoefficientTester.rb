#!/usr/bin/env ruby

def Factorial(k)
	ret = 1
	1.upto(k) do |a|
		ret *= a
	end
	ret
end

def BinomialCoefficient(n,k)
	if (n == k || k == 0) then
		return 1
	elsif (k > n) then
		return 0
	else
		return Factorial(n) / (Factorial(k) * Factorial(n - k))
	end
end

MAX_UINT = 4294967295 #2 ** 32 - 1
dtStart = Time.now
puts dtStart.to_s + " - Beginning test"
nTestCount = 0
nFailures = 0

500.times do |n|
	0.upto(n) do |k|
		nRet = `BinomialCoefficient #{n} #{k}`.to_i
		nExpected = BinomialCoefficient(n, k)
		
		if nExpected > MAX_UINT && nRet != 0 then
			puts "#{n}/#{k} should overflow (#{nExpected}), but I received a #{nRet}"
			nFailures += 1
		elsif nExpected.between?(0, MAX_UINT) && nRet != nExpected then
			puts "#{n}/#{k} yields #{nExpected}, but I received #{nRet}"
			nFailures += 1
		end
		
		nTestCount += 1
	end
	
	puts "Executing tests with n = #{n + 1}..." if (n + 1) % 10 == 0
end

dtEnd = Time.now
puts dtEnd.to_s + " - Test completed in #{((dtEnd - dtStart) / 60.0).to_i} minutes."
puts "Unexpected values received #{nFailures} times on #{nTestCount} total calls."
