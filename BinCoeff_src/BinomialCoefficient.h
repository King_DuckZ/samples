//BinomialCoefficient.h

#ifndef MY_INCL_BINOMIALCOEFFICIENT_H
#define MY_INCL_BINOMIALCOEFFICIENT_H

namespace ducklib {
	unsigned int dlGetBinomialCoefficient ( unsigned int uChoices, unsigned int uSlots );
	unsigned int dlGetGCD ( unsigned int a, unsigned int b );
}

#endif // MY_INCL_BINOMIALCOEFFICIENT_H
