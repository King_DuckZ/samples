//BinomialCoefficient.cpp


#include "BinomialCoefficient.h"
#include <cassert>


//********************************************************************************
//********************************************************************************
unsigned int ducklib::dlGetBinomialCoefficient (unsigned int uChoices, unsigned int uSlots) {
	if (uSlots == 0 || uChoices == uSlots)
		return 1;
	else if (uSlots > uChoices)
		return 0;

	//Smaller values for uSlots should yield better chances to not overflow
	if (uChoices - uSlots < uSlots) //prop: (n|k) = (n|n-k)
		uSlots = uChoices - uSlots;

	//See if a simpler formula can be used
	if (uSlots == uChoices - 1) //uSlots == 1
		return uChoices;
	else if (uSlots == uChoices - 2) {//uSlots == 2
		if (~0U / uChoices < uChoices - 1)
			return 0; //Overflow :/
		else
			return ((uChoices * (uChoices - 1)) >> 1);
	}

	//No luck, uh? Do it the boring way :/
	const unsigned int& n = uChoices;
	const unsigned int& k = uSlots;
	unsigned int uRet = 1;
	unsigned int uFactorial = 1;
	unsigned int uFact = k;

	for (unsigned int i = 0; i < k; ++i) {
		//We need an overflow check inside the loop, too
		if ((~0U / uRet) < (n - i)) { // translates to (n - i) * uRet > MAX_UINT
			//Likely, the result can't be stored into an UINT, but let's try harder
			const unsigned int uGCD = dlGetGCD(n - i, uFactorial);
			const unsigned int uFactorSimpl = (n - i) / uGCD;
			if ((~0U / uRet) < uFactorSimpl) {
				return 0; //Too bad, we can't store next product into an UINT ;(
			}
			uFactorial /= uGCD;
			uFactorial *= uFact;
			uRet *= (n - i) / uGCD;
		}
		else {
			uRet *= n - i;
			uFactorial *= uFact;

			const unsigned int uGCD = dlGetGCD(uFactorial, uRet);
			uRet /= uGCD;
			uFactorial /= uGCD;
		}

		--uFact;
	}

//	for (unsigned int z = uFact; z < k; ++z)
//		uFactorial *= z;

	return uRet / uFactorial;
}



//********************************************************************************
//********************************************************************************
unsigned int ducklib::dlGetGCD (unsigned int a, unsigned int b) {
	if ((static_cast<bool>(a) && static_cast<bool>(b)) == false) {
		assert(true);
		return 0;
	}

	unsigned int r;
	unsigned int q;
	unsigned int uRet;

	do {
		uRet = b;

		// a = b * q + r
		q = a / b;
		r = a - q * b;

		a = b;
		b = r;
	} while (r);
	return uRet;
}
