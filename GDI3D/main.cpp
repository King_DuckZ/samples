//main.cpp


#define WIN32_LEAN_AND_MEAN
#define DEF_WIN_TITLE "GDI 3D"
#define DEF_CLASS_NAME "CGDI3D_WIN"
#define DEF_STR_LEN 1024
#define AREA_WIDTH 640
#define AREA_HEIGHT 480

#include <windows.h>
#include <mmsystem.h>
#include "CMathEngine.h"
#include "resource.h"
#include "CVertexMem.h"


LRESULT CALLBACK MsgProc ( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam );


//********************************************************************************
//WinMain
//********************************************************************************
int __stdcall WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
#pragma unused (hPrevInstance)
#pragma unused (lpCmdLine)
	WNDCLASSEX wnd;
	HWND hWnd;
	RECT rClient;
	char* strMess;
	char* strTitle;
	MSG msg;
	float t1, t2;
	bool bShowErr = false;
	
	strMess = new char[DEF_STR_LEN];
	strTitle = new char[32];
	if ( (strMess == NULL ) || (strTitle == NULL) )
		return 0;
	LoadString (hInstance, IDS_ERR_TITLE, strTitle, 32); 
	
	wnd.cbClsExtra = 0;
	wnd.cbSize = sizeof(WNDCLASSEX);
	wnd.cbWndExtra = 0;
	wnd.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wnd.hCursor = LoadCursor (NULL, IDC_ARROW);
	wnd.hIcon = LoadIcon (hInstance, MAKEINTRESOURCE(IDI_MAIN_ICON));
	wnd.hIconSm = LoadIcon (hInstance, MAKEINTRESOURCE(IDI_MAIN_ICON));
	wnd.hInstance = hInstance;
	wnd.lpfnWndProc = MsgProc;
	wnd.lpszClassName = DEF_CLASS_NAME;
	wnd.lpszMenuName = NULL;
	wnd.style = CS_VREDRAW | CS_HREDRAW | CS_DBLCLKS;
	
	RegisterClassEx (&wnd);
	hWnd = CreateWindowEx (0, DEF_CLASS_NAME, DEF_WIN_TITLE,
		WS_OVERLAPPEDWINDOW, 0, 0, 640, 480, NULL, NULL, wnd.hInstance, NULL);
	
	if (hWnd == NULL) {
		LoadString (hInstance, IDS_ERR_HWND, strMess, DEF_STR_LEN-1);
		MessageBox (NULL, strMess, strTitle, MB_OK | MB_ICONSTOP);
	}
	else {
		GetClientRect (hWnd, &rClient);
		const int nWidth = AREA_WIDTH + 640 - (rClient.right - rClient.left);
		const int nHeight = AREA_HEIGHT + 480 - (rClient.bottom - rClient.top);
		
		SetWindowPos (hWnd, NULL, 0, 0, nWidth, nHeight, SWP_NOMOVE | SWP_NOACTIVATE);
		ShowWindow (hWnd, nCmdShow);
		UpdateWindow (hWnd);
		
		t1 = 0.0f;
		t2 = timeGetTime() / 1000.0f;
		ZeroMemory (&msg, sizeof(MSG));
		while ( msg.message != WM_QUIT ) {
			if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE)) {
				if (msg.message == WM_QUIT)
					break;
				TranslateMessage (&msg);
				DispatchMessage (&msg);
			}
			else {
				t1 = t2;
				t2 = (float)timeGetTime () / 1000.0f;
			}//EndIf
		}//Wend
	}
	
	//Pulisco e ciao
	if (strMess != NULL) {
		delete[] strMess;
		strMess = NULL;
	}
	if (strTitle != NULL) {
		delete[] strTitle;
		strTitle = NULL;
	}
	UnregisterClass (DEF_CLASS_NAME, wnd.hInstance);
	return (int)msg.wParam;
}



//********************************************************************************
//Event handler
//********************************************************************************
LRESULT CALLBACK MsgProc (HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	switch (uMsg) {
	case WM_DESTROY:
		PostQuitMessage(0);
		return (long)wParam;
	
#ifndef _DEBUG
	case WM_SETCURSOR:
		SetCursor (NULL);
		return TRUE;
#endif
	}
	return DefWindowProc (hwnd, uMsg, wParam, lParam);
}
