//CRasterizer.cpp


#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include "VectorBlaster_comm.h"
#include "CRasterizer.h"



//********************************************************************************
//Costruttore standard
//********************************************************************************
CRasterizer::CRasterizer () {
	m_Reset ();
}



//********************************************************************************
//Distruttore standard
//********************************************************************************
CRasterizer::~CRasterizer () {
	m_Dispose ();
}



//********************************************************************************
//Imposta tutte le proprietÓ sui valori iniziali.
//********************************************************************************
inline void CRasterizer::m_Reset () {
	m_prvVert = NULL;
	m_uVertCount = 0;
}



//********************************************************************************
//Libera la memoria occupata e distrugge gli oggetti creati.
//********************************************************************************
inline void CRasterizer::m_Dispose () {
	VBLAST_SAFEDELETE (m_prvVert);
}