//CVertexMem.h


#ifndef _INCL_CVERTEXMEM_H
#define _INCL_CVERTEXMEM_H

#include "VectorBlaster_comm.h"
#include "CRasterizer.h"



class CVertexMem : public CRasterizer {
public:
	CVertexMem ( void );
	~CVertexMem ( void );
	void Dispose ( void );
	
	VBRET CreateBuffer ( VBUINT uVertexCount );

private:
	inline void m_Reset ( void );
	inline void m_Dispose ( void );

protected:
};

#endif
