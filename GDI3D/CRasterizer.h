//CRasterizer.h


#ifndef _INCL_CRASTERIZER_H
#define _INCL_CRASTERIZER_H

typedef struct STRUCT_RASTERIZER_VERTEX {
	float fx;
	float fy;
	float fz;
	VBDWORD dwColor;
} RASTERIZER_VERTEX, *LPRASTERIZER_VERTEX;


class CRasterizer {
public:
	CRasterizer ( void );
	~CRasterizer ( void );
	
protected:
	inline void m_Reset ( void );
	inline void m_Dispose ( void );
	
	LPRASTERIZER_VERTEX m_prvVert;
	VBUINT m_uVertCount;
};

#endif