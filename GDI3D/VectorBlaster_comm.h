//VectorBlaster_comm.h


#ifndef _INCL_VECTORBLASTER_COMM
#define _INCL_VECTORBLASTER_COMM


enum VBLAST_RETURNCODE {
	VBRC_ALLRIGHT = 0,
	VBRC_NOMEM,
	VBRC_BADPARAM,
	VBRC_NULLPTR,
	VBRC_UNKNOWN,
	
	VBRC_FORCEDWORD = 0x7FFFFFFF
};
typedef VBLAST_RETURNCODE VBRET;
typedef unsigned int VBUINT;
typedef unsigned char VBBYTE;
typedef unsigned short VBWORDt;
typedef unsigned int VBDWORD;


#define VBLAST_SUCC(func) ((func) == VBRC_ALLRIGHT)
#define VBLAST_FAIL(func) ((func) > VBRC_ALLRIGHT)


#if (!defined TRUE) || (!defined TRUE)
#warning "Tipo di dati BOOL non definito. In definizione con costanti di default."
#ifdef TRUE
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif

typedef unsigned int BOOL;
#define TRUE 1
#define FALSE 0
#endif


#define VBLAST_SAFEDELETE(mem) {if (mem) { delete[] (mem); (mem) = NULL; }}
#endif