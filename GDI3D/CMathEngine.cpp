//CMathEngine.cpp


#define TABLE_ENTRIES 361

#include <windows.h>
#include <math.h>
#include "CMathEngine.h"

#ifndef ZERO
#define ZERO 0
#endif
#ifndef NONZERO
#define NONZERO (-1)
#endif

float* CMathEngine::CosTable = NULL;
float* CMathEngine::SinTable = NULL;
unsigned int CMathEngine::nTableEntries = 0;
unsigned int CMathEngine::m_nRefCount = 0;


#ifdef SAFE_DELETE
#undef SAFE_DELETE
#endif
#define SAFE_DELETE(mem) { if ((mem) != NULL) { delete[] (mem); (mem) = NULL; } }


//********************************************************************************
//Costruttore standard
//********************************************************************************
CMathEngine::CMathEngine () {
	m_nRefCount++;
	if (nTableEntries == 0) {
		CosTable = new float[TABLE_ENTRIES];
		SinTable = new float[TABLE_ENTRIES];
		
		if (CosTable == NULL || SinTable == NULL) {
			SAFE_DELETE (CosTable);
			SAFE_DELETE (SinTable);
		}
		else {
			nTableEntries = TABLE_ENTRIES;
			float a = 0.0f;
			for (int z = 0; z < TABLE_ENTRIES; z++, a += 1.0f) {
				CosTable[z] = cosf(MATH_DEGTORAD(a));
				SinTable[z] = sinf(MATH_DEGTORAD(a));
			} //Next z
			
		} //End If
	} //End If
}



//********************************************************************************
//Distruttore standard
//********************************************************************************
CMathEngine::~CMathEngine () {
	m_nRefCount--;
	if (m_nRefCount == 0) {
		SAFE_DELETE (CosTable);
		SAFE_DELETE (SinTable);
		nTableEntries = 0;
	}
}



//********************************************************************************
//Prende in input un angolo in radianti e ne restituisce il seno. Restituisce
//2.0f se l'array precalcolato per il seno � NULL.
//********************************************************************************
float CMathEngine::GetInterpolatedSine (float fRad) {
	if (SinTable == NULL)
		return 2.0f;
	return m_GetInterpolatedFunc (fRad, SinTable);
}



//********************************************************************************
//Prende in input un angolo in radianti e ne restituisce il coseno. Restituisce
//2.0f se l'array precalcolato per il coseno � NULL.
//********************************************************************************
float CMathEngine::GetInterpolatedCosine (float fRad) {
	if (CosTable == NULL)
		return 2.0f;
	return m_GetInterpolatedFunc(fRad, CosTable);
}



//********************************************************************************
//Converte le coordinate sferiche specificate in s, in coordinate
//cartesiane, restituendole in c. Restituisce ZERO se va tutto bene, altrimenti
//NONZERO.
//********************************************************************************
int CMathEngine::SphericalToCartesian (const LPMATH_SPHERICAL s, LPMATH_3DVECTOR c) {
	if ( (s && c && CosTable && SinTable) == false )
		return NONZERO;
	
	const float fSinPhi = m_GetInterpolatedFunc (s->phi, SinTable);
	const float fCosPhi = m_GetInterpolatedFunc (s->phi, CosTable);
	const float fSinTheta = m_GetInterpolatedFunc (s->theta, SinTable);
	const float fCosTheta = m_GetInterpolatedFunc (s->theta, CosTable);
	
//	c->x = s->rho * fSinPhi * fCosTheta;
//	c->y = s->rho * fSinPhi * fSinTheta;
//	c->z = s->rho * fCosPhi;
	c->x = s->rho * fCosTheta * fSinPhi;
	c->y = s->rho * fSinTheta;
	c->z = s->rho * fCosTheta * fCosPhi;
	return ZERO;
}



//********************************************************************************
//La funzione prende in input un angolo in radianti e una tabella di valori
//precalcolati da usare per il calcolo del seno o coseno. Restituisce il
//valore calcolato. Non effettua controlli sui puntatori.
//********************************************************************************
inline float CMathEngine::m_GetInterpolatedFunc (float fRad, const float* fTable) {
/*
	float fDeg = MATH_RADTODEG(fRad);
	fDeg = fmodf(fDeg, 360.0f);
	if (fDeg < 0.0f)
		fDeg += 360.0f;
	
	const int nDeg = (int)fDeg;
	const float fFrac = fDeg - (float)nDeg;
	
	return (fTable[nDeg] + fFrac * (fTable[nDeg+1] - fTable[nDeg]));
*/
	static const float f360 = 360.0f, f180 = 180.0f;
	float fDeg, fFrac;
	int nDeg;
	asm {
		FINIT
		//Conversione radianti -> gradi
		FLD f180
		FLDPI
		FDIV
		
		FLD fRad
		FMUL ST(1)
		
		//fmodf() e aggiunta di 360 se < 0
		FLD f360
		FXCH
		FPREM
		FTST
		FSTSW AX
		SAHF
		JAE Next
		FLD f360
		FADDP ST(1), ST
		
		Next:
		//Assegnazione alle variabili
		FST fDeg
		FRNDINT
		FIST nDeg
		
		//Calcolo della differenza (mantissa)
		FLD fDeg
		FSUB ST, ST(1)
		FSTP fFrac
	}
	
	return (fTable[nDeg] + fFrac * (fTable[nDeg+1] - fTable[nDeg]));
}



//********************************************************************************
//Imposta m in modo che sia la projection matrix per un sistema cartesiano
//sinistrirso. Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CMathEngine::Mat16ProjectionLH (LPMATH_MATRIX16 m, float width, float height, float NearPlane, float FarPlane) {
	if (m == NULL || width < 1.0f || height < 1.0f)
		return NONZERO;
	if (NearPlane > FarPlane)
		MATH_SWITCH (NearPlane, FarPlane, float);
	
	MATH_INITMATRIX16 (*m);
	
	m->_00 = 2 * NearPlane / width;
	m->_11 = 2 * NearPlane / height;
	m->_22 = FarPlane / (FarPlane - NearPlane);
	m->_23 = 1.0f;
	m->_32 = m->_22 * -NearPlane;
	
	return ZERO;
}



//********************************************************************************
//Calcola l'intersezione fra la retta specificata da v1 e v2 e il piano
//specificato da plane. Restituisce in *pt il punto di intersezione e
//l'indirizzo di pt in caso di successo, altrimenti NULL (*pt resta invariato).
//********************************************************************************
LPMATH_3DVECTOR CMathEngine::PlaneIntersectLine (LPMATH_3DVECTOR pt, const LPMATH_PLANE plane, const LPMATH_3DVECTOR v1, const LPMATH_3DVECTOR v2) {
	if ( (pt && plane && v1 && v2) == FALSE )
		return NULL;
	
	//Per prima cosa, bisogna trovare il versore v
	//Questo � un vettore unitario in direzione v1 --> v2
	MATH_3DVECTOR v;
	Vec3Subtract (&v, v1, v2);
	Vec3Normalize (&v);
	
	//Ora si tratta di risolvere il sistema
	// { x = (-bY -cZ -d) / a
	// { (Y - Uy) / Vy = (X - Ux) / Vx = (Z - Uz) / Vz
	//Dove U e V sono i vettori v1 e v (il punto iniziale e la direzione)
	//a, b, c, d sono i coefficienti e il termine noto dell'equazione del piano
	// e X, Y, Z sono le incognite.
	
/*	pt->z = (v.z * (-plane->c * v.z - plane->d - plane->a * v1->x - plane->b * v1->y)) / (plane->a + v.y * plane->b) -
		(v.z * v1->y * plane->a) / (v.y * (plane->a + v.y * plane->b)) +
		v.y * v1->z;
	pt->y = (v.y * (-plane->c * pt->z - plane->d - plane->a * v1->x)) / (v.y * plane->b + plane->a);
	
	pt->x = -( (v.x * (-plane->c * pt->z - plane->d - plane->b * v1->y) + v.y * plane->b * v1->x) / (-v.x * plane->a - v.y * plane->b));
*/
	
	//Il codice sopra per qualche motivo non funziona bene.
	//Alla fine ho trovato questa formuletta su internet, che pare restituisca
	//il punto di intersezione sulla retta come valore 0..1.
	const float fDenominatore = 
		plane->a * (v1->x - v2->x) + plane->b * (v1->y - v2->y) + plane->c * (v1->z - v2->z);
	//Nessuna soluzione o soluzioni infinite
	if (fDenominatore == 0.0f)
		return NULL;
	const float u = (plane->a * v1->x + plane->b * v1->y + plane->c * v1->z + plane->d) / fDenominatore;
	
	MATH_3DVECTOR vDiff;
	const float fLen = MATH_LENGHT3DVECTOR (*Vec3Subtract (&vDiff, v2, v1));
	
	Vec3Scale (&v, fLen * u);
	Vec3Subtract (pt, v1, &v);
	
	return pt;
}



//********************************************************************************
//Trasforma il quaternione specificato moltiplicandolo per la matrice
//specificata. Restituisce in *dst il quaternione trasformato e l'indirizzo
//dst in caso di successo, altrimenti NULL (*dst resta invariato).
//********************************************************************************
LPMATH_QUATERNION CMathEngine::QuatTransform (LPMATH_QUATERNION dst, const LPMATH_MATRIX16 matTransf) {
	if (dst == NULL || matTransf == NULL)
		return NULL;
	
	MATH_QUATERNION qTmp;
	qTmp.f[0] = matTransf->M[0][0] * dst->f[0] +
				matTransf->M[1][0] * dst->f[1] +
				matTransf->M[2][0] * dst->f[2] +
				matTransf->M[3][0] * dst->f[3];
	qTmp.f[1] = matTransf->M[0][1] * dst->f[0] +
				matTransf->M[1][1] * dst->f[1] +
				matTransf->M[2][1] * dst->f[2] +
				matTransf->M[3][1] * dst->f[3];
	qTmp.f[2] = matTransf->M[0][2] * dst->f[0] +
				matTransf->M[1][2] * dst->f[1] +
				matTransf->M[2][2] * dst->f[2] +
				matTransf->M[3][2] * dst->f[3];
	qTmp.f[3] = matTransf->M[0][3] * dst->f[0] +
				matTransf->M[1][3] * dst->f[1] +
				matTransf->M[2][3] * dst->f[2] +
				matTransf->M[3][3] * dst->f[3];
	
	MATH_COPYQUATERNION (*dst, qTmp);
	return dst;
}



//********************************************************************************
//Calcola il prodotto mat1 x mat2 e restituisce in matDst il risultato.
//Restituisce l'indirizzo di matDst in caso di successo, altrimenti NULL
//(*matDst resta invariato).
//********************************************************************************
LPMATH_MATRIX16 CMathEngine::Mat16Multiply (LPMATH_MATRIX16 matDst, const LPMATH_MATRIX16 mat1, const LPMATH_MATRIX16 mat2) {
	if ( (matDst && mat2 && mat1) == FALSE )
		return NULL;

	MATH_MATRIX16 matStore;
	float fSum = 0.0f;
	int i, j;
	
	for (int nRow = 0; nRow < 4; nRow++) {
		j = 0;
		for (int nCol = 0; nCol < 4; nCol++) {
			for (i = 0; i < 4; i++) {
				fSum += mat1->M[nRow][i] * mat2->M[i][j];
			}
			matStore.M[nRow][nCol] = fSum;
			fSum = 0.0f;
			j++;
		}
	}
	
	//*matDst = matStore;
	this->Mat16Copy (matDst, &matStore);
	return matDst;
}



//********************************************************************************
//Moltiplica il vettore ricevuto per la matrice *mat, supponendo che il
//quarto valore del vettore sia 1. Restituisce vecDst se va tutto bene,
//altrimenti NULL.
//********************************************************************************
LPMATH_3DVECTOR CMathEngine::Vec3Transform (LPMATH_3DVECTOR vecDst, const LPMATH_MATRIX16 mat) {
	if (vecDst == NULL || mat == NULL)
		return NULL;
	
	MATH_3DVECTOR vecTmp;
	vecTmp.f[0] = mat->M[0][0] * vecDst->f[0] +
				  mat->M[1][0] * vecDst->f[1] +
				  mat->M[2][0] * vecDst->f[2] +
				  mat->M[3][0];
	vecTmp.f[1] = mat->M[0][1] * vecDst->f[0] +
				  mat->M[1][1] * vecDst->f[1] +
				  mat->M[2][1] * vecDst->f[2] +
				  mat->M[3][1];
	vecTmp.f[2] = mat->M[0][2] * vecDst->f[0] +
				  mat->M[1][2] * vecDst->f[1] +
				  mat->M[2][2] * vecDst->f[2] +
				  mat->M[3][2];
	
	MATH_COPY3DVECTOR (*vecDst, vecTmp);
	return vecDst;
}