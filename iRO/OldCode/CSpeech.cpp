//CSpeech.cpp


#include "Constants.h"
#include <windows.h>
#include <d3d9.h>
#include <stdio.h>
#include "CSpeech.h"


#define MIN_WIDTH 50
#define MIN_HEIGHT 25

//********************************************************************************
//Costruttore standard
//********************************************************************************
CSpeech::CSpeech () {
	ZeroMemory (&m_rDst, sizeof(RECT));
	ZeroMemory (&m_rVisible, sizeof(RECT));
	ZeroMemory (&m_chInfo, sizeof(CHARSETHEADER));
	m_strCharset = NULL;
	m_CharsetSurf = NULL;
	m_Surf = NULL;
	m_dwTops = NULL;
	m_rList = NULL;
	m_nWidth = 0;
	m_nHeight = 0;
	m_ColorKey = D3DCOLOR_XRGB (0, 0, 0);
	m_nCharsetWidth = 0;
	m_nCharsetHeight = 0;
	
	m_rScreen.left = 0;
	m_rScreen.top = 0;
	m_rScreen.right = AREA_WIDTH;
	m_rScreen.bottom = AREA_HEIGHT;
}



//********************************************************************************
//Distruttore standard
//********************************************************************************
CSpeech::~CSpeech () {
	m_Dispose ();
}



//********************************************************************************
//Chiama m_Dispose()
//********************************************************************************
void CSpeech::Dispose () {
	m_Dispose ();
}



//********************************************************************************
//Libera la memoria occupata e distrugge gli oggetti creati.
//********************************************************************************
inline void CSpeech::m_Dispose () {
	SAFE_RELEASE (m_CharsetSurf);
	SAFE_RELEASE (m_Surf);
	SAFE_DELETE (m_strCharset);
	SAFE_DELETE (m_dwTops);
	SAFE_DELETE (m_rList);
}



//********************************************************************************
//Crea in memoria una surface delle dimensioni specificate, che verr� poi
//disegnata nell'area identificata dal RECT. Tale surface verr� usata per
//la rappresentazione delle parole. La dimensione della surface avr�
//effetto anche sulla formattazione del testo. Se un'altra surface era
//gi� stata creata, ma con dimensioni differenti, questa viene cancellata
//e ne viene creata una nuova. Se Viene richiesta un'altra surface con le stesse
//dimensioni della precedente, la surface verr� semplicemente pulita.
//Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CSpeech::BuildSurface (LPDIRECT3DDEVICE9 dev,  const LPRECT r) {
	if (dev == NULL || r == NULL)
		return NONZERO;
	
	D3DDISPLAYMODE mode;
	ZeroMemory (&mode, sizeof(D3DDISPLAYMODE));
	if (FAILED(dev->GetDisplayMode (0, &mode)))
		return NONZERO;

	//Controllo se c'� un'altra surface
	if (m_Surf) {
		if (m_nWidth == r->right - r->left && m_nHeight == r->bottom - r->top) {
			return EraseSurf ();
		}
		else {
			SAFE_RELEASE (m_Surf);
		}
	}
	
	//Creo la surface
	m_nWidth = r->right - r->left;
	m_nHeight = r->bottom - r->top;
	
	if (m_nWidth < MIN_WIDTH || m_nHeight < MIN_HEIGHT)
		return NONZERO;
	
	if (FAILED(dev->CreateOffscreenPlainSurface (m_nWidth, m_nHeight, mode.Format, D3DPOOL_SYSTEMMEM, &m_Surf, NULL)))
		return NONZERO;
	
	return EraseSurf(dev);
}



//********************************************************************************
//********************************************************************************
int CSpeech::LoadCharset (LPDIRECT3DDEVICE9 dev, const char* strCharset, const char* strSurface) {
	if (dev == NULL || strCharset == NULL || strSurface == NULL)
		return NONZERO;
	
	if (m_LoadPicture (dev, strSurface))
		return NONZERO;
	
	FILE* f;
	
	if ( (f=fopen(strCharset, "rb")) == NULL )
		return NONZERO;
	
	fseek (f, 0, SEEK_END);
	const int nFileSize = ftell(f);
	fseek (f, 0, SEEK_SET);
	
	if (nFileSize < sizeof(CHARSETHEADER)) {
		fclose (f);
		return NONZERO;
	}
	
	//Controlli assortiti
	fread (&m_chInfo, sizeof(CHARSETHEADER), 1, f);
	if (nFileSize != m_chInfo.chFileSize) {
		fclose (f);
		return NONZERO;
	}
	if (m_chInfo.chSignature != 'GD') {
		fclose (f);
		return NONZERO;
	}
	if (m_chInfo.chRectCount <= 0) {
		fclose (f);
		return NONZERO;
	}
	if (m_chInfo.chSize != sizeof(CHARSETHEADER)) {
		fclose (f);
		return NONZERO;
	}
	if (!m_chInfo.chDefinesCharset || !m_chInfo.chDefinesTop) {
		fclose (f);
		return NONZERO;
	}
	
	SAFE_DELETE (m_strCharset);
	SAFE_DELETE (m_dwTops);
	SAFE_DELETE (m_rList);
	
	m_strCharset = new unsigned char[m_chInfo.chRectCount];
	m_dwTops = new DWORD[m_chInfo.chRectCount];
	m_rList = new RECT[m_chInfo.chRectCount];
	
	fread (m_strCharset, 1, m_chInfo.chRectCount, f);
	fseek (f, m_chInfo.chRectDataStart, SEEK_SET);
	fread (m_rList, sizeof(RECT), m_chInfo.chRectCount, f);
	fread (m_dwTops, sizeof(DWORD), m_chInfo.chRectCount, f);

	fclose (f);
	return ZERO;
}



//********************************************************************************
//********************************************************************************
int CSpeech::Render (LPDIRECT3DDEVICE9 dev) {
	if (dev == NULL || m_CharsetSurf == NULL)
		return NONZERO;
	
	const int nBuffSurf = 0;
	const int nColorSurf = 1;
	LPDIRECT3DSURFACE9 BuffSurf = NULL;
	int nRet = ZERO;
	D3DLOCKED_RECT lr[2];
	LPDWORD BuffPtr;
	LPDWORD SurfPtr;
	
	ZeroMemory (lr, sizeof(D3DLOCKED_RECT) << 1);
	
	if (FAILED(dev->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &BuffSurf)))
		return NONZERO;
	
//	if (FAILED(dev->UpdateSurface (m_CharsetSurf, NULL, BuffSurf, NULL)))
//		nRet = NONZERO;

	if (SUCCEEDED(BuffSurf->LockRect(lr+nBuffSurf, NULL, 0))) {
		if (SUCCEEDED(m_CharsetSurf->LockRect(lr+nColorSurf, NULL, 0))) {
			
			BuffPtr = (LPDWORD)
			for (int y = 0; y < m_nCharsetHeight; y++) {
				for (int x = 0; x < m_nCharsetWidth; x++) {
					
				}
			}
			
			m_CharsetSurf->UnlockRect ();
		}
		BuffSurf->UnlockRect ();
	}
	
	SAFE_RELEASE (BuffSurf);
	return nRet;
}



//********************************************************************************
//Imposta la dimensione dello schermo su una dimensione diversa da quella di
//default. Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CSpeech::UpdateScreenSize (int cx, int cy) {
	if (cx > 0 && cy > 0) {
		m_rScreen.left = 0;
		m_rScreen.top = 0;
		m_rScreen.right = cx;
		m_rScreen.bottom = cy;
		
		if (m_Surf)
			IntersectRect (&m_rVisible, &m_rScreen, &m_rDst);
		
		return ZERO;
	}
	else
		return NONZERO;
}



//********************************************************************************
//Se � presente una surface, il contenuto di questa viene cancellato.
//Restituisce ZERO se la cancellazione avviene con successo, NONZERO se si
//verificano errori o se la surface non esiste.
//********************************************************************************
int CSpeech::EraseSurf () {
	if (m_Surf == NULL)
		return NONZERO;
	
	D3DLOCKED_RECT lr;
	ZeroMemory (&lr, sizeof(D3DLOCKED_RECT));
	
	if (FAILED(m_Surf->LockRect (&lr, NULL, 0)))
		return NONZERO;
	
	ZeroMemory (lr.pBits, lr.Pitch * m_nHeight);
	
	m_Surf->UnlockRect ();
	return ZERO;
}



//********************************************************************************
//********************************************************************************
int CSpeech::EraseSurf (LPDIRECT3DDEVICE9 dev) {
	if (m_Surf == NULL || dev == NULL)
		return NONZERO;
	
	if (SUCCEEDED(dev->ColorFill (m_Surf, NULL, D3DCOLOR_XRGB(0,0,0))))
		return ZERO;
	else
		return NONZERO;
}



//********************************************************************************
//La funzione carica nella surface m_CharsetSurf la bitmap specificata. Se
//� gi� caricata una bitmap, questa viene cancellata e la nuova viene
//caricata al posto della vecchia. La funzione non effettua alcun controllo
//sui puntatori ricevuti, � pertanto necessario assicurarsi di non passare
//valori NULL alla funzione. Restituisce ZERO se va tutto bene, altrimenti
//NONZERO.
//********************************************************************************
int CSpeech::m_LoadPicture (LPDIRECT3DDEVICE9 dev, const char* strFileName) {
	HDC MemDC = NULL;
	HBITMAP hSurf, hSurfOld;
	HDC SurfDC = NULL;
	int nRet = NONZERO;
	BITMAP bmp;
	D3DDISPLAYMODE mode;
	
	SAFE_RELEASE (m_CharsetSurf);
	ZeroMemory (&mode, sizeof(D3DDISPLAYMODE));
	if (FAILED(dev->GetDisplayMode (0, &mode)))
		return NONZERO;
	
	//Caricamento bitmap
	hSurf = (HBITMAP)LoadImage (NULL, strFileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	if (hSurf == NULL)
		goto Farewell;
	
	ZeroMemory (&bmp, sizeof(BITMAP));
	GetObject (hSurf, sizeof(BITMAP), &bmp);
	m_nCharsetWidth = bmp.bmWidth;
	m_nCharsetHeight = bmp.bmHeight;

	//Creazione surface
	if (FAILED(dev->CreateOffscreenPlainSurface (bmp.bmWidth, bmp.bmHeight, mode.Format, D3DPOOL_SYSTEMMEM, &m_CharsetSurf, NULL)))
		goto Farewell;
	
	//Creazione DC
	MemDC = CreateCompatibleDC (NULL);
	if (MemDC == NULL)
		goto Farewell;
	
	//Creazione DC D3D
	if (FAILED(m_CharsetSurf->GetDC (&SurfDC)))
		goto Farewell;
	
	//Copia della bitmap
	hSurfOld = (HBITMAP)SelectObject (MemDC, hSurf);
	BitBlt (SurfDC, 0, 0, bmp.bmWidth, bmp.bmHeight, MemDC, 0, 0, SRCCOPY);
	SelectObject (MemDC, hSurfOld);
	
	nRet = ZERO;
Farewell:
	
	if (MemDC)
		DeleteDC (MemDC);
	if (hSurf)
		DeleteObject (hSurf);
	if (SurfDC)
		m_CharsetSurf->ReleaseDC (SurfDC);
	
	return nRet;
}



//********************************************************************************
//********************************************************************************
int CSpeech::UpdateStencil (LPDIRECT3DDEVICE9 dev) {
	if (dev == NULL || m_Surf == NULL)
		return NONZERO;
	
	LPDIRECT3DSURFACE9 Stencil = NULL;
	
	if (FAILED(dev->GetDepthStencilSurface (&Stencil)))
		return NONZERO;
	if (SUCCEEDED(Ste
	
	SAFE_RELEASE (Stencil);	
	return ZERO;
}