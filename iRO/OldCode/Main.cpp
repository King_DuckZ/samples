//Main.cpp


//Costanti
#define INITGUID
#define DEF_CLASS_NAME "RO_WIN"
#define DEF_WIN_TITLE "iRO sim"
#define DIRECTINPUT_VERSION 0x0800


//Headers
#include "constants.h"
#include <windows.h>
#include <d3d9.h>
#include <dinput.h>
#include <d3dx9.h>
#include "CCamera.h"
#include "resource.h"
#include "CBitmapSarge.h"
#include "Cust_Input.h"
#include "CFlatSprite.h"

#ifdef WIN32_LEAN_AND_MEAN
#include <mmsystem.h>
#endif


//Strutture
typedef struct STRUCT_CUSTOMVERTEX {
	float x, y, z;
	D3DCOLOR color;
	float u, v;
} CUSTOMVERTEX, *LPCUSTOMVERTEX;


//Prototipi
int WINAPI WinMain ( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow );
LRESULT CALLBACK MsgProc ( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam );
HRESULT InitD3D ( HWND hWnd );
void EndIt ( void );
HRESULT InitGeometry ( void );
int Render ( void );
int MakeQuad ( float fWidth, float fHeight, float fDepth, float fTex, LPCUSTOMVERTEX* vert, LPWORD* ind );
HRESULT InitTextures ( void );
int LoadSprite ( const char* strFileName, int nHrzCount, int nVrtCount, LPDIRECT3DTEXTURE9* tex );


//Variabili globali
LPDIRECT3D9 g_d3d = NULL;
LPDIRECT3DDEVICE9 g_dev = NULL;
LPDIRECT3DVERTEXBUFFER9 g_vbTerrain = NULL;
LPDIRECT3DINDEXBUFFER9 g_ibTerrain = NULL;
LPDIRECT3DVERTEXBUFFER9 g_vbPuppet = NULL;
LPDIRECT3DINDEXBUFFER9 g_ibPuppet = NULL;
D3DCAPS9 g_caps;
CCamera g_cam;
LPDIRECT3DTEXTURE9 g_texTerrain = NULL;
LPDIRECT3DTEXTURE9* g_texPuppet = NULL;
DWORD g_dwAnisotropy;
DWORD g_dwMagFilter;
DWORD g_dwMinFilter;
const int g_nSpriteCount = 9 * 5 + 3 * 9;
float g_fSpriteIndex = 0.0f;
MERGED_INPUT g_miInput;


//Altre costanti
#define D3DFVF_CUSTOM (D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1)
#ifndef WM_MOUSEWHEEL
#define WM_MOUSEWHEEL                   0x020A
#endif
#ifndef WHEEL_DELTA
#define WHEEL_DELTA 120
#endif
#ifndef GET_WHEEL_DELTA_WPARAM
#define GET_WHEEL_DELTA_WPARAM(wParam)  ((short)HIWORD(wParam))
#endif

//********************************************************************************
//WinMain
//********************************************************************************
int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
#pragma unused (hPrevInstance)
#pragma unused (lpCmdLine)
	WNDCLASSEX wnd;
	HWND hWnd;
	RECT rClient;
	char* strMess;
	char* strTitle;
	MSG msg;
	float t1, t2;
	bool bShowErr = false;
	BOOL bSuccess;
	HRESULT hrD3D = S_OK, hrGeometry = S_OK, hrDInput = S_OK, hrTexture = S_OK;
	
	strMess = new char[DEF_STR_LEN];
	strTitle = new char[32];
	if ( (strMess == NULL ) || (strTitle == NULL) )
		return 0;
	LoadString (hInstance, IDS_ERR_TITLE, strTitle, 32); 
	
	wnd.cbClsExtra = 0;
	wnd.cbSize = sizeof(WNDCLASSEX);
	wnd.cbWndExtra = 0;
	wnd.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wnd.hCursor = LoadCursor (NULL, IDC_ARROW);
	wnd.hIcon = LoadIcon (hInstance, MAKEINTRESOURCE(IDI_MAIN_ICON));
	wnd.hIconSm = LoadIcon (hInstance, MAKEINTRESOURCE(IDI_MAIN_ICON));
	wnd.hInstance = hInstance;
	wnd.lpfnWndProc = MsgProc;
	wnd.lpszClassName = DEF_CLASS_NAME;
	wnd.lpszMenuName = NULL;
	wnd.style = CS_VREDRAW | CS_HREDRAW | CS_DBLCLKS;
	
	RegisterClassEx (&wnd);
	hWnd = CreateWindowEx (0, DEF_CLASS_NAME, DEF_WIN_TITLE,
		WS_OVERLAPPEDWINDOW, 0, 0, 640, 480, NULL, NULL, wnd.hInstance, NULL);
	
	if (hWnd == NULL) {
		LoadString (hInstance, IDS_ERR_HWND, strMess, DEF_STR_LEN-1);
		MessageBox (NULL, strMess, strTitle, MB_OK | MB_ICONSTOP);
	}
	else {
		GetClientRect (hWnd, &rClient);
		const int nWidth = AREA_WIDTH + 640 - (rClient.right - rClient.left);
		const int nHeight = AREA_HEIGHT + 480 - (rClient.bottom - rClient.top);
		
		SetWindowPos (hWnd, NULL, 0, 0, nWidth, nHeight, SWP_NOMOVE | SWP_NOACTIVATE);
		ShowWindow (hWnd, nCmdShow);
		UpdateWindow (hWnd);
		
		bSuccess =
			SUCCEEDED(hrD3D = InitD3D(hWnd)) &&
			SUCCEEDED(hrGeometry = InitGeometry()) &&
			SUCCEEDED(hrTexture = InitTextures());
			SUCCEEDED(hrDInput = InitDInput (hWnd, hInstance));
		
		//Variabili varie
		g_dwAnisotropy = MIN(g_caps.MaxAnisotropy, 8);
		g_dwMinFilter = (g_caps.TextureFilterCaps & D3DPTFILTERCAPS_MINFANISOTROPIC ? D3DTEXF_ANISOTROPIC : D3DTEXF_LINEAR);
		g_dwMagFilter = (g_caps.TextureFilterCaps & D3DPTFILTERCAPS_MAGFANISOTROPIC ? D3DTEXF_ANISOTROPIC : D3DTEXF_LINEAR);
		g_dev->SetSamplerState (0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);
		g_dev->SetSamplerState (0, D3DSAMP_MAXANISOTROPY, g_dwAnisotropy);
		
		if (bSuccess) {
			//g_keyboard->Acquire();
			t1 = 0.0f;
			t2 = timeGetTime() / 1000.0f;
			ZeroMemory (&msg, sizeof(MSG));
			while (msg.message != WM_QUIT) {
				if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE)) {
					TranslateMessage (&msg);
					DispatchMessage (&msg);
				}
				else {
					t1 = t2;
					t2 = (float)timeGetTime () / 1000.0f;
					MergeInput (&g_miInput);
					
					if (g_miInput.bEsc)
						PostQuitMessage(0);
					//if (g_miInput.fMouseScroll != 0.0f)
					//	g_cam.OffsetRotate (0.0f, 0.0f, g_miInput.fMouseScroll);
					g_cam.InterpolateAnimation (t2 - t1, g_miInput.fMouseScroll);
					
					//FrameMove (t2 - t1);
					g_cam.OffsetRotate (0.0f, (t2 - t1) / 4.0f, 0.0f);
					g_cam.BuildCamMatrix();
					g_dev->SetTransform (D3DTS_VIEW, &g_cam.matCam);
					g_fSpriteIndex += (t2 - t1) * 10.0f;
					while (g_fSpriteIndex >= 72.0f)
						g_fSpriteIndex -= 72.0f;

					if (Render()) {
						bShowErr = true;
						LoadString (hInstance, IDS_ERR_RENDERING, strMess, DEF_STR_LEN-1);
						PostMessage (hWnd, WM_DESTROY, 0, 0);
					}
				}//EndIf
			}//Wend
		} //if (bSuccess)
		else {
			if (FAILED(hrD3D))
				LoadString (hInstance, IDS_ERR_D3DINIT, strMess, DEF_STR_LEN-1);
			else if (FAILED(hrGeometry))
				LoadString (hInstance, IDS_ERR_GEOMETRY, strMess, DEF_STR_LEN-1);
			else if (FAILED(hrDInput))
				LoadString (hInstance, IDS_ERR_DINPUT, strMess, DEF_STR_LEN-1);
			else if (FAILED(hrTexture))
				LoadString (hInstance, IDS_ERR_TEXTURE, strMess, DEF_STR_LEN-1);
			else
				LoadString (hInstance, IDS_ERR_GENERIC, strMess, DEF_STR_LEN-1);
			bShowErr = true;
		}//if (bSuccess)
	}//if (hWnd == NULL)
	
	if (bShowErr) {
		EndIt ();
		MessageBox (NULL, strMess, strTitle, MB_OK | MB_ICONSTOP);
	}
	
	//Pulisco e ciao
	if (strMess != NULL) {
		delete[] strMess;
		strMess = NULL;
	}
	if (strTitle != NULL) {
		delete[] strTitle;
		strTitle = NULL;
	}
	UnregisterClass (DEF_CLASS_NAME, wnd.hInstance);
	return 0;
}



//********************************************************************************
//Event handler
//********************************************************************************
LRESULT CALLBACK MsgProc (HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
//	float fRotation;
	
	switch (uMsg) {
	case WM_DESTROY:
		PostQuitMessage(0);
		EndIt();
		return 0;
	
/*	case WM_MOUSEWHEEL:
		fRotation = (float)GET_WHEEL_DELTA_WPARAM(wParam) / 120.0f;
		g_cam.OffsetRotate (0.0f, 0.0f, fRotation / -25.0f);
		
		return 0;
*/	}
	return DefWindowProc (hwnd, uMsg, wParam, lParam);
}



//********************************************************************************
//Inizializza Direct3D. Restituisce S_OK se va tuttto bene, altrimenti E_FAIL.
//********************************************************************************
HRESULT InitD3D (HWND hWnd) {
	D3DPRESENT_PARAMETERS pparams;
	
	//D3D9
	g_d3d = Direct3DCreate9 (D3D_SDK_VERSION);
	if (g_d3d == NULL)
		return E_FAIL;
	
	//Caps
	ZeroMemory (&g_caps, sizeof(D3DCAPS9));
	if (FAILED(g_d3d->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &g_caps)))
		return E_FAIL;
	
	//Device
	ZeroMemory (&pparams, sizeof(D3DPRESENT_PARAMETERS));
	pparams.AutoDepthStencilFormat = D3DFMT_D16;
	pparams.EnableAutoDepthStencil = TRUE;
	pparams.BackBufferWidth = AREA_WIDTH;
	pparams.BackBufferHeight = AREA_HEIGHT;
#ifdef FULL_SCREEN
	pparams.BackBufferFormat = D3DFMT_A8R8G8B8;
#else
	pparams.BackBufferFormat = D3DFMT_UNKNOWN;
	pparams.Windowed = TRUE;
#endif
	pparams.hDeviceWindow = hWnd;
	pparams.SwapEffect = D3DSWAPEFFECT_DISCARD;
	
	if (FAILED(g_d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
		D3DCREATE_SOFTWARE_VERTEXPROCESSING,
		&pparams, &g_dev)))
		
		return E_FAIL;
	
	g_dev->SetRenderState (D3DRS_ZENABLE, TRUE);
	g_dev->SetRenderState (D3DRS_AMBIENT, D3DCOLOR_XRGB(0xFF, 0xFF, 0xFF));
	g_dev->SetRenderState (D3DRS_LIGHTING, FALSE);
	//g_dev->SetRenderState (D3DRS_CULLMODE, D3DCULL_NONE);
	g_dev->SetRenderState (D3DRS_ALPHABLENDENABLE, TRUE);
	g_dev->SetRenderState (D3DRS_ALPHAREF, 1);
	g_dev->SetRenderState (D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);
	return S_OK;
}



//********************************************************************************
//Crea la grafica del gioco. Restituisce S_OK se va tutto bene, altrimenti
//E_FAIL.
//********************************************************************************
HRESULT InitGeometry () {
	LPCUSTOMVERTEX vertices;
	LPWORD indices;
	void* dst;
	
	if (g_dev == NULL)
		return E_FAIL;
	
	vertices = new CUSTOMVERTEX[4];
	indices = new WORD[6];
	
	//Creazione del terrain
	MakeQuad (80.0f, 0.0f, 80.0f, 17.0f, &vertices, &indices);	
	if (FAILED(g_dev->CreateVertexBuffer (sizeof(CUSTOMVERTEX) << 2, D3DUSAGE_WRITEONLY, D3DFVF_CUSTOM, D3DPOOL_MANAGED, &g_vbTerrain, NULL)))
		return NONZERO;
	dst = NULL;
	if (FAILED(g_vbTerrain->Lock (0, sizeof(CUSTOMVERTEX) << 2, &dst, 0)))
		return NONZERO;
	CopyMemory (dst, vertices, sizeof(CUSTOMVERTEX) << 2);
	g_vbTerrain->Unlock();
	
	if (FAILED(g_dev->CreateIndexBuffer (sizeof(WORD) * 6, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &g_ibTerrain, NULL)))
		return NONZERO;
	dst = NULL;
	if (FAILED(g_ibTerrain->Lock (0, sizeof(WORD) * 6, &dst, 0)))
		return NONZERO;
	CopyMemory (dst, indices, sizeof(WORD) * 6);
	g_ibTerrain->Unlock ();
	
	//Creazione del pupazzo
	MakeQuad (1.0f, 2.0f, 0.0f, 1.0f, &vertices, &indices);
	if (FAILED(g_dev->CreateVertexBuffer (sizeof(CUSTOMVERTEX) << 2, D3DUSAGE_WRITEONLY, D3DFVF_CUSTOM, D3DPOOL_MANAGED, &g_vbPuppet, NULL)))
		return NONZERO;
	dst = NULL;
	if (FAILED(g_vbPuppet->Lock (0, sizeof(CUSTOMVERTEX) << 2, &dst, 0)))
		return NONZERO;
	vertices[1].u = 0.5f;
	vertices[2].u = 0.5f;
	CopyMemory (dst, vertices, sizeof(CUSTOMVERTEX) << 2);
	g_vbPuppet->Unlock();
	
	if (FAILED(g_dev->CreateIndexBuffer (sizeof(WORD) * 6, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &g_ibPuppet, NULL)))
		return NONZERO;
	dst = NULL;
	if (FAILED(g_ibPuppet->Lock (0, sizeof(WORD) * 6, &dst, 0)))
		return NONZERO;
	CopyMemory (dst, indices, sizeof(WORD) * 6);
	g_ibPuppet->Unlock ();
	
	//Imposto la telecamera
	g_cam.BuildDefaultProjection();
	g_cam.SetZoomRange (1.0f, 40.0f);
	g_cam.SetTarget(0.0f, 0.0f, 0.0f);
	g_cam.Rotate(0.0f, 0.0f, D3DX_PI / 4.0f);
	g_cam.BuildCamMatrix ();
	g_dev->SetTransform (D3DTS_PROJECTION, &g_cam.matProj);
	g_dev->SetTransform (D3DTS_VIEW, &g_cam.matCam);
	
	//Pulisco e ciao
	SAFE_DELETE (vertices);
	SAFE_DELETE (indices);
	dst = NULL;
	return S_OK;
}



//********************************************************************************
//Distrugge gli oggetti creati
//********************************************************************************
void EndIt () {
	DestroyDInput ();

	for (int z = 0; z < g_nSpriteCount; z++) {
		SAFE_RELEASE (g_texPuppet[z]);
	}
	SAFE_DELETE (g_texPuppet);
	
	SAFE_RELEASE (g_texTerrain);
	SAFE_RELEASE (g_ibPuppet);
	SAFE_RELEASE (g_vbPuppet);
	SAFE_RELEASE (g_ibTerrain);
	SAFE_RELEASE (g_vbTerrain);
	SAFE_RELEASE (g_dev);
	SAFE_RELEASE (g_d3d);
}



//********************************************************************************
//Disegna su schermo la scena corrente. Restituisce ZERO se � andato tutto
//bene, altrimenti NONZERO.
//********************************************************************************
int Render () {
	D3DXMATRIXA16 matWorld;
	D3DXMATRIXA16 matTmp;

	g_dev->Clear (0, NULL, D3DCLEAR_ZBUFFER|D3DCLEAR_TARGET, D3DCOLOR_XRGB(10, 36, 106), 1.0f, 0);
	
	if (SUCCEEDED(g_dev->BeginScene ())) {
		
		g_dev->SetFVF (D3DFVF_CUSTOM);
		
		//Terrain
		D3DXMatrixIdentity (&matWorld);
		g_dev->SetTransform (D3DTS_WORLD, &matWorld);
		
		g_dev->SetTexture (0, g_texTerrain);
		
		g_dev->SetSamplerState (0, D3DSAMP_MAGFILTER, g_dwMagFilter);
		g_dev->SetSamplerState (0, D3DSAMP_MINFILTER, g_dwMinFilter);
		
		g_dev->SetTextureStageState (0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		g_dev->SetTextureStageState (0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
		
		g_dev->SetStreamSource (0, g_vbTerrain, 0, sizeof(CUSTOMVERTEX));
		g_dev->SetIndices (g_ibTerrain);
		g_dev->DrawIndexedPrimitive (D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
//		g_dev->SetStreamSource (0, NULL, 0, 0);
//		g_dev->SetIndices (NULL);
		
		
		//Puppet
		D3DXMatrixRotationYawPitchRoll (&matWorld, -g_cam.GetRotationY() + D3DX_PI * 3.0f / 2.0f, g_cam.GetRotationZ(), 0.0f);
		D3DXMatrixTranslation (&matTmp, 0.0f, 1.0f, 0.0f);
		D3DXMatrixMultiply (&matWorld, &matTmp, &matWorld);
		g_dev->SetTransform (D3DTS_WORLD, &matWorld);
		
		g_dev->SetTexture (0, g_texPuppet[(DWORD)g_fSpriteIndex]);
		g_dev->SetTextureStageState (0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		g_dev->SetTextureStageState (0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
		//g_dev->SetRenderState (D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		g_dev->SetRenderState (D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		
		g_dev->SetStreamSource (0, g_vbPuppet, 0, sizeof(CUSTOMVERTEX));
		g_dev->SetIndices (g_ibPuppet);
		g_dev->DrawIndexedPrimitive (D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
		g_dev->SetStreamSource (0, NULL, 0, 0);
		g_dev->SetIndices (NULL);
		
		g_dev->SetTexture (0, NULL);
		g_dev->EndScene();
	}
	
	g_dev->Present (NULL, NULL, NULL, NULL);
	return ZERO;
}



//********************************************************************************
//Crea un quadrato avente le caratteristiche specificate. vert e ind
//devono puntare a zone di memoria sufficiantemente grandi per contenere 4
//vertici e 6 indici. Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int MakeQuad (float fWidth, float fHeight, float fDepth, float fTex, LPCUSTOMVERTEX* vert, LPWORD* ind) {
	if (!(vert && ind))
		return NONZERO;
	
	(*vert)[0].color = 0xFFFFFFFF;
	(*vert)[0].u = 0.0f;
	(*vert)[0].v = fTex;
	(*vert)[0].x = -fWidth / 2.0f;
	(*vert)[0].y = -fHeight / 2.0f;
	(*vert)[0].z = -fDepth / 2.0f;
	//-
	(*vert)[1].color = 0xFFFFFFFF;
	(*vert)[1].u = fTex;
	(*vert)[1].v = fTex;
	(*vert)[1].x = fWidth / 2.0f;
	(*vert)[1].y = -fHeight / 2.0f;
	(*vert)[1].z = -fDepth / 2.0f;
	//-
	(*vert)[2].color = 0xFFFFFFFF;
	(*vert)[2].u = fTex;
	(*vert)[2].v = 0.0f;
	(*vert)[2].x = fWidth / 2.0f;
	(*vert)[2].y = fHeight / 2.0f;
	(*vert)[2].z = fDepth / 2.0f;
	//-
	(*vert)[3].color = 0xFFFFFFFF;
	(*vert)[3].u = 0.0f;
	(*vert)[3].v = 0.0f;
	(*vert)[3].x = -fWidth / 2.0f;
	(*vert)[3].y = fHeight / 2.0f;
	(*vert)[3].z = fDepth / 2.0f;
	
	(*ind)[0] = 0;
	(*ind)[1] = 3;
	(*ind)[2] = 2;
	(*ind)[3] = 0;
	(*ind)[4] = 2;
	(*ind)[5] = 1;
	
	return ZERO;
}



//********************************************************************************
//Carica le texture. Restituisce S_OK se va tutto bene, altrimenti E_FAIL.
//********************************************************************************
HRESULT InitTextures () {
	D3DXCreateTextureFromFile (g_dev, "czac24.bmp", &g_texTerrain);
	//D3DXCreateTextureFromFile (g_dev, "woodenplan.jpg", &g_texPuppet);
	
	g_texPuppet = new LPDIRECT3DTEXTURE9[g_nSpriteCount];
	ZeroMemory (g_texPuppet, sizeof(LPDIRECT3DTEXTURE9) * g_nSpriteCount);
	if (LoadSprite("bsmith.bmp", 9, 5, g_texPuppet) == NONZERO)
		return E_FAIL;
	
	return S_OK;
}



//********************************************************************************
//Carica dal file specificato - che deve essere nel formato A1R5G5B5 - lo
//spriteset specificato. nHrzCount e nVrtCount specificano rispettivamente
//il numero di sprite che compongono una riga e una colonna, tex � l'indirizzo
//del primo elemento di un array gi� allocato, sufficientemente grande da
//contenere nHrzCount * nVrtCount sprite. Restituisce ZERO se va tutto bene,
//altrimenti NONZERO.
//********************************************************************************
int LoadSprite (const char* strFileName, int nHrzCount, int nVrtCount, LPDIRECT3DTEXTURE9* tex) {
	CBitmapSarge bmp;
	WORD* data = NULL;
	int nTexSize = 1;
	D3DLOCKED_RECT LockedRect;
	D3DLOCKED_RECT LockedRect2;
	WORD* dst;
	WORD* dst2;
	WORD wPixel;
	int nDstPadd;
	int nDstPadd2;
	bool bSpecular;
	int nIndex = 0;
	int nMirror;
	
	if (g_dev && strFileName && tex == NULL)
		return NONZERO;
	
	if (bmp.LoadPicture(strFileName))
		return NONZERO;
	
	bmp.GetDataPtr ((void**)&data);
	if (data == NULL)
		return NONZERO;
	
	const int nWidth = bmp.GetPictureWidth();
	const int nHeight = bmp.GetPictureHeight();
	const int nSpriteWidth = nWidth / nHrzCount;
	const int nSpriteHeight = nHeight / nVrtCount;
	const int nMaxLen = MAX (nWidth / nHrzCount, nHeight / nVrtCount);
	
	while (nTexSize < nMaxLen)
		nTexSize <<= 1;
	if (nTexSize > g_caps.MaxTextureWidth || nTexSize > g_caps.MaxTextureHeight)
		return NONZERO;
	
	for (int y = 0; y < nVrtCount; y++) {
		bSpecular = (bool)(y != 0 && y != 4);
		
		for (int x = 0; x < nHrzCount; x++) {
			if (FAILED(g_dev->CreateTexture(nTexSize, nTexSize, 1, 0, D3DFMT_A1R5G5B5, D3DPOOL_MANAGED, tex + nIndex, NULL)))
				return NONZERO;
			if (bSpecular) {
				nMirror = 72 - (y * 9) + x;
				if (FAILED(g_dev->CreateTexture(nTexSize, nTexSize, 1, 0, D3DFMT_A1R5G5B5, D3DPOOL_MANAGED, tex + nMirror, NULL)))
					return NONZERO;
			}
			
			//Lock della texture corrente
			ZeroMemory (&LockedRect, sizeof(D3DLOCKED_RECT));
			(tex[nIndex])->LockRect(0, &LockedRect, NULL, 0);
			dst = (WORD*)LockedRect.pBits;
			nDstPadd = LockedRect.Pitch - sizeof(WORD) * nTexSize;
			
			//Lock della texture speculare (se necessario)
			if (bSpecular) {
				ZeroMemory (&LockedRect2, sizeof(D3DLOCKED_RECT));
				(tex[nMirror])->LockRect(0, &LockedRect2, NULL, 0);
				nDstPadd2 = LockedRect2.Pitch - sizeof(WORD) * nTexSize;
				dst2 = (WORD*)((char*)LockedRect2.pBits + LockedRect2.Pitch - nDstPadd2);
			}
			else
				dst2 = NULL;
			
			
			for (int y1 = 0; y1 < nSpriteHeight; y1++) {
				for (int x1 = 0; x1 < nSpriteWidth; x1++) {
					if (dst2 == NULL)
						*dst = *((WORD*)((char*)data + bmp.GetPitch() * (y1 + y * nSpriteHeight)) + x1 + nSpriteWidth * x);
					else {
						wPixel = *((WORD*)((char*)data + bmp.GetPitch() * (y1 + y * nSpriteHeight)) + x1 + nSpriteWidth * x);
						*dst = wPixel;
						*(dst2 - (nTexSize - nSpriteWidth) - x1 - 1) = wPixel;
					}
					dst++;
				}
				for (int x2 = nSpriteWidth; x2 < nTexSize; x2++) {
					*dst = 0;
					if (dst2)
						*(dst2 - (x2 - nSpriteWidth) - 1) = 0;
					dst++;
				}
				dst = (WORD*)((char*)dst + nDstPadd);
				if (dst2)
					dst2 = (WORD*)((char*)LockedRect2.pBits + LockedRect2.Pitch * (y1 + 2) - nDstPadd2);
			}
			for (int y2 = nSpriteHeight; y2 < nTexSize; y2++) {
				for (int x3 = 0; x3 < nTexSize; x3++) {
					*dst = 0;
					if (dst2)
						*(dst2 - x3 - 1) = 0;
					dst++;
				}
				if (dst2)
					dst2 = (WORD*)((char*)LockedRect2.pBits + LockedRect2.Pitch * (y2 + 1) - nDstPadd2);
				dst = (WORD*)((char*)dst + nDstPadd);
			}
			
			(tex[nIndex])->UnlockRect(0);
			if (bSpecular)
				(tex[nMirror])->UnlockRect(0);
			nIndex++;
		}
	}
	
	
	bmp.Dispose();
	return ZERO;
}