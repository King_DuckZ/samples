//CSmartGuy.h


#ifndef _INCL_CSMARTGUY_H
#define _INCL_CSMARTGUY_H

#include "CFlatSprite.h"
//#include <d3dx9.h>
#include "CMathEngine.h"


enum SMARTGUY_STATUS {
	SGS_UNKNOWN = 0,
	SGS_MOVING = 1,
	SGS_SITTING = 2,
	SGS_STANDING = 3
};

class CSmartGuy : public CFlatSprite {
public:
	CSmartGuy ( void );
	~CSmartGuy ( void );
	void Dispose ( void );
	int SetDestination ( float x, float z );
	int Walk ( float fDelta );
	inline void ToggleStand ( void );
	inline void Sit ( void );
	inline void Stand ( void );

	SMARTGUY_STATUS Status;
private:
	inline void m_Dispose ( void );
	int m_Bresenham ( float x0, float y0, float x1, float y1 );
	inline int m_SetDestination ( float x, float z );
	
	MATH_2DVECTOR m_vDestination;
	MATH_2DVECTOR m_vDirection;
	
	POINTFLOAT* m_pffSteps;
	UINT m_unSteps;
	UINT m_unIndex; //Step corrente
	float m_fRad; //Raggio, a partire dall'ultimo step
	float m_fTheta; //Angolo
	float m_fVelocity;
	float m_fPosOldX;
	float m_fPosOldZ;
	bool m_bDestination; //Flag che indica se bisogna cambiare la destinazione del pupazzo appena possibile
};



//********************************************************************************
//********************************************************************************
inline void CSmartGuy::ToggleStand () {
}



//********************************************************************************
//********************************************************************************
inline void CSmartGuy::Stand () {
}


//********************************************************************************
//********************************************************************************
inline void CSmartGuy::Sit () {
}


#endif