//CFlatSprite.cpp


#include "Constants.h"
#include <windows.h>
#include "CFlatSprite.h"
#include "CBitmapSarge.h"
#include <d3dx9.h>
#include <math.h>


#define D3DFVF_CUSTOM (D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1)
#define WRAP_ANGLE(a) { a = fmodf(a,g_f2pi); if (a < 0.0f) a = g_f2pi + a; }


typedef struct STRUCT_CUSTOMVERTEX {
	float x, y, z;
	D3DCOLOR color;
	float tu, tv;
} CUSTOMVERTEX, *LPCUSTOMVERTEX;


const float g_f2pi = D3DX_PI * 2.0f;
float g_fAngleList[] = {
	D3DX_PI * 15.0f / 8.0f,
	D3DX_PI / 8.0f,
	D3DX_PI * 3.0f / 8.0f,
	D3DX_PI * 5.0f / 8.0f,
	D3DX_PI * 7.0f / 8.0f,
	D3DX_PI * 9.0f / 8.0f,
	D3DX_PI * 11.0f / 8.0f,
	D3DX_PI * 13.0f / 8.0f,
	D3DX_PI * 15.0f / 8.0f //Il primo va ripetuto alla fine
};
const DWORD g_dwAngleCount = 9;


//********************************************************************************
//Costruttore standard
//********************************************************************************
CFlatSprite::CFlatSprite () {
	m_vb = NULL;
	m_ib = NULL;
	m_tex = NULL;
	m_nSpriteCount = 0;
	m_fSpriteX = 0.0f;
	m_fFirstSprite = 0.0f;
	m_fCameraAngle = 0.0f;
	m_fHalfHeight = 0.0f;
	m_fSpriteIndex = 0.0f;
	
	fRotY = 0.0f;
	fRotZ = 0.0f;
	fPosX = 0.0f;
	fPosZ = 0.0f;

	m_dwTimeTexture = 0;
	m_dwTimeBuffer = 0;
	
	m_dwSubsetCount = 0;
	m_pfSubset = NULL;
	m_dwSubsetActive = 0;
	m_fSubsetOffset = 0.0f;
}



//********************************************************************************
//Distruttore standard
//********************************************************************************
CFlatSprite::~CFlatSprite () {
	m_Dispose ();
}



//********************************************************************************
//Chiama m_Dispose()
//********************************************************************************
void CFlatSprite::Dispose () {
	m_Dispose ();
}



//********************************************************************************
//Libera la memoria occupata e distrugge gli oggetti creati.
//********************************************************************************
inline void CFlatSprite::m_Dispose () {
	//Se le texture sono state create dopo i buffer, vanno cancellate prima
	if (m_dwTimeTexture > m_dwTimeBuffer) {
		if (m_tex) {
			for (int z = 0; z < m_nSpriteCount; z++) {
				SAFE_RELEASE (m_tex[z]);
			}
		}
		SAFE_DELETE (m_tex);

		SAFE_RELEASE (m_ib);
		SAFE_RELEASE (m_vb);
	}
	//se no
	else {
		SAFE_RELEASE (m_ib);
		SAFE_RELEASE (m_vb);
		
		if (m_tex) {
			for (int z = 0; z < m_nSpriteCount; z++) {
				SAFE_RELEASE (m_tex[z]);
			}
		}
		SAFE_DELETE (m_tex);
	}
	m_dwTimeBuffer = 0;
	m_dwTimeTexture = 0;
	ResetSubset ();
}



//********************************************************************************
//
//********************************************************************************
HRESULT CFlatSprite::BuildQuad (float fWidth, float fHeight, LPDIRECT3DDEVICE9 dev) {
	LPCUSTOMVERTEX vert = new CUSTOMVERTEX[4];
	LPWORD indices = new WORD[6];
	const float fTexU = MIN(fWidth / fHeight, 1.0f); //MIN(fWidth,fHeight) / MAX(fWidth,fHeight);
	const float fTexV = MIN(fHeight / fWidth, 1.0f);
	void* dst;
	
	if (dev == NULL || m_vb != NULL || m_ib != NULL)
		return E_FAIL;
	
	//Impostazione di variabili varie
	m_dwTimeBuffer = GetTickCount();
	m_fHalfHeight = fabs(fHeight) / 2.0f;
	
	vert[0].color = 0xFFFFFFFF;
	vert[0].tu = 0.0f;
	vert[0].tv = 0.0f;
	vert[0].x = 0.0f;
	vert[0].y = fHeight / 2.0f;
	vert[0].z = -fWidth / 2.0f;
	//-
	vert[1].color = 0xFFFFFFFF;
	vert[1].tu = fTexU;
	vert[1].tv = 0.0f;
	vert[1].x = 0.0f;
	vert[1].y = fHeight / 2.0f;
	vert[1].z = fWidth / 2.0f;
	//-
	vert[2].color = 0xFFFFFFFF;
	vert[2].tu = fTexU;
	vert[2].tv = fTexV;
	vert[2].x = 0.0f;
	vert[2].y = -fHeight / 2.0f;
	vert[2].z = fWidth / 2.0f;
	//-
	vert[3].color = 0xFFFFFFFF;
	vert[3].tu = 0.0f;
	vert[3].tv = fTexV;
	vert[3].x = 0.0f;
	vert[3].y = -fHeight / 2.0f;
	vert[3].z = -fWidth / 2.0f;
	
	indices[0] = 0;
	indices[1] = 1;
	indices[2] = 2;
	indices[3] = 0;
	indices[4] = 2;
	indices[5] = 3;
	
	//Creazione del pupazzo
	if (FAILED(dev->CreateVertexBuffer (sizeof(CUSTOMVERTEX) << 2, D3DUSAGE_WRITEONLY, D3DFVF_CUSTOM, D3DPOOL_MANAGED, &m_vb, NULL)))
		return NONZERO;
	dst = NULL;
	if (FAILED(m_vb->Lock (0, sizeof(CUSTOMVERTEX) << 2, &dst, 0)))
		return NONZERO;
	CopyMemory (dst, vert, sizeof(CUSTOMVERTEX) << 2);
	m_vb->Unlock();
	
	if (FAILED(dev->CreateIndexBuffer (sizeof(WORD) * 6, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_ib, NULL)))
		return NONZERO;
	dst = NULL;
	if (FAILED(m_ib->Lock (0, sizeof(WORD) * 6, &dst, 0)))
		return NONZERO;
	CopyMemory (dst, indices, sizeof(WORD) * 6);
	m_ib->Unlock ();

	SAFE_DELETE (vert);
	SAFE_DELETE (indices);
	return S_OK;
}



//********************************************************************************
//Carica dal file specificato - che deve essere nel formato A1R5G5B5 - lo
//spriteset specificato. nHrzCount e nVrtCount specificano rispettivamente
//il numero di sprite che compongono una riga e una colonna, tex � l'indirizzo
//del primo elemento di un array gi� allocato, sufficientemente grande da
//contenere nHrzCount * nVrtCount sprite. Restituisce ZERO se va tutto bene,
//altrimenti NONZERO.
//********************************************************************************
int CFlatSprite::LoadSprite (const char* strFileName, int nHrzCount, LPDIRECT3DDEVICE9 dev, const D3DCAPS9* caps) {
	CBitmapSarge bmp;
	WORD* data = NULL;
	int nTexSize = 1;
	D3DLOCKED_RECT LockedRect;
	D3DLOCKED_RECT LockedRect2;
	WORD* dst;
	WORD* dst2;
	WORD wPixel;
	int nDstPadd;
	int nDstPadd2;
	bool bSpecular;
	int nIndex = 0;
	int nMirror;
	
	//Controllo che non ci siano teture gi� caricate, poi imposto variabili e costanti
	if (m_tex != NULL)
		return NONZERO;
	const int nVrtCount = 5;
	m_nSpriteCount = nHrzCount * 8;
	m_tex = new LPDIRECT3DTEXTURE9[m_nSpriteCount];
	ZeroMemory (m_tex, sizeof(LPDIRECT3DTEXTURE9) * m_nSpriteCount);
	m_dwTimeTexture = GetTickCount();
	m_fSpriteX = (float)nHrzCount;
	
	//Controllo i puntatori
	if (dev && strFileName && caps == false)
		return NONZERO;
	
	//Inizio a caricare la bitmap
	if (bmp.LoadPicture(strFileName))
		return NONZERO;
	
	bmp.GetDataPtr ((void**)&data);
	if (data == NULL)
		return NONZERO;
	
	const int nWidth = bmp.GetPictureWidth();
	const int nHeight = bmp.GetPictureHeight();
	const int nSpriteWidth = nWidth / nHrzCount;
	const int nSpriteHeight = nHeight / nVrtCount;
	const int nMaxLen = MAX (nWidth / nHrzCount, nHeight / nVrtCount);
	
	while (nTexSize < nMaxLen)
		nTexSize <<= 1;
	if (nTexSize > caps->MaxTextureWidth || nTexSize > caps->MaxTextureHeight)
		return NONZERO;
	
	for (int y = 0; y < nVrtCount; y++) {
		bSpecular = (bool)(y != 0 && y != 4);
		
		for (int x = 0; x < nHrzCount; x++) {
			if (FAILED(dev->CreateTexture(nTexSize, nTexSize, 1, 0, D3DFMT_A1R5G5B5, D3DPOOL_MANAGED, m_tex + nIndex, NULL)))
				return NONZERO;
			if (bSpecular) {
				nMirror = m_nSpriteCount - (y * nHrzCount) + x;
				if (FAILED(dev->CreateTexture(nTexSize, nTexSize, 1, 0, D3DFMT_A1R5G5B5, D3DPOOL_MANAGED, m_tex + nMirror, NULL)))
					return NONZERO;
			}
			
			//Lock della texture corrente
			ZeroMemory (&LockedRect, sizeof(D3DLOCKED_RECT));
			(m_tex[nIndex])->LockRect(0, &LockedRect, NULL, 0);
			dst = (WORD*)LockedRect.pBits;
			nDstPadd = LockedRect.Pitch - sizeof(WORD) * nTexSize;
			
			//Lock della texture speculare (se necessario)
			if (bSpecular) {
				ZeroMemory (&LockedRect2, sizeof(D3DLOCKED_RECT));
				(m_tex[nMirror])->LockRect(0, &LockedRect2, NULL, 0);
				nDstPadd2 = LockedRect2.Pitch - sizeof(WORD) * nTexSize;
				dst2 = (WORD*)((char*)LockedRect2.pBits + LockedRect2.Pitch - nDstPadd2);
			}
			else
				dst2 = NULL;
			
			
			for (int y1 = 0; y1 < nSpriteHeight; y1++) {
				for (int x1 = 0; x1 < nSpriteWidth; x1++) {
					if (dst2 == NULL)
						*dst = *((WORD*)((char*)data + bmp.GetPitch() * (y1 + y * nSpriteHeight)) + x1 + nSpriteWidth * x);
					else {
						wPixel = *((WORD*)((char*)data + bmp.GetPitch() * (y1 + y * nSpriteHeight)) + x1 + nSpriteWidth * x);
						*dst = wPixel;
						*(dst2 - (nTexSize - nSpriteWidth) - x1 - 1) = wPixel;
					}
					dst++;
				}
				for (int x2 = nSpriteWidth; x2 < nTexSize; x2++) {
					*dst = 0;
					if (dst2)
						*(dst2 - (x2 - nSpriteWidth) - 1) = 0;
					dst++;
				}
				dst = (WORD*)((char*)dst + nDstPadd);
				if (dst2)
					dst2 = (WORD*)((char*)LockedRect2.pBits + LockedRect2.Pitch * (y1 + 2) - nDstPadd2);
			}
			for (int y2 = nSpriteHeight; y2 < nTexSize; y2++) {
				for (int x3 = 0; x3 < nTexSize; x3++) {
					*dst = 0;
					if (dst2)
						*(dst2 - x3 - 1) = 0;
					dst++;
				}
				if (dst2)
					dst2 = (WORD*)((char*)LockedRect2.pBits + LockedRect2.Pitch * (y2 + 1) - nDstPadd2);
				dst = (WORD*)((char*)dst + nDstPadd);
			}
			
			(m_tex[nIndex])->UnlockRect(0);
			if (bSpecular)
				(m_tex[nMirror])->UnlockRect(0);
			nIndex++;
		}
	}
	
	
	bmp.Dispose();
	return ZERO;
}



//********************************************************************************
//Imposta il valore di rotazione della telecamera che sta inquadrando lo
//sprite. Tale valore sar� poi usato per calcolare lo sprite corretto da
//visualizzare. Restituisce ZERO.
//********************************************************************************
int CFlatSprite::SetCameraAngle (float fa) {
	bool bNotFound = true;
	
/*	if (fa > 0.0f) {
		while (fa > g_f2pi)
			fa -= g_f2pi;
	}
	else if (fa < 0.0f) {
		while (fa < -g_f2pi)
			fa += g_f2pi;
		fa = g_f2pi + fa;
	}
	
	fRotY %= g_f2pi;
	if (fRotY < 0.0f)
		fRotY = g_f2pi + fRotY;
*/
	fa = -fa;
	WRAP_ANGLE (fa);
	WRAP_ANGLE (fRotY);
	m_fCameraAngle = fa;
	fa += fRotY;
	WRAP_ANGLE (fa);
	
	//Salto l'ultimo valore, che � uguale al primo
	//Salto anche il primo, che non pu� verificarsi mai ( fa > 360 E fa < 5)
	for (DWORD z = 1; z < g_dwAngleCount - 1; z++) {
		if ((g_fAngleList[z] <= fa) && (fa < g_fAngleList[z+1])) {
			m_fFirstSprite = (float)z * m_fSpriteX;
			m_fSpriteIndex = fmodf(m_fSpriteIndex, m_fSpriteX) + m_fFirstSprite;
			bNotFound = false;
			break;
		}	
	}
	//Se l'angolo non � stato trovato, � perch� era il primo
	if (bNotFound/*g_dwAngleCount - 1 == z*/) {
		m_fFirstSprite = 0.0f;
		m_fSpriteIndex = fmodf(m_fSpriteIndex, m_fSpriteX);
	}
	
	return ZERO;
}



//********************************************************************************
//Renderizza lo sprite. Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CFlatSprite::Render (LPDIRECT3DDEVICE9 dev) {
	D3DXMATRIXA16 matWorld;
	D3DXMATRIXA16 matTemp;
	
	//D3DXMatrixIdentity (&matWorld);
	//D3DXMatrixIdentity (&matTemp);
	
	D3DXMatrixRotationYawPitchRoll (&matTemp, -m_fCameraAngle, 0.0f, fRotZ);
	D3DXMatrixTranslation (&matWorld, fPosX, 0.0f, fPosZ);
	D3DXMatrixMultiply (&matWorld, &matTemp, &matWorld);
	D3DXMatrixTranslation (&matTemp, 0.0f, m_fHalfHeight, 0.0f);
	D3DXMatrixMultiply (&matWorld, &matTemp, &matWorld);
	dev->SetTransform (D3DTS_WORLD, &matWorld);

	m_WrapIndex ();
	
	dev->SetFVF (D3DFVF_CUSTOM);
	dev->SetTexture (0, m_tex[(DWORD)m_fSpriteIndex]);
	dev->SetTextureStageState (0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	
	//dev->SetRenderState (D3DRS_ALPHATESTENABLE, TRUE);
	//dev->SetRenderState (D3DRS_ALPHABLENDENABLE, FALSE);

	dev->SetRenderState (D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	dev->SetRenderState (D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	
	dev->SetStreamSource (0, m_vb, 0, sizeof(CUSTOMVERTEX));
	dev->SetIndices (m_ib);
	dev->DrawIndexedPrimitive (D3DPT_TRIANGLELIST, 0, 0, 6, 0, 2);

	return ZERO;
}



//********************************************************************************
//Controlla la validit� dell'indice dello sprite corrente, modificandolo
//in caso di necessit�.
//********************************************************************************
inline void CFlatSprite::m_WrapIndex () {
/*
	float m_fSpriteX; //Numero di sprite per riga
	float m_fFirstSprite; //Indice del primo sprite della riga da disegnare
	float m_fSpriteIndex; //Indice dello sprite corrente
*/
	//Se � stato definito un subset e questo � attivo, lo uso
	if (m_dwSubsetActive) {
		while (m_fSpriteIndex >= m_fFirstSprite + m_fSubsetOffset + m_pfSubset[m_dwSubsetActive - 1])
			m_fSpriteIndex -= m_pfSubset[m_dwSubsetActive - 1];
	}
	//Se no parte in loop tutta la riga
	else {
		while (m_fSpriteIndex >= m_fFirstSprite + m_fSpriteX)
			m_fSpriteIndex -= m_fSpriteX;
	}
}



//********************************************************************************
//Avanza l'animazione dello sprite.
//********************************************************************************
void CFlatSprite::AdvanceAnimation (float fDelta) {
	m_fSpriteIndex += fDelta * 10.0f;
}



//********************************************************************************
//Libera la memoria occupata dalla lista dei subset e resetta tutte
//le variabili relative. Restituisce ZERO.
//********************************************************************************
int CFlatSprite::ResetSubset () {
	m_dwSubsetActive = 0;
	SAFE_DELETE (m_pfSubset);
	m_dwSubsetCount = 0;
	m_fSubsetOffset = 0.0f;
	return ZERO;
}



//********************************************************************************
//Aggiunge alla lista di subset il valore specificato. Restituisce l'indice
//per il subset appena aggiunto (valore intero progressivo > 0) in caso
//di successo, 0 in caso di errore.
//********************************************************************************
DWORD CFlatSprite::AddSubSet (DWORD dwLenght) {
	if (m_EnlargeSubset())
		return 0;
	
	m_pfSubset[m_dwSubsetCount-1] = (float)dwLenght;
	
	return m_dwSubsetCount;
}



//********************************************************************************
//Rialloca l'array per i subset, aggiungendo un nuovo elemento alla fine
//di questo. Il valore dell'elemento aggiunto non viene inizializzato.
//Il contatore degli elementi (m_dwSubsetCount) viene incrementato di 1.
//Restituisce ZERO.
//********************************************************************************
int CFlatSprite::m_EnlargeSubset () {
	float* pfTmp;
	
	if (m_dwSubsetCount > 0) {
		pfTmp = new float[m_dwSubsetCount];
		CopyMemory (pfTmp, m_pfSubset, m_dwSubsetCount * sizeof(float));
		SAFE_DELETE (m_pfSubset);
		m_pfSubset = new float[m_dwSubsetCount + 1];
		CopyMemory (m_pfSubset, pfTmp, m_dwSubsetCount * sizeof(float));
		SAFE_DELETE (pfTmp);
		m_dwSubsetCount++;
	}
	else {
		m_pfSubset = new float[1];
		m_dwSubsetCount = 1;
	}
	
	return ZERO;
}



//********************************************************************************
//Se dwIndex specifica un indice valido per un subset, questo viene
//impostato come attivo. Restituisce ZERO se l'indice viene accettato,
//altrimenti NONZERO.
//********************************************************************************
int CFlatSprite::SetActiveSubset (DWORD dwIndex) {
	if (dwIndex <= m_dwSubsetCount) {
		
		//0 disattiva i subset
		if (dwIndex == 0) {
			m_dwSubsetActive = 0;
			return ZERO;
		}
		m_dwSubsetActive = dwIndex;
		
		m_fSubsetOffset = 0.0f;
		for (DWORD z = 0; z < m_dwSubsetActive - 1; z++) {
			m_fSubsetOffset += m_pfSubset[z];
		}
		
		m_fSpriteIndex = m_fFirstSprite + m_fSubsetOffset;
		return ZERO;
	}
	else
		return NONZERO;
}