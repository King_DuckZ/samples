//Main.cpp


//Costanti
#define INITGUID
#define DEF_CLASS_NAME "RO_WIN"
#define DEF_WIN_TITLE "iRO sim"
#define DIRECTINPUT_VERSION 0x0800


//Headers
#include "constants.h"
#include <windows.h>
#include <d3d9.h>
#include <dinput.h>
#include <d3dx9.h>
#include "CCamera.h"
#include "resource.h"
#include "Cust_Input.h"
#include "CMouse.h"
#include "CDialogue.h"
#include "CSpeech.h"
#include "CSmartGuy.h"
#include "CExecutor.h"

#ifdef WIN32_LEAN_AND_MEAN
#include <mmsystem.h>
#endif


//Strutture
typedef struct STRUCT_CUSTOMVERTEX {
	float x, y, z;
	D3DCOLOR color;
	float u, v;
} CUSTOMVERTEX, *LPCUSTOMVERTEX;


//Prototipi
int WINAPI WinMain ( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow );
LRESULT CALLBACK MsgProc ( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam );
HRESULT InitD3D ( HWND hWnd );
void EndIt ( void );
HRESULT InitGeometry ( void );
int Render ( void );
int MakeQuad ( float fWidth, float fHeight, float fDepth, float fTex, LPCUSTOMVERTEX* vert, LPWORD* ind );
HRESULT InitTextures ( void );
int ComputeSelectorPos ( float fMouseX, float fMouseY );


//Variabili globali
LPDIRECT3D9 g_d3d = NULL;
LPDIRECT3DDEVICE9 g_dev = NULL;
LPDIRECT3DVERTEXBUFFER9 g_vbTerrain = NULL;
LPDIRECT3DINDEXBUFFER9 g_ibTerrain = NULL;
D3DCAPS9 g_caps;
CCamera g_cam;
LPDIRECT3DTEXTURE9 g_texTerrain = NULL;
DWORD g_dwAnisotropy;
DWORD g_dwMagFilter;
DWORD g_dwMinFilter;
MERGED_INPUT g_miInput;
CSmartGuy g_sprtPuppet;
LPDIRECT3DVERTEXBUFFER9 g_vbSelector = NULL;
LPDIRECT3DINDEXBUFFER9 g_ibSelector = NULL;
LPDIRECT3DTEXTURE9 g_texSelector = NULL;
float g_fSelectorX = 0.0f;
float g_fSelectorZ = 0.0f;
CMouse g_mouse;
CDialogue g_Dialogue;
CSpeech g_Speech;
bool g_bRotating = false;
CExecutor g_Executor;


//Altre costanti
#define D3DFVF_CUSTOM (D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1)
#ifndef WM_MOUSEWHEEL
#define WM_MOUSEWHEEL                   0x020A
#endif
#ifndef WHEEL_DELTA
#define WHEEL_DELTA 120
#endif
#ifndef GET_WHEEL_DELTA_WPARAM
#define GET_WHEEL_DELTA_WPARAM(wParam)  ((short)HIWORD(wParam))
#endif
#define SUBSET_WALK_LEN 8
#define SUBSET_IDLE_LEN 1
#define SUBSET_SITTING_LEN 1
#define SUBSET_WALK 2
#define SUBSET_IDLE 1
#define SUBSET_SITTING 3
#define SUBSET_DEFAULT SUBSET_SITTING

//********************************************************************************
//WinMain
//********************************************************************************
int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
#pragma unused (hPrevInstance)
#pragma unused (lpCmdLine)
	WNDCLASSEX wnd;
	HWND hWnd;
	RECT rClient;
	char* strMess;
	char* strTitle;
	MSG msg;
	float t1, t2;
	bool bShowErr = false;
	BOOL bSuccess;
	HRESULT hrD3D = S_OK, hrGeometry = S_OK, hrDInput = S_OK, hrTexture = S_OK;
	
	strMess = new char[DEF_STR_LEN];
	strTitle = new char[32];
	if ( (strMess == NULL ) || (strTitle == NULL) )
		return 0;
	LoadString (hInstance, IDS_ERR_TITLE, strTitle, 32); 
	
	wnd.cbClsExtra = 0;
	wnd.cbSize = sizeof(WNDCLASSEX);
	wnd.cbWndExtra = 0;
	wnd.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wnd.hCursor = LoadCursor (NULL, IDC_ARROW);
	wnd.hIcon = LoadIcon (hInstance, MAKEINTRESOURCE(IDI_MAIN_ICON));
	wnd.hIconSm = LoadIcon (hInstance, MAKEINTRESOURCE(IDI_MAIN_ICON));
	wnd.hInstance = hInstance;
	wnd.lpfnWndProc = MsgProc;
	wnd.lpszClassName = DEF_CLASS_NAME;
	wnd.lpszMenuName = NULL;
	wnd.style = CS_VREDRAW | CS_HREDRAW | CS_DBLCLKS;
	
	RegisterClassEx (&wnd);
	hWnd = CreateWindowEx (0, DEF_CLASS_NAME, DEF_WIN_TITLE,
		WS_OVERLAPPEDWINDOW, 0, 0, 640, 480, NULL, NULL, wnd.hInstance, NULL);
	
	if (hWnd == NULL) {
		LoadString (hInstance, IDS_ERR_HWND, strMess, DEF_STR_LEN-1);
		MessageBox (NULL, strMess, strTitle, MB_OK | MB_ICONSTOP);
	}
	else {
		GetClientRect (hWnd, &rClient);
		const int nWidth = AREA_WIDTH + 640 - (rClient.right - rClient.left);
		const int nHeight = AREA_HEIGHT + 480 - (rClient.bottom - rClient.top);
		
		SetWindowPos (hWnd, NULL, 0, 0, nWidth, nHeight, SWP_NOMOVE | SWP_NOACTIVATE);
		ShowWindow (hWnd, nCmdShow);
		UpdateWindow (hWnd);
		
		bSuccess =
			SUCCEEDED(hrD3D = InitD3D(hWnd)) &&
			SUCCEEDED(hrGeometry = InitGeometry()) &&
			SUCCEEDED(hrTexture = InitTextures());
			SUCCEEDED(hrDInput = InitDInput (hWnd, hInstance));
		
		//Variabili varie
		g_dwAnisotropy = MIN(g_caps.MaxAnisotropy, 8);
		g_dwMinFilter = (g_caps.TextureFilterCaps & D3DPTFILTERCAPS_MINFANISOTROPIC ? D3DTEXF_ANISOTROPIC : D3DTEXF_LINEAR);
		g_dwMagFilter = (g_caps.TextureFilterCaps & D3DPTFILTERCAPS_MAGFANISOTROPIC ? D3DTEXF_ANISOTROPIC : D3DTEXF_LINEAR);
		g_dev->SetSamplerState (0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);
		g_dev->SetSamplerState (0, D3DSAMP_MAXANISOTROPY, g_dwAnisotropy);
		g_sprtPuppet.fRotY = 0.0f;//D3DX_PI;
		g_cam.Rotate (0.0f, D3DX_PI / 4.0f);
		
		g_Dialogue.SetFactor (800.0f, AREA_WIDTH_FLOAT, 600.0f, AREA_HEIGHT_FLOAT);
		g_Speech.SetReferenceArea (800.f, 600.0f);
		g_Speech.LoadCharset (g_dev, "Charset01.dlg", "Charset01.bmp");
		g_Executor.LoadSpeechDump ("speech.bin");
		g_Executor.SetCSpeech (&g_Speech);
		g_Executor.SetCDialogue (&g_Dialogue);

		POINT p = {AREA_WIDTH >> 1, AREA_HEIGHT >> 1};
		float fOffset;
		float fOldSelectorX = 0.0f, fOldSelectorZ = 0.0f;
		ClientToScreen (hWnd, &p);
		SetCursorPos (p.x, p.y);
		g_mouse.fX = AREA_WIDTH_FLOAT / 2.0f;
		g_mouse.fY = AREA_HEIGHT_FLOAT / 2.0f;
		g_mouse.SetPause (0.0f);
		
		if (bSuccess) {
			//g_keyboard->Acquire();
			t1 = 0.0f;
			t2 = timeGetTime() / 1000.0f;
			ZeroMemory (&msg, sizeof(MSG));
			while ( true /*msg.message != WM_QUIT*/ ) {
				if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE)) {
					if (msg.message == WM_QUIT)
						break;
					TranslateMessage (&msg);
					DispatchMessage (&msg);
				}
				else {
					t1 = t2;
					t2 = (float)timeGetTime () / 1000.0f;
					MergeInput (&g_miInput);

					if (g_miInput.bEsc) {
						break;
					}
					
					g_cam.InterpolateAnimation (t2 - t1, g_miInput.fMouseScroll * 1.5f);
					g_cam.SetTarget (g_sprtPuppet.fPosX, 0.0f, g_sprtPuppet.fPosZ);
					g_sprtPuppet.fRotZ = g_cam.GetRotationZ();
					
					g_mouse.fX += (float)g_miInput.nMouseX;
					g_mouse.fY += (float)g_miInput.nMouseY;
					fOffset = g_mouse.fX;
					g_mouse.WrapMouse();
					fOffset = (float)g_miInput.nMouseX + (g_mouse.fX - fOffset);
					g_mouse.AdvanceFrame (t2 - t1);
					ComputeSelectorPos (g_mouse.fX, g_mouse.fY);
					if (g_miInput.bMouseLeftBtn && g_sprtPuppet.GetActiveSubset() != SUBSET_SITTING) {
						if (fOldSelectorX != g_fSelectorX || fOldSelectorZ != g_fSelectorZ) {
							fOldSelectorX = g_fSelectorX;
							fOldSelectorZ = g_fSelectorZ;
							g_sprtPuppet.SetDestination (g_fSelectorX, g_fSelectorZ);
						}
					}
					
					if (g_sprtPuppet.GetActiveSubset() != SUBSET_SITTING) {
						g_sprtPuppet.Walk (t2 - t1);
						if (g_sprtPuppet.Status == SGS_MOVING && g_sprtPuppet.GetActiveSubset() != SUBSET_WALK)
							g_sprtPuppet.SetActiveSubset (SUBSET_WALK);
						else if (g_sprtPuppet.Status == SGS_STANDING)
							g_sprtPuppet.SetActiveSubset (SUBSET_IDLE);
					}
						
					
					g_Executor.Behave (g_dev, t2 - t1, g_miInput.bSkipDialogue);
					//FrameMove (t2 - t1);
					if (fOffset != 0.0f && g_miInput.bMouseRightBtn) {
						g_cam.OffsetRotate ((t2 - t1) * fOffset / 3.0f, 0.0f);
					}
					g_bRotating = g_miInput.bMouseRightBtn;
					
					g_cam.BuildCamMatrix();
					g_sprtPuppet.SetCameraAngle (g_cam.GetRotationY());
					g_sprtPuppet.AdvanceAnimation (t2 - t1);
					g_dev->SetTransform (D3DTS_VIEW, (D3DMATRIX*)&g_cam.matCam);
					
					if (g_miInput.bInsert && g_sprtPuppet.GetActiveSubset() == SUBSET_SITTING)
						g_sprtPuppet.SetActiveSubset (SUBSET_IDLE);
					else if (g_miInput.bInsert)
						g_sprtPuppet.SetActiveSubset (SUBSET_SITTING);

					if (Render()) {
						bShowErr = true;
						LoadString (hInstance, IDS_ERR_RENDERING, strMess, DEF_STR_LEN-1);
						//PostMessage (hWnd, WM_DESTROY, 0, 0);
						break;
					}
				}//EndIf
			}//Wend
		} //if (bSuccess)
		else {
			if (FAILED(hrD3D))
				LoadString (hInstance, IDS_ERR_D3DINIT, strMess, DEF_STR_LEN-1);
			else if (FAILED(hrGeometry))
				LoadString (hInstance, IDS_ERR_GEOMETRY, strMess, DEF_STR_LEN-1);
			else if (FAILED(hrDInput))
				LoadString (hInstance, IDS_ERR_DINPUT, strMess, DEF_STR_LEN-1);
			else if (FAILED(hrTexture))
				LoadString (hInstance, IDS_ERR_TEXTURE, strMess, DEF_STR_LEN-1);
			else
				LoadString (hInstance, IDS_ERR_GENERIC, strMess, DEF_STR_LEN-1);
			bShowErr = true;
		}//if (bSuccess)
	}//if (hWnd == NULL)
	
	EndIt ();
	if (bShowErr) {
		MessageBox (NULL, strMess, strTitle, MB_OK | MB_ICONSTOP);
	}
	
	//Pulisco e ciao
	if (strMess != NULL) {
		delete[] strMess;
		strMess = NULL;
	}
	if (strTitle != NULL) {
		delete[] strTitle;
		strTitle = NULL;
	}
	UnregisterClass (DEF_CLASS_NAME, wnd.hInstance);
	return msg.wParam;
}



//********************************************************************************
//Event handler
//********************************************************************************
LRESULT CALLBACK MsgProc (HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
//	float fRotation;
	
	switch (uMsg) {
	case WM_DESTROY:
		PostQuitMessage(0);
		return wParam;
	
/*	case WM_MOUSEWHEEL:
		fRotation = (float)GET_WHEEL_DELTA_WPARAM(wParam) / 120.0f;
		g_cam.OffsetRotate (0.0f, 0.0f, fRotation / -25.0f);
		
		return 0;
*/	
	case WM_SETCURSOR:
		SetCursor (NULL);
		return TRUE;
	}
	return DefWindowProc (hwnd, uMsg, wParam, lParam);
}



//********************************************************************************
//Inizializza Direct3D. Restituisce S_OK se va tuttto bene, altrimenti E_FAIL.
//********************************************************************************
HRESULT InitD3D (HWND hWnd) {
	D3DPRESENT_PARAMETERS pparams;
	
	//D3D9
	g_d3d = Direct3DCreate9 (D3D_SDK_VERSION);
	if (g_d3d == NULL)
		return E_FAIL;
	
	//Caps
	ZeroMemory (&g_caps, sizeof(D3DCAPS9));
	if (FAILED(g_d3d->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &g_caps)))
		return E_FAIL;
	
	//Device
	ZeroMemory (&pparams, sizeof(D3DPRESENT_PARAMETERS));
	pparams.AutoDepthStencilFormat = D3DFMT_D16;
	pparams.EnableAutoDepthStencil = TRUE;
	pparams.BackBufferWidth = AREA_WIDTH;
	pparams.BackBufferHeight = AREA_HEIGHT;
#ifdef FULL_SCREEN
	pparams.BackBufferFormat = D3DFMT_A8R8G8B8;
#else
	pparams.BackBufferFormat = D3DFMT_UNKNOWN;
	pparams.Windowed = TRUE;
#endif
	pparams.hDeviceWindow = hWnd;
	pparams.SwapEffect = D3DSWAPEFFECT_DISCARD;
	
	if (FAILED(g_d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
		D3DCREATE_SOFTWARE_VERTEXPROCESSING,
		&pparams, &g_dev)))
		
		return E_FAIL;
	
	g_dev->SetRenderState (D3DRS_ZENABLE, TRUE);
	g_dev->SetRenderState (D3DRS_AMBIENT, D3DCOLOR_XRGB(0xFF, 0xFF, 0xFF));
	g_dev->SetRenderState (D3DRS_LIGHTING, FALSE);
	//g_dev->SetRenderState (D3DRS_CULLMODE, D3DCULL_NONE);
	g_dev->SetRenderState (D3DRS_ALPHABLENDENABLE, TRUE);
	g_dev->SetRenderState (D3DRS_ALPHAREF, 1);
	g_dev->SetRenderState (D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);
	return S_OK;
}



//********************************************************************************
//Crea la grafica del gioco. Restituisce S_OK se va tutto bene, altrimenti
//E_FAIL.
//********************************************************************************
HRESULT InitGeometry () {
	LPCUSTOMVERTEX vertices;
	LPWORD indices;
	void* dst;
	
	if (g_dev == NULL)
		return E_FAIL;
	
	vertices = new CUSTOMVERTEX[4];
	indices = new WORD[6];
	
	//Creazione del terrain
	MakeQuad (81.0f, 0.0f, 81.0f, 17.0f, &vertices, &indices);	
	if (FAILED(g_dev->CreateVertexBuffer (sizeof(CUSTOMVERTEX) << 2, D3DUSAGE_WRITEONLY, D3DFVF_CUSTOM, D3DPOOL_MANAGED, &g_vbTerrain, NULL)))
		return NONZERO;
	dst = NULL;
	if (FAILED(g_vbTerrain->Lock (0, sizeof(CUSTOMVERTEX) << 2, &dst, 0)))
		return NONZERO;
	CopyMemory (dst, vertices, sizeof(CUSTOMVERTEX) << 2);
	g_vbTerrain->Unlock();
	
	if (FAILED(g_dev->CreateIndexBuffer (sizeof(WORD) * 6, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &g_ibTerrain, NULL)))
		return NONZERO;
	dst = NULL;
	if (FAILED(g_ibTerrain->Lock (0, sizeof(WORD) * 6, &dst, 0)))
		return NONZERO;
	CopyMemory (dst, indices, sizeof(WORD) * 6);
	g_ibTerrain->Unlock ();
	
	//Creazione del pupazzo
	if (FAILED(g_sprtPuppet.BuildQuad (1.0f, 2.0f, g_dev)))
		return E_FAIL;
	
	//Creazione del cursore sul terreno
	MakeQuad (1.0f, 0.0f, 1.0f, 1.0f, &vertices, &indices);
	if (FAILED(g_dev->CreateVertexBuffer (sizeof(CUSTOMVERTEX) << 2, D3DUSAGE_WRITEONLY, D3DFVF_CUSTOM, D3DPOOL_MANAGED, &g_vbSelector, NULL)))
		return NONZERO;
	dst = NULL;
	if (FAILED(g_vbSelector->Lock (0, sizeof(CUSTOMVERTEX) << 2, &dst, 0)))
		return NONZERO;
	CopyMemory (dst, vertices, sizeof(CUSTOMVERTEX) << 2);
	g_vbSelector->Unlock();
	
	if (FAILED(g_dev->CreateIndexBuffer (sizeof(WORD) * 6, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &g_ibSelector, NULL)))
		return NONZERO;
	dst = NULL;
	if (FAILED(g_ibSelector->Lock (0, sizeof(WORD) * 6, &dst, 0)))
		return NONZERO;
	CopyMemory (dst, indices, sizeof(WORD) * 6);
	g_ibSelector->Unlock ();
	
	//Creazione della finestra di dialogo
	g_Dialogue.PreBuild (g_dev, "Dialogue01.bmp");
		
	//Imposto la telecamera
	g_cam.BuildDefaultProjection();
	g_cam.SetZoomRange (1.0f, 40.0f);
	g_cam.SetTarget(0.0f, 0.0f, 0.0f);
	g_cam.Rotate(0.0f, D3DX_PI / 4.0f);
	g_cam.BuildCamMatrix ();
	g_dev->SetTransform (D3DTS_PROJECTION, (D3DMATRIX*)&g_cam.matProj);
	g_dev->SetTransform (D3DTS_VIEW, (D3DMATRIX*)&g_cam.matCam);
	
	//Pulisco e ciao
	SAFE_DELETE (vertices);
	SAFE_DELETE (indices);
	dst = NULL;
	return S_OK;
}



//********************************************************************************
//Distrugge gli oggetti creati
//********************************************************************************
void EndIt () {
	DestroyDInput ();

	g_Speech.Dispose();
	g_Dialogue.Dispose ();
	g_mouse.Dispose();
	SAFE_RELEASE (g_texSelector);
	SAFE_RELEASE (g_texTerrain);
	SAFE_RELEASE (g_ibSelector);
	SAFE_RELEASE (g_vbSelector);
	g_sprtPuppet.Dispose();
	SAFE_RELEASE (g_ibTerrain);
	SAFE_RELEASE (g_vbTerrain);
	SAFE_RELEASE (g_dev);
	SAFE_RELEASE (g_d3d);
}



//********************************************************************************
//Disegna su schermo la scena corrente. Restituisce ZERO se � andato tutto
//bene, altrimenti NONZERO.
//********************************************************************************
int Render () {
	D3DXMATRIXA16 matWorld;

	g_dev->Clear (0, NULL, D3DCLEAR_ZBUFFER|D3DCLEAR_TARGET, D3DCOLOR_XRGB(10, 36, 106), 1.0f, 0);
	
	if (SUCCEEDED(g_dev->BeginScene ())) {
		
		//Terrain
		g_dev->SetFVF (D3DFVF_CUSTOM);
		D3DXMatrixIdentity (&matWorld);
		g_dev->SetTransform (D3DTS_WORLD, &matWorld);
		
		g_dev->SetTexture (0, g_texTerrain);
		
		g_dev->SetSamplerState (0, D3DSAMP_MAGFILTER, g_dwMagFilter);
		g_dev->SetSamplerState (0, D3DSAMP_MINFILTER, g_dwMinFilter);
		
		g_dev->SetTextureStageState (0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		g_dev->SetTextureStageState (0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
		
		g_dev->SetStreamSource (0, g_vbTerrain, 0, sizeof(CUSTOMVERTEX));
		g_dev->SetIndices (g_ibTerrain);
		g_dev->DrawIndexedPrimitive (D3DPT_TRIANGLELIST, 0, 0, 6, 0, 2);
		
		//Selector
		if (!g_bRotating) {
			D3DXMatrixTranslation (&matWorld, roundf(g_fSelectorX), 0.0f, roundf(g_fSelectorZ));
			g_dev->SetTransform (D3DTS_WORLD, &matWorld);
			g_dev->SetRenderState (D3DRS_ZFUNC, D3DCMP_ALWAYS);
			g_dev->SetSamplerState (0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
			g_dev->SetSamplerState (0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
			g_dev->SetTextureStageState (0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
			g_dev->SetTextureStageState (0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
			g_dev->SetRenderState (D3DRS_DESTBLEND, D3DBLEND_ONE);
			g_dev->SetRenderState (D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			g_dev->SetTexture (0, g_texSelector);
			g_dev->SetStreamSource (0, g_vbSelector, 0, sizeof(CUSTOMVERTEX));
			g_dev->SetIndices (g_ibSelector);
			g_dev->DrawIndexedPrimitive (D3DPT_TRIANGLELIST, 0, 0, 6, 0, 2);
			g_dev->SetRenderState (D3DRS_DESTBLEND, D3DBLEND_ZERO);
			g_dev->SetRenderState (D3DRS_SRCBLEND, D3DBLEND_ONE);
			g_dev->SetRenderState (D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
		}
		
		//Puppet
		g_sprtPuppet.Render (g_dev);
		
		//Dialogo
		if (g_Speech.Status != SPEECHS_IDLE) {
			g_Dialogue.Render (g_dev);
			g_Speech.Render (g_dev);
		}
		g_dev->SetRenderState (D3DRS_ALPHATESTENABLE, FALSE);
		
		//Mouse
		g_mouse.DrawMouse();
		
		//Pulizia
		g_dev->SetStreamSource (0, NULL, 0, 0);
		g_dev->SetIndices (NULL);
		g_dev->SetTexture (0, NULL);
		g_dev->EndScene();
	}
	
	g_dev->Present (NULL, NULL, NULL, NULL);
	return ZERO;
}



//********************************************************************************
//Crea un quadrato avente le caratteristiche specificate. vert e ind
//devono puntare a zone di memoria sufficiantemente grandi per contenere 4
//vertici e 6 indici. Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int MakeQuad (float fWidth, float fHeight, float fDepth, float fTex, LPCUSTOMVERTEX* vert, LPWORD* ind) {
	if (!(vert && ind))
		return NONZERO;
	
	(*vert)[0].color = 0xFFFFFFFF;
	(*vert)[0].u = 0.0f;
	(*vert)[0].v = fTex;
	(*vert)[0].x = -fWidth / 2.0f;
	(*vert)[0].y = -fHeight / 2.0f;
	(*vert)[0].z = -fDepth / 2.0f;
	//-
	(*vert)[1].color = 0xFFFFFFFF;
	(*vert)[1].u = fTex;
	(*vert)[1].v = fTex;
	(*vert)[1].x = fWidth / 2.0f;
	(*vert)[1].y = -fHeight / 2.0f;
	(*vert)[1].z = -fDepth / 2.0f;
	//-
	(*vert)[2].color = 0xFFFFFFFF;
	(*vert)[2].u = fTex;
	(*vert)[2].v = 0.0f;
	(*vert)[2].x = fWidth / 2.0f;
	(*vert)[2].y = fHeight / 2.0f;
	(*vert)[2].z = fDepth / 2.0f;
	//-
	(*vert)[3].color = 0xFFFFFFFF;
	(*vert)[3].u = 0.0f;
	(*vert)[3].v = 0.0f;
	(*vert)[3].x = -fWidth / 2.0f;
	(*vert)[3].y = fHeight / 2.0f;
	(*vert)[3].z = fDepth / 2.0f;
	
	(*ind)[0] = 0;
	(*ind)[1] = 3;
	(*ind)[2] = 2;
	(*ind)[3] = 0;
	(*ind)[4] = 2;
	(*ind)[5] = 1;
	
	return ZERO;
}



//********************************************************************************
//Carica le texture. Restituisce S_OK se va tutto bene, altrimenti E_FAIL.
//********************************************************************************
HRESULT InitTextures () {
	if (FAILED(D3DXCreateTextureFromFile (g_dev, "czac24.bmp", &g_texTerrain)))
		return E_FAIL;
	
	if (g_sprtPuppet.LoadSprite ("bsmith.bmp", 10, g_dev, &g_caps) == NONZERO)
		return E_FAIL;
	g_sprtPuppet.AddSubSet (SUBSET_IDLE_LEN);
	g_sprtPuppet.AddSubSet (SUBSET_WALK_LEN);
	g_sprtPuppet.AddSubSet (SUBSET_SITTING_LEN);
	g_sprtPuppet.SetActiveSubset (SUBSET_DEFAULT);
	
	if (FAILED(D3DXCreateTextureFromFile (g_dev, "Selector32.bmp", &g_texSelector)))
		return E_FAIL;
	
	g_mouse.BuildCursor ("cursor32.bmp", g_dev, 4.0f, 7.0f, 4, 4);
	return S_OK;
}



//********************************************************************************
//Calcola la posizione in cui disegnare il selector, basandosi sulla posizione
//della telecamera e del mouse sullo schermo. Restituisce ZERO.
//********************************************************************************
int ComputeSelectorPos (float fMouseX, float fMouseY) {
/*	POINT p;
	GetCursorPos (&p);
	ScreenToClient (hWnd, &p);
	fMouseX = (float)p.x;//((float)p.x - 400.0f) / 800.0f;
	fMouseY = (float)p.y;//(600.0f - (float)p.y - 300.0f) / 600.0f;
*/	
	D3DXVECTOR3 vec1(fMouseX, fMouseY, 0.0f);
	D3DXVECTOR3 vec2(fMouseX, fMouseY, 1.0f);
	D3DXVECTOR3 vecNormal;
	D3DXVECTOR3 vecU (1.0f, 0.0f, 0.0f);
	D3DXVECTOR3 vecV (0.0f, 0.0f, 1.0f);
	D3DXPLANE plane;
	D3DXVECTOR3 vecRes;
	
	D3DVIEWPORT9 vp;
	D3DXMATRIXA16 matWorld;
	
	vp.MinZ = 0.0f;
	vp.MaxZ = 1.0f;
	vp.X = 0;
	vp.Y = 0;
	vp.Width = AREA_WIDTH;
	vp.Height = AREA_HEIGHT;
	D3DXMatrixIdentity (&matWorld);
	D3DXVec3Unproject (&vec1, &vec1, &vp, (D3DXMATRIX*)&g_cam.matProj, (D3DXMATRIX*)&g_cam.matCam, (D3DXMATRIX*)&matWorld);
	D3DXVec3Unproject (&vec2, &vec2, &vp, (D3DXMATRIX*)&g_cam.matProj, (D3DXMATRIX*)&g_cam.matCam, (D3DXMATRIX*)&matWorld);
	
	D3DXVec3Cross (&vecNormal, &vecU, &vecV);
//	plane = D3DXPLANE(vecNormal.x, vecNormal.y, vecNormal.z, D3DXVec3Dot(&vecNormal, &vecU));
	plane = D3DXPLANE(0.0f, 1.0f, 0.0f, 0.0f);
	D3DXPlaneIntersectLine (&vecRes, &plane, &vec1, &vec2);
	
	g_fSelectorZ = vecRes.z;
	g_fSelectorX = vecRes.x;
	return ZERO;
}



//********************************************************************************
//Aggiorna la velocit�
//********************************************************************************
void UpdateOmega ( float fDelta );
