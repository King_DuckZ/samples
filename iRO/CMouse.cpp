//CMouse.cpp


#include "constants.h"
#include <windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "CMouse.h"
#include <math.h>


//#define D3DFVF_CUSTOM (D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1)

const int g_nFrameWidth = 32;
const int g_nFrameHeight = 32;


//********************************************************************************
//Costruttore standard
//********************************************************************************
CMouse::CMouse () {
	fX = 0.0f;
	fY = 0.0f;
	m_tex = NULL;
	m_Sprite = NULL;
	m_fHotSpotX = 0.0f;
	m_fHotSpotY = 0.0f;
	m_nXCount = 0;
	m_nYCount = 0;
	m_fFrame = 0.0f;
	m_fFrameCount = 0.0f;
	m_fPause = 0.0f;
	m_fPauseLeft = 0.0f;
	
	ZeroMemory (&m_rFrame, sizeof(RECT));
}



//********************************************************************************
//Distruttore standard
//********************************************************************************
CMouse::~CMouse () {
	m_Dispose ();
}



//********************************************************************************
//Chiama m_Dispose()
//********************************************************************************
void CMouse::Dispose () {
	m_Dispose ();
}



//********************************************************************************
//Libera la memoria occupata e distrugge gli oggetti creati.
//********************************************************************************
inline void CMouse::m_Dispose () {
	SAFE_RELEASE (m_Sprite);
	SAFE_RELEASE (m_tex);
}



//********************************************************************************
//Crea vertex e index buffer per la rappresentazione del cursore. La
//funzione carica anche uno sprite per il cursore, i cui 
//********************************************************************************
int CMouse::BuildCursor (const char* strFileName, LPDIRECT3DDEVICE9 dev, float fX, float fY, int nFrameCountX, int nFrameCountY) {
	if (dev == NULL)
		return NONZERO;
	
	m_Dispose();
	if (FAILED(
		D3DXCreateTextureFromFileEx (dev, strFileName, D3DX_DEFAULT, D3DX_DEFAULT,
			1, 0, D3DFMT_UNKNOWN, D3DPOOL_MANAGED, D3DX_DEFAULT,
			D3DX_DEFAULT, 0, NULL, NULL, &m_tex)
		))
		return NONZERO;
		
	m_fHotSpotX = MIN(MAX(fX, 0.0f), 31.0f);
	m_fHotSpotY = MIN(MAX(fY, 0.0f), 31.0f);
	m_nXCount = nFrameCountX;
	m_nYCount = nFrameCountY;
	m_fFrameCount = (float)(m_nXCount * m_nYCount);
	
	D3DXCreateSprite (dev, &m_Sprite);
	return ZERO;
}



//********************************************************************************
//********************************************************************************
int CMouse::DrawMouse () {
	if (m_Sprite && m_tex == false)
		return NONZERO;
	
	WrapMouse ();
	D3DXVECTOR3 vPos (fX - m_fHotSpotX, fY - m_fHotSpotY, 0.0f);
	m_Sprite->Begin (D3DXSPRITE_ALPHABLEND);
	m_Sprite->Draw (m_tex, &m_rFrame, NULL, &vPos, 0xFFFFFFFF);
	m_Sprite->End();
	return ZERO;
}



//********************************************************************************
//********************************************************************************
int CMouse::AdvanceFrame (float fDelta) {
	if (m_fPauseLeft <= 0.0f)
		m_fFrame += fDelta * 15.0f;
	else
		m_fPauseLeft -= fDelta;
	
	if (m_fFrame >= m_fFrameCount) {
		m_fFrame = fmodf(m_fFrame, m_fFrameCount);
		m_fPauseLeft = m_fPause;
	}
	const int nIndex = (int)m_fFrame;
	
	m_rFrame.left = (nIndex % m_nXCount) * g_nFrameWidth;
	m_rFrame.top = (nIndex / m_nXCount) * g_nFrameHeight;
	m_rFrame.right = m_rFrame.left + g_nFrameWidth;
	m_rFrame.bottom = m_rFrame.top + g_nFrameHeight;
	return ZERO;
}



//********************************************************************************
//Imposta una pausa in secondi da effettuare fra un ciclo di animazione
//e l'altro. Per valori <= 0, la pausa viene rimossa. Restituisce ZERO.
//********************************************************************************
int CMouse::SetPause (float fSeconds) {
	m_fPause = m_fPauseLeft = fSeconds;
	return ZERO;
}