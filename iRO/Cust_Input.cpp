//Input.cpp


#include "Constants.h"
#ifndef DIRECTINPUT_VERSION
#define DIRECTINPUT_VERSION 0x0800
#endif

#include <windows.h>
#include <dinput.h>
#include "Cust_Input.h"


extern LPDIRECTINPUT8 g_di = NULL;
extern LPDIRECTINPUTDEVICE8 g_diKeyboard = NULL;
extern LPDIRECTINPUTDEVICE8 g_diMouse = NULL;

unsigned char* g_KeyState = NULL;
DIMOUSESTATE g_MouseState;
unsigned char g_keyInsertOld = 0;
unsigned char g_keySkipDialogueOld = 0;


//********************************************************************************
//Inizializza DirectInput. Restituisce S_OK se va tutto bene, altrimenti E_FAIL.
//********************************************************************************
HRESULT InitDInput (HWND hWnd, HINSTANCE hInst) {
	if (FAILED(DirectInput8Create (hInst, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&g_di, NULL)))
		return E_FAIL;
	
	//Tastiera
	if (FAILED(g_di->CreateDevice (GUID_SysKeyboard, &g_diKeyboard, NULL)))
		return E_FAIL;
#ifndef FULL_SCREEN
	if (FAILED(g_diKeyboard->SetCooperativeLevel (hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE)))
		return E_FAIL;
#else
	if (FAILED(g_diKeyboard->SetCooperativeLevel (hWnd, DISCL_FOREGROUND | DISCL_EXCLUSIVE)))
		return E_FAIL;
#endif
	
	//Mouse
	if (FAILED(g_di->CreateDevice (GUID_SysMouse, &g_diMouse, NULL)))
		return E_FAIL;
#ifndef FULL_SCREEN
	if (FAILED(g_diMouse->SetCooperativeLevel (hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE)))
		return E_FAIL;
#else
	if (FAILED(g_diMouse->SetCooperativeLevel (hWnd, DISCL_FOREGROUND | DISCL_EXCLUSIVE)))
		return E_FAIL;
#endif
	
	g_KeyState = new unsigned char[256];
	if (g_KeyState == NULL)
		return E_FAIL;
	ZeroMemory (g_KeyState, 256);
	ZeroMemory (&g_MouseState, sizeof(DIMOUSESTATE));
	
	g_diKeyboard->SetDataFormat (&c_dfDIKeyboard);
	g_diMouse->SetDataFormat (&c_dfDIMouse);
	
	g_diKeyboard->Acquire();
	g_diMouse->Acquire();
	return S_OK;
}



//********************************************************************************
//Distrugge gli oggetti DirectInput creati.
//********************************************************************************
void DestroyDInput () {
	if (g_diMouse)
		g_diMouse->Unacquire();
	if (g_diKeyboard)
		g_diKeyboard->Unacquire();
	
	SAFE_RELEASE (g_diMouse);
	SAFE_RELEASE (g_diKeyboard);
	SAFE_RELEASE (g_di);
	
	SAFE_DELETE (g_KeyState);
}



//********************************************************************************
//Esegue il poll di mouse e tastiera, ottiene l'input dai due dispositivi
//e restituisce le informazioni necessarie al videogame nella struttura
//specificata. Se il puntatore alla struttura � NULL, la funzione ritorna
//ZERO dopo aver eseguito il poll. Restituisce ZERO se va tutto bene,
//altrimenti NONZERO.
//********************************************************************************
int MergeInput (LPMERGED_INPUT mi) {
	g_diKeyboard->Poll();
	g_diMouse->Poll();
	
	if (mi == NULL)
		return ZERO;
	
	HRESULT hrRes = g_diKeyboard->GetDeviceState (256, (void*)g_KeyState);
	if ( (hrRes == DIERR_INPUTLOST) || (hrRes == DIERR_NOTACQUIRED) ) {
		if (FAILED(g_diKeyboard->Acquire()))
			return NONZERO;
	}
	
	hrRes = g_diMouse->GetDeviceState (sizeof(DIMOUSESTATE), (void*)&g_MouseState);
	if ( (hrRes == DIERR_INPUTLOST) || (hrRes == DIERR_NOTACQUIRED) ) {
		if (FAILED(g_diMouse->Acquire()))
			return NONZERO;
	}
	
	mi->nMouseX = g_MouseState.lX;
	mi->nMouseY = g_MouseState.lY;
	mi->fMouseScroll = ((float)g_MouseState.lZ) / 1000.0f;
	mi->bMouseLeftBtn = (g_MouseState.rgbButtons[0] & 0x80 ? true : false);
	mi->bMouseRightBtn = (g_MouseState.rgbButtons[1] & 0x80 ? true : false);
	
	mi->bEsc = (g_KeyState[DIK_ESCAPE] & 0x80 ? true : false);
	
	//Variabili da impostare solo alla prima pressione di un tasto
	if (!(g_keyInsertOld & 0x80) && (g_KeyState[DIK_INSERT] & 0x80))
		mi->bInsert = true;
	else
		mi->bInsert = false;
	if (!(g_keySkipDialogueOld & 0x80) && (g_KeyState[DIK_NUMPAD0] & 0x80))
		mi->bSkipDialogue = true;
	else
		mi->bSkipDialogue = false;
	
	g_keySkipDialogueOld = g_KeyState[DIK_NUMPAD0];
	g_keyInsertOld = g_KeyState[DIK_INSERT];
	
	return ZERO;
}
