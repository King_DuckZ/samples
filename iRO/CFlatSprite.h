//CFlatSprite.h


#ifndef _INCL_CFLATSPRITE_H
#define _INCL_CFLATSPRITE_H


#include <d3d9.h>


class CFlatSprite {
public:
	CFlatSprite ( void );
	~CFlatSprite ( void );
	void Dispose ( void );
	
	HRESULT BuildQuad ( float fWidth, float fHeight, LPDIRECT3DDEVICE9 dev );
	int LoadSprite ( const char* strFileName, int nHrzCount, LPDIRECT3DDEVICE9 dev, const D3DCAPS9* caps );
	int SetCameraAngle ( float fa );
	int Render ( LPDIRECT3DDEVICE9 dev );
	void AdvanceAnimation ( float fDelta );
	
	int ResetSubset ( void );
	DWORD AddSubSet ( DWORD dwLenght );
	int SetActiveSubset ( DWORD dwIndex );
	inline DWORD GetActiveSubset ( void );
	
	float fRotY;
	float fRotZ;
	float fPosX;
	float fPosZ;
private:
	inline void m_Dispose ( void );
	inline void m_WrapIndex ( void );
	int m_EnlargeSubset ( void );
	
	LPDIRECT3DVERTEXBUFFER9 m_vb;
	LPDIRECT3DINDEXBUFFER9 m_ib;
	LPDIRECT3DTEXTURE9* m_tex;
	int m_nSpriteCount;
	DWORD m_dwTimeTexture; //Ora di creazione delle texture
	DWORD m_dwTimeBuffer; //Ora di creazione di vertex e index buffer
	float m_fSpriteX; //Numero di sprite per riga
	float m_fFirstSprite; //Indice del primo sprite della riga da disegnare
	float m_fSpriteIndex; //Indice dello sprite corrente
	float m_fCameraAngle;
	float m_fHalfHeight; //Altezza del quad / 2
	DWORD m_dwSubsetCount; //Numero di sottoanimazioni in ogni riga di sprite
	float* m_pfSubset; //Numero di sprite che compone ogni subset
	DWORD m_dwSubsetActive; //Indice del subset in uso
	float m_fSubsetOffset; //Numero di fotogrammi da saltare in ogni riga
};



//********************************************************************************
//Restituisce m_dwSubsetActive.
//********************************************************************************
inline DWORD CFlatSprite::GetActiveSubset () {
	return m_dwSubsetActive;
}

#endif