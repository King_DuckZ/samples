//CMathEngine.h


#ifndef _INCL_CMATHENGINE_H
#define _INCL_CMATHENGINE_H

#include <math.h>

typedef struct STRUCT_MATH_2DVECTOR {
	union {
		struct {
			float x, y;
		};
		float f[2];
	};
} MATH_2DVECTOR, *LPMATH_2DVECTOR;
typedef struct STRUCT_MATH_3DVECTOR {
	union {
		struct {
			float x, y, z;
		};
		float f[3];
	};
} MATH_3DVECTOR, *LPMATH_3DVECTOR;
typedef struct STRUCT_MATH_QUATERNION {
	union {
		struct {
			float w, x, y, z;
		};
		struct {
			float q0;
			MATH_3DVECTOR qv;
		};
		float f[4];
	};
} MATH_QUATERNION, *LPMATH_QUATERNION;
typedef struct STRUCT_MATH_SPHERICAL {
	float rho;
	float phi;
	float theta;
} MATH_SPHERICAL, *LPMATH_SPHERICAL;
typedef struct STRUCT_MATH_MATRIX16 {
	union {
		struct {
			float _00, _01, _02, _03;
			float _10, _11, _12, _13;
			float _20, _21, _22, _23;
			float _30, _31, _32, _33;
		};
		float M[4][4];
	};
} MATH_MATRIX16, *LPMATH_MATRIX16;


//Angoli
#define MATH_PI (3.14159265359f)
#define MATH_2PI (6.28318530718f)
#define MATH_DEGTORAD(deg) ( (deg) * (MATH_PI / 180.0f) )
#define MATH_RADTODEG(rad) ( (rad) * (180.0f / MATH_PI) )

//Inizializzazione struct
#define MATH_INIT2DVECTOR(vector) ( (vector).x = (vector).y = 0.0f )
#define MATH_INIT3DVECTOR(vector) ( (vector).x = (vector).y = (vector).z = 0.0f )
#define MATH_INITQUATERNION(quat) { (quat). x = (quat).y = (quat).z = 0.0f; (quat).w = 1.0f; }
#define MATH_INITSPHERICAL(s) ( (s).rho = (s).phi = (s).theta = 0.0f )
#define MATH_INITMATRIX16(m) { (m)._00 = (m)._01 = (m)._02 = (m)._03 = 0.0f; (m)._10 = (m)._11 = (m)._12 = (m)._13 = 0.0f; (m)._20 = (m)._21 = (m)._22 = (m)._23 = 0.0f; (m)._30 = (m)._31 = (m)._32 = (m)._33 = 0.0f; }

//Valorizzazione variabili
#define MATH_SET2DVECTOR(vector,vx,vy) { (vector).x = (vx); (vector).y = (vy); }
#define MATH_SET3DVECTOR(vector,vx,vy,vz) { (vector).x = (vx); (vector).y = (vy); (vector).z = (vz); }
#define MATH_SETQUATERNION(quat,q0,q1,q2,q3) { (quat).x = (q1); (quat).y = (q2); (quat).z = (q3); (quat).w = (q0); }
#define MATH_SETSPHERICAL(s,r,f,t) { (s).rho = (r); (s).phi = (f); (s).theta = t; }

#define MATH_COPY2DVECTOR(dst,src) { (dst).x = (src).x; (dst).y = (src).y; }
#define MATH_COPY3DVECTOR(dst,src) { (dst).x = (src).x; (dst).y = (src).y; (dst).z = (src).z; }
#define MATH_COPYQUATERNION(dst,src) { (dst).x = (src).x; (dst).y = (src).y; (dst).z = (src).z; (dst).w = (src).w; }
#define MATH_COPYSPHERICAL(dst,src) { (dst).rho = (src).rho; (dst).phi = (src).phi; (dst).theta = (src).theta; }

//Lunghezza di un vettore
#define MATH_LENGHT2DVECTOR(vector) (sqrtf( (vector).x * (vector).x + (vector).y * (vector).y ))
#define MATH_LENGHT3DVECTOR(vector) (sqrtf( (vector).x * (vector).x + (vector).y * (vector).y + (vector).z * (vector).z ))

//Varie
#define MATH_SWITCH(a,b,type) { const type math_switch_tmp = (a); (a) = (b); (b) = math_switch_tmp; }

class CMathEngine {
public:
	CMathEngine ( void );
	~CMathEngine ( void );
	
	static float* CosTable;
	static float* SinTable;
	static unsigned int nTableEntries;
	
	inline void Vec2Normalize ( LPMATH_2DVECTOR v );
	inline void Vec3Normalize ( LPMATH_3DVECTOR v );
	
	inline void Mat16Identity ( LPMATH_MATRIX16 m );
	int Mat16ProjectionLH ( LPMATH_MATRIX16 m, float width, float height, float NearPlane, float FarPlane );
	
	float GetInterpolatedSine ( float fRad );
	float GetInterpolatedCosine ( float fRad );
	int SphericalToCartesian ( const LPMATH_SPHERICAL s, LPMATH_3DVECTOR c );
	inline void SwapDouble ( double* a, double* b );
	inline void SwapDWord ( void* a, void* b );
	
private:
	static unsigned int m_nRefCount;
	
	inline float m_GetInterpolatedFunc ( float fRad, const float* fTable );
};



//********************************************************************************
//Normalizza un vettore 2D.
//********************************************************************************
inline void CMathEngine::Vec2Normalize (LPMATH_2DVECTOR v) {
	const float fLen = MATH_LENGHT2DVECTOR (*v);
	
	v->x /= fLen;
	v->y /= fLen;
}



//********************************************************************************
//Normalizza un vettore 3D.
//********************************************************************************
inline void CMathEngine::Vec3Normalize (LPMATH_3DVECTOR v) {
	const float fLen = MATH_LENGHT3DVECTOR (*v);
	
	v->x /= fLen;
	v->y /= fLen;
	v->z /= fLen;
}



//********************************************************************************
//Imposta la matrice specificata in modo che diventi una identity matrix.
//********************************************************************************
inline void CMathEngine::Mat16Identity (LPMATH_MATRIX16 m) {
	m->_01 = m->_02 = m->_03 = m->_10 = 0.0f;
	m->_12 = m->_13 = m->_20 = m->_21 = 0.0f;
	m->_23 = m->_30 = m->_31 = m->_32 = 0.0f;

	m->_00 = m->_11 = m->_22 = m->_33 = 1.0f;
}



//********************************************************************************
//Scambia i valori di a e b, facendo uso dell'FPU.
//Non effettua controlli sui puntatori.
//********************************************************************************
inline void CMathEngine::SwapDouble (double* a, double* b) {
	asm {
		MOV EAX, a
		MOV EBX, b
		FLD dword ptr [EAX]
		FLD dword ptr [EBX]
		FSTP dword ptr [EAX]
		FSTP dword ptr [EBX]
	}
}



//********************************************************************************
//Scambia i valori di a e b. Non effettua controlli sui puntatori, e presuppone
//che entrambe le zone di memoria siano lunghe almeno 4 byte.
//********************************************************************************
inline void CMathEngine::SwapDWord (void* a, void* b) {
	asm {
		MOV EAX, a
		MOV EBX, b
		PUSH dword ptr [EBX]
		PUSH dword ptr [EAX]
		POP dword ptr [EBX]
		POP dword ptr [EAX]
	}
}


#endif