//CDialogue.cpp


#include "constants.h"
#include <windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "CDialogue.h"


#define D3DFVF_CUSTOM (D3DFVF_XYZRHW | D3DFVF_TEX1 | D3DFVF_DIFFUSE)

//********************************************************************************
//Costruttore standard
//********************************************************************************
CDialogue::CDialogue () {
	m_vb = NULL;
	m_ib = NULL;
	m_tex = NULL;
	m_Vertices = NULL;
	m_fLeft = 0.0f;
	m_fTop = 0.0f;
	m_fRight = 0.0f;
	m_fBottom = 0.0f;
	m_fWidthFactor = 0.0f;
	m_fHeightFactor = 0.0f;
	m_bEnabled = FALSE;
}



//********************************************************************************
//Distruttore standard
//********************************************************************************
CDialogue::~CDialogue () {
	m_Dispose ();
}



//********************************************************************************
//Chiama m_Dispose()
//********************************************************************************
void CDialogue::Dispose () {
	m_Dispose ();
}



//********************************************************************************
//Libera la memoria occupata e distrugge gli oggetti creati.
//********************************************************************************
inline void CDialogue::m_Dispose () {
	SAFE_RELEASE (m_tex);
	SAFE_RELEASE (m_ib);
	SAFE_RELEASE (m_vb);
	SAFE_DELETE (m_Vertices);
}



//********************************************************************************
//Alloca la memoria per i vertici e per i buffer, oltre a caricare la texture.
//Se � gi� stata fatta un'inizializzazione, � necessario chiamare Dispose()
//prima di richiamare questa funzione. Stessa procedura anche se la precedente
//chiamata a PreBuild() � fallita. Restituisce ZERO se va tutto bene,
//altrimenti NONZERO.
//********************************************************************************
int CDialogue::PreBuild (LPDIRECT3DDEVICE9 dev, const char* strFileName) {
	if (((dev && strFileName) == false) || ((m_tex || m_ib || m_vb) == true))
		return NONZERO;
	
	m_Vertices = new DIALOGUE_VERTEX[4];
	if (m_Vertices == NULL)
		return NONZERO;
	
	WORD pwIndices[6];
	void* dst = NULL;
	
	m_Vertices[0].x = 0.0f;
	m_Vertices[0].y = 0.0f;
	m_Vertices[0].z = 0.0f;
	m_Vertices[0].w = 1.0f;
	m_Vertices[0].color = 0xFFFFFFFF;
	m_Vertices[0].tu = 0.0f;
	m_Vertices[0].tv = 0.0f;
	//--
	m_Vertices[1].x = 10.0f;
	m_Vertices[1].y = 0.0f;
	m_Vertices[1].z = 0.0f;
	m_Vertices[1].w = 1.0f;
	m_Vertices[1].color = 0xFFFFFFFF;
	m_Vertices[1].tu = 1.0f;
	m_Vertices[1].tv = 0.0f;
	//--
	m_Vertices[2].x = 0.0f;
	m_Vertices[2].y = 10.0f;
	m_Vertices[2].z = 0.0f;
	m_Vertices[2].w = 1.0f;
	m_Vertices[2].color = 0xFFFFFFFF;
	m_Vertices[2].tu = 0.0f;
	m_Vertices[2].tv = 1.0f;
	//--
	m_Vertices[3].x = 10.0f;
	m_Vertices[3].y = 10.0f;
	m_Vertices[3].z = 0.0f;
	m_Vertices[3].w = 1.0f;
	m_Vertices[3].color = 0xFFFFFFFF;
	m_Vertices[3].tu = 1.0f;
	m_Vertices[3].tv = 1.0f;
	pwIndices[0] = 0;
	pwIndices[1] = 1;
	pwIndices[2] = 2;
	pwIndices[3] = 2;
	pwIndices[4] = 1;
	pwIndices[5] = 3;

	if (FAILED(dev->CreateVertexBuffer (sizeof(DIALOGUE_VERTEX) * 4, D3DUSAGE_WRITEONLY, D3DFVF_CUSTOM, D3DPOOL_MANAGED, &m_vb, NULL)))
		return NONZERO;
	if (FAILED(dev->CreateIndexBuffer (sizeof(WORD) * 6, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_ib, NULL)))
		return NONZERO;
	if (FAILED(m_ib->Lock(0, sizeof(WORD) * 6, &dst, 0)))
		return NONZERO;
	CopyMemory (dst, pwIndices, sizeof(WORD) * 6);
	m_ib->Unlock ();
	
	if (FAILED(D3DXCreateTextureFromFileEx (dev, strFileName, D3DX_DEFAULT, D3DX_DEFAULT,
		1, 0, D3DFMT_UNKNOWN, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT,
		0, NULL, NULL, &m_tex)))
		
		return NONZERO;

	return ZERO;
}



//********************************************************************************
//Esegue il lock del vertex buffer e vi copia m_Vertices. La funzione
//non esegue alcun controllo sui puntatori, � pertanto necessario assicurarsi
//che m_vb e m_Vertices siano non NULL prima di chiamare questa funzione.
//Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
inline int CDialogue::m_UpdateVertexBuffer () {
	void* dst = NULL;
	const DWORD dwVertSize = sizeof(DIALOGUE_VERTEX) << 2;
	
	if (FAILED(m_vb->Lock(0, dwVertSize, &dst, 0)))
		return NONZERO;
	CopyMemory (dst, m_Vertices, dwVertSize);
	m_vb->Unlock ();
	
	return ZERO;
}



//********************************************************************************
//Imposta la dimensione della finestra su cui verr� disegnato il testo. Se
//r = NULL, l'oggetto viene disattivato.
//Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CDialogue::SetDialogueRect (const LPRECT r) {
	if (m_Vertices == NULL || m_vb == NULL)
		return NONZERO;
	
	if (r == NULL) {
		m_bEnabled = FALSE;
		return ZERO;
	}
	
	const RECT rScreen = {0, 0, AREA_WIDTH, AREA_HEIGHT};
	const float fWidth = (float)(r->right - r->left); //Larghezza del RECT non clippato
	const float fHeight = (float)(r->bottom - r->top); //Altezza del RECT non clippato
	RECT rInt;
	
	//Che razza di poligono creo se � tutto da clippare?
	if (!IntersectRect (&rInt, r, &rScreen))
		return NONZERO;
	
	m_fLeft = (float)MIN(r->left, r->right) * m_fWidthFactor;
	m_fTop = (float)MIN(r->top, r->bottom) * m_fHeightFactor;
	m_fRight = (float)MAX(r->left, r->right) * m_fWidthFactor;
	m_fBottom = (float)MAX(r->top, r->bottom) * m_fHeightFactor;
	
	m_Vertices[0].x = MAX(m_fLeft, 0.0f);
	m_Vertices[0].y = MAX(m_fTop, 0.0f);
	m_Vertices[1].x = MIN(m_fRight, AREA_WIDTH_FLOAT);
	m_Vertices[1].y = m_Vertices[0].y;
	m_Vertices[2].x = m_Vertices[0].x;
	m_Vertices[2].y = MIN(m_fBottom, AREA_HEIGHT_FLOAT);
	m_Vertices[3].x = m_Vertices[1].x;
	m_Vertices[3].y = m_Vertices[2].y;
	
	m_Vertices[0].tu = (float)(r->left - rInt.left) / -fWidth;
	m_Vertices[0].tv = (float)(r->top - rInt.top) / -fHeight;
	m_Vertices[1].tu = 1.0f - (float)(r->right - rInt.right) / fWidth;
	m_Vertices[1].tv = m_Vertices[0].tv;
	m_Vertices[2].tu = m_Vertices[0].tu;
	m_Vertices[2].tv = 1.0f - (float)(r->bottom - rInt.bottom) / fHeight;
	m_Vertices[3].tu = m_Vertices[1].tu;
	m_Vertices[3].tv = m_Vertices[2].tv;
	
	m_bEnabled = TRUE;
	return m_UpdateVertexBuffer ();
}



//********************************************************************************
//
//********************************************************************************
int CDialogue::Render (LPDIRECT3DDEVICE9 dev) {
	if ((dev && m_vb && m_ib && m_tex) == false)
		return NONZERO;
	else if (!m_bEnabled)
		return ZERO;
	
	dev->SetTexture (0, m_tex);
	dev->SetStreamSource (0, m_vb, 0, sizeof(DIALOGUE_VERTEX));
	dev->SetIndices (m_ib);
	dev->SetFVF (D3DFVF_CUSTOM);
	dev->DrawIndexedPrimitive (D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
	return ZERO;
}



//********************************************************************************
//Imposta i fattori per i quali le coordinate in pixel andranno moltiplicate.
//Reference rappresenta i valori per i quali i valori in pixel sono espressi,
//Area rappresenta le dimensioni dell'area di rendering di destinazione (per
//esempio, la dimensione dello schermo). Restituisce ZERO se i valori sono
//stati accettati, altrimenti NONZERO.
//********************************************************************************
int CDialogue::SetFactor (float fReferenceWidth, float fAreaWidth, float fReferenceHeight, float fAreaHeight) {
	if (fReferenceWidth == 0.0f || fReferenceHeight == 0.0f)
		return NONZERO;
	
	m_fWidthFactor = fAreaWidth / fReferenceWidth;
	m_fHeightFactor = fAreaHeight / fReferenceHeight;
	return ZERO;
}