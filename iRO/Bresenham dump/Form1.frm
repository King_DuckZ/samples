VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Type POINTFLOAT
    x As Single
    y As Single
End Type

Private Declare Function Rectangle Lib "gdi32" (ByVal hdc As Long, ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long

Private g_pffSteps()   As POINTFLOAT
Private g_strAppPath As String
Private g_nCount As Long

Private Sub Form_Load()
    Dim FileNum As Integer
    Dim ffDummy As POINTFLOAT
    
    g_strAppPath = App.Path & IIf(Right(App.Path, 1) = "\", "", "\")
    FileNum = FreeFile()
    
    Open g_strAppPath & "bresenham dump.bin" For Binary Access Read Shared As #FileNum
    g_nCount = LOF(FileNum) / Len(ffDummy)
    ReDim g_pffSteps(LOF(FileNum) / Len(ffDummy) - 1) As POINTFLOAT
    Get #FileNum, , g_pffSteps
    Close #FileNum
End Sub

Private Sub Form_Paint()
    Dim z As Long
    
    Me.CurrentX = 0
    Me.CurrentY = 0
    For z = 0 To g_nCount - 1
        Call Rectangle(Me.hdc, CLng(g_pffSteps(z).x) * 16 + 50, _
                        CLng(g_pffSteps(z).y) * 16 + 10, _
                        CLng(g_pffSteps(z).x) * 16 + 16 + 50, _
                        CLng(g_pffSteps(z).y) * 16 + 16 + 10)
        Print "(" & CStr(g_pffSteps(z).x) & " ; " & CStr(g_pffSteps(z).y) & ")"
    Next z
End Sub
