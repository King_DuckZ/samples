//CSpeech.cpp


#include "Constants.h"
#include <windows.h>
#include <stdio.h>
#include <string.h>
#include "CSpeech.h"
#include <d3dx9.h>


#define MIN_WIDTH 50
#define MIN_HEIGHT 25
#define D3DFVF_SPEECH (D3DFVF_XYZRHW | D3DFVF_TEX1 | D3DFVF_DIFFUSE)
const unsigned char SPACE_ID = 0xFF;
const unsigned char RETURN_ID = 0xFE;

typedef struct STRUCT_SPEECHVERTEX {
	float x, y, z;
	float rhw;
	D3DCOLOR diffuse;
	float tu, tv;
} SPEECHVERTEX, *LPSPEECHVERTEX;



//********************************************************************************
//Costruttore standard
//********************************************************************************
CSpeech::CSpeech () {
	ZeroMemory (&m_chInfo, sizeof(CHARSETHEADER));
	Status = SPEECHS_IDLE;
	m_pfTops = NULL;
	m_prList = NULL;
	m_texCharset = NULL;
	m_vb = NULL;
	m_ib = NULL;
	m_fText = 0.0f;
	m_fStripedText = 0.0f;
	m_fIndex = 0.0f;
	m_fVB = 0.0f;
	m_fScreenWidth = m_fReferenceWidth = AREA_WIDTH_FLOAT;
	m_fScreenHeight = m_fReferenceHeight = AREA_HEIGHT_FLOAT;
	m_fLeft = 0.0f;
	m_fTop = 0.0f;
	m_bEnabled = FALSE;
}



//********************************************************************************
//Distruttore standard
//********************************************************************************
CSpeech::~CSpeech () {
	m_Dispose ();
}



//********************************************************************************
//Chiama m_Dispose()
//********************************************************************************
void CSpeech::Dispose () {
	m_Dispose ();
}



//********************************************************************************
//Libera la memoria occupata e distrugge gli oggetti creati.
//********************************************************************************
inline void CSpeech::m_Dispose () {
	SAFE_RELEASE (m_ib);
	SAFE_RELEASE (m_vb);
	SAFE_RELEASE (m_texCharset);
	
	SAFE_DELETE (m_pfTops);
	SAFE_DELETE (m_prList);
	m_fIndex = m_fText = m_fStripedText = 0.0f;
	m_fVB = 0.0f;
}



//********************************************************************************
//Imposta l'area di destinazione per il rendering del testo. Restituisce
//ZERO.
//********************************************************************************
int CSpeech::SetDialoguePoint (float x, float y) {
	m_fLeft = MAX(x, 0.0f);
	m_fTop = MAX(y, 0.0f);
	return ZERO;
}



//********************************************************************************
//********************************************************************************
int CSpeech::LoadCharset (LPDIRECT3DDEVICE9 dev, const char* strCharset, const char* strSurface) {
	if (dev == NULL || strCharset == NULL || strSurface == NULL)
		return NONZERO;
	
	SAFE_RELEASE (m_texCharset);
	if (FAILED(D3DXCreateTextureFromFileEx (dev, strSurface, D3DX_DEFAULT, D3DX_DEFAULT,
		1, 0, D3DFMT_UNKNOWN, D3DPOOL_MANAGED, D3DX_FILTER_NONE, D3DX_FILTER_NONE, 0, NULL, NULL, &m_texCharset)))
		return NONZERO;
	
	FILE* f;
	LPDWORD pdwTops;
	
	if ( (f=fopen(strCharset, "rb")) == NULL )
		return NONZERO;
	
	fseek (f, 0, SEEK_END);
	const int nFileSize = ftell(f);
	fseek (f, 0, SEEK_SET);
	
	if (nFileSize < sizeof(CHARSETHEADER)) {
		fclose (f);
		return NONZERO;
	}
	
	//Controlli assortiti
	fread (&m_chInfo, sizeof(CHARSETHEADER), 1, f);
	if (nFileSize != m_chInfo.chFileSize) {
		fclose (f);
		return NONZERO;
	}
	if (m_chInfo.chSignature != 'GD') {
		fclose (f);
		return NONZERO;
	}
	if (m_chInfo.chRectCount <= 0) {
		fclose (f);
		return NONZERO;
	}
	if (m_chInfo.chSize != sizeof(CHARSETHEADER)) {
		fclose (f);
		return NONZERO;
	}
	if (!m_chInfo.chDefinesCharset || !m_chInfo.chDefinesTop) {
		fclose (f);
		return NONZERO;
	}
	
	SAFE_DELETE (m_pfTops);
	SAFE_DELETE (m_prList);
	
	pdwTops = new DWORD[m_chInfo.chRectCount];
	m_pfTops = new float[m_chInfo.chRectCount];
	m_prList = new RECT[m_chInfo.chRectCount];
	
	//fread (m_strCharset, 1, m_chInfo.chRectCount, f);
//	fseek (f, m_chInfo.chRectCount, SEEK_CUR);
	fseek (f, m_chInfo.chRectDataStart, SEEK_SET);
	fread (m_prList, sizeof(RECT), m_chInfo.chRectCount, f);
	fread (pdwTops, sizeof(DWORD), m_chInfo.chRectCount, f);

	//Conversione dei tops da DWORD a float
	for (int z = 0; z < m_chInfo.chRectCount; z++) {
		m_pfTops[z] = (float)(pdwTops[z]); // / m_fReferenceHeight;
	}
	
	//Pulisco e ciao
	fclose (f);
	SAFE_DELETE (pdwTops);
	return ZERO;
}



//********************************************************************************
//********************************************************************************
int CSpeech::Render (LPDIRECT3DDEVICE9 dev) {
	if ((dev && m_texCharset && m_ib && m_vb) == false)
		return NONZERO;
	if (Status == SPEECHS_IDLE || !m_bEnabled)
		return ZERO;
	
	const UINT uIndex = (UINT)m_fIndex;
	
	dev->SetRenderState (D3DRS_ALPHATESTENABLE, TRUE);
	dev->SetRenderState (D3DRS_ALPHAREF, 1);
	dev->SetRenderState (D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);
	dev->SetSamplerState (0, D3DSAMP_MAGFILTER, D3DTEXF_NONE);
	dev->SetSamplerState (0, D3DSAMP_MINFILTER, D3DTEXF_NONE);
	
	dev->SetTexture (0, m_texCharset);
	dev->SetStreamSource (0, m_vb, 0, sizeof(SPEECHVERTEX));
	dev->SetIndices (m_ib);
	dev->SetFVF (D3DFVF_SPEECH);
	dev->DrawIndexedPrimitive (D3DPT_TRIANGLELIST, 0, 0, uIndex << 2, 0, uIndex << 1);
	return ZERO;
}



//********************************************************************************
//Imposta la dimensione dello schermo su una dimensione diversa da quella di
//default. Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CSpeech::UpdateScreenSize (int cx, int cy) {
	if (cx > 0 && cy > 0) {
		m_fScreenWidth = (float)cx;
		m_fScreenHeight = (float)cy;
		
		return ZERO;
	}
	else
		return NONZERO;
}



//********************************************************************************
//Copia il vettore ricevuto nel buffer interno. Se Text == NULL o nLenght <= 0,
//il buffer interno viene eliminato. Restituisce ZERO se va tutto bene,
//altrimenti NONZERO.
//********************************************************************************
int CSpeech::SetTranslatedString (LPDIRECT3DDEVICE9 dev, const unsigned char* Text, int nLenght) {
	if (Text == NULL || nLenght <= 0) {
		Status = SPEECHS_IDLE;
		m_bEnabled = FALSE;
		return ZERO;
	}
	
	LPSPEECHVERTEX vertices = NULL;
	D3DSURFACE_DESC desc;
	
	ZeroMemory (&desc, sizeof(D3DSURFACE_DESC));
	m_fText = (float)nLenght;
	m_fStripedText = 0.0f;
	m_fIndex = 0.0f;
	
	for (int z = 0; z < nLenght; z++) {
		if (Text[z] != SPACE_ID && Text[z] != RETURN_ID)
			m_fStripedText += 1.0f;
	}
	if (m_PreBuildDialogue (dev, m_fStripedText) == NONZERO)
		return NONZERO;
	
	//Creazione dei quad
	m_texCharset->GetLevelDesc (0, &desc);
	const float fTexFactorX = 1.0f / (float)desc.Width;
	const float fTexFactorY = 1.0f / (float)desc.Height;
	const float fSpace = (float)m_chInfo.chSpacePixel / m_fReferenceWidth * m_fScreenWidth;
	const float fAvgHeight = (float)m_chInfo.chAverageHeight * 1.4f / m_fReferenceHeight * m_fScreenHeight;
	const float fDistance = (float)m_chInfo.chDistance / m_fReferenceWidth * m_fScreenWidth;
	const float fWidthFactor = m_fScreenWidth / m_fReferenceWidth;
	const float fHeightFactor = m_fScreenHeight / m_fReferenceHeight;
	float fLeft = m_fLeft * fWidthFactor;
	float fTop = m_fTop * fHeightFactor;
	int nIndex;
	float fTopOffset;
	float fWidth, fHeight;
	
	if (SUCCEEDED(m_vb->Lock(0, nLenght * sizeof(SPEECHVERTEX) * 4, (void**)&vertices, 0))) {
		for (int z = 0; z < nLenght; z++) {
			nIndex = (int)Text[z];
			if (nIndex == SPACE_ID) {
				fLeft += fSpace;
			}
			else if (nIndex == RETURN_ID) {
				fLeft = m_fLeft * fWidthFactor;
				fTop += fAvgHeight;
			}
			else {
				fTopOffset = m_pfTops[nIndex] * fHeightFactor;
				fWidth = (float)(m_prList[nIndex].right - m_prList[nIndex].left) * fWidthFactor;
				fHeight = (float)(m_prList[nIndex].bottom - m_prList[nIndex].top) * fHeightFactor;
				//179
				vertices->x = fLeft;
				vertices->y = fTop + fTopOffset;
				vertices->z = 0.0f;
				vertices->rhw = 1.0f;
				vertices->diffuse = 0xFFFFFFFF;
				vertices->tu = (float)(m_prList[nIndex].left) * fTexFactorX;
				vertices->tv = (float)(m_prList[nIndex].top) * fTexFactorY;
				vertices++;
				
				//793
				vertices->x = fLeft + fWidth;
				vertices->y = fTop + fTopOffset;
				vertices->z = 0.0f;
				vertices->rhw = 1.0f;
				vertices->diffuse = 0xFFFFFFFF;
				vertices->tu = (float)(m_prList[nIndex].right) * fTexFactorX;
				vertices->tv = (float)(m_prList[nIndex].top) * fTexFactorY;
				vertices++;

				//931
				vertices->x = fLeft + fWidth;
				vertices->y = fTop + fTopOffset + fHeight;
				vertices->z = 0.0f;
				vertices->rhw = 1.0f;
				vertices->diffuse = 0xFFFFFFFF;
				vertices->tu = (float)(m_prList[nIndex].right) * fTexFactorX;
				vertices->tv = (float)(m_prList[nIndex].bottom) * fTexFactorY;
				vertices++;

				//317
				vertices->x = fLeft;
				vertices->y = fTop + fTopOffset + fHeight;
				vertices->z = 0.0f;
				vertices->rhw = 1.0f;
				vertices->diffuse = 0xFFFFFFFF;
				vertices->tu = (float)(m_prList[nIndex].left) * fTexFactorX;
				vertices->tv = (float)(m_prList[nIndex].bottom) * fTexFactorY;
				vertices++;
				
				fLeft += fDistance + fWidth;
			}
		}
		
		m_vb->Unlock();
	}
	
	Status = SPEECHS_IDLE;
	m_bEnabled = TRUE;
	return ZERO;
}



//********************************************************************************
//Incrementa l'indice della stringa da visualizzare, facendo in modo che
//questa venga visualizzata un po' per volta. Restituisce ZERO.
//********************************************************************************
int CSpeech::AdvanceAnimation (float fDelta) {
	if (!m_bEnabled)
		return ZERO;

	m_fIndex += fDelta * 30.0f;
	if (m_fIndex > m_fStripedText) {
		Status = SPEECHS_COMPLETED;
		m_fIndex = m_fStripedText;
	}
	else
		Status = SPEECHS_WRITING;
	return ZERO;
}



//********************************************************************************
//Ri-alloca vb e ib se non sono abbastanza grandi da contenere il numero di
//caratteri specificato (tale numero non include spazi n� ritorni di riga).
//Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CSpeech::m_PreBuildDialogue (LPDIRECT3DDEVICE9 dev, float fCount) {
	if (dev == NULL || fCount <= 0.0f)
		return NONZERO;
	if (fCount <= m_fVB)
		return ZERO;
	
	const int nCount = (int)fCount;
	const UINT uIndexSize = sizeof(WORD) * 6 * nCount;
	const UINT uVertexCount = nCount << 2;
	LPWORD pwIndices = NULL;

	SAFE_RELEASE (m_ib);
	SAFE_RELEASE (m_vb);
	m_fVB = 0.0f;
	if (FAILED(dev->CreateVertexBuffer (sizeof(SPEECHVERTEX) * uVertexCount, D3DUSAGE_WRITEONLY, D3DFVF_SPEECH, D3DPOOL_MANAGED, &m_vb, NULL)))
		return NONZERO;
	if (FAILED(dev->CreateIndexBuffer (uIndexSize, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_ib, NULL)))
		return NONZERO;
	
	//Aggiorno l'index buffer
	if (SUCCEEDED(m_ib->Lock(0, uIndexSize, (void**)&pwIndices, 0))) {
		
		for (UINT z = 0, i = 0; z < uVertexCount; z += 4, i += 6) {
			pwIndices[0 + i] = 0 + z;
			pwIndices[1 + i] = 1 + z;
			pwIndices[2 + i] = 3 + z;
			pwIndices[3 + i] = 3 + z;
			pwIndices[4 + i] = 1 + z;
			pwIndices[5 + i] = 2 + z;
		}
		
		m_ib->Unlock();
	}
	else
		return NONZERO;
	
	m_fVB = fCount;
	return ZERO;
}



//********************************************************************************
//Imposta le dimensioni di riferimento per le quali il charset in uso �
//ottimizzato. Di default, tali riferimenti sono uguali alla dimensione
//corrente dello schermo. Tali valori vengono usati in LoadCharset(), vanno
//pertanto impostati prima di usare quella funzione. Restituisce ZERO se i
//valori sono stati accettati, altrimenti NONZERO.
//********************************************************************************
int CSpeech::SetReferenceArea (float cx, float cy) {
	if (cx <= 0.0f || cy <= 0.0f)
		return NONZERO;
	else {
		m_fReferenceWidth = cx;
		m_fReferenceHeight = cy;
		return ZERO;
	}
}