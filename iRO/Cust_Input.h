//Input.h


#ifndef _INCL_CUST_INPUT_H
#define _INCL_CUST_INPUT_H


typedef struct STRUCT_MERGED_INPUT {
	int nMouseX;
	int nMouseY;
	float fMouseScroll;
	bool bMouseLeftBtn;
	bool bMouseRightBtn;
	bool bEsc;
	bool bInsert;
	bool bSkipDialogue;
	char padd[7];
} MERGED_INPUT, *LPMERGED_INPUT;


HRESULT InitDInput ( HWND hWnd, HINSTANCE hInst );
void DestroyDInput ( void );
int MergeInput ( LPMERGED_INPUT mi );


#endif