//Constants.h


#ifndef _INCL_CONSTANTS_H
#define _INCL_CONSTANTS_H

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef MIN
#define MIN(a,b) ( (a) < (b) ? (a) : (b) )
#endif
#ifndef MAX
#define MAX(a,b) ( (a) > (b) ? (a) : (b) )
#endif

#define DEF_STR_LEN 128
#define AREA_WIDTH 800
#define AREA_HEIGHT 600
//#ifndef FULL_SCREEN
//#define FULL_SCREEN
//#endif
#define AREA_WIDTH_FLOAT 800.0f
#define AREA_HEIGHT_FLOAT 600.0f

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(obj) { if ((obj) != NULL) { (obj)->Release(); (obj) = NULL; } }
#endif
#ifndef SAFE_DELETE
#define SAFE_DELETE(arr) { if ((arr) != NULL) { delete[] (arr); (arr) = NULL; } }
#endif

#ifndef ZERO
#define ZERO 0
#endif
#ifndef NONZERO
#define NONZERO (-1)
#endif

#endif