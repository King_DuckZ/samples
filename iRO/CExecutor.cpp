//CExecutor.cpp


#include "constants.h"
#include <windows.h>
#include "CExecutor.h"
#include <stdio.h>

#define DEF_TIMEOUT 12.5f
#define MAX_SPEECH_COUNT 32
#define DEF_WRAPDIALOGUE false
#define DEF_BORDERWIDTH 6


//********************************************************************************
//Costruttore standard
//********************************************************************************
CExecutor::CExecutor () {
	m_Reset ();
	m_nIndex = 0;
	m_pucText = NULL;
	m_pnLenght = NULL;
	m_prArea = NULL;
	m_nCount = 0;
	m_Speech = NULL;
	m_pffCorner = NULL;
}



//********************************************************************************
//Distruttore standard
//********************************************************************************
CExecutor::~CExecutor () {
	m_Dispose ();
}



//********************************************************************************
//Chiama m_Reset().
//********************************************************************************
void CExecutor::Reset () {
	m_Reset ();
}



//********************************************************************************
//Imposta tutte le propriet� sui valori iniziali.
//********************************************************************************
inline void CExecutor::m_Reset () {
	m_fElapsed = DEF_TIMEOUT;
	m_fTimeout = DEF_TIMEOUT;
	m_bWrapDialogue = DEF_WRAPDIALOGUE;
	m_nBorderWidth = DEF_BORDERWIDTH;
	m_bAutoSkipDialogue = true;
}



//********************************************************************************
//Chiama m_Dispose()
//********************************************************************************
void CExecutor::Dispose () {
	m_Dispose ();
	m_Reset ();
	m_nCount = 0;
	m_nIndex = 0;
	m_Speech = NULL;
}



//********************************************************************************
//Libera la memoria occupata e distrugge gli oggetti creati.
//********************************************************************************
inline void CExecutor::m_Dispose () {
	SAFE_DELETE (m_pucText);
	SAFE_DELETE (m_pnLenght);
	SAFE_DELETE (m_prArea);
	SAFE_DELETE (m_pffCorner);
}



//********************************************************************************
//Fa avanzare il contatore interno della classe, che determina il comportamento
//che questa avr�. Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CExecutor::Behave (LPDIRECT3DDEVICE9 dev, float fDelta, bool bSkipDlg) {
	if (m_Speech == NULL)
		return NONZERO;
	
	m_Speech->AdvanceAnimation (fDelta);
	if (m_bAutoSkipDialogue) {
		m_fElapsed += fDelta;
		if (m_fElapsed < m_fTimeout)
			return ZERO;
		else
			m_fElapsed = 0.0f;
	}
	else {
		if (bSkipDlg) {
			if (m_Speech->Status == SPEECHS_WRITING) {
				m_Speech->CompleteAnimation();
				return ZERO;
			}
		}
		else
			return ZERO;
	}
	
	if (m_nIndex >= m_nCount) {
		if (m_bWrapDialogue) {
			m_nIndex = 0;
		}
		else {
			m_Speech->SetTranslatedString (NULL, NULL, 0);
			if (m_Dialogue)
				m_Dialogue->SetDialogueRect (NULL);
			return ZERO;
		}
	}
	
	int nOffset = 0;
	for (int z = 0; z < m_nIndex; z++) {
		nOffset += m_pnLenght[z];
	}
	m_Speech->SetDialoguePoint (m_pffCorner[m_nIndex].x, m_pffCorner[m_nIndex].y);
	m_Speech->SetTranslatedString (dev, m_pucText + nOffset, m_pnLenght[m_nIndex]);
	
	if (m_Dialogue) {
		RECT rTmp = { m_prArea[m_nIndex].left - m_nBorderWidth,
							m_prArea[m_nIndex].top - m_nBorderWidth,
							m_prArea[m_nIndex].right + m_nBorderWidth,
							m_prArea[m_nIndex].bottom + m_nBorderWidth
						  };
		if ( (rTmp.right - rTmp.left <= m_nBorderWidth << 1) ||
			(rTmp.bottom - rTmp.top <= m_nBorderWidth << 1) ) {
			
			m_Dialogue->SetDialogueRect (NULL);
			m_bAutoSkipDialogue = true;
		}
		else {
			m_Dialogue->SetDialogueRect (&rTmp);
			m_bAutoSkipDialogue = false;
		}
	}
	
	m_nIndex++;
	return ZERO;
}



//********************************************************************************
//Carica il dump dei dialoghi dal file specificato. Tali dialoghi saranno
//poi impostati dalla funzione Behave() nell'oggetto CSpeech impostato
//tramite SetCSpeech(). Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CExecutor::LoadSpeechDump (const char* strFileName) {
	FILE* f;
	int nTmp;
	int nSum = 0;
	
	if ( (f=fopen(strFileName, "rb")) == NULL )
		return NONZERO;
	
	fread (&nTmp, sizeof(int), 1, f);
	if (nTmp > MAX_SPEECH_COUNT) {
		fclose (f);
		return NONZERO;
	}
	m_nCount = nTmp;
	
	fseek (f, 0, SEEK_END);
	const int nFileSize = ftell(f);
	fseek (f, sizeof(int), SEEK_SET);
	
	if ( nFileSize <= (sizeof(int) + m_nCount * (sizeof(int) + sizeof(RECT))) ) {
		fclose (f);
		return NONZERO;
	}
	
	SAFE_DELETE (m_pucText);
	SAFE_DELETE (m_pnLenght);
	SAFE_DELETE (m_prArea);
	SAFE_DELETE (m_pffCorner);
	
	m_pnLenght = new int[m_nCount];
	m_prArea = new RECT[m_nCount];
	m_pffCorner = new POINTFLOAT[m_nCount];
	fread (m_pnLenght, sizeof(int), m_nCount, f);
	fread (m_pffCorner, sizeof(POINTFLOAT), m_nCount, f);
	fread (m_prArea, sizeof(RECT), m_nCount, f);
	
	for (int z = 0; z < m_nCount; z++) {
		nSum += m_pnLenght[z];
	}
	
	m_pucText = new unsigned char[nSum];
	fread (m_pucText, sizeof(unsigned char), nSum, f);
	
	fclose (f);
	return ZERO;
}



//********************************************************************************
//Imposta il puntatore interno all'istanza di CSpeech che si desidera venga
//impostata con i dialoghi da mostrare. Restituisce ZERO.
//********************************************************************************
int CExecutor::SetCSpeech (CSpeech* ptr) {
	m_Speech = ptr;
	return ZERO;
}



//********************************************************************************
//Imposta il puntatore interno all'istanza di CDialogue che si desidera venga
//impostata con le cornici da mostrare. Restituisce ZERO.
//********************************************************************************
int CExecutor::SetCDialogue (CDialogue* ptr) {
	m_Dialogue = ptr;
	return ZERO;
}