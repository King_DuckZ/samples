//CMathEngine.cpp


#define TABLE_ENTRIES 361

#include "Constants.h"
#include <windows.h>
#include <math.h>
#include "CMathEngine.h"

#ifndef ZERO
#define ZERO 0
#endif
#ifndef NONZERO
#define NONZERO (-1)
#endif

float* CMathEngine::CosTable = NULL;
float* CMathEngine::SinTable = NULL;
unsigned int CMathEngine::nTableEntries = 0;
unsigned int CMathEngine::m_nRefCount = 0;


//********************************************************************************
//Costruttore standard
//********************************************************************************
CMathEngine::CMathEngine () {
	m_nRefCount++;
	if (nTableEntries == 0) {
		CosTable = new float[TABLE_ENTRIES];
		SinTable = new float[TABLE_ENTRIES];
		
		if (CosTable == NULL || SinTable == NULL) {
			SAFE_DELETE (CosTable);
			SAFE_DELETE (SinTable);
		}
		else {
			nTableEntries = TABLE_ENTRIES;
			float a = 0.0f;
			for (int z = 0; z < TABLE_ENTRIES; z++, a += 1.0f) {
				CosTable[z] = cosf(MATH_DEGTORAD(a));
				SinTable[z] = sinf(MATH_DEGTORAD(a));
			} //Next z
			
		} //End If
	} //End If
}



//********************************************************************************
//Distruttore standard
//********************************************************************************
CMathEngine::~CMathEngine () {
	m_nRefCount--;
	if (m_nRefCount == 0) {
		SAFE_DELETE (CosTable);
		SAFE_DELETE (SinTable);
		nTableEntries = 0;
	}
}



//********************************************************************************
//Prende in input un angolo in radianti e ne restituisce il seno. Restituisce
//2.0f se l'array precalcolato per il seno � NULL.
//********************************************************************************
float CMathEngine::GetInterpolatedSine (float fRad) {
	if (SinTable == NULL)
		return 2.0f;
	return m_GetInterpolatedFunc (fRad, SinTable);
}



//********************************************************************************
//Prende in input un angolo in radianti e ne restituisce il coseno. Restituisce
//2.0f se l'array precalcolato per il coseno � NULL.
//********************************************************************************
float CMathEngine::GetInterpolatedCosine (float fRad) {
	if (CosTable == NULL)
		return 2.0f;
	return m_GetInterpolatedFunc(fRad, CosTable);
}



//********************************************************************************
//Converte le coordinate sferiche specificate in s, in coordinate
//cartesiane, restituendole in c. Restituisce ZERO se va tutto bene, altrimenti
//NONZERO.
//********************************************************************************
int CMathEngine::SphericalToCartesian (const LPMATH_SPHERICAL s, LPMATH_3DVECTOR c) {
	if ( (s && c && CosTable && SinTable) == false )
		return NONZERO;
	
	const float fSinPhi = m_GetInterpolatedFunc (s->phi, SinTable);
	const float fCosPhi = m_GetInterpolatedFunc (s->phi, CosTable);
	const float fSinTheta = m_GetInterpolatedFunc (s->theta, SinTable);
	const float fCosTheta = m_GetInterpolatedFunc (s->theta, CosTable);
	
//	c->x = s->rho * fSinPhi * fCosTheta;
//	c->y = s->rho * fSinPhi * fSinTheta;
//	c->z = s->rho * fCosPhi;
	c->x = s->rho * fCosTheta * fSinPhi;
	c->y = s->rho * fSinTheta;
	c->z = s->rho * fCosTheta * fCosPhi;
	return ZERO;
}



//********************************************************************************
//La funzione prende in input un angolo in radianti e una tabella di valori
//precalcolati da usare per il calcolo del seno o coseno. Restituisce il
//valore calcolato. Non effettua controlli sui puntatori.
//********************************************************************************
inline float CMathEngine::m_GetInterpolatedFunc (float fRad, const float* fTable) {
/*
	float fDeg = MATH_RADTODEG(fRad);
	fDeg = fmodf(fDeg, 360.0f);
	if (fDeg < 0.0f)
		fDeg += 360.0f;
	
	const int nDeg = (int)fDeg;
	const float fFrac = fDeg - (float)nDeg;
	
	return (fTable[nDeg] + fFrac * (fTable[nDeg+1] - fTable[nDeg]));
*/
	static const float f360 = 360.0f, f180 = 180.0f;
	float fDeg, fFrac;
	int nDeg;
	asm {
		FINIT
		//Conversione radianti -> gradi
		FLD f180
		FLDPI
		FDIV
		
		FLD fRad
		FMUL ST(1)
		
		//fmodf() e aggiunta di 360 se < 0
		FLD f360
		FXCH
		FPREM
		FTST
		FSTSW AX
		SAHF
		JAE Next
		FLD f360
		FADDP ST(1), ST
		
		Next:
		//Assegnazione alle variabili
		FST fDeg
		FRNDINT
		FIST nDeg
		
		//Calcolo della differenza (mantissa)
		FLD fDeg
		FSUB ST, ST(1)
		FSTP fFrac
	}
	
	return (fTable[nDeg] + fFrac * (fTable[nDeg+1] - fTable[nDeg]));
}



//********************************************************************************
//Imposta m in modo che sia la projection matrix per un sistema cartesiano
//sinistrirso. Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CMathEngine::Mat16ProjectionLH (LPMATH_MATRIX16 m, float width, float height, float NearPlane, float FarPlane) {
	if (m == NULL || width < 1.0f || height < 1.0f)
		return NONZERO;
	if (NearPlane > FarPlane)
		MATH_SWITCH (NearPlane, FarPlane, float);
	
	MATH_INITMATRIX16 (*m);
	
	m->_00 = 2 * NearPlane / width;
	m->_11 = 2 * NearPlane / height;
	m->_22 = FarPlane / (FarPlane - NearPlane);
	m->_23 = 1.0f;
	m->_32 = m->_22 * -NearPlane;
	
	return ZERO;
}