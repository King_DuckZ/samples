//CExecutor.h


#ifndef _INCL_CEXECUTOR_H
#define _INCL_CEXECUTOR_H

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>
#include "CSpeech.h"
#include "CDialogue.h"

/*
typedef struct STRUCT_RECTFLOAT {
	float left;
	float top;
	float right;
	float bottom;
} RECTFLOAT, *LPRECTFLOAT;
*/

class CExecutor {
public:
	CExecutor ( void );
	~CExecutor ( void );
	void Reset ( void );
	void Dispose ( void );
	int Behave ( LPDIRECT3DDEVICE9 dev, float fDelta, bool bSkipDlg );
	int LoadSpeechDump ( const char* strFileName );
	int SetCSpeech ( CSpeech* ptr );
	int SetCDialogue ( CDialogue* ptr );
	
private:
	inline void m_Reset ( void );
	inline void m_Dispose ( void );
	
	int m_nIndex;
	int m_nCount;
	unsigned char* m_pucText;
	int* m_pnLenght;
	LPRECT m_prArea;
	POINTFLOAT* m_pffCorner;
	float m_fElapsed;
	float m_fTimeout;
	CSpeech* m_Speech;
	CDialogue* m_Dialogue;
	bool m_bWrapDialogue;
	bool m_bAutoSkipDialogue;
	int m_nBorderWidth;
};

#endif