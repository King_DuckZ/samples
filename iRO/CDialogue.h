//CDialogue.h


#ifndef _INCL_CDIALOGUE_H
#define _INCL_CDIALOGUE_H


typedef struct STRUCT_DIALOGUE_VERTEX {
	float x, y, z, w;
	D3DCOLOR color;
	float tu, tv;
} DIALOGUE_VERTEX, *LPDIALOGUE_VERTEX;


class CDialogue {
public:
	CDialogue ( void );
	~CDialogue ( void );
	void Dispose ( void );
	int PreBuild ( LPDIRECT3DDEVICE9 dev, const char* strFileName );
	int SetDialogueRect ( const LPRECT r );
	int Render ( LPDIRECT3DDEVICE9 dev );
	int SetFactor ( float fReferenceWidth, float fAreaWidth, float fReferenceHeight, float fAreaHeight );
	
private:
	inline void m_Dispose ( void );
	inline int m_UpdateVertexBuffer ( void );
	
	LPDIRECT3DVERTEXBUFFER9 m_vb;
	LPDIRECT3DINDEXBUFFER9 m_ib;
	LPDIRECT3DTEXTURE9 m_tex;
	LPDIALOGUE_VERTEX m_Vertices;
	float m_fLeft;
	float m_fTop;
	float m_fRight;
	float m_fBottom;
	float m_fWidthFactor;
	float m_fHeightFactor;
	BOOL m_bEnabled;
};

#endif