//CSimpleSolid.h


#ifndef _INCL_CSIMPLESOLID_H
#define _INCL_CSIMPLESOLID_H


#include <d3d9.h>
#include "CMathEngine.h"

#ifdef _DEFINE_FVF
#ifdef D3DFVF_SIMPLESOLID
#undef D3DFVF_SIMPLESOLID
#endif
#define D3DFVF_SIMPLESOLID (D3DFVF_DIFFUSE | D3DFVF_TEX1 | D3DFVF_XYZ)
#endif //#ifdef _DEFINE_FVF

#define DEF_FLAGS ((SOLID_FLAGS)(SOLIDF_TEXTURIZED | SOLIDF_GOURAUD | SOLIDF_ALPHABLEND | SOLIDF_USETEXTUREALPHA | SOLIDF_SETOBJECTTEXTURE))

enum SOLID_FLAGS {
	SOLIDF_NONE = 0, //Lascia i settaggi del device cos� come sono
	SOLIDF_ALPHABLEND = 1, //Attiva l'alpha blending
	SOLIDF_TEXTURIZED = 2, //Renderizza il solido applicando una texture
	SOLIDF_2SIDED = 4, //Non effettua il culling
	SOLIDF_GOURAUD = 8, //Utilizza il gouraud shading invece del flat shading
	SOLIDF_ALPHATEST = 16, //Attiva l'alpha test
	SOLIDF_IGNOREZ = 32, //Disattiva lo z test
	SOLIDF_USETEXTUREALPHA = 64, //Applica l'alpha della texture (SOLIDF_ALPHABLEND deve essere impostato)
	SOLIDF_USEDIFFUSEALPHA = 128, //Applica l'alpha del colore (SOLIDF_ALPHAENABLE deve essere impostato)
	SOLIDF_SETOBJECTTEXTURE = 256, //Applica la texture texTexture al solido (SOLIDF_TEXTURIZED deve essere impostato)
	SOLIDF_DWORD = 0xFFFFFFFF //Non usato
};
typedef struct STRUCT_SIMPLESOLID_VERTEX {
	float x, y, z;
	D3DCOLOR color;
	float tu, tv;
} SIMPLESOLID_VERTEX, *LPSIMPLESOLID_VERTEX;


class CSimpleSolid {
public:
	CSimpleSolid ( void );
	~CSimpleSolid ( void );
	void Dispose ( void );
	virtual int BuildSolid ( float fScale, D3DCOLOR color, unsigned int param = 0 ) = 0;
	virtual int Render ( LPDIRECT3DDEVICE9 dev );
	
	bool bApplyTransform;
	SOLID_FLAGS Flags;
	MATH_MATRIX16 matTransform;
	LPDIRECT3DTEXTURE9 texTexture;
	
protected:
	inline void m_Dispose ( void );
	inline void m_Reset ( void );
	
	int m_nVertexCount;
	int m_nIndexCount;
	LPDIRECT3DVERTEXBUFFER9 m_vb;
	LPDIRECT3DINDEXBUFFER9 m_ib;
};

#endif