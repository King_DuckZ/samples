//CMouse.h


#ifndef _INCL_CMOUSE_H
#define _INCL_CMOUSE_H


#ifdef DEF_MOUSEWIDTH
#undef DEF_MOUSEWIDTH
#endif
#ifdef DEF_MOUSEHEIGHT
#undef DEF_MOUSEHEIGHT
#endif
#define DEF_MOUSEWIDTH 32
#define DEF_MOUSEHEIGHT 32

class CMouse {
public:
	CMouse ( void );
	~CMouse ( void );
	void Dispose ( void );
	inline void WrapMouse ( void );
	int BuildCursor ( const char* strFileName, LPDIRECT3DDEVICE9 dev, float fX, float fY, int nFrameCountX, int nFrameCountY );
	int DrawMouse ( void );
	int AdvanceFrame ( float fDelta );
	int SetPause ( float fSeconds );
	
	float fX;
	float fY;
private:
	inline void m_Dispose ( void );
	
	LPDIRECT3DTEXTURE9 m_tex;
	LPD3DXSPRITE m_Sprite;
	float m_fHotSpotX;
	float m_fHotSpotY;
	int m_nXCount;
	int m_nYCount;
	float m_fFrame;
	float m_fFrameCount;
	RECT m_rFrame;
	float m_fPause;
	float m_fPauseLeft;
};



//********************************************************************************
//Controlla che le coordinate del mouse siano comprese nell'area client.
//********************************************************************************
inline void CMouse::WrapMouse () {
	if (fX >= AREA_WIDTH_FLOAT)
		fX = AREA_WIDTH_FLOAT - 1.0f;
	else if (fX < 0.0f)
		fX = 0.0f;
	
	if (fY >= AREA_HEIGHT_FLOAT)
		fY = AREA_HEIGHT_FLOAT - 1.0f;
	else if (fY < 0.0f)
		fY = 0.0f;
}

	
#endif