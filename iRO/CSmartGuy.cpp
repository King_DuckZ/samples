//CSmartGuy.cpp


#include "constants.h"
#include <windows.h>
#include "CSmartGuy.h"
#include <stdio.h>
#include <math.h>



//********************************************************************************
//Costruttore standard
//********************************************************************************
CSmartGuy::CSmartGuy () {
	m_pffSteps = NULL;
	m_unSteps = 0;
	m_unIndex = 0;
	m_fVelocity = 1.0f;
	m_fTheta = 0.0f;
	m_fPosOldX = fPosX;
	m_fPosOldZ = fPosZ;
	Status = SGS_UNKNOWN;
	m_bDestination = false;
	
	MATH_INIT2DVECTOR (m_vDestination);
	MATH_INIT2DVECTOR (m_vDirection);
}



//********************************************************************************
//Distruttore standard
//********************************************************************************
CSmartGuy::~CSmartGuy () {
	m_Dispose ();
}



//********************************************************************************
//Chiama m_Dispose()
//********************************************************************************
void CSmartGuy::Dispose () {
	m_Dispose ();
	m_unSteps = 0;
	m_unIndex = 0;
}



//********************************************************************************
//Libera la memoria occupata e distrugge gli oggetti creati.
//********************************************************************************
inline void CSmartGuy::m_Dispose () {
	((CFlatSprite*)this)->Dispose();
	SAFE_DELETE (m_pffSteps);
}



//********************************************************************************
//Applica l'algoritmo di Bresenham in modo che il personaggio sia in grado di
//seguire il percorso in forma di retta fino al punto di destinazione.
//Le tappe del percorso cos� calcolato vengono memorizzate in m_pffSteps.
//Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CSmartGuy::m_Bresenham (float x0, float y0, float x1, float y1) {
	float x = x0;
	float y = y0;
	float fError;
	
	const float dx = fabsf(x1 - x0);
	const float dy = fabsf(y1 - y0);
	const float dx2 = dx * 2.0f;
	const float dy2 = dy * 2.0f;
	const float xinc = (x1 - x0 >= 0.0f ? 1.0f : -1.0f);
	const float yinc = (y1 - y0 >= 0.0f ? 1.0f : -1.0f);
	
	SAFE_DELETE (m_pffSteps);
	m_unSteps = 0;
	m_unIndex = 0;
	
	if (dx > dy) {
		fError = dy2 - dx;
		const UINT ndx = (UINT)dx;
		m_pffSteps = new POINTFLOAT[ndx];
		if (m_pffSteps == NULL)
			return NONZERO;
		m_unSteps = ndx;
		
		for (UINT z = 0; z < ndx; z++) {
			if (fError >= 0.0f) {
				fError -= dx2;
				y += yinc;
			}
			fError += dy2;
			x += xinc;
			
			m_pffSteps[z].x = x;
			m_pffSteps[z].y = y;
		}
	}
	else {
		fError = dx2 - dy;
		const UINT ndy = (UINT)dy;
		m_pffSteps = new POINTFLOAT[ndy];
		if (m_pffSteps == NULL)
			return NONZERO;
		m_unSteps = ndy;
		
		for (UINT z = 0; z < ndy; z++) {
			if (fError >= 0.0f) {
				fError -= dy2;
				x += xinc;
			}
			fError += dx2;
			y += yinc;
			
			m_pffSteps[z].x = x;
			m_pffSteps[z].y = y;
		}
	}
	
//	FILE* f;
//	f = fopen ("bresenham dump.bin", "wb");
//	fwrite (m_pffSteps, sizeof(POINTFLOAT), m_unSteps, f);
//	fflush (f);
//	fclose (f);
	
	return ZERO;
}



//********************************************************************************
//Imposta un punto di destinazione per il personaggio. Restituisce ZERO
//se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CSmartGuy::SetDestination (float x, float z) {
	if (Status == SGS_MOVING) {
		m_vDestination.x = x;
		m_vDestination.y = z;
		m_bDestination = true;
		
		return ZERO;
	}
	else {
		return m_SetDestination (x, z);
	}
}



//********************************************************************************
//Esegue lo spostamento del personaggio, interpolando i vari step.
//Restituisce ZERO se va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CSmartGuy::Walk (float fDelta) {
	m_fRad += fDelta * m_fVelocity;
	if (m_fRad > 1.0f)
		m_fRad = 1.0f;
	
	fPosX = m_fPosOldX + m_fRad * m_vDirection.x;
	fPosZ = m_fPosOldZ + m_fRad * m_vDirection.y;
	
	if ( (m_fRad == 1.0f) && (m_unIndex < m_unSteps) ) {
		//Potrebbe essere necessario reimpostare la direzione del pupazzo
		if (m_bDestination) {
			m_bDestination = false;
			m_unIndex = 0;
			if (m_SetDestination (m_vDestination.x, m_vDestination.y)) {
				m_unIndex = m_unSteps;
				Status = SGS_STANDING;
				return ZERO;
			}
		}
		
		m_fPosOldX += m_vDirection.x;
		m_fPosOldZ += m_vDirection.y;
		m_vDirection.x = (m_pffSteps[m_unIndex].x - m_fPosOldX);
		m_vDirection.y = (m_pffSteps[m_unIndex].y - m_fPosOldZ);
		
		float &fX = m_vDirection.x;
		float &fY = m_vDirection.y;
		
		if (fX == 1.0f && fY == 0.0f)
			this->fRotY = 0.0f;
		else if (fX == 1.0f && fY == -1.0f)
			this->fRotY = MATH_PI / 4.0f;
		else if (fX == 0.0f && fY == -1.0f)
			this->fRotY = MATH_PI / 2.0f;
		else if (fX == -1.0f && fY == -1.0f)
			this->fRotY = MATH_PI - MATH_PI / 4.0f;
		else if (fX == -1.0f && fY == 0.0f)
			this->fRotY = MATH_PI;
		else if (fX == -1.0f && fY == 1.0f)
			this->fRotY = MATH_PI + MATH_PI / 4.0f;
		else if (fX == 0.0f && fY == 1.0f)
			this->fRotY = MATH_PI + MATH_PI / 2.0f;
		else if (fX == 1.0f && fY == 1.0f)
			this->fRotY = MATH_PI * 7.0f / 4.0f;
		
		m_fRad = 0.0f;
		Status = SGS_MOVING;
		m_unIndex++;
	}
	else if (m_unIndex == m_unSteps && m_fRad == 1.0f) {
		m_fPosOldX += m_vDirection.x;
		m_fPosOldZ += m_vDirection.y;
		m_vDirection.x = 0.0f;
		m_vDirection.y = 0.0f;
		m_fRad = 1.0f;
		m_unIndex++;
		Status = SGS_STANDING;
		
		//Potrebbe essere necessario reimpostare la direzione del pupazzo
		if (m_bDestination) {
			m_bDestination = false;
			if (m_SetDestination (m_vDestination.x, m_vDestination.y) == ZERO)
				m_unIndex = 0;
		}
	}
	
	return ZERO;
}



//********************************************************************************
//********************************************************************************
inline int CSmartGuy::m_SetDestination (float x, float z) {
	x = roundf(x);
	z = roundf(z);
	if (this->fPosX == x && this->fPosZ == z)
		return NONZERO;
	
	if (m_Bresenham (fPosX, fPosZ, x, z))
		return NONZERO;
	
	return ZERO;
}
