//CCamera.cpp


//NOTA:
//Per pitch si intende la rotazione sull'asse delle Z, in quanto la posizione
//iniziale della telecamera (posizione non ruotata) si intende sia 1,0,0 sulla
//circonferenza goniometrica.

#include "Constants.h"
#include <windows.h>
#include "CCamera.h"
//#include <math.h>
#include <d3dx9.h>


#define DEF_ZOOM_MIN 1.0f
#define DEF_ZOOM_MAX 20.0f
#define DEF_FOV (MATH_PI / 4.0f)
#define DEF_PITCH_MIN (MATH_PI / 6.0f)
#define DEF_PITCH_MAX (MATH_PI / 3.0f)

CMathEngine g_math;


//********************************************************************************
//Costruttore standard
//********************************************************************************
CCamera::CCamera () {
	m_Reset ();
}



//********************************************************************************
//Distruttore standard
//********************************************************************************
CCamera::~CCamera () {
	m_Dispose ();
}



//********************************************************************************
//Chiama m_Reset().
//********************************************************************************
void CCamera::Reset () {
	m_Reset ();
}



//********************************************************************************
//Imposta tutte le propriet� sui valori iniziali.
//********************************************************************************
inline void CCamera::m_Reset () {
	g_math.Mat16Identity (&matProj);
	g_math.Mat16Identity (&matCam);
	MATH_INITSPHERICAL (m_sphPosition);
	
	m_fZoomMin = DEF_ZOOM_MIN;
	m_fZoomMax = DEF_ZOOM_MAX;
	m_sphPosition.rho = m_fZoomMin + (m_fZoomMax - m_fZoomMin) / 2.0f;
	m_fPitchMin = DEF_PITCH_MIN;
	m_fPitchMax = DEF_PITCH_MAX;
	m_bChanged = true;
	
	MATH_SET3DVECTOR (m_vUp, 0.0f, 1.0f, 0.0f);
	MATH_SET3DVECTOR (m_vEye, m_sphPosition.rho, 0.0f, 0.0f);
	MATH_SET3DVECTOR (m_vLookAt, 0.0f, 0.0f, 0.0f);
	
	m_fFOV = DEF_FOV;
	BuildDefaultProjection ();
}



//********************************************************************************
//Chiama m_Dispose()
//********************************************************************************
void CCamera::Dispose () {
	m_Dispose ();
}



//********************************************************************************
//Libera la memoria occupata e distrugge gli oggetti creati.
//********************************************************************************
inline void CCamera::m_Dispose () {
}



//********************************************************************************
//Ruota la telecamera. Restituisce ZERO.
//********************************************************************************
inline int CCamera::m_Rotate (float fY, float fZ) {
	fZ = fmodf (fZ, MATH_2PI);
	if (fZ < 0.0f)
		fZ += MATH_2PI;
	if (fZ < m_fPitchMin)
		fZ = m_fPitchMin;
	else if (fZ > m_fPitchMax)
		fZ = m_fPitchMax;
	
	m_sphPosition.phi = fY;
	m_sphPosition.theta = fZ;
	m_bChanged = true;
	return ZERO;
}



//********************************************************************************
//Imposta la telecamera con dei valori standard. Restituisce ZERO.
//********************************************************************************
int CCamera::BuildDefaultProjection () {
	D3DXMatrixPerspectiveFovLH ((D3DXMATRIX*)&matProj, DEF_FOV, AREA_WIDTH_FLOAT / AREA_HEIGHT_FLOAT, m_fZoomMin, m_fZoomMax);
	//g_math.Mat16ProjectionLH (&matProj, AREA_WIDTH_FLOAT, AREA_HEIGHT_FLOAT, m_fZoomMin, m_fZoomMax);
	return ZERO;
}



//********************************************************************************
//Imposta il punto verso cui la telecamera � orientata. Restituisce ZERO.
//********************************************************************************
int CCamera::SetTarget (float fX, float fY, float fZ) {
	MATH_SET3DVECTOR (m_vLookAt, fX, fY, fZ);
	m_bChanged = true;
	return ZERO;
}



//********************************************************************************
//Imposta il punto verso cui la telecamera � orientata. Restituisce ZERO se
//va tutto bene, altrimenti NONZERO.
//********************************************************************************
int CCamera::SetTarget (const LPMATH_3DVECTOR v) {
	if (v == NULL)
		return NONZERO;
	
	MATH_COPY3DVECTOR (m_vLookAt, *v);
	m_bChanged = true;
	return ZERO;
}



//********************************************************************************
//Imposta i valori che delimitano il range valido per lo zoom. Restituisce ZERO
//se i valori sono stati accettati, altrimenti NONZERO.
//********************************************************************************
int CCamera::SetZoomRange (float fMin, float fMax) {
	if (fMin >= fMax)
		//MATH_SWITCH (fMin, fMax, float);
		g_math.SwapDWord (&fMin, &fMax);

	m_fZoomMin = fMin;
	m_fZoomMax = fMax;
	
	D3DXMatrixPerspectiveFovLH ((D3DXMATRIX*)&matProj, m_fFOV, AREA_WIDTH_FLOAT / AREA_HEIGHT_FLOAT, fMin, fMax);
	//g_math.Mat16ProjectionLH (&matProj, AREA_WIDTH_FLOAT, AREA_HEIGHT_FLOAT, fMin, fMax);
	return ZERO;
}



//********************************************************************************
//Imposta la distanza della telecamera dal target, con rispetto al range
//impostato tramite SetZoomRange(). Restituisce ZERO se la telecamera �
//stata posizionata nel punto indicato, NONZERO se la telecamera � stata
//posizionata in un estremo del range.
//********************************************************************************
inline int CCamera::m_Zoom (float f) {
/*	int nRet = ZERO;
	
	if (f < m_fZoomMin) {
		f = m_fZoomMin;
		nRet = NONZERO;
	}
	else if (f > m_fZoomMax) {
		f = m_fZoomMax;
		nRet = NONZERO;
	}
	
	const float fFakeRadius = f * cos(m_fRotZ);
	m_fZoom = f;
	m_vEye.x = fFakeRadius * cos(m_fRotY);
	m_vEye.y = f * sin(m_fRotZ);
	m_vEye.z = fFakeRadius * sin(m_fRotY);
	
*/	
	m_bChanged = true;
	return 0;
}



//********************************************************************************
//Costruisce la matCam.
//********************************************************************************
void CCamera::BuildCamMatrix () {
	if (m_bChanged) {
		m_bChanged = false;
		g_math.SphericalToCartesian (&m_sphPosition, &m_vEye);
		m_vEye.x += m_vLookAt.x;
		m_vEye.z += m_vLookAt.z;
		D3DXMatrixLookAtLH ((D3DXMATRIX*)&matCam, (D3DXVECTOR3*)&m_vEye, (D3DXVECTOR3*)&m_vLookAt, (D3DXVECTOR3*)&m_vUp);
	}
}



//********************************************************************************
//Chiama m_Rotate() e ne restituisce il valore di ritorno.
//********************************************************************************
int CCamera::Rotate (float fY, float fZ) {
	return m_Rotate (fY + MATH_PI / 2.0f, fZ);
}



//********************************************************************************
//Chiama m_Rotate() e ne restituisce il valore di ritorno.
//********************************************************************************
int CCamera::OffsetRotate (float fY, float fZ) {
	return m_Rotate (fY + m_sphPosition.phi, fZ + m_sphPosition.theta);
}



//********************************************************************************
//Imposta il FoV da usare per la projection matrix. Restituisce ZERO se il
//valore � stato accettato, altrimenti NONZERO.
//********************************************************************************
int CCamera::SetFov (float f) {
	if (f <= 0.0f || f >= D3DX_PI)
		return NONZERO;
	
	m_fFOV = f;
	D3DXMatrixPerspectiveFovLH ((D3DXMATRIX*)&matProj, m_fFOV, AREA_WIDTH_FLOAT / AREA_HEIGHT_FLOAT, m_fZoomMin, m_fZoomMax);
	return ZERO;
}



//********************************************************************************
//Imposta il range per il pitch della telecamera. Restituisce ZERO se il range
//� stato accettato, altrimenti NONZERO.
//********************************************************************************
int CCamera::SetPitchRange (float fMin, float fMax) {
	if (fMin <= 0.0f || fMax >= D3DX_PI / 2.0f)
		return NONZERO;
	
	m_fPitchMin = fMin;
	m_fPitchMax = fMax;
	return ZERO;
}



//********************************************************************************
//Chiama m_Rotate() utilizzando valori piccoli, in modo da interpolare il
//movimento della telecamera fino a farla ruotare di fRad radianti. Se
//la funzione � gi� impegnata nel muovere la telecamera, ogni chiamata
//verr� ignorata. Restituisce ZERO se la funzione ha accettato la chiamata,
//altrimenti NONZERO.
//********************************************************************************
int CCamera::InterpolateAnimation (float fDelta, float fRad) {
	static bool bAvailable = true;
	static float fRotation;
	static float fRotated;
	static float fSgn;
	int nRet;
	
	if (bAvailable) {
		if (fRad == 0.0f)
			return NONZERO;
		
		fRotation = fRad;
		bAvailable = false;
		fRotated = 0.0f;
		fSgn = fabs(fRad) / fRad;
		nRet = ZERO;
	}
	else
		nRet = NONZERO;
	
	//fDelta *= 2.0f;
	
	fRotated += fDelta * fSgn;
	if (fabs(fRotated) >= fabs(fRotation)) {
		fDelta = (fRotated - fRotation) * fSgn;
		bAvailable = true;
	}
	
	m_Rotate (m_sphPosition.phi, m_sphPosition.theta + fDelta * fSgn);
	return nRet;
}



//********************************************************************************
//Chiama m_Zoom() e ne restituisce il valore di ritorno.
//********************************************************************************
int CCamera::OffsetZoom (float f) {
	return m_Zoom (m_sphPosition.rho + f);
}



//********************************************************************************
//Chiama m_Zoom() e ne restituisce il valore di ritorno.
//********************************************************************************
int CCamera::Zoom (float f) {
	return m_Zoom (f);
}