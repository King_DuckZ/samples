VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "P_Syst"
   ClientHeight    =   7380
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9675
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7380
   ScaleWidth      =   9675
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdStart 
      Caption         =   "T.N.T."
      Height          =   375
      Left            =   3600
      TabIndex        =   0
      Top             =   1800
      Width           =   2295
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const AREA_WIDTH = 640
Private Const AREA_HEIGHT = 480
Private Const DIR_GRAPHICZ = "Graphicz\"
Private Const FILE_PARTICLE1 = "particle03.bmp"
Private Const FILE_PARTICLE2 = "particle04.bmp"
Private Const FILE_PARTICLE3 = "particle05.bmp"
Private Const FILE_SURFX = "AnitraZ.bmp"
Private Const PARTICLE_WIDTH = 16
Private Const PARTICLE_HEIGHT = 16
Private Const SURFX_WIDTH = 256
Private Const SURFX_HEIGHT = 64
Private Const MAX_PARTICLES = 512
Private Const PARTICLE_LIFETIME = 1000
Private Const VELOCITY_MULTIPLIER = 1.2
Private Const VERO = -1
Private Const FALSO = 0

Private Const LR_SHARED = &H8000
Private Const LR_LOADFROMFILE = &H10
Private Const IMAGE_BITMAP = 0
Private Const AC_SRC_ALPHA = &H1 'AlphaFormat
Private Const AC_SRC_OVER = &H0 'BlendOp
Private Const PS_SOLID = 0

Private Const SRCAND = &H8800C6  ' (DWORD) dest = origine AND dest
Private Const SRCCOPY = &HCC0020 ' (DWORD) dest = origine
Private Const SRCERASE = &H440328        ' (DWORD) dest = origine AND (NOT dest )
Private Const SRCINVERT = &H660046       ' (DWORD) dest = origine XOR dest
Private Const SRCPAINT = &HEE0086        ' (DWORD) dest = origine OR dest

Private Type BITMAPFILEHEADER
    bfType As Integer
    bfSize As Long
    bfReserved1 As Integer
    bfReserved2 As Integer
    bfOffBits As Long
End Type
Private Type RGBQUAD
    rgbBlue As Byte
    rgbGreen As Byte
    rgbRed As Byte
    rgbReserved As Byte
End Type
Private Type BITMAPINFOHEADER '40 bytes
    biSize As Long
    biWidth As Long
    biHeight As Long
    biPlanes As Integer
    biBitCount As Integer
    biCompression As Long
    biSizeImage As Long
    biXPelsPerMeter As Long
    biYPelsPerMeter As Long
    biClrUsed As Long
    biClrImportant As Long
End Type
Private Type BITMAPINFO
    bmiHeader As BITMAPINFOHEADER
    bmiColors As RGBQUAD
End Type
Private Type BLENDFUNCTION
    BlendOp As Byte
    BlendFlags As Byte
    SourceConstantAlpha As Byte
    AlphaFormat As Byte
End Type
Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type
Private Type TEXTMETRIC
    tmHeight As Long
    tmAscent As Long
    tmDescent As Long
    tmInternalLeading As Long
    tmExternalLeading As Long
    tmAveCharWidth As Long
    tmMaxCharWidth As Long
    tmWeight As Long
    tmOverhang As Long
    tmDigitizedAspectX As Long
    tmDigitizedAspectY As Long
    tmFirstChar As Byte
    tmLastChar As Byte
    tmDefaultChar As Byte
    tmBreakChar As Byte
    tmItalic As Byte
    tmUnderlined As Byte
    tmStruckOut As Byte
    tmPitchAndFamily As Byte
    tmCharSet As Byte
End Type
Private Type PARTICLE_INFO
    piLeft As Single
    piTop As Single
    piWidth As Long
    piHeight As Long
    piBirthTime As Long
    piAlive As Long
    piVelocity As Single
    piPhase As Single
    piDirection As Long
    piSurf As Long
End Type

Private Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Long, ByVal X As Long, ByVal Y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal dwRop As Long) As Long
Private Declare Function StretchBlt Lib "gdi32" (ByVal hdc As Long, ByVal X As Long, ByVal Y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal nSrcWidth As Long, ByVal nSrcHeight As Long, ByVal dwRop As Long) As Long
Private Declare Function StretchDIBits Lib "gdi32" (ByVal hdc As Long, ByVal X As Long, ByVal Y As Long, ByVal dx As Long, ByVal dy As Long, ByVal SrcX As Long, ByVal SrcY As Long, ByVal wSrcWidth As Long, ByVal wSrcHeight As Long, lpBits As Any, lpBitsInfo As BITMAPINFO, ByVal wUsage As Long, ByVal dwRop As Long) As Long
Private Declare Function CreateBitmap Lib "gdi32" (ByVal nWidth As Long, ByVal nHeight As Long, ByVal nPlanes As Long, ByVal nBitCount As Long, lpBits As Any) As Long
Private Declare Function CreateCompatibleBitmap Lib "gdi32" (ByVal hdc As Long, ByVal nWidth As Long, ByVal nHeight As Long) As Long
Private Declare Function CreateCompatibleDC Lib "gdi32" (ByVal hdc As Long) As Long
Private Declare Function CreateDIBSection Lib "gdi32" (ByVal hdc As Long, pBitmapInfo As BITMAPINFO, ByVal un As Long, ByVal lplpVoid As Long, ByVal handle As Long, ByVal dw As Long) As Long
Private Declare Function CreatePen Lib "gdi32" (ByVal nPenStyle As Long, ByVal nWidth As Long, ByVal crColor As Long) As Long
Private Declare Function CreateSolidBrush Lib "gdi32" (ByVal crColor As Long) As Long
Private Declare Function GetDC Lib "user32" (ByVal hwnd As Long) As Long
Private Declare Function ReleaseDC Lib "user32" (ByVal hwnd As Long, ByVal hdc As Long) As Long
Private Declare Function SelectObject Lib "gdi32" (ByVal hdc As Long, ByVal hObject As Long) As Long
Private Declare Function DeleteObject Lib "gdi32" (ByVal hObject As Long) As Long
Private Declare Function AlphaBlend Lib "MSIMG32.dll" (ByVal hdcDest As Long, ByVal nXOriginDest As Long, ByVal nYOriginDest As Long, ByVal nWidthDest As Long, ByVal nHeightDest As Long, ByVal hdcSrc As Long, ByVal nXOriginSrc As Long, ByVal nYOriginSrc As Long, ByVal nWidthSrc As Long, ByVal nHeightSrc As Long, ByVal lBlendFunction As Long) As Long
Private Declare Function Rectangle Lib "gdi32" (ByVal hdc As Long, ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long
Private Declare Function IntersectRect Lib "user32" (lpDestRect As RECT, lpSrc1Rect As RECT, lpSrc2Rect As RECT) As Long
Private Declare Function LoadImage Lib "user32" Alias "LoadImageA" (ByVal hInst As Long, ByVal lpsz As String, ByVal un1 As Long, ByVal n1 As Long, ByVal n2 As Long, ByVal un2 As Long) As Long
Private Declare Function DeleteDC Lib "gdi32" (ByVal hdc As Long) As Long
Private Declare Function timeGetTime Lib "winmm.dll" () As Long
Private Declare Function DrawText Lib "user32" Alias "DrawTextA" (ByVal hdc As Long, ByVal lpStr As String, ByVal nCount As Long, lpRect As RECT, ByVal wFormat As Long) As Long
Private Declare Function GetTextMetrics Lib "gdi32" Alias "GetTextMetricsA" (ByVal hdc As Long, lpMetrics As TEXTMETRIC) As Long
Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)

'DC
Dim hMem As Long
Dim BackDC As Long
'Bitmap
Dim hPart As Long
Dim hBackBuf As Long
Dim hSurfX As Long
Dim hPart01 As Long
Dim hPart02 As Long
Dim hPart03 As Long
'Altro
Dim AppPath As String
Dim Running As Boolean
Dim rPart() As PARTICLE_INFO
Dim t1 As Long, t2 As Long
Dim Pi As Double
Dim SurfXx As Long, SurfXy As Long
Dim ShouldDrag As Boolean
Dim ShowFps As Boolean

Private Sub cmdStart_Click()
'Loop!
cmdStart.Visible = False
Running = True
Call MainLoop
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF11 Then
    ShowFps = Not ShowFps
ElseIf KeyCode = vbKey1 Then
    hPart = hPart01
ElseIf KeyCode = vbKey2 Then
    hPart = hPart02
ElseIf KeyCode = vbKey3 Then
    hPart = hPart03
End If
End Sub

Private Sub Form_Load()
Dim DeskDC As Long

'AppPath & altre inizializzazioni
AppPath = App.Path & IIf(Right(App.Path, 1) = "\", "", "\")
ReDim rPart(MAX_PARTICLES - 1) As PARTICLE_INFO
Pi = 4 * Atn(1)
ShouldDrag = True
ShowFps = True

'Setto le dimensioni della finestra
Me.Width = (Me.Width - Me.ScaleWidth) + AREA_WIDTH * Screen.TwipsPerPixelX
Me.Height = (Me.Height - Me.ScaleHeight) + AREA_HEIGHT * Screen.TwipsPerPixelY
Me.BackColor = vbBlack
Me.Left = (Screen.Width - Me.Width) / 2
Me.Top = (Screen.Height - Me.Height) / 2
cmdStart.Left = (Me.Width - cmdStart.Width) / 2

'Inizializzo gli HBITMAP e i DC
DeskDC = GetDC(0&)
hMem = CreateCompatibleDC(DeskDC)
BackDC = CreateCompatibleDC(DeskDC)
hBackBuf = CreateCompatibleBitmap(DeskDC, AREA_WIDTH, AREA_HEIGHT)
Call ReleaseDC(0&, DeskDC)
DeskDC = 0

'Carico le immagini
hPart01 = LoadImage(App.hInstance, AppPath & DIR_GRAPHICZ & FILE_PARTICLE1, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE)
hPart02 = LoadImage(App.hInstance, AppPath & DIR_GRAPHICZ & FILE_PARTICLE2, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE)
hPart03 = LoadImage(App.hInstance, AppPath & DIR_GRAPHICZ & FILE_PARTICLE3, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE)
hSurfX = LoadImage(App.hInstance, AppPath & DIR_GRAPHICZ & FILE_SURFX, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE)
hPart = hPart01
End Sub

Private Function MainLoop() As Long
Dim hOldBuf As Long
Dim hOldBmp As Long
Dim TextNfo As TEXTMETRIC
Dim rText As RECT
Dim Z As Integer
Dim bFunc As BLENDFUNCTION
Dim bFuncLong As Long

'Preparo la scrittura del framerate
Call GetTextMetrics(BackDC, TextNfo)
rText.Left = 0
rText.Top = 0
rText.Bottom = TextNfo.tmHeight
rText.Right = 10 * TextNfo.tmMaxCharWidth '###.## FPS

'Imposto bFunc
bFunc.AlphaFormat = AC_SRC_ALPHA
bFunc.SourceConstantAlpha = 32
bFunc.BlendOp = AC_SRC_OVER
bFunc.BlendFlags = 0
Call CopyMemory(bFuncLong, bFunc, Len(bFuncLong))

Randomize timeGetTime()
hOldBuf = SelectObject(BackDC, hBackBuf)

t2 = timeGetTime()

While (Running)
    t1 = t2
    t2 = timeGetTime()
    'Pulisco il backbuffer
    Call ClearSurf(AREA_WIDTH, AREA_HEIGHT, BackDC)
    
    'Disegno la SurfX
    hOldBmp = SelectObject(hMem, hSurfX)
    Call BitBlt(BackDC, SurfXx, SurfXy, SURFX_WIDTH, SURFX_HEIGHT, hMem, 0, 0, SRCCOPY)
    
    'Disegno le particelle
    For Z = 0 To MAX_PARTICLES - 1
        Call SelectObject(hMem, rPart(Z).piSurf)
        Call MenageParticle(CLng(Z))
        Call AlphaBlend(BackDC, rPart(Z).piLeft, rPart(Z).piTop, PARTICLE_WIDTH, PARTICLE_HEIGHT, hMem, 0, 0, PARTICLE_WIDTH, PARTICLE_HEIGHT, bFuncLong)
    Next
    Call SelectObject(hMem, hOldBmp)
    
    'Calcolo e scrivo il framerate
    If ShowFps Then _
        Call DrawText(BackDC, Format(GetFps(), "#000.00") & " FPS", 10, rText, 0)
    
    'Flip
    Call BitBlt(Me.hdc, 0, 0, AREA_WIDTH, AREA_HEIGHT, BackDC, 0, 0, SRCCOPY)
    
    'DoEvents
    DoEvents
Wend

Call SelectObject(BackDC, hOldBuf)

Call EndIt
End Function

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Button = vbLeftButton Then _
    ShouldDrag = Not ShouldDrag
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
If ShouldDrag Then
    SurfXx = X / Screen.TwipsPerPixelX - SURFX_WIDTH / 2
    SurfXy = Y / Screen.TwipsPerPixelY - SURFX_HEIGHT / 2
End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
If Running Then
    Running = False
    Cancel = -1
Else
    AppPath = vbNullString
    Erase rPart()
End If
End Sub

Private Sub ClearSurf(ByVal nWidth As Long, ByVal nHeight As Long, ByVal SurfDC As Long, Optional ByVal FillColor As Long = vbBlack)
'Dim TmpDC As Long
'Dim hBmpOld As Long
Dim hPem As Long
Dim hBr As Long
Dim hPemOld As Long
Dim hBrOld As Long

'TmpDC = CreateCompatibleDC(0&)

hPem = CreatePen(PS_SOLID, 1, FillColor)
hBr = CreateSolidBrush(FillColor)

'hBmpOld = SelectObject(TmpDC, hSurf)
hBrOld = SelectObject(SurfDC, hBr)
hPemOld = SelectObject(SurfDC, hPem)

Call Rectangle(SurfDC, 0, 0, nWidth, nHeight)

'Call SelectObject(TmpDC, hBmpOld)
Call SelectObject(SurfDC, hPemOld)
Call SelectObject(SurfDC, hBrOld)

Call DeleteObject(hPem)
Call DeleteObject(hBr)
'Call DeleteDC(TmpDC)
End Sub

Private Function GetFps() As Double
Static OldFPS As Double
Static RetFPS As Double
Static OldTime As Long

If (timeGetTime() - OldTime) >= 250 Then
    OldTime = timeGetTime()
    RetFPS = OldFPS
    OldFPS = 0
Else
    OldFPS = OldFPS + 0.25
End If
GetFps = RetFPS * 4
End Function

Private Sub MenageParticle(ByVal Index As Long)
    If ((timeGetTime() - rPart(Index).piBirthTime >= PARTICLE_LIFETIME)) Then _
        rPart(Index).piAlive = FALSO
    
    If (Not CBool(rPart(Index).piAlive)) Then
        rPart(Index).piBirthTime = timeGetTime() - Fix(Rnd() * 500)
        rPart(Index).piTop = AREA_HEIGHT - PARTICLE_HEIGHT * Fix(Rnd() + 1)
        rPart(Index).piLeft = (AREA_WIDTH - PARTICLE_WIDTH) / 2 + Fix(Rnd() * 40 - 20)
        rPart(Index).piHeight = PARTICLE_HEIGHT
        rPart(Index).piWidth = PARTICLE_WIDTH
        rPart(Index).piVelocity = 1# + Rnd() * VELOCITY_MULTIPLIER
        rPart(Index).piPhase = Rnd() * Pi
        rPart(Index).piDirection = Fix(Rnd() * 3) - 1
        rPart(Index).piSurf = hPart
        rPart(Index).piAlive = VERO
    End If
    
    If (rPart(Index).piAlive) Then
        rPart(Index).piTop = rPart(Index).piTop - rPart(Index).piVelocity * (t2 - t1) * 0.12
        
        rPart(Index).piLeft = rPart(Index).piLeft + Cos(rPart(Index).piPhase) * 0.7 * rPart(Index).piDirection
        rPart(Index).piPhase = rPart(Index).piPhase + (t2 - t1) * 0.005
        If rPart(Index).piPhase >= 2 * Pi Then _
            rPart(Index).piPhase = rPart(Index).piPhase - 2 * Pi
    End If
End Sub

Private Sub EndIt()
'Libero la memoria, gli handle ed esco
Call DeleteObject(hBackBuf)
Call DeleteObject(hPart01)
Call DeleteObject(hPart02)
Call DeleteObject(hPart03)
Call DeleteObject(hSurfX)
Call DeleteDC(hMem)
Call DeleteDC(BackDC)

hBackBuf = 0
hPart = 0
hPart01 = 0
hPart02 = 0
hPart03 = 0
hMem = 0
BackDC = 0
hSurfX = 0

Unload Me
End 'Ciao!
End Sub
