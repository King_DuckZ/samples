StarHunt
Demonstrates parallax technique applied to T&L vertices, using Direct3D. Vertices are only generated once, then they wrap back each time they fall outside the screen. Press esc or Q to quit.

QuadTree_AMD
Demonstrates the generation of a quad tree, based on randomly placed objects. Press A key to generate another tree, esc to quit.

GDI_3D
A simple rotating cube entirely made with win32 GDI: no Direct3D nor acceleration. Press + or - keys to show or hide faces, esc to quit.